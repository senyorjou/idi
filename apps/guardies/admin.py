from apps.guardies.models import Incidencia, MetaDiaFestiu, Guardia
from django.contrib import admin
from apps.meta.models import AuthUser


class MetaDiaFestiuAdmin(admin.ModelAdmin):
    list_display = ('id', 'centre', 'desc', 'data')
    readonly_fields = ['id']

class GuardiaAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'setmana', 'any')
    readonly_fields = ['id']
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "user":
            kwargs["queryset"] = AuthUser.objects.filter(is_superuser=1)
            return db_field.formfield(**kwargs)
        return super(GuardiaAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

class IncidenciaAdmin(admin.ModelAdmin):
    list_display = ('id', 'data', 'centre', 'motiu', 'solucio', 'desplacament')
    readonly_fields = ['id']
    

admin.site.register(Incidencia, IncidenciaAdmin)
admin.site.register(MetaDiaFestiu, MetaDiaFestiuAdmin)
admin.site.register(Guardia, GuardiaAdmin)