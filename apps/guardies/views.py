from django.shortcuts import render_to_response
from django.template import RequestContext
from django.db import connections, transaction
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from models import Guardia
from apps.meta.models import MetaDiaFestiu
#import apps.meta.auditoria
import time
from django.http import HttpResponseRedirect
from apps.meta.forms import AnyMesForm

@login_required
@permission_required('meta.guardies', login_url='/accounts/permisdenegat/')
def index(request):
    query = "select g.setmana, u.last_name, g.any from guardies_setmana g, auth_user u where g.user_id = u.id order by any desc limit 10;"
    cursor = connections['default'].cursor()
    
    cursor.execute(query, [])
    result = cursor.fetchall()
    
    resultat = []
    for row in result:
        setmana = time.strftime("%d/%m/%Y", time.strptime("%d %d 1" % (row[2], row[0]), '%Y %W %w')) + "-"
        if row[0] == int(53):
            setmana = setmana + time.strftime("%d/%m/%Y", time.strptime("%d %d 0" % (int(row[2])+1, 0), '%Y %W %w'))
        else:
            setmana = setmana + time.strftime("%d/%m/%Y", time.strptime("%d %d 0" % (row[2], row[0]), '%Y %W %w'))
        resultat.append((str(row[0]), setmana, row[1], row[2]))

    return render_to_response('guardies/index.html',
                               {'resultat':resultat, 'nomCamps':['setmana', 'dates', 'usuari', 'any']},
                               context_instance=RequestContext(request))

@login_required
@permission_required('meta.guardies', login_url='/accounts/permisdenegat/')
def assignar(request):
    if 'informatic' in request.POST and 'setmana' in request.POST:
        informatic_id = int(request.POST['informatic'])
        setmana = int(request.POST['setmana'])
        currYear = int(time.strftime("%Y", time.gmtime()))
        query = "insert into guardies_setmana (user_id, setmana, any) values (%d, %d, %d)" % (informatic_id, setmana, currYear)
        cursor = connections['default'].cursor()
    
        cursor.execute(query, [])
        transaction.commit_unless_managed()
        return HttpResponseRedirect("/guardies/")       
    else:
        query = "select u.id, u.first_name, u.last_name from auth_user u where u.is_superuser = 1;"
        cursor = connections['default'].cursor()
    
        cursor.execute(query, [])
        informatics = cursor.fetchall()
    
        guardies = []
        
        currYear = int(time.strftime("%Y", time.gmtime()))
        #setmana = str(0) + " " + time.strftime("%d/%m/%Y", time.strptime("%d %d 1" % (2010, 52), '%Y %W %w')) + "-"
        #setmana = setmana + time.strftime("%d/%m/%Y", time.strptime("%d %d 0" % (currYear, 0), '%Y %W %w'))
        #guardies.append({'id':0, 'desc':setmana})
        
         
        for i in range(1, 53):
            setmana = str(i) + " " + time.strftime("%d/%m/%Y", time.strptime("%d %d 1" % (currYear, i), '%Y %W %w')) + "-"
            if i == 52:
                setmana = setmana + time.strftime("%d/%m/%Y", time.strptime("%d 0 0" % (currYear+1), '%Y %W %w'))
            else:
                setmana = setmana + time.strftime("%d/%m/%Y", time.strptime("%d %d 0" % (currYear, i), '%Y %W %w'))
            guardies.append({'id':i, 'desc':setmana})
                        
        return render_to_response('guardies/assignar.html',
                                  {'informatics':informatics, 'guardies':guardies},
                                  context_instance=RequestContext(request))

@login_required
@permission_required('meta.guardies', login_url='/accounts/permisdenegat/')
def report(request):

    report = []
    diesFestius = []
    if request.method != 'POST':
        form = AnyMesForm()
    else:        
        form = AnyMesForm(request.POST)
        if form.is_valid():
            any = form.cleaned_data['any']
            mes = form.cleaned_data['mes']
            setmana_inici = time.strftime("%W", time.strptime("%d %d 1" % (any, mes), '%Y %m %d'))
            dia_any_fi = int(time.strftime("%j", time.strptime("%d %d 1" % (any, mes), '%Y %m %d')))+30
            setmana_fi = time.strftime("%W", time.strptime("%d" % dia_any_fi, '%j'))
            
            report = []
            if mes == 1:
                report += list(Guardia.objects.filter(setmana=53, any=any-1).order_by('setmana'))
            report += list(Guardia.objects.filter(setmana__range=(setmana_inici, setmana_fi), any=any).order_by('setmana'))
            # TODO: els caps de setmana no s'han de contar.
            diesFestius = MetaDiaFestiu.objects.filter(data__range=(report[0].dataInici(), report[len(report)-1].dataFinal()))
            
            diesFestius.query.group_by = ['data']
        
    return render_to_response('guardies/report.html',
                              {'guardies': report,
                               'diesFestius': diesFestius,
                               'form':form},
                              context_instance=RequestContext(request))

