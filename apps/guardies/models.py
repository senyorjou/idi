# coding: utf-8
from django.db import models
from apps.meta.models import AuthUser, MetaCentreidi, MetaDiaFestiu
import time

class Incidencia(models.Model):
    id = models.IntegerField(primary_key=True)
    centre = models.ForeignKey(MetaCentreidi)
    data = models.DateField(null=True, blank=True)
    motiu = models.TextField(blank=True)
    solucio = models.TextField(blank=True)
    desplacament = models.NullBooleanField(null=True, blank=True, default=False)
    class Meta:
        db_table = u'guardies_incidencia'
    def __unicode__(self):
        return "[" + unicode(self.data) + "] "  + unicode(self.centre)

class Guardia(models.Model):
    id = models.IntegerField(primary_key=True)
    user = models.ForeignKey(AuthUser, null=True, blank=True)
    setmana = models.IntegerField(blank=False)
    any = models.IntegerField(blank=False)    
    class Meta:
        db_table = u'guardies_setmana'
    def __unicode__(self):
        return "Setmana: " + unicode(self.setmana) + ", any: " + unicode(self.any)
    def dataInici(self):
        return time.strftime("%Y-%m-%d", time.strptime("%s %s 1" % (self.any, self.setmana), '%Y %W %w'))
    def dataFinal(self):
        if self.setmana == 53:
            return time.strftime("%Y-%m-%d", time.strptime("%s %s 0" % (self.any+1, 0), '%Y %W %w'))
        else:
            return time.strftime("%Y-%m-%d", time.strptime("%s %s 0" % (self.any, self.setmana), '%Y %W %w'))
    def dataRang(self):
        return self.dataInici() + " - " + self.dataFinal()
    def preu(self):
        preu_setmana = 239.15
        dies_festius = MetaDiaFestiu.objects.filter(data__range=(self.dataInici(), self.dataFinal()))
        dies_festius.query.group_by = ['data']
        # TODO: Mirar si el dia �s diumenge per en cas de no ser-ho, sumar-li tamb� el preu de dia festiu.
        if len(dies_festius) > 0:
            preu_setmana += (preu_setmana * 0.3 * len(dies_festius))
        return preu_setmana
