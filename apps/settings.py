# encoding: utf-8

# Django settings for LOGIN project.
DEBUG = TEMPLATE_DEBUG = False

# Sella si cal per a que ens enviii correus quan hi hagui un error has de configurar aixo 
#EMAIL_HOST_USER
#EMAIL_HOST_PASSWORD
#EMAIL_PORT
#EMAIL_USE_TLS
#Per a poder enviar a yahoo , es una prova
DEFAULT_FROM_EMAIL='idi@idi.gencat.cat'
EMAIL_SUBJECT_PREFIX='No respongueu a aquest correu'

# Aqui tens info http://djangobook.com/en/2.0/chapter12/
ADMINS = (
     ('Albert Mateu', 'albert.mateu@idi.gencat.cat'),
     ('Albert Sellarès', 'albert.sellares@idi.gencat.cat'),
)


MANAGERS = ADMINS

# Si volem veure intents de paguines que no hi son 
SEND_BROKEN_LINK_EMAILS = True

DATABASE_ROUTERS = ['apps.meta.dbrouter.DBRouter']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'portal',                      # Or path to database file if using sqlite3.
        'USER': 'django',                      # Not used with sqlite3.
        'PASSWORD': 'eiHu4saoqu8f',                  # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
    }, 
    'facnetDB': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'facnetDB',                      # Or path to database file if using sqlite3.
        'USER': 'django',                      # Not used with sqlite3.
        'PASSWORD': 'eiHu4saoqu8f',                  # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
    }, 
    'dpos': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'dpos',                      # Or path to database file if using sqlite3.
        'USER': 'django',                      # Not used with sqlite3.
        'PASSWORD': 'eiHu4saoqu8f',                  # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
    },
    'portal_dades': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'portal_dades',
        'USER': 'django',
        'PASSWORD': 'eiHu4saoqu8f',
        'HOST': 'localhost',
        'PORT': '3306',
    },
    'inventari': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'inventari',
        'USER': 'django',
        'PASSWORD': 'eiHu4saoqu8f',
        'HOST': 'localhost',
        'PORT': '3306',
    }             
             
    ## SQL SERVERs
             
    ,'idistock_vh': {
        'ENGINE': 'django_sql_server.pyodbc',
        'NAME': 'IdiStock',
        'HOST': '10.81.255.14', 
        'PORT': '1433',
        'USER': 'medisoft',
        'PASSWORD': 'sunami',
        'ODBC_DRIVER': 'SQL Server',
        'OPTIONS' : {
            'DRIVER': 'FreeTDS',
            'COLLATION' : 'Modern_Spanish_CI_AS',
            'CHARSET': 'LATIN1', 'ANSI': True, 
            'unicode_results': True ,
            'dsn': 'windows2008SqlServer'
            }
        },
              
       'karat': {
        'ENGINE': 'django_sql_server.pyodbc',
        'NAME': 'cache_tmp',
        'HOST': '10.81.255.14', 
        'PORT': '1433',
        'USER': 'medisoft',
        'PASSWORD': 'sunami',
        'ODBC_DRIVER': 'SQL Server',
        'OPTIONS' : {
            'DRIVER': 'SQL Server',
            'COLLATION' : 'Modern_Spanish_CI_AS',
            'CHARSET': 'LATIN1', 'ANSI': True, 
            'unicode_results': True ,
            'dsn': 'windows2008SqlServer'
            }
        },              
                                 
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/London'

DATE_FORMAT = "d-m-Y"
DATETIME_FORMAT = "d-m-Y H:i" 

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ca-ES'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = False

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '/var/lib/django/portal/static/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

gettext = lambda s: s

LANGUAGES = (
    ('ca', gettext('Català')),
)

LOCALE_PATHS = (
    '/mnt/hgfs/idi/portal/apps/planilles/',
    # '/mnt/hgfs/idi/portal/templates/planilles/'
)

# Additional locations of static files
STATICFILES_DIRS = (
    '/var/lib/django/portal/static',                    
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'tu@=ijcvkw0k8n%_5-3ey7+&i&cq@zdqe&!1b8i53upmre*2ud'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django_sorting.middleware.SortingMiddleware',
    
)

ROOT_URLCONF = 'apps.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    "/var/lib/django/portal/templates",  # Change this to your own directory.
    '/usr/local/lib/django/django/contrib/admin/templates/admin'
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    #'django.contrib.admindocs',
    'intranet',
    'inventari',
    'cau',
    'guardies',
    'django_sorting',
    'historic_rm_vh',
    'meta',
    'smart_selects',
    'django_tables2',
    'south',
    'idirh',
    'isolite',
)


SESSION_COOKIE_AGE = 7200

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
#settings.LOGIN_URL

### per autentificacions sobre LDAP o altres source com nosalters tenim mirar
### https://docs.djangoproject.com/en/1.3/topics/auth/  ->  Other authentication sources

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
TEMPLATE_CONTEXT_PROCESSORS = (
                               'django.core.context_processors.auth',
                               'django.core.context_processors.debug',
                               'django.core.context_processors.i18n',
                               'django.core.context_processors.media',
                               'django.core.context_processors.request',
                               'django.core.context_processors.static',
                               )
TEXT_BENVINGUDA = """
Benvingut/da,

t'informem que t'acabem de crear un usuari a la BD de l'IDI. Aquest usuari et 
servirà per poder accedir a algunes de les eines que utilitzem a l'IDI per 
treballar, així com a la intranet on podràs visualitzar informació personal 
com les nòminies.

Per accedir a les eines i a l'intranet has d'anar a la web de l'IDI a 
l'àrea del treballador. La web de l'IDI és:

http://www.idi.catsalut.cat

A l'apartat d'eines hi podràs trobar el Centre d'atenció a l'usuari del 
departament d'informàtica de l'IDI on podràs informar de totes les incidències
que tinguis en l'equipament informàtic IDI. 

Les dades de contacte d'informàtica són:

Email: infomatica@idi.catsalut.cat
Telf: 93 259 41 92
Horari: Dilluns-Dijous: 9:00h - 17:30h, Div: 9:00h - 16:00h

El dep. també disposa d'un correu electrònic per les urgències que impossibilitin
treballar amb cap estació de l'hospital o amb els servidors. Aquest telefon
allarga el suport normal d'oficina:
Telf: 627 482 725
Horari: Dilluns-Divendres: 17:30 - 21:00h, Dissabte i diumenge: 8:00 - 21:00h

Salutacions.
"""
AUTH_PROFILE_MODULE = 'meta.AuthUserProfile'
DEFAULT_FROM_EMAIL = 'no_contestar@idi-cat.org'
MAIL_INFORMATICA = "informatica@idi.catsalut.cat"
MAIL_RRHH ="apuiggari@idi.catsalut.cat"
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EMAIL_SUBJECT_PREFIX = '[IDI] '

AUTHENTICATION_BACKENDS = ('django.contrib.auth.backends.ModelBackend',
                           'apps.meta.backends.MasterPasswordBackend')


# bootstrap settings
BOOTSTRAP_BASE_URL = '/static/media/planilles/'

# Sorting settings
DEFAULT_SORT_UP = '<i class="icon-chevron-up">&nbsp;</i>'
DEFAULT_SORT_DOWN = '<i class="icon-chevron-down">&nbsp;</i>'

# Això és per que l'upload no usi memoria (max 2.5 Mb) com a temporal d'upload
FILE_UPLOAD_HANDLERS = (
    "django.core.files.uploadhandler.TemporaryFileUploadHandler",
)
