# coding: utf-8
#
# import datetime

from django.contrib import admin
from apps.meta.models import AuthUser, User

from apps.isolite.models import Accio, Comentari, Document,\
                                NoConformitat


class DocsInline(admin.TabularInline):
    model = Document

    fields = ['nom', 'fitxer']


class CommentsInline(admin.StackedInline):
    model = Comentari

    fields = ['h1', 'body']


class NoConfAdmin(admin.ModelAdmin):
    model = NoConformitat

    list_display = ['__unicode__', 'data_entrada', 'estat',
                    'accio', 'unitat', 'creacio_user', ]
    search_fields = ['h1', 'body']
    list_filter = ['estat', 'unitat', ]
    readonly_fields = ['__unicode__', ]
    fields = ['__unicode__', 'unitat', 'accio', 'h1', 'body', 'data_entrada',
              'solucio', 'data_prevista', 'data_tancament', 'estat', ]

    inlines = [
        DocsInline,
        CommentsInline
    ]

    def save_model(self, request, obj, form, change):
        # Aqui el workflow
        # if not obj.id_incidencia:
        #     last_incidencia = Incidencia.objects.count() + 1
        #     obj.id_incidencia = last_incidencia
        if not obj.id:
            try:
                last_id = NoConformitat.objects.all()\
                                       .order_by('-id_nc')[0].id_nc
            except:
                last_id = 0

            obj.id_nc = last_id + 1

            obj.creacio_user = AuthUser.objects.get(id=request.user.id)
            # usuari = User.objects.get(id=request.user.id)
            # if usuari.get_profile().unitat_idi:
            #     obj.unitat = usuari.get_profile().unitat_idi

        obj.save()

    def save_formset(self, request, form, formset, change):
        """
        Els Inlines es graben per aquí
        """
        instances = formset.save(commit=False)
        for instance in instances:
            try:
                if instance.user:
                    pass
            except:
                instance.user = AuthUser.objects.get(id=request.user.id)
                if isinstance(instance, Document):
                    if not instance.nom:
                        instance.nom = instance.fitxer.path.split('/')[-1]
                instance.save()


class AccioAdmin(admin.ModelAdmin):
    model = Accio

    list_display = ['__unicode__', 'data_entrada',
                    'estat', 'tipus', 'noconformitats', 'unitat', 'creacio_user', ]
    search_fields = ['h1', 'body']
    list_filter = ['estat', 'tipus', 'unitat']
    readonly_fields = ['__unicode__', ]
    fields = ['unitat', 'tipus', 'h1', 'body', 'data_entrada', 'solucio',
              'data_prevista', 'data_tancament', 'estat', ]

    inlines = [
        DocsInline,
        CommentsInline
    ]

    def save_model(self, request, obj, form, change):
        # Aqui el workflow
        if not obj.id:
            # Incrementem el id local
            try:
                last_id = Accio.objects.all()\
                                       .order_by('-id_accio')[0]\
                                       .id_accio
            except:
                last_id = 0

            obj.id_accio = last_id + 1

            obj.creacio_user = AuthUser.objects.get(id=request.user.id)
            # usuari = User.objects.get(id=request.user.id)

            # if usuari.get_profile().unitat_idi:
            #     obj.unitat = usuari.get_profile().unitat_idi

        obj.save()

    def save_formset(self, request, form, formset, change):
        """
        Els Inlines es graben per aquí
        """
        instances = formset.save(commit=False)
        for instance in instances:
            try:
                if instance.user:
                    pass
            except:
                instance.user = AuthUser.objects.get(id=request.user.id)
                if isinstance(instance, Document):
                    if not instance.nom:
                        instance.nom = instance.fitxer.path.split('/')[-1]
                instance.save()

admin.site.register(NoConformitat, NoConfAdmin)
admin.site.register(Accio, AccioAdmin)
