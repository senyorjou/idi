# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'IsoItem.solucio'
        db.add_column(u'isolite_isoitem', 'solucio',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'IsoItem.solucio'
        db.delete_column(u'isolite_isoitem', 'solucio')


    models = {
        'isolite.accio': {
            'Meta': {'object_name': 'Accio', '_ormbases': ['isolite.IsoItem']},
            'aprobacio_data_q': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'aprobacio_user_q': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'aprobacio_user_q'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'estat': ('django.db.models.fields.CharField', [], {'default': "'O'", 'max_length': '1'}),
            'incidencia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['isolite.Incidencia']"}),
            'isoitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['isolite.IsoItem']", 'unique': 'True', 'primary_key': 'True'}),
            'tipus': ('django.db.models.fields.CharField', [], {'default': "'C'", 'max_length': '1'}),
            'verificat_data_q': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'verificat_user_q': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'verificat_user_q'", 'null': 'True', 'to': "orm['meta.AuthUser']"})
        },
        'isolite.comentari': {
            'Meta': {'object_name': 'Comentari'},
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'comentari_pare': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['isolite.Comentari']", 'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'h1': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso_item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['isolite.IsoItem']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.AuthUser']"})
        },
        'isolite.document': {
            'Meta': {'object_name': 'Document'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fitxer': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso_item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['isolite.IsoItem']"}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.AuthUser']"})
        },
        'isolite.incidencia': {
            'Meta': {'object_name': 'Incidencia', '_ormbases': ['isolite.IsoItem']},
            'estat': ('django.db.models.fields.CharField', [], {'default': "'O'", 'max_length': '1'}),
            'genera_accio': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'isoitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['isolite.IsoItem']", 'unique': 'True', 'primary_key': 'True'})
        },
        'isolite.isoitem': {
            'Meta': {'object_name': 'IsoItem'},
            'aprobacio_data': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'aprobacio_user': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'aprobacio_user'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'creacio_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'creacio_user'", 'to': "orm['meta.AuthUser']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_entrada': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 1, 31, 0, 0)'}),
            'data_prevista': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'data_tancament': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'h1': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_incidencia': ('django.db.models.fields.BigIntegerField', [], {'db_index': 'True'}),
            'solucio': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'unitat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaUnitatIdi']", 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'verificat_data': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'verificat_user': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'verificat_user'", 'null': 'True', 'to': "orm['meta.AuthUser']"})
        },
        'meta.authuser': {
            'Meta': {'object_name': 'AuthUser', 'db_table': "u'auth_user'"},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '225'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.IntegerField', [], {}),
            'is_staff': ('django.db.models.fields.IntegerField', [], {}),
            'is_superuser': ('django.db.models.fields.IntegerField', [], {}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '384'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '90'})
        },
        'meta.metacentreidi': {
            'Meta': {'ordering': "['descripcio']", 'object_name': 'MetaCentreIdi', 'db_table': "u'meta_centre_idi'"},
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'meta.metaunitatidi': {
            'Meta': {'ordering': "['descripcio']", 'object_name': 'MetaUnitatIdi', 'db_table': "u'meta_unitat_idi'"},
            'actiu': ('django.db.models.fields.IntegerField', [], {}),
            'calendari_laboral': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['planilles.CalendariLaboral']", 'null': 'True'}),
            'centre_idi': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCentreIdi']"}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unitatat_facturacio_id': ('django.db.models.fields.IntegerField', [], {})
        },
        'planilles.calendarilaboral': {
            'Meta': {'ordering': "['nom']", 'object_name': 'CalendariLaboral', 'db_table': "u'plan_calendari_laboral'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'es_mestre': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['isolite']