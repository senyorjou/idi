# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'IsoItem.slug'
        db.delete_column('isolite_isoitem', 'slug')


    def backwards(self, orm):
        # Adding field 'IsoItem.slug'
        db.add_column('isolite_isoitem', 'slug',
                      self.gf('django.db.models.fields.SlugField')(default='000', max_length=50),
                      keep_default=False)


    models = {
        'isolite.accio': {
            'Meta': {'object_name': 'Accio', '_ormbases': ['isolite.IsoItem']},
            'aprobacio_data_q': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'aprobacio_user_q': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'aprobacio_user_q'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'estat': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'isoitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['isolite.IsoItem']", 'unique': 'True', 'primary_key': 'True'}),
            'tipus': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'verificat_data_q': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'verificat_user_q': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'verificat_user_q'", 'null': 'True', 'to': "orm['meta.AuthUser']"})
        },
        'isolite.comentari': {
            'Meta': {'object_name': 'Comentari'},
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'comentari_pare': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['isolite.Comentari']", 'null': 'True', 'blank': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'h1': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso_item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['isolite.IsoItem']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.AuthUser']"})
        },
        'isolite.document': {
            'Meta': {'object_name': 'Document'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fitxer': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso_item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['isolite.IsoItem']"}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.AuthUser']"})
        },
        'isolite.incidencia': {
            'Meta': {'object_name': 'Incidencia', '_ormbases': ['isolite.IsoItem']},
            'estat': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'genera_accio': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'isoitem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['isolite.IsoItem']", 'unique': 'True', 'primary_key': 'True'})
        },
        'isolite.isoitem': {
            'Meta': {'object_name': 'IsoItem'},
            'aprobacio_data': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'aprobacio_user': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'aprobacio_user'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'body': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'codi': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True'}),
            'creacio_user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'creacio_user'", 'to': "orm['meta.AuthUser']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_entrada': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 1, 30, 0, 0)'}),
            'data_prevista': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'data_tancament': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'h1': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'iso_item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['isolite.IsoItem']", 'null': 'True', 'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'verificat_data': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'verificat_user': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'verificat_user'", 'null': 'True', 'to': "orm['meta.AuthUser']"})
        },
        'meta.authuser': {
            'Meta': {'object_name': 'AuthUser', 'db_table': "u'auth_user'"},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '225'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.IntegerField', [], {}),
            'is_staff': ('django.db.models.fields.IntegerField', [], {}),
            'is_superuser': ('django.db.models.fields.IntegerField', [], {}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '384'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '90'})
        }
    }

    complete_apps = ['isolite']