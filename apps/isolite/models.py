# coding: utf-8
import os
from datetime import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _

from apps.meta.models import AuthUser, MetaUnitatIdi

NOCONFORMITAT_ESTAT = (
    ('O', _('Oberta')),
    ('T', _('Tancada')),
)

ACCIO_TIPUS = (
    ('C', _('Correctiva')),
    ('P', _('Preventiva')),
)

ACCIO_ESTAT = (
    ('O', _('Oberta')),
    ('C', _('En Curs')),
    ('T', _('Tancada')),
)


def get_isoitem_path(instance, filename):
    """
    Retorna el nom del fitxer en format
    folder/06d[id]/[Tipus_doc]__nom_fitxer
    """
    try:
        noconf = instance.iso_item.noconformitat
        folder = 'NoConfs'
        prepend = 'NoConf'
        local_id = noconf.id_nc
    except:
        tipus = instance.iso_item.accio.tipus
        if tipus == 'C':
            prepend = 'Correctiva'
        else:
            prepend = 'Preventiva'

        folder = 'Accions'
        local_id = instance.iso_item.accio.id_accio

    return os.path.join(
      'isodocs', folder, '%06d' % local_id,
      '%s_%06d__%s' % (prepend, local_id, filename))


class IsoItem(models.Model):
    creacio_user = models.ForeignKey(AuthUser,
                                     related_name='creacio_user',
                                     verbose_name=_('Creat per'))
    unitat = models.ForeignKey(MetaUnitatIdi, limit_choices_to={'actiu': True})
    # id_incidencia = models.BigIntegerField(db_index=True)
    h1 = models.CharField(max_length=150, verbose_name=_('Titol'))
    body = models.TextField(blank=True, verbose_name=_('Descripcio'))
    solucio = models.TextField(blank=True, verbose_name=_('Solucio'))
    data_entrada = models.DateField(default=datetime.now().date(),
                                    verbose_name=_('Data d\'Entrada'))
    data_prevista = models.DateField(blank=True, null=True,
                                     verbose_name=_('Data de tancament previst'))
    data_tancament = models.DateField(blank=True, null=True,
                                      verbose_name=_('Data de Tancament'))

    aprobacio_user = models.ForeignKey(
                        AuthUser,
                        related_name='aprobacio_user',
                        null=True,
                        blank=True)
    aprobacio_data = models.DateField(blank=True, null=True)

    verificat_user = models.ForeignKey(
                        AuthUser,
                        related_name='verificat_user',
                        null=True,
                        blank=True)
    verificat_data = models.DateField(blank=True, null=True)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = u'isolite_isoitem'

    def __unicode__(self):
        return _('%06d: %s') % (self.id, self.h1)

    def delete(self, *args, **kwargs):
        """
        Esborrar els fitxers del disc
        """
        for f in self.document_set.all():
            if os.path.exists(f.fitxer.path):
                os.unlink(f.fitxer.path)

        super(IsoItem, self).delete(*args, **kwargs)



class Accio(IsoItem):
    id_accio = models.BigIntegerField(db_index=True)
    # incidencia = models.ForeignKey(Incidencia,
    #                              verbose_name=_('Incidencia'))
    tipus = models.CharField(max_length=1,
                             choices=ACCIO_TIPUS,
                             default='C')
    estat = models.CharField(max_length=1,
                             choices=ACCIO_ESTAT,
                             default='O')
    aprobacio_user_q = models.ForeignKey(
                        AuthUser,
                        related_name='aprobacio_user_q',
                        null=True,
                        blank=True)
    aprobacio_data_q = models.DateField(blank=True, null=True)

    verificat_user_q = models.ForeignKey(
                        AuthUser,
                        related_name='verificat_user_q',
                        null=True,
                        blank=True)
    verificat_data_q = models.DateField(blank=True, null=True)

    class Meta:
        db_table = u'isolite_accio'
        verbose_name_plural = _('Accions')

    def __unicode__(self):
        return _('%06d: %s') % (self.id_accio, self.h1)

    def noconformitats(self):
        """
        Retorna llista de NC's
        Al futur incorporar això:
        '<a href="%s">%s</a>' % (reverse("admin:auth_user_change", args=(self.user.id,)) , escape(self.user))
        """
        html = _('No hi ha NC\'s associades')
        ncs = self.noconformitat_set.all()

        if ncs:
            html = '<ul>'
            for nc in ncs:
                html += '<li>' + '%06d: %s' % (nc.id, nc.h1)

            html += '</ul>'

        return html

    noconformitats.allow_tags = True
    noconformitats.short_description = "No Conformitats Associades"


class NoConformitat(IsoItem):
    id_nc = models.BigIntegerField(db_index=True)
    accio = models.ForeignKey(Accio, null=True, blank=True)
    estat = models.CharField(max_length=1,
                             choices=NOCONFORMITAT_ESTAT,
                             default='O')  # Oberta
    genera_accio = models.BooleanField(
                            default=False,
                            verbose_name=_('Genera accio correctiva?'))

    class Meta:
        db_table = u'isolite_noconformitat'
        verbose_name = _('No Conformitat')
        verbose_name_plural = _('No Conformitats')

    def __unicode__(self):
        return _('%06d: %s') % (self.id_nc, self.h1)

    def accions_list(self):
        return self.accio_set.count()


class Comentari(models.Model):
    iso_item = models.ForeignKey(IsoItem)
    comentari_pare = models.ForeignKey('self', null=True, blank=True)
    user = models.ForeignKey(AuthUser)
    h1 = models.CharField(max_length=150, verbose_name=_('Titol'))
    body = models.TextField(blank=True, verbose_name=_('Descripcio'))
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = u'isolite_comentari'


class Document(models.Model):
    iso_item = models.ForeignKey(IsoItem)
    nom = models.CharField(max_length=200, blank=True)
    user = models.ForeignKey(AuthUser)
    fitxer = models.FileField(upload_to=get_isoitem_path)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = u'isolite_document'

    def __unicode__(self):
        return self.nom
