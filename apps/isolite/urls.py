# coding: utf-8
#
"""
Created on 25/01/2013
@author: mjou
"""

from django.conf.urls.defaults import url, patterns

urlpatterns = patterns('apps.isolite',
    
    url(r'^$', 'views.index', name='index'),
)
