# coding: utf8

'''
Created on 26/07/2011
@author: amateu
'''
# Create your views here.

from __future__ import division# per fer divions i que retorniel reste /
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from apps.quadre_comandament.models import  ProvaOhdim
from apps.meta.models import AuthUser,AuthUserProfile
from django.db.models import F# https://docs.djangoproject.com/en/dev/topics/db/queries/#query-expressions
from django.db.models import Max
from apps.quadre_comandament.objectius.radiolegs.views import activitatDetallSeramProcedure
from apps.quadre_comandament.models import Metge

indicadors = ['',]
objectiu = "Activitat realitzada"
objectiu_url = "radiolegs"

def get_index(request):
  
    index = {}                                                                                     
    # em si te permisos  15 i 17 informatics, 26 cap unitat, 27 director unitat 
    # 9,8,6, 30 son facultatius adjunts, els resis cobren minuta i no estan donats d'alta al sistema per tant no podran veure's aqui
    #Facultatius 
    if AuthUserProfile.objects.get( user = AuthUser.objects.get(username =str(request.user)).id).categoria_profesional.id in [4,5,6,15,17,26,27,31,30]:#
        index = {'Seguiment facultatiu' :"Seguiment personal de l'activitat realitzada. ( facultatius )"}
    #Tecnics /dueTer
#    if AuthUserProfile.objects.get( user = AuthUser.objects.get(username =str(request.user)).id).categoria_profesional.id in [15,17,26,27,31, BALB BLABALBABABa ]:
#        index = {'seguiment_tecnics':"Seguiment personal de l'activitat realitzada. ( Due. / Ter. )"}

    return index


@login_required
#@permission_required('meta.seguiment_personal', login_url='/accounts/permisdenegat/')
def index(request):

    darrera_actulitzacio_mdos =  ProvaOhdim.objects.all().aggregate(Max('data_cita'))['data_cita__max']
    #darrera_actulitzacio_facturacio  = calendar.month_name[ datetime.now().month-1].decode(myencoding)
    #TODO: aqui farem que nmes els facultatius veguin el seu indicador i els tecnics el seu indicador ...
    index = get_index(request)
    
    return render_to_response('seguiment_personal/index.html', 
                               {'darrera_actulitzacio_mdos':darrera_actulitzacio_mdos,
                                'index':index },                         
                              context_instance=RequestContext(request))


@login_required
#@permission_required('meta.seguiment_personal', login_url='/accounts/permisdenegat/')
def seguiment_facultatiu(request):
    
    try:
        
        metges=  Metge.objects.filter( dni__startswith  = str(request.user)).values('id')
        metge_list = []
        for m in metges:
            metge_list.append(  m['id'])
        
        #podem fer una validacio extra de si es facultatiu , però com el menu nomes li surt si es facultatiu ..
        #tipus_consulta tot, si volguessim que pugui veure la ordinaria nomes cal cridar el prpocedure amb tipus_consulta='ordinaria'
        return activitatDetallSeramProcedure(request,metge_list, get_index(request),es_seguiment_personal = True, tipus_consulta='total')
    
    except Metge.DoesNotExist:
     
        
        return render_to_response('seguiment_personal/permisdenegat.html')

    except Metge.MultipleObjectsReturned:
        
        return render_to_response('seguiment_personal/permisdenegat.html')
        










