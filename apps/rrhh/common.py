# coding: utf8
from pyPdf import PdfFileWriter, PdfFileReader
import os
import re
from django.db import connections

# TODO: Posar aquesta variable en algún altre lloc.
BASE = "/var/lib/django/portal/data/treballadors"

def import_nomines(year, mes, rtfFile, pdfFile):
    pattern = r".*phmrg\\posx6476\\pvmrg\\posy1090.*"
    fileName = "%s.pdf" % mes
    relPath = os.path.join("nomines", year)    
    import_documents(pattern, fileName, relPath, rtfFile, pdfFile)
    
def import_irpf(year, rtfFile, pdfFile):
    pattern = r".*phmrg\\posx661\\pvmrg\\posy2146.*"
    fileName = "%s.pdf" % year
    relPath = "irpf"
    import_documents(pattern, fileName, relPath, rtfFile, pdfFile)
    
def import_dpos(year, rtfFile, pdfFile):
    pattern = r".*phmrg\\posx6476\\pvmrg\\posy1090.*"
    fileName = "%s.pdf" % year
    relPath = "dpos"
    import_documents(pattern, fileName, relPath, rtfFile, pdfFile)

def import_documents(pattern, fileName, relPath, rtfFile, pdfFile):
    result = ""
     
    input = PdfFileReader(file(pdfFile, "rb"))
    rtf = open(rtfFile, 'r').read()
    
    dniLines = re.findall(r"%s" % pattern, rtf)
    dic = {}
    page = 0
    dnis = []
    for line in dniLines:
        dni = re.sub(r"%s (.*)}" % pattern, "\g<1>", line).rstrip()[:-1]
        if not dic.has_key(dni):
            output = PdfFileWriter()
            output.addPage(input.getPage(page))
            dic[dni] = output
        else:
            dic[dni].addPage(input.getPage(page))
        page += 1
    
    # Write pdf files
    for dni in dic.keys():
        path = os.path.join(BASE, dni, relPath)
        if not os.path.exists(path):
            os.makedirs(path)
        fh = file(os.path.join(path, fileName), "wb")
        dic[dni].write(fh)
        fh.close()
    
    result += "S'han importat correctament a %d treballadors.\n" % len(dic.keys())
    
    cursor = connections['default'].cursor()
    cursor.execute ("SELECT username from auth_user")
    rows = cursor.fetchall()
    for row in rows:
        dnis.append(row[0])
    
    unexistent = set(dic.keys()).difference(set(dnis))
    if len(unexistent) > 0:
        result += "Els seguents treballadors no estan donats d'alta:\n"
    for dni in unexistent:
        result += dni + "\n"
    return result