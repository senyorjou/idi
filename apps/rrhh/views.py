# coding: utf8
from django.shortcuts import render_to_response,HttpResponse
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from apps.rrhh.common import import_nomines
from apps.quadre_comandament.models import ProvaOhdimCodificacioGenerica
import apps.django_tables2 as tables
#from django.db.models import Count
from operator import itemgetter
from apps.meta.forms import ComplementairesForm, AnyMesForm
#from django.http import HttpResponseRedirect
#import time
import datetime
import calendar
from apps.meta import auditoria
from apps.quadre_comandament.objectius.iso.views import *
from apps.django_excel_response import ExcelResponse

from apps.meta.models import AuthUserProfile,AuthUser

indicadors = ['Nòmines', 'Pagues extra',  'DPOs', 'IRPF', 'Activitat Complementària']
indicadors = ['Activitat Complementària']

@login_required
@permission_required('meta.rrhh', login_url='/accounts/permisdenegat/')
def index(request):
    
    usuari = AuthUser.objects.get(username =str(request.user))
    #Aixo era per quan feia jo les extraccions
    es_informatic = AuthUserProfile.objects.get( user = usuari.id).categoria_profesional.id in (15,17,21,36)

    return render_to_response('rrhh/index.html',{ 'es_informatic':es_informatic, }, context_instance=RequestContext(request))
 
@login_required
def nomines(request):
    resultat = "TESTTT"
    if ('any' in request.POST and 'mes' in request.POST and 
        'rtf' in request.FILES and 'pdf' in request.FILES):
        resultat = import_nomines(request.POST['year'], request.POST['mes'], request.POST['rtf'], request.POST['pdf'])
        
    auditoria.request_handler(request,9,'Tipus consulta: nomines ; ' + str(indicadors) + ' ')    
    return render_to_response('rrhh/nomines.html',
                              {'indicadors': indicadors,
                               'response': resultat},
                              context_instance=RequestContext(request))
@login_required
   
def dpos(request):
    resultat = "TESTTT"
    if ('any' in request.POST and 'mes' in request.POST and 
        'rtf' in request.FILES and 'pdf' in request.FILES):
        resultat = import_nomines(request.POST['year'], request.POST['mes'], request.POST['rtf'], request.POST['pdf'])
        
    return render_to_response('rrhh/dpos.html',
                              {'indicadors': indicadors,
                               'response': resultat},
                              context_instance=RequestContext(request))    
@login_required
  
def irpf(request):
    resultat = "TESTTT"
    if ('any' in request.POST and 'mes' in request.POST and 
        'rtf' in request.FILES and 'pdf' in request.FILES):
        resultat = import_nomines(request.POST['year'], request.POST['mes'], request.POST['rtf'], request.POST['pdf'])
        
    return render_to_response('rrhh/irpf.html',
                              {'indicadors': indicadors,
                               'response': resultat},
                              context_instance=RequestContext(request)) 
@login_required
 
def extres(request):
    resultat = "TESTTT"
    if ('any' in request.POST and 'mes' in request.POST and 
        'rtf' in request.FILES and 'pdf' in request.FILES):
        resultat = import_nomines(request.POST['year'], request.POST['mes'], request.POST['rtf'], request.POST['pdf'])
        
    return render_to_response('rrhh/extres.html',
                              {'indicadors': indicadors,
                               'response': resultat},
                              context_instance=RequestContext(request))     
@login_required
@permission_required('meta.rrhh', login_url='/accounts/permisdenegat/')   
def activitatcomplementaria(request):
    result = []  
    any = 0
    mes = 0
    if request.method != 'POST':
        form = AnyMesForm() # An unbound form
    else:        
        form = AnyMesForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            any = form.cleaned_data['any']
            mes = form.cleaned_data['mes']
            
            dies_mes = calendar.monthrange(any, mes)
            start_date = datetime.date(any, mes, 1)
            end_date = datetime.date(any, mes, dies_mes[1])
                               
            table = []
                
            # ACI
            queryset = ProvaOhdimCodificacioGenerica.objects.using('portal_dades'
                                        ).filter(
                                                 #status_prestacio_codi='CO',
                                                 #agendaidi__unitat_idi_id=1, 
                                                 #agendaidi__idtipoexploracion=1,
                                                 prova_ohdim__data_document_revisat__range=(start_date, end_date),
                                                 codificacio_generica_final__id__in=(7,8),
                                               #  codificacio_generica_modificada=7,
                                               #  codificacio_generica_modificada__isnull=False,
                                                 ).order_by('prova_ohdim__metge_allibera', 'prova_ohdim__hospital', 'prova_ohdim__prestacio')
            metges = {}
            #proves = []
            
            # TODO:
            # 2- comentar que les proves complementaries ara mateix es cobren dobles. Veure què hem de fer.
            icona_oky = '<IMG SRC="/static/admin/img/admin/icon_success.gif" BORDER=0 ALT="Fet">'
            icona_nooky = '<IMG SRC="/static/admin/img/admin/icon_alert.gif" BORDER=0 ALT="Per Fer">'
            for prova in queryset:
                try:
                    s = prova.prova_ohdim.prestacio.zona_anatomica_idi
                except:
                    s = 'Desconegut'
                
                key = unicode(prova.prova_ohdim.metge_allibera_id)+unicode(s)
                if key not in metges:
                    metges[key] = [prova.prova_ohdim.metge_allibera.cognoms_nom,  s, 1, []]
                    metges[key][3].append(( "<b>Id</b>", "<b>Hospital (prestació)</b>" , "<b>Desc. prestació</b>", "<b>Data</b>", "<b>Hora</b>", "<b>Hospital</b>", "<b>Núm. d'història</b>", "<b>Modalitat</b>", "<b>Codificació revisada</b>", "<b>Modificat</b>","<b>Validat</b>"))
                    
                    if prova.validat == True :
                        validat = icona_oky
                    else:
                        validat=icona_nooky
                       
                    if (prova.modificat_per == None ) or  ( prova.modificat_per ==''):
                        modificat = ''
                    else:
                        modificat =icona_oky
                        
                     
                    item = (prova.prova_ohdim.id, prova.prova_ohdim.hospital.descripcio , prova.prova_ohdim.prestacio.descripcio, prova.prova_ohdim.data_document_revisat, prova.prova_ohdim.hora_document_revisat, prova.prova_ohdim.hospital,
                            prova.prova_ohdim.historia_clinica_id, prova.prova_ohdim.tipus_modalitat, prova.codificacio_generica_final , modificat,validat)            
                    metges[key][3].append(item)         
                else:
                    
                    if prova.validat == True :
                        validat = icona_oky
                    else:
                        validat=icona_nooky
                       
                    if (prova.modificat_per == None ) or  ( prova.modificat_per ==''):
                        modificat = ''
                    else:
                        modificat =icona_oky
                        
                    item = (prova.prova_ohdim.id, prova.prova_ohdim.hospital.descripcio, prova.prova_ohdim.prestacio.descripcio, prova.prova_ohdim.data_document_revisat, prova.prova_ohdim.hora_document_revisat, prova.prova_ohdim.hospital,
                            prova.prova_ohdim.historia_clinica_id, prova.prova_ohdim.tipus_modalitat, prova.codificacio_generica_final,modificat,validat)            
                    metges[key][3].append(item)
                    metges[key][2] += 1
            
            table = []
            for item in metges.itervalues():
                table.append(item)
            table = sorted(table, key=itemgetter(0))        
        
            lastmetge = ""
            currmetge = ()
            for item in table:
                if item[0] == lastmetge:
                    currmetge[1].append((item[1], item[2], item[3]))
                else:
                    if len(currmetge) > 0:
                        result.append(currmetge)
                    currmetge = (item[0], [(item[1], item[2], item[3])])
                lastmetge = item[0]
            #El ultim sempre es queda fora == a que l'agfegim nosaltres
            if len(currmetge) > 0:
                result.append(currmetge)
    
    auditoria.request_handler(request,10,'Tipus consulta: activitatcomplementaria ; ' + str(indicadors) + ' any: ' + str(any) + 'mes:' + str(mes)) 
    return render_to_response('rrhh/activitat_complementaria.html',
                              {'indicadors': indicadors,
                               'metges':result,
                               'form':form},
                              context_instance=RequestContext(request))    

#from contrib.auth.decorators import user_passes_test
#def staff_required(login_url=None):

#    return user_passes_test(lambda u: u.is_staff, login_url=login_url)

#@staff_required(login_url="../admin")
#def series_info(request)
#def series_info(request)

@login_required
@permission_required('meta.rrhh', login_url='/accounts/permisdenegat/')
def activitatcomplementaria2(request):
    result = []  

    informes_lliurats_des_de_dia  = '' 
    informes_lliurats_fins_a_dia  = ''       
    informes_validats_a_partir_de = ''
    dades  =''      
    
    
    if request.method != 'POST':
        form = ComplementairesForm() # An unbound form
        dat = auditoria.Audit.objects.filter(idtipusaudit=10 , dades__contains = 'extraccio correcte').values('dataevent','dades').order_by('-idaudit')
        dat = dat[0]
        dades = ""#str(dat['dades']) #'2012-05-20'# han de ser mes grans que la data de la darrera extracció ( es dir els que no estaven vaildats fins llavors)
        data_tall = 'Darrera execució: ' + str(dat['dataevent']) #'2012-05-20'# han de ser mes grans que la data de la darrera extracció ( es dir els que no estaven vaildats fins llavors)
            
        
        responsel = render_to_response('rrhh/activitat_complementaria.html',
                              {'indicadors': indicadors,
                               'metges':result,
                                'dades':dades,
                               'data_tall':data_tall,
                               'form':form},
                              context_instance=RequestContext(request))
    else:        
        form = ComplementairesForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            
            #any = form.cleaned_data['any']
            #mes = form.cleaned_data['mes']
            #mes = form.cleaned_data['mes']
            #data_inici =form.cleaned_data['data_inici'] 
            #data_fi =  form.cleaned_data['data_fi'] 
            #dies_mes = calendar.monthrange(any, mes)
            #start_date = datetime.date(any, mes, 1)
            #end_date = datetime.date(any, mes, dies_mes[1])
                    
            informes_lliurats_des_de_dia  = form.cleaned_data['informes_alliberats_des_de_dia'] 
            informes_lliurats_fins_a_dia  = form.cleaned_data['informes_alliberats_fins_a_dia']       
            informes_validats_a_partir_de = form.cleaned_data['informes_validats_a_partir_de']
            
                    
                    
                    
                               
            from django.db import connections
            cursor = connections['portal_dades'].cursor()
            
            
            query  = """
                    SELECT DISTINCTROW
                    
                        metge.cognoms_nom ,
                        zona_anatomica.descripcio,
                        prova_ohdim_codificacio_generica.id as Proces,
                        hospital.descripcio as 'Hospital Prestacio',
                        prestacio.descripcio,
                        #date_format(prova_ohdim.data_document_revisat, %s ) as 'data',
                        prova_ohdim.data_document_revisat as 'data',
                        (`prova_ohdim`.`hora_document_revisat`) as 'hora',
                        hospital.descripcio as 'Hospital',
                       `prova_ohdim`.historia_clinica_id as 'Núm.Historia',
                        tipus_modalitat.descripcio ,
                        if( `prova_ohdim_codificacio_generica`.`codificacio_generica_final_id` = 7 , 'ACI' , 'ACH'   ) as 'codificació revisada',
                        if( `prova_ohdim_codificacio_generica`.`modificat_per_id` is  not null, 'Fet', '' ) as Modificat ,
                        if( `prova_ohdim_codificacio_generica`.`validat` = True, 'Fet', 'Per Fer' ) as Validat ,
                        hour(  `prova_ohdim`.`hora_document_revisat` ) as 'Banda hora',
                        if( `prova_ohdim_codificacio_generica`.`validat` = True, 'Fet', 'Per Fer' ) as VALID

                   FROM `prova_ohdim_codificacio_generica`
                   
                       INNER JOIN `prova_ohdim` ON (`prova_ohdim_codificacio_generica`.`prova_ohdim_id` = `prova_ohdim`.`id`)
                       LEFT OUTER JOIN `metge` ON (`prova_ohdim`.`metge_allibera_id` = `metge`.`id`)
                       LEFT OUTER JOIN `hospital` ON (`prova_ohdim`.`hospital_id` = `hospital`.`id`)
                       LEFT OUTER JOIN `prestacio` ON (`prova_ohdim`.`prestacio_id` = `prestacio`.`id`)
                       LEFT JOIN `zona_anatomica` ON ( prestacio.zona_anatomica_idi_id = zona_anatomica.id)
                       LEFT JOIN tipus_modalitat on  (prova_ohdim.tipus_modalitat_id = tipus_modalitat.id )

                   WHERE (`prova_ohdim`.`data_document_revisat` BETWEEN %s and %s
                        AND `prova_ohdim_codificacio_generica`.`codificacio_generica_final_id` IN (7, 8)
                        AND  `prova_ohdim_codificacio_generica`.`validat_data` >= %s)

                  ORDER BY `metge`.`cognoms_nom` ASC, `hospital`.`descripcio` ASC, `prestacio`.`codi` ASC
                  ;""" 
            #variables passades a la query injection-less
            formata= "'%Y-%m-%d'"
            # data_mes_escollit = end_date
            data_tall = str(auditoria.Audit.objects.filter(idtipusaudit=10).values('dataevent','dades').order_by('-idaudit')[0]['dataevent']  + datetime.timedelta(seconds=60)) #'2012-05-20'# han de ser mes grans que la data de la darrera extracció ( es dir els que no estaven vaildats fins llavors) li sumem 1 minut despres d'haber fet l'extracció
            

            cursor.execute(query, [formata,informes_lliurats_des_de_dia,informes_lliurats_fins_a_dia,informes_validats_a_partir_de])#s'ha de fer aixi per evitar el injection!!!! no a a la string sino aqui , en el execute
            response = cursor.fetchall()
            
            if len(response) == 0:
                missatge = "No s'han trobat prestacions validades o ja s'havia fet una extracció on hi eren incloses!."  
                responsel = render_to_response('rrhh/activitat_complementaria.html',
                              {'indicadors': indicadors,
                               'form':form,
                               'missatge':missatge
                               },
                              context_instance=RequestContext(request))
            else:
                responsel =  HttpResponse(ExcelResponse(response) , content_type='application/vnd.ms-excel; charset=utf-8')
                filename = "complementaries_%s-%s__%s.xls" % (informes_lliurats_des_de_dia,informes_lliurats_fins_a_dia,  str(datetime.datetime.now()) )
                responsel['Content-Disposition'] = 'attachment; filename='+filename
                
                auditoria.request_handler(request,10,'Tipus consulta: activitatcomplementaria ; extraccio correcte:' + filename + '  ;nova_data_tall:' + str(datetime.datetime.now() ))
            
        else:
            form = ComplementairesForm(request.POST) # A form bound to the POST data
            missatge = "Les dades introduïdes no son valides!."  
            responsel = render_to_response('rrhh/activitat_complementaria.html',
                              {'indicadors': indicadors,
                               'form':form,
                               'missatge':missatge
                               },
                              context_instance=RequestContext(request))
        
        auditoria.request_handler(request,10,'Tipus consulta: activitatcomplementaria , lliurats des de:' + str(informes_lliurats_des_de_dia)  + ' lliurats fins a dia:  ' + str(informes_lliurats_fins_a_dia) + ' validats_a_partir_de ' + str(informes_validats_a_partir_de) )
            
            
    return responsel    
    
    
@login_required
@permission_required('meta.rrhh', login_url='/accounts/permisdenegat/')
def extraccioActivitatcomplementaria(request):
    result = []  
    any = 0
    mes = 0
    if request.method != 'POST':
        form = ComplementairesForm() # An unbound form
        dat = auditoria.Audit.objects.filter(idtipusaudit=10).values('dataevent','dades').order_by('-idaudit')[0]
        dades = str(dat['dades']) #'2012-05-20'# han de ser mes grans que la data de la darrera extracció ( es dir els que no estaven vaildats fins llavors)
        data_tall = str(dat['dataevent']) #'2012-05-20'# han de ser mes grans que la data de la darrera extracció ( es dir els que no estaven vaildats fins llavors)
            
        responsel = render_to_response('rrhh/activitat_complementaria.html',
                              {'indicadors': indicadors,
                               'metges':result,
                                'dades':dades,
                               'data_tall':data_tall,
                               'form':form},
                              context_instance=RequestContext(request))
    else:        
        form = ComplementairesForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            
            any = form.cleaned_data['any']
            mes = form.cleaned_data['mes']
            
            dies_mes = calendar.monthrange(any, mes)
            start_date = datetime.date(any, mes, 1)
            end_date = datetime.date(any, mes, dies_mes[1])
                               
            from django.db import connections
            cursor = connections['portal_dades'].cursor()
            # Data retrieval operation - no commit required
            dada = 10846106
            
            query  = """
                    SELECT DISTINCTROW
                    
                        metge.cognoms_nom ,
                        zona_anatomica.descripcio,
                        prova_ohdim_codificacio_generica.id as Proces,
                        hospital.descripcio as 'Hospital Prestacio',
                        prestacio.descripcio,
                        #date_format(prova_ohdim.data_document_revisat, %s ) as 'data',
                        prova_ohdim.data_document_revisat as 'data',
                        (`prova_ohdim`.`hora_document_revisat`) as 'hora',
                        hospital.descripcio as 'Hospital',
                       `prova_ohdim`.historia_clinica_id as 'Núm.Historia',
                        tipus_modalitat.descripcio ,
                        if( `prova_ohdim_codificacio_generica`.`codificacio_generica_final_id` = 7 , 'ACI' , 'ACH'   ) as 'codificació revisada',
                        if( `prova_ohdim_codificacio_generica`.`modificat_per_id` is  not null, 'Fet', '' ) as Modificat ,
                        if( `prova_ohdim_codificacio_generica`.`validat` = True, 'Fet', 'Per Fer' ) as Validat ,
                        hour(  `prova_ohdim`.`hora_document_revisat` ) as 'Banda hora',
                        if( `prova_ohdim_codificacio_generica`.`validat` = True, 'Fet', 'Per Fer' ) as VALID

                   FROM `prova_ohdim_codificacio_generica`
                   
                       INNER JOIN `prova_ohdim` ON (`prova_ohdim_codificacio_generica`.`prova_ohdim_id` = `prova_ohdim`.`id`)
                       LEFT OUTER JOIN `metge` ON (`prova_ohdim`.`metge_allibera_id` = `metge`.`id`)
                       LEFT OUTER JOIN `hospital` ON (`prova_ohdim`.`hospital_id` = `hospital`.`id`)
                       LEFT OUTER JOIN `prestacio` ON (`prova_ohdim`.`prestacio_id` = `prestacio`.`id`)
                       LEFT JOIN `zona_anatomica` ON ( prestacio.zona_anatomica_idi_id = zona_anatomica.id)
                       LEFT JOIN tipus_modalitat on  (prova_ohdim.tipus_modalitat_id = tipus_modalitat.id )

                   WHERE (`prova_ohdim`.`data_document_revisat` BETWEEN '2012-01-01' and %s
                        AND `prova_ohdim_codificacio_generica`.`codificacio_generica_final_id` IN (7, 8)
                        AND  `prova_ohdim_codificacio_generica`.`validat_data` >= %s)

                  ORDER BY `metge`.`cognoms_nom` ASC, `hospital`.`descripcio` ASC, `prestacio`.`codi` ASC
                  ;""" 
            #variables passades a la query injection-less
            formata= "'%Y-%m-%d'"
            data_mes_escollit = end_date
            data_tall = str(auditoria.Audit.objects.filter(idtipusaudit=10).values('dataevent','dades').order_by('-idaudit')[0]['dataevent']  + datetime.timedelta(seconds=60)) #'2012-05-20'# han de ser mes grans que la data de la darrera extracció ( es dir els que no estaven vaildats fins llavors) li sumem 1 minut despres d'haber fet l'extracció
            

            cursor.execute(query, [formata,data_mes_escollit,data_tall])#s'ha de fer aixi per evitar el injection!!!! no a a la string sino aqui , en el execute
            response = cursor.fetchall()
            
            if len(response) == 0:
                missatge = "No s'han trobat prestacions validades o ja s'havia fet una extracció on hi eren incloses!."  
                responsel = render_to_response('rrhh/activitat_complementaria.html',
                              {'indicadors': indicadors,
                               'form':form,
                               'missatge':missatge
                               },
                              context_instance=RequestContext(request))
            else:
                responsel =  HttpResponse(ExcelResponse(response) , content_type='application/vnd.ms-excel; charset=utf-8')
                filename = "complementaries_%s_%s.xls" % (data_mes_escollit,  str(datetime.datetime.now()) )
                responsel['Content-Disposition'] = 'attachment; filename='+filename
    
        auditoria.request_handler(request,10,'Tipus consulta: activitatcomplementaria ; data_mes_escollit:' + str(any) + '/' +  str(mes) + ';data_tall:' + str(data_tall) + ';nova_data_tall:' + str(datetime.datetime.now() ))
            
    return responsel    


