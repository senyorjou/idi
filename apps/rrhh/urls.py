'''
Created on 17/10/2011
@author: amateu
'''

from django.conf.urls.defaults import url, patterns, include

urlpatterns = patterns('apps.rrhh.views',
    url(r'^$', 'index'), #
    url(r'^index$', 'index'), #
    url(r'^nomines$', 'nomines'), #
    url(r'^dpos$', 'dpos'),
    url(r'^irpf$', 'irpf'),
    url(r'^pagues-extra$', 'extres'),
    url(r'^activitat-complementaria', 'activitatcomplementaria2'),
    # url(r'^afegir_usuari$', 'afegir_usuari'),
    #url(r'^modificar_usuari$', 'modificar_usuari'),
    #url(r'^eliminar_usuari$', 'eliminar_usuari'),
    #url(r'^eliminar_usuari$', 'eliminar_usuari'),
    )