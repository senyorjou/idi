# coding: utf-8
#
from datetime import datetime, timedelta

from django.utils.translation import ugettext_lazy as _
from django.utils.simplejson import loads

from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib import messages

# from django.contrib.auth.models import User
from apps.meta.models import MetaUnitatIdi, MetaGrupCategoriaProfessional

from apps.idirh.models import GrupUnitat, LlocTreballGrup, FAPMaquina,\
                                  Maquina, PeriodeMaquina, Equip, PeriodeEquip,\
                                  FAPEquip, IncidenciaEquip,\
                                  PeriodeEquipSequenciaPersonal

from apps.idirh.forms import GrupUnitatForm, MetaUnitatIdiForm,\
                                 LLocTreballGrupForm, MaquinaForm,\
                                 PeriodeMaquinaForm, UnitatGrupEquipForm,\
                                 EquipPeriodeForm


@login_required
def index(request):
    """
    Vista principal llistat de d'unitats
    """
    # Nomes es veuen les unitats a les que es te acces
    unitats = request.user.get_profile().unitats_idi_permissos\
                                        .filter(actiu=True)\
                                        .order_by('centre_idi')

    context = {'unitats': unitats}

    return render_to_response('planilles/unitats_index.html', context,
                              context_instance=RequestContext(request))


@login_required
def unitat(request, unitat_id):
    """
    Vista d'unitat, amb atributs necessaris
    """
    unitat = MetaUnitatIdi.objects.get(id=unitat_id)

    if request.method == 'POST':
        form = MetaUnitatIdiForm(request.POST, instance=unitat)
        if form.is_valid():
            form.save()
    else:
        form = MetaUnitatIdiForm(instance=unitat)

    context = {
        'unitat_id': unitat_id,
        'form': form,
        'unitat': unitat,
    }

    return render_to_response('planilles/unitats_unitat.html', context,
                              context_instance=RequestContext(request))


@login_required
def unitat_grup(request, unitat_id=0, grup_id=0):
    unitat = MetaUnitatIdi.objects.get(id=unitat_id)

    if int(grup_id):
        grup = GrupUnitat.objects.get(id=grup_id)
    else:
        grup = GrupUnitat(unitat=unitat)

    # Si es un post es passa per aqui
    if request.method == 'POST':
        form = GrupUnitatForm(request.POST, instance=grup)
        if form.is_valid():
            form.save()
            return redirect('unitat', unitat_id)
    else:
        form = GrupUnitatForm(instance=grup)

    context = {
        'grup': grup,
        'form': form,
    }

    return render_to_response('planilles/grups_grup.html', context,
                              context_instance=RequestContext(request))


@login_required
def unitat_grup_delete(request, unitat_id=0, grup_id=0):
    """
    TODO. Comprobacions???
    """
    grup = GrupUnitat.objects.get(id=grup_id)

    #llocs de treball depen de grups_unitat
    if len(grup.lloctreballgrup_set.all()):

        msg_out = _('Aquest grup no es pot esborrar.\
                     Disposa de franges ja assignades ')
        messages.error(request, msg_out)
        return redirect('grup_unitat', unitat_id, grup_id)
    else:
        if (grup):
            grup.delete()

    return redirect('unitat', unitat_id)


@login_required
def unitat_grup_equip(request, unitat_id=0, grup_id=0, equip_id=0):
    form_periode = None
    grup = GrupUnitat.objects.get(id=grup_id)

    if int(equip_id):
        equip = Equip.objects.get(id=equip_id)
        form_periode = EquipPeriodeForm(unitat_id, request.POST)
    else:
        equip = Equip(grup=grup)

    # Si es un post es passa per aqui
    if request.method == 'POST':
        form = UnitatGrupEquipForm(request.POST, instance=equip)
        if form.is_valid():
            form.save()
            return redirect('unitat_grup', unitat_id, grup_id)
    else:
        form = UnitatGrupEquipForm(instance=equip)

    context = {
        'equip': equip,
        'form': form,
        'form_periode': form_periode
    }

    return render_to_response('planilles/equips_equip.html', context,
                              context_instance=RequestContext(request))


@login_required
def unitat_grup_equip_delete(request, unitat_id=0, grup_id=0, equip_id=0):
    equip = Equip.objects.get(id=equip_id)
    equip.delete()

    return redirect('unitat_grup', unitat_id, grup_id)


@login_required
def unitat_grup_equip_add_periode(request, unitat_id=0, grup_id=0, equip_id=0):
    if request.method == 'POST':
        form = EquipPeriodeForm(unitat_id, request.POST)
        if form.is_valid():
            tipus_accio = form.cleaned_data['tipus_accio']
            seq_data_inici = form.cleaned_data['seq_data_inici']
            seq_data_fi = form.cleaned_data['seq_data_fi']
            incidencia = form.cleaned_data['incidencia']

            sequencia_m = form.cleaned_data['sequencia_m']
            sequencia_t = form.cleaned_data['sequencia_t']
            sequencia_n = form.cleaned_data['sequencia_n']

            # personal = form.cleaned_data['personal']
            mov_hora_inici = form.cleaned_data['mov_hora_inici']
            mov_hora_final = form.cleaned_data['mov_hora_final']
            mov_comentari = form.cleaned_data['mov_comentari']

            # Aquest camp és un dict jsonificat
            # l'haig de converir del sting original a dict again amb json.loads
            categories_personal = loads(form.cleaned_data['categories_personal'])

            equip = Equip.objects.get(id=equip_id)
            anyo = datetime.now().year

            try:
                data_inici = datetime.strptime(seq_data_inici, '%d-%m-%Y')
            except:
                data_inici = datetime.strptime('01-01-%s' % anyo, '%d-%m-%Y')

            try:
                data_fi = datetime.strptime(seq_data_fi, '%d-%m-%Y')
            except:
                data_fi = data_inici

            # Si grabem un PERIODE/EQUIP
            # 1.- Grabem priode
            # 2.- Grabem franges de periode
            # 3.- Grabem sequencia/personal/numero que tenim al array:

            if tipus_accio == 'S' and (sequencia_m or sequencia_t or sequencia_n):
                for mtn, sequencia in [('m', sequencia_m),
                                       ('t', sequencia_t),
                                       ('n', sequencia_n)]:
                    if sequencia:
                        periode_equip = PeriodeEquip(
                            equip=equip,
                            data_inici=data_inici,
                            data_fi=data_fi,
                            sequencia=sequencia)

                        periode_equip.save()

                        for franja in periode_equip.sequencia.franjatemplate_set.all():
                            fap_equip = FAPEquip(
                                periode_equip=periode_equip,
                                franja_template=franja,
                                dia_setmana=franja.dia_setmana,
                                hora_inici=franja.inici,
                                hora_fi=franja.fi,
                                sequencia=franja.sequencia)
                            fap_equip.save()

                        # Categories_personal ve en format x, y; m, n;...
                        els = [el.split(',') for el in categories_personal[mtn].split(';') if len(el)]
                        for el in els:
                            cat_id = el[0]
                            cat_qty = el[1]
                            categoria = MetaGrupCategoriaProfessional.objects.get(id=cat_id)
                            per_equip_seq_pers = PeriodeEquipSequenciaPersonal(
                                periode_equip=periode_equip,
                                categoria=categoria,
                                personal=cat_qty)

                            per_equip_seq_pers.save()

            if tipus_accio == 'M' and incidencia:
                dies = data_fi - data_inici
                for dia in xrange((dies.days) + 1):
                    dia_i = data_inici + timedelta(days=dia)
                    incidencia_equip = IncidenciaEquip(
                        equip=equip,
                        dia=dia_i,
                        incidencia=incidencia,
                        hora_inici=mov_hora_inici,
                        hora_fi=mov_hora_final,
                        comentari=mov_comentari)
                    incidencia_equip.save()

    return redirect('unitat_grup_equip', unitat_id, grup_id, equip_id)


@login_required
def unitat_grup_equip_delete_periode(request, unitat_id, grup_id, equip_id,
                                     periode_id):
    periode = PeriodeEquip.objects.get(id=periode_id)
    periode.delete()

    return redirect('unitat_grup_equip', unitat_id, grup_id, equip_id)


@login_required
def unitat_grup_equip_delete_incidencia(request, unitat_id, grup_id, equip_id,
                                        incidencia_id):
    incidencia = IncidenciaEquip.objects.get(id=incidencia_id)
    incidencia.delete()

    return redirect('unitat_grup_equip', unitat_id, grup_id, equip_id)


@login_required
def index_grups(request, unitat_id=0):
    unitat = MetaUnitatIdi.objects.get(id=unitat_id, actiu=1)
    grups = GrupUnitat.objects.filter(unitat=unitat)

    context = {
        'unitat': unitat,
        'grups': grups
    }

    return render_to_response('planilles/grups_index.html', context,
                              context_instance=RequestContext(request))


@login_required
def grup(request, unitat_id=0, grup_id=0):
        # Si ve de _new es crea una instancia buida
    unitat = get_object_or_404(MetaUnitatIdi, id=unitat_id)
    if int(grup_id):
        grup = GrupUnitat.objects.get(id=grup_id)
    else:
        grup = GrupUnitat()

    # Si es un post es passa per aqui
    if request.method == 'POST':
        form = GrupUnitatForm(request.POST, instance=grup)
        if form.is_valid():
            form.save()
            grup_id = grup.id
            return redirect('grup_unitat', unitat_id, grup_id)
    else:
        form = GrupUnitatForm(instance=grup)

    llocstreball_query = LlocTreballGrup.objects.raw("\
        SELECT *, count(categoria_id) as total_especialistes\
        FROM plan_lloc_treball_grup\
        WHERE grup_id = %s\
        GROUP BY sequencia_id, categoria_id\
        ORDER BY inici, sequencia_id" % grup_id)

    context = {
        'grup_id': grup_id,
        'grup': grup,
        'form': form,
        'unitat': unitat,
        'llocstreball_query': llocstreball_query,
        'llocstreball_bool': len(list(llocstreball_query))
    }

    return render_to_response('planilles/grups_grup.html', context,
                              context_instance=RequestContext(request))


@login_required
def grup_unitat_delete(request, unitat_id=0, grup_id=0):
    """
    Al esborrar un grup ens hem d'assegurar que no hi ha instanciat res
    Si hi ha dades instanciades, deixem un missatge a la sessió i refresh
    """
    grup = GrupUnitat.objects.get(id=grup_id)
    
    # llocs de treball depen de grups_unitat
    if len(grup.lloctreballgrup_set.all()):

        msg_out = _('Aquest grup no es pot esborrar.\
                     Disposa de franges ja assignades ')
        messages.error(request, msg_out)
        return redirect('grup_unitat', unitat_id, grup_id)
    else:
        if (grup):
            grup.delete()

    return redirect('unitat', unitat_id)

@login_required
def grup_unitat_lloc(request, unitat_id=0, grup_id=0, lloc_id=0):
    """
    S'ha de grabar en 2 taules.
    LlocTreballGrup --> 1 registre x especialista que conté FK's a:
     > grup, categoria prof. (especialista), sequencia 
    i atributs: 
     > data_inici, data_fi


    I per cada registre s'instancien les franges que conté cada seqüencia
    al objecte FAPMaquina (Franja Aplicada a Màquina)

    """
    unitat = get_object_or_404(MetaUnitatIdi, id=unitat_id)
    if int(grup_id):
        grup = GrupUnitat.objects.get(id=grup_id)
    else:
        grup = GrupUnitat()

    # Si es un post es passa per aqui
    if request.method == 'POST':
        form = LLocTreballGrupForm(request.user, request.POST)
        inici = datetime.strptime(form['inici'].value(),  '%d-%m-%Y')
        try:
            fi = datetime.strptime(form['fi'].value(),  '%d-%m-%Y')
        except:
            fi = None

        if form.is_valid():
            # especialistes arriba en format 1,2,2,2,3,4,
            # trec la darrera coma i creo array d'INT
            especialistes = [int(x) for x in form.cleaned_data['especialistes_pack'][:-1].split(',')]
            sequencia = form.cleaned_data['sequencies']
            franges = sequencia.franjatemplate_set.all()

            for especialista in especialistes:
                # S ha de grabar:
                # Grup, sequencia, categoria i dates
                categoria = MetaGrupCategoriaProfessional.objects\
                                                         .get(id=especialista)
                lloc_treball = LlocTreballGrup(
                    grup=grup,
                    categoria=categoria,
                    sequencia=sequencia,
                    inici=inici,
                    fi=fi
                )

                lloc_treball.save()
                # Grabem a FAPMaquina totes les franges per registre
                for franja in franges:
                    fap_maquina = FAPMaquina(
                        franja_template=franja,
                        dia_setmana=franja.dia_setmana,
                        inici=franja.inici,
                        fi=franja.fi,
                        sequencia=sequencia,
                        lloc_treball_grup=lloc_treball
                    )
                    fap_maquina.save()

            return redirect('grup_unitat', grup.unitat.id, grup.id)
    else:
        form = LLocTreballGrupForm(request.user)

    context = {
        'grup_id': grup_id,
        'grup': grup,
        'form': form,
        'unitat': unitat
    }

    return render_to_response('planilles/grups_grup_lloc_treball.html', context,
                              context_instance=RequestContext(request))


@login_required
def grup_lloc_treball(request, grup_id=0):
    if int(grup_id):
        grup = GrupUnitat.objects.get(id=grup_id)
    else:
        grup = GrupUnitat()

    form = LLocTreballGrupForm(request.POST)

    if form.is_valid():
        # especialistes arriba en format 1,2,2,2,3,4,
        # trec la darrera coma i creo array d'INT
        especialistes = [int(x) for x in form.cleaned_data['especialistes_seleccionats'][:-1].split(',')]
        #sequencia = Sequencia.objects.get(id=form.cleaned_data['sequencies'])

        for especialista in especialistes:
            if especialista:
                # S ha de grabar: 
                # Grup, sequencia, categoria
                categoria = MetaGrupCategoriaProfessional.objects\
                                                         .get(id=especialista)
                lloc_treball = LlocTreballGrup(
                    grup = grup,
                    categoria = categoria,
                    sequencia = form.cleaned_data['sequencies']
                )

                lloc_treball.save()

        grup_id = grup.id

    context = {
        'grup_id': grup_id,
        'grup': grup,
        'form': form,
    }

    return render_to_response('planilles/grups_grup.html', context,
                              context_instance=RequestContext(request))


@login_required
def grup_lloc_treball_delete(request, unitat_id=0, grup_id=0, seq_id=0, cat_id=0):
    """
    Al esborrar un lloc de treball s'ha d'esborrar també tot el conjunt
    de FAP's associades a ell
    """
    llocs_treball = LlocTreballGrup.objects.filter(grup=grup_id,
                                                   sequencia=seq_id,
                                                   categoria=cat_id)
    # A lo loco. REVIEW!!!
    for lloc in llocs_treball:
        lloc.fapmaquina_set.all().delete()

    llocs_treball.delete()

    return redirect('grup_unitat', unitat_id, grup_id)


@login_required
def maquines_index(request):

    # unitat = MetaUnitatIdi.objects.get(id=unitat_id, actiu=1)
    # grups = GrupUnitat.objects.filter(unitat=unitat)

    unitats = request.user.get_profile().unitats_idi_permissos\
                                        .filter(actiu=1)\
                                        .order_by('centre_idi')
    grups = GrupUnitat.objects.filter(unitat__in=unitats)
    equips = Equip.objects.filter(grup__in=grups)\
                          .order_by('grup', 'nom')

    context = {'equips': equips}

    return render_to_response('planilles/maquines_index.html', context,
                              context_instance=RequestContext(request))


@login_required
def maquines_maquina(request, maquina_id=0):
    if int(maquina_id):
        try:
            maquina = Maquina.objects.get(id=maquina_id)
        except Maquina.DoesNotExist:
            return redirect('maquines')

    else:
        maquina = Maquina()

    # Si es un post es passa per aqui
    if request.method == 'POST':
        form = MaquinaForm(request.POST, instance=maquina)
        if form.is_valid():
            form.save()
            maquina_id = maquina.id
            return redirect('maquina', maquina_id)
    else:
        form = MaquinaForm(instance=maquina)

    unitats = request.user.get_profile().unitats_idi_permissos\
                                        .filter(actiu=True)\
                                        .order_by('centre_idi')
    grups = GrupUnitat.objects.filter(unitat__in=unitats)

    context = {
        'maquina_id': maquina_id,
        'maquina': maquina,
        'form': form,
        'grups': grups
    }

    return render_to_response('planilles/maquines_maquina.html', context,
                              context_instance=RequestContext(request))


@login_required
def maquines_maquina_delete(request, maquina_id=0):
    # ALL-in-one solution per a esborrar un registre, passant de TRY/CATCH
    q = Maquina.objects.filter(id=maquina_id)
    if q.exists():
        q[0].delete()

    return redirect('maquines')


@login_required
def maquines_periode_maquina(request, maquina_id=0):
    if int(maquina_id):
        maquina = Maquina.objects.get(id=maquina_id)
    else:
        maquina = Maquina()

    if request.method == 'POST':
        form = PeriodeMaquinaForm(request.POST)
        data_inici = datetime.strptime(form['data_inici'].value(),  '%d-%m-%Y')
        try:
            data_fi = datetime.strptime(form['data_fi'].value(),  '%d-%m-%Y')
        except:
            data_fi = None

        if form.is_valid():
            periode_maquina = PeriodeMaquina(
                maquina=maquina,
                sequencia=form.cleaned_data['sequencia'],
                data_inici=data_inici,
                data_fi=data_fi,
                # personal = form.cleaned_data['personal']
            )
            periode_maquina.save()
            return redirect('maquina', maquina_id)
    else:
        form = PeriodeMaquinaForm()

    context = {
        'maquina_id': maquina_id,
        'maquina': maquina,
        'form': form,
    }

    return render_to_response('planilles/maquines_maquina_periode.html', context,
                              context_instance=RequestContext(request))


@login_required
def maquines_periode_maquina_delete(request, maquina_id=0, seq_id=0):
    if int(seq_id):
        periode = PeriodeMaquina.objects.get(id=seq_id)
        periode.delete()

    return redirect('maquina', maquina_id)
