# -*- coding: utf-8 -*-
# utils.py
#   per guardar-hi funcions globals
import re
import math
from datetime import timedelta, datetime, date, time

from django.contrib.auth.models import User
from django.db.models import Q



def permisos(user):
    """
    Syntactic sugar per no escriure
    request.user.get_profile().permis_planilles()
    """
    return user.get_profile().permis_planilles()


# Retorna el rang d'hores d'intersecció
def interseccio_hores_mateix_dia(hora_inici1, hora_fi1, hora_inici2, hora_fi2):
    if not hora_inici1 or not hora_fi1 or not hora_inici2 or not hora_fi2:
        raise ValueError("Parametres invàlids")

    if hora_inici1 < hora_inici2:
        if hora_fi1 <= hora_inici2:
            # Els rangs estan totalment separats
            return None, None
        elif hora_fi1 <= hora_fi2:
            # Hi ha interseccio
            return hora_inici2, hora_fi1
        elif hora_fi1 > hora_fi2:
            # El primer rang es menja totalmentel segon
            # interseccio = (hora_inici2, hora_fi2)
            return hora_inici2, hora_fi2
    else:
        if hora_inici1 >= hora_inici2:
            if hora_inici1 >= hora_fi2:
                # Els rangs estan totalment separats
                return None, None
            elif hora_inici1 < hora_fi2:
                # El segon rang es menja totalmentel segon
                # interseccio = (hora_inici1, hora_fi1)
                return hora_inici1, hora_fi1


def get_segons_mateix_dia(hora_inici, hora_fi):
    if hora_inici > hora_fi:
        raise ValueError("Parametres invàlids")

    hores = hora_fi.hour - hora_inici.hour
    minuts = hora_fi.minute - hora_inici.minute

    return hores * 60 * 60 + minuts * 60


def get_segons_dia_nit_mateix_dia(hora_inici, hora_fi):
    # Horari de nit de 22 a 8
    if not hora_inici or not hora_fi:
        raise ValueError("Parametres invàlids")

    hora_inici_nit = time(22, 0)
    segons_dia = segons_nit = 0
    if hora_fi <= hora_inici_nit:
        # Tot són segons de dia
        segons_dia = get_segons_mateix_dia(hora_inici, hora_fi)
    else:
        # Tots són segons de nit
        if hora_inici >= hora_inici_nit:
            segons_nit = get_segons_mateix_dia(hora_inici, hora_fi)
        else:
            # Hi han segons de dia i de nit
            segons_dia = get_segons_mateix_dia(hora_inici, hora_inici_nit)
            segons_nit = get_segons_mateix_dia(hora_inici_nit, hora_fi)

    return segons_dia, segons_nit


def get_hores_en_format(segons, format='resum'):
    """
    Retorna un string amb hores i minuts.
    Es retorna en format:
    resum : hh:mm
    hhmm : xxh yym
    llarg : <b>hh</b>hores <b>mm</b> minuts

    """
    minuts, segons = divmod(segons, 60)
    hores, minuts = divmod(minuts, 60)
    if format == 'hhmm':
        return u'<strong>%d</strong>h <strong>%02d</strong>m' %\
               (hores, minuts)

    if format == 'llarg':
        return u'<strong>%d</strong> hores <strong>%02d</strong> minuts' %\
               (hores, minuts)

    return u'%d:%02d' % (hores, minuts)


# Util per cerques directes amb OR a varis camps
# tret de
#   http://julienphalip.com/post/2825034077/adding-search-to-a-django-site-in-a-snap
def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
        and grouping quoted words together.
        Example:
        >>> normalize_query('  some random  words "with   quotes  " and   spaces')
        ['some', 'random', 'words', 'with quotes', 'and', 'spaces']
    '''
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)]


def get_query(query_string, search_fields):
    ''' Returns a query, that is a combination of Q objects. That combination
        aims to search keywords within a model by testing the given search fields.
    '''
    query = None  # Query to search for every search term
    terms = normalize_query(query_string)
    for term in terms:
        or_query = None  # Query to search for a given term in each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query


# Solucio guapa per iterar entre dates, treta de
# http://stackoverflow.com/questions/1060279/iterating-through-a-range-of-dates-in-python#31060330
# Millorada per agafar el dia final, o no.
def daterange(start_date, end_date, end=False):
    days_plus = 0
    if end:
        days_plus = 1
    for n in range(int((end_date - start_date).days) + days_plus):
        yield start_date + timedelta(n)


def tooltip_html(dia, fap):
    br = u'<br />'
    s = dia.strftime('%d/%m/%Y')
    used = []
    for items in fap:
        if items['sequencia'] not in used:
            s += br + u'<strong>%s</strong>' % items['sequencia']
            if items['reduccio']:
                s += ' (%s %%)' % items['reduccio']
        used.append(items['sequencia'])  # Per no repetir sequencia
    segons_total = 0
    for items in fap:
        s += br + u'<strong>%s</strong> - ' % str(items['hora_inici'])[:-3]
        s += u'<strong>%s</strong>' % str(items['hora_fi'])[:-3]
        s += u' · %s' % get_hores_en_format(items['segons'], format='hhmm')
        segons_total += items['segons']
    if len(fap) > 1:  # Si més d'una franja, un tota maco
        s += br + u'Hores: %s' % get_hores_en_format(segons_total, format='hhmm')

    return s


def get_treballadors_actius():
    query = User.objects.filter(Q(authuserprofile__es_idi=True, authuserprofile__es_actiu=True)
                                | Q(authuserprofile__es_idi=False, authuserprofile__es_gestio_idi=True))

    return query

def get_dia(treballador, dia, calendari, periodes):
    cal_obj = calendari.get_calendari(dia.year)
    for periode in periodes:
        fap = {}
        seqs = []
        # fap, emmagtzena les dades del dia
        # fap[id][dia_setmana] = [franges_aplicades, ]
        # seqs emmagatzema els periode sequencia que actuen en el periode
        for per_seq in periode.rotacioaplicada_set.all():
            seqs.append(per_seq)
            p_seq_id = per_seq.sequencia.id
            fap[p_seq_id] = (dict([(x, []) for x in range(7)]))
            for franja in per_seq.faptreballador_set.all():
                fap[p_seq_id][franja.dia_setmana].append({
                                        'sequencia': franja.sequencia,
                                        'segons': franja.get_seconds().seconds,
                                        'hora_inici': franja.hora_inici,
                                        'hora_fi': franja.hora_fi})
        seq_index = 0
        p_seq_id = seqs[seq_index].sequencia.id
        first_pass = True  # The secret of looping here
        # En aquesta iteració es fa tot, es plenen els dies segons
        # toqui a la sequencia
        for data_i in daterange(periode.data_inici, periode.data_fi, end=True):
            dia_obj = cal_obj[data_i.month - 1][1][data_i.day - 1]
            weekday = data_i.weekday()
            if weekday == 0 and not first_pass:
                # next seq
                if seq_index == len(seqs) - 1:
                    seq_index = 0
                else:
                    seq_index += 1
                p_seq_id = seqs[seq_index].sequencia.id
            if fap[p_seq_id][weekday]:
                if dia_obj['tipus'] not in ('F', 'FL'):
                    dia_obj['tipus'] = 'T'
            first_pass = False

    return cal_obj[dia.month - 1][1][dia.day - 1]


def omplir_calendari(treballador, periodes, moviments, extres, cal_obj, cal_data, anyo, consolidat=[], congelat=False):
    """
    Crea un calendari pels parametres de periodes i moviments
    Modifica els objectes cal_obj i consolidat.
    Si consolidat és false es que cridem desde planilla, no es necessita
    """
    nocturns = segons_fins_avui = segons_totals = 0
    from apps.meta.models import AuthUserProfile
    jornada_anual = AuthUserProfile.objects.get(user=treballador).jornada_anual
    data_quitansa = treballador.get_profile().get_quita() or date(1970, 1, 1)

    RATI_NOCTURN = float(jornada_anual.hores / jornada_anual.hores_nit) - 1

    # Això és per agafar els periodes en ordre correcte,
    # venen ordenats per periode_data_inici, i han de calcular-se en ordre
    # d'inserció
    periodes = periodes.order_by('id')

    if consolidat:
        segons_conveni = 0
        # Per calcular les reduccions
        ratio = float(consolidat['hores_totals']) / cal_data['dies_any']
        #ratio = float(jornada_anual.hores) / cal_data['dies_any']

    data_avui = datetime.now().date()

    # 1 de Gener, per la data Gregoriana, per fer un index de dies[0:DIES_ANY-1]
    dia_1_de_gener = cal_data['dia_1_de_gener_gregoria']

    # La millor manera d'init una llista.
    # Cada dia conté la proporcional del conveni
    dies_conveni = [0] * cal_data['dies_any']
    dies_tipus_dia = [''] * cal_data['dies_any']


    # PRIMER ELS PERIODES
    for periode in periodes:
        fap = {}
        seqs = []
        # fap, emmagtzema les dades del dia
        # fap[id][dia_setmana] = [franges_aplicades, ]
        # seqs emmagatzema els periode sequencia que actuen en el periode
        for per_seq in periode.rotacioaplicada_set.all().order_by('ordre'):
            seqs.append(per_seq)
            p_seq_id = per_seq.sequencia.id
            fap[p_seq_id] = (dict([(x, []) for x in range(7)]))
            for franja in per_seq.faptreballador_set.all():
                fap[p_seq_id][franja.dia_setmana].append({
                                        'sequencia': franja.sequencia,
                                        'segons': franja.get_seconds().seconds,
                                        # 'segons_nit': franja.get_seconds_nit().seconds,
                                        'hora_inici': franja.hora_inici,
                                        'hora_fi': franja.hora_fi,
                                        'reduccio': periode.reduccio,
                                        'abrev_torn': franja.abrev_torn,
                                        'nocturns': franja.get_seconds_nit()})

        if seqs:
            seq_index = 0
            p_seq_id = seqs[seq_index].sequencia.id

            first_pass = True  # The secret of looping here
            # En aquesta iteració es fa tot, es plenen els dies segons
            # toqui a la sequencia
            for i, data_i in enumerate(daterange(periode.data_inici,
                                                 periode.data_fi,
                                                 end=True)):

                idx = data_i.toordinal() - dia_1_de_gener
                dia_obj = cal_obj[data_i.month - 1][1][data_i.day - 1]

                if consolidat and data_i > data_quitansa:
                    if periode.reduccio:
                        dies_conveni[idx] = ratio * (float(periode.reduccio) / 100)
                    else:
                        dies_conveni[idx] = ratio

                weekday = data_i.weekday()
                if weekday == 0 and not first_pass:
                    # next seq
                    if seq_index == len(seqs) - 1:
                        seq_index = 0
                    else:
                        seq_index += 1
                    p_seq_id = seqs[seq_index].sequencia.id

                if fap[p_seq_id][weekday]:
                    if dia_obj['tipus'] not in ('F', 'FL'):
                        dia_obj['tipus'] = 'T'
                        dia_obj['abrev_torn'] = fap[p_seq_id][weekday][0]['abrev_torn']

                        dia_obj['tooltip'] = tooltip_html(data_i, fap[p_seq_id][weekday])
                        dia_obj['css'] = 'torn-%s' % dia_obj['abrev_torn']

                        if fap[p_seq_id][weekday][0]['reduccio']:
                            dia_obj['css'] += ' lower'

                        # Si el dia treballat és en weekend, afegim classe
                        if weekday > 4:
                            dia_obj['css'] += '-weekend'

                        # Si ja tinc segons acumulat d'un periode anterior,
                        # els resto del dia, per poder acumular torns un sobre
                        # l'altre
                        #
                        # Això implica que s'agafen NOMÉS els del darrer periode
                        # entrat
                        # Com que (id_new > id_old) es posa a zero el nombre de
                        # segons a cada periode
                        if dia_obj.get('segons_dia', 0):
                            segons_totals -= dia_obj['segons_dia']
                            segons_fins_avui -= dia_obj['segons_dia']

                        if dia_obj.get('nocturns', 0):
                            nocturns -= dia_obj['nocturns']

                        nocturns_dia = segons_dia = 0
                        for item in fap[p_seq_id][weekday]:
                            segons_dia += item['segons']
                            nocturns_dia += item['nocturns']

                        dia_obj['segons_dia'] = segons_locals = segons_dia
                        dia_obj['nocturns'] = nocturns_dia
                        # afegit per agafar la extra
                        dia_obj['hora_inici'] = fap[p_seq_id][weekday][0]['hora_inici']
                        dia_obj['hora_fi'] = fap[p_seq_id][weekday][0]['hora_fi']

                        segons_totals += segons_locals
                        nocturns += nocturns_dia

                        if consolidat and data_i > data_quitansa:
                            if dia_obj['nocturns']:
                                dies_tipus_dia[idx] = 'N'
                            elif dia_obj['segons_dia']:
                                dies_tipus_dia[idx] = 'D'

                        if data_i < data_avui:
                            segons_fins_avui += segons_locals
                else:
                    #  Si el dia no és treballat, però està dintre periode...
                    if dia_obj['tipus'] not in ('F', 'FL')\
                       and weekday < 5\
                       and not dia_obj.get('css', ''):
                        dia_obj['css'] = 'no-laboral'

                if  data_quitansa and data_i > data_quitansa:
                    if per_seq.sequencia.abrev_torn == 'N':
                        dies_tipus_dia[idx] = 'N'
                    else:
                        dies_tipus_dia[idx] = 'D'


                first_pass = False

    if consolidat:
        segons_conveni = sum(dies_conveni) - (nocturns * RATI_NOCTURN)

    # DESPRÉS ELS MOVIMENTS
    # Quan un moviment es COMPUTABLE imputa les hores
    # de la jornada assignada
    segons_mov_dia = [0] * 366
    for moviment in moviments:
        idx = moviment.dia.toordinal() - dia_1_de_gener
        mes = moviment.dia.month - 1
        dia = moviment.dia.day - 1
        dia_obj = cal_obj[mes][1][dia]

        segons_calcul = segons_dia = dia_obj.get('segons_dia', 0)
        nocturns_calcul = dia_obj.get('nocturns', 0)

        segons_mov = moviment.get_segons()
        segons_nit = moviment.get_segons_nit()

        if segons_mov:
            segons_calcul = segons_mov

        #  ES COMPUTABLE.
        if moviment.moviment.es_computable:
            if moviment.moviment.es_jornada:
                if not segons_mov_dia[idx]:
                    segons_totals -= segons_dia

                segons_totals += segons_mov
                segons_mov_dia[idx] += moviment.get_segons()

                if consolidat and moviment.dia > data_quitansa:
                    segons_conveni += nocturns_calcul * RATI_NOCTURN
                    segons_conveni -= segons_nit * RATI_NOCTURN
                if moviment.dia < data_avui:
                    segons_fins_avui -= segons_dia
                    segons_fins_avui += segons_mov
                    # TODO: mirar gethoresmix de utils
#             elif moviment.moviment.imputa_hores:
#                 # El moviment s'ha de considerar jornada efectiva de manera que 
#                 # hem de sumar les hores que es passen de la jornada que té la persona aquell dia
#                 dia_hinici = dia_obj.get('hora_inici')
#                 dia_hfi = dia_obj.get('hora_fi')
#                 mov_hinici = moviment.moviment.hora_inici
#                 mov_hfi = moviment.moviment.hora_fi
#                 if mov_hinici < dia_hinici:
#                     if mov_hfi <= dia_hinici:
#                         # El moviment està totalment fora per tant ha de sumar
#                     elif mov_hfi <= dia_hfi:
#                         # El moviment acaba dins de jornada normal
#                     elif mov_hfi > dia_hfi:
#                         # El moviment es menja tota la jornada normal
#                 else:
#                     if mov_hinici >= dia_hinici:
#                         if mov_hinici => dia_hfi:
#                             # El moviment esta totalment fora per tant ha de sumar
#                         elif mov_hinici < dia_hfi:
#                             # El moviment comença dins la jornada normal

        # No es computable, haig de restar les hores
        # si tenia nocturnes
        else:
            segons_totals -= segons_calcul
            if consolidat and moviment.dia > data_quitansa:
                segons_conveni += nocturns_calcul * RATI_NOCTURN
            if moviment.dia < data_avui:
                segons_fins_avui -= segons_calcul

        #  ES_EXCEDENCIA: Ha de restar les hores totals de conveni
        if moviment.moviment.es_excedencia:
            if consolidat and moviment.dia > data_quitansa:
                segons_conveni -= segons_calcul
                dies_tipus_dia[idx] = ''
                # dies_conveni[moviment.dia.toordinal() - dia_1_de_gener] = 0

        # FIX
        if consolidat and moviment.dia > data_quitansa:
            if moviment.moviment.codi == 'LL':
                consolidat['lleure_totals'] += 1
                if moviment.dia < data_avui:
                    consolidat['lleure_fets'] += 1
            elif moviment.moviment.codi == 'V':
                consolidat['vacas_totals'] += 1
                if moviment.dia < data_avui:
                    consolidat['vacas_fetes'] += 1

        dia_obj['tooltip'] = dia_obj.get('dia_tooltip',
                                         moviment.dia.strftime('%d/%m/%Y'))

        if dia_obj['tipus'] not in ('T', 'FL', 'F', 'W', ''):  # DIA DOBLE
            if 'css' in dia_obj:
                dia_obj['css'] = 'dia-doble'
            dia_obj['tipus'] = '+1'
            dia_obj['style'] = ''
            dia_obj['tooltip'] += u'<br><strong>%s</strong>' % moviment.moviment

        else:  # DIA SIMPLE
            dia_obj['tipus'] = moviment.moviment.codi
            dia_obj['style'] = u'color:%s;background-color:%s' %\
                               (moviment.moviment.color_text,
                                moviment.moviment.color)
            dia_obj['tooltip'] += u'<br><strong>%s</strong>'\
                                  % moviment.moviment
        if moviment.comentari:
            dia_obj['tooltip'] += u'<br>'\
                                  + u'<em>' + moviment.comentari + u'</em>'
        if moviment.get_segons():
            dia_obj['tooltip'] += u'<br>'\
                                  + moviment.get_franja_horaria_format() + ' | '\
                                  + get_hores_en_format(moviment.get_segons(), format='hhmm')

        segons_dia = dia_obj.get('segons_dia', 0)
        if moviment.moviment.codi == 'TE':
            dia_obj['hora_inici'] = moviment.hora_inici
            dia_obj['hora_fi'] = moviment.hora_fi

    # Sumem al consolidat els moviments que afecten a aquest any
    # pero estan imputats a l'any vinent.
    # En el futur, això vindrà marcat per un flag i serà un calcul més generic per
    # tenir en compte la resta de flags.
    if consolidat:
        from models import MovimentTreballador, Moviment, CalendariTreballador
        mts = MovimentTreballador.objects.filter(user=treballador)
        mts = mts.filter(dia__range=(datetime.date(datetime(anyo + 1, 1, 1)),
                                   datetime.date(datetime(anyo + 1, 12, 31))))
        mts = mts.filter(moviment=Moviment.objects.get(codi='VP'))
        consolidat['vacas_totals'] += mts.count()
        segons_nit = 0
        for mt in mts:
            ct = CalendariTreballador(treballador, mt.dia, mt.dia)
            st = ct.get_segons_torn(mt.dia, mt.dia)
            segons_totals -= st[0] + st[1]
            segons_nit = st[1]
            if segons_nit != 0:
                segons_conveni -= segons_nit * RATI_NOCTURN

    # Sumem al consolidat els moviments que afecten a aquest any
    # pero estan imputats a l'any vinent.
    # En el futur, això vindrà marcat per un flag i serà un calcul més generic per
    # tenir en compte la resta de flags.
    if consolidat:
        from models import MovimentTreballador, Moviment, CalendariTreballador
        mts = MovimentTreballador.objects.filter(user=treballador)
        mts = mts.filter(dia__range=(datetime.date(datetime(anyo + 1, 1, 1)),
                                   datetime.date(datetime(anyo + 1, 12, 31))))
        mts = mts.filter(moviment=Moviment.objects.get(codi='VP'))
        consolidat['vacas_totals'] += mts.count()
        segons_nit = 0
        for mt in mts:
            ct = CalendariTreballador(treballador, mt.dia, mt.dia)
            st = ct.get_segons_torn(mt.dia, mt.dia)
            segons_totals -= st[0] + st[1]
            segons_nit = st[1]
            if segons_nit != 0:
                segons_conveni -= segons_nit * RATI_NOCTURN

    # Sumem al consolidat els moviments que afecten a aquest any
    # pero estan imputats a l'any vinent.
    # En el futur, això vindrà marcat per un flag i serà un calcul més generic per
    # tenir en compte la resta de flags.
    if consolidat:
        from models import MovimentTreballador, Moviment, CalendariTreballador
        mts = MovimentTreballador.objects.filter(user=treballador)
        mts = mts.filter(dia__range=(datetime.date(datetime(anyo + 1, 1, 1)),
                                   datetime.date(datetime(anyo + 1, 12, 31))))
        mts = mts.filter(moviment=Moviment.objects.get(codi='VP'))
        consolidat['vacas_totals'] += mts.count()
        segons_nit = 0
        for mt in mts:
            ct = CalendariTreballador(treballador, mt.dia, mt.dia)
            st = ct.get_segons_torn(mt.dia, mt.dia)
            segons_totals -= st[0] + st[1]
            segons_nit = st[1]
            if segons_nit != 0:
                segons_conveni -= segons_nit * RATI_NOCTURN





    for extra in extres:
        mes = extra.dia.month - 1
        dia = extra.dia.day - 1
        dia_obj = cal_obj[mes][1][dia]
        dia_obj['css'] = dia_obj.get('css', 'no-laboral')
        if dia_obj['css'] == 'no-laboral':
            dia_obj['css'] = 'extra'
            dia_obj['tipus'] = 'X'

        dia_obj['extra'] = True
        dia_obj['hora_inici_extra'] = extra.hora_inici
        dia_obj['hora_fi_extra'] = extra.hora_fi
        dia_obj['tooltip'] = dia_obj.get('tooltip', '')
        dia_obj['tooltip'] += u'<div class="hora-extra-tooltip">%s</div>' %\
                              extra.get_extra_tooltip()


    if consolidat:
        from apps.idirh.models import CalendariTreballador
        # TODO:
        #  - Cal reflexar els moviments que tinguin el flag any passat
        inici = datetime(anyo, 1, 1, 0, 0).date()
        fi = datetime(anyo, 12, 31, 23, 59).date()
        ct = CalendariTreballador(treballador, inici, fi, congelat)
        segons_conveni = ct.get_segons_conveni(data_quitansa, fi)
        segons_efectius_dia, segons_efectius_nit = ct.get_segons_efectius(
                                data_quitansa, fi)
        segons_totals = segons_efectius_dia + segons_efectius_nit
        segons_fins_avui_dia, segons_fins_avui_nit = ct.get_segons_efectius(
                                data_quitansa, data_avui - timedelta(days=1))
        segons_fins_avui = segons_fins_avui_dia + segons_fins_avui_nit


        consolidat['data_quitansa'] = data_quitansa
        consolidat['hores_treballades'] = segons_totals / 60 / 60
        consolidat['hores_treballades_avui'] = segons_fins_avui / 60 / 60
        consolidat['hores_totals'] = segons_conveni / 60 / 60

        dia_index = datetime.toordinal(data_avui) - dia_1_de_gener
        dies_treballats = len([dia for dia in dies_conveni[:dia_index] if dia != 0])

        dies_meritats = len([dia for dia in dies_conveni if dia != 0])

        dies_meritats_dia = len([dia for dia in dies_tipus_dia if dia == 'D'])
        dies_meritats_nit = len([dia for dia in dies_tipus_dia if dia == 'N'])

        hores_promig = ratio * dies_treballats / 60 / 60

        if hores_promig > (consolidat['hores_treballades_avui'] + 8):
            consolidat['class_hores'] = 'menys-hores-treballades'

        if hores_promig < (consolidat['hores_treballades_avui'] - 8):
            consolidat['class_hores'] = 'mes-hores-treballades'

        dies_vacances = treballador.get_profile().jornada_anual.dies_vacances
        dies_vacances_nit = treballador.get_profile().jornada_anual.dies_vacances_nit

        consolidat['vacas_meritades'] = dies_meritats_dia * dies_vacances / (cal_data['dies_any'] * 1.0)
        consolidat['vacas_meritades'] += dies_meritats_nit * dies_vacances_nit / (cal_data['dies_any'] * 1.0)
        consolidat['vacas_meritades'] = int(math.ceil(consolidat['vacas_meritades']))
        
        dies_lleure = treballador.get_profile().jornada_anual.dies_lleure
        dies_lleure_nit = treballador.get_profile().jornada_anual.dies_lleure_nit


        consolidat['lleure_meritats'] = dies_meritats_dia * dies_lleure / (cal_data['dies_any'] * 1.0)
        consolidat['lleure_meritats'] += dies_meritats_nit * dies_lleure_nit / (cal_data['dies_any'] * 1.0)
        consolidat['lleure_meritats'] = int(math.ceil(consolidat['lleure_meritats']))
        
        # Aquests prints son molt útils per debug
        # print 'Meritats: Dia', dies_meritats_dia, 'Nit', dies_meritats_nit
        # print math.ceil(dies_meritats_dia * dies_vacances / cal_data['dies_any']),
        # print math.ceil(dies_meritats_nit * dies_vacances_nit / cal_data['dies_any']),
        # print math.ceil(dies_meritats * dies_vacances / cal_data['dies_any']), dies_meritats

    return


def get_hores_mix(hora_inici, hora_fi, franja_inici, franja_fi):
    debug = ret_val = 0

    dia = 1
    if hora_fi <= hora_inici:
        dia += 1
    hora_inici = datetime(1970, 1, 1, hora_inici.hour, hora_inici.minute)
    hora_fi = datetime(1970, 1, dia, hora_fi.hour, hora_fi.minute)

    dia = 1
    if franja_fi <= franja_inici:
        dia += 1

    franja_inici = datetime(1970, 1, 1, franja_inici.hour, franja_inici.minute)
    franja_fi = datetime(1970, 1, dia, franja_fi.hour, franja_fi.minute)

    if hora_inici <= franja_inici and hora_fi >= franja_fi:
        if debug:
            print '1->',
        ret_val = (timedelta(hours=hora_fi.hour, minutes=hora_fi.minute) -
                   timedelta(hours=hora_inici.hour, minutes=hora_inici.minute))

        ret_val = franja_fi - franja_inici

    elif hora_fi > franja_inici and hora_inici < franja_inici:
        if debug:
            print '2->',
        ret_val = (timedelta(hours=hora_fi.hour, minutes=hora_fi.minute) -
                   timedelta(hours=franja_inici.hour, minutes=franja_inici.minute))

    elif hora_inici >= franja_inici and hora_fi <= franja_fi:
        if debug:
            print '3->',
        ret_val = (timedelta(hours=hora_fi.hour, minutes=hora_fi.minute) -
                   timedelta(hours=hora_inici.hour, minutes=hora_inici.minute))

    elif hora_inici <= franja_fi and hora_inici >= franja_inici and hora_fi >= franja_fi:
        if debug:
            print '4->',
        ret_val = (timedelta(hours=franja_fi.hour, minutes=franja_fi.minute) -
                   timedelta(hours=hora_inici.hour, minutes=hora_inici.minute))

    elif hora_inici >= franja_inici and hora_fi >= franja_fi:
        if debug:
            print '5->',
        ret_val = (timedelta(hours=franja_fi.hour, minutes=franja_fi.minute) -
                   timedelta(hours=hora_inici.hour, minutes=hora_inici.minute))

    if debug and type(ret_val) is not int:
        print ret_val.seconds / 60 / 60, ret_val

    if ret_val:
        return ret_val.seconds

    return ret_val


def get_segons_nit(self):
    if (self.hora_inici and self.hora_fi):
        debug = False  # Yes, I know...
        ret_val = 0
        dia = 1

        # La franja nocturna esta definida aquí a manija
        # delta = timedelta(0, inici_fals * 60 * 60)

        # if self.hora_inici.hour < inici_fals:
        #     dia += 1
        inici = datetime(1970, 1, dia,
                         self.hora_inici.hour,
                         self.hora_inici.minute)

        
        if self.hora_fi.hour < self.hora_inici.hour and self.hora_inici.hour != 0:
            dia += 1

        final = datetime(1970, 1, dia,
                         self.hora_fi.hour,
                         self.hora_inici.minute)

        # if self.hora_fi.hour < self.hora_inici.hour:
        #     print 'Its zerozero', inici, final


        # Inici / fi franja nocturma
        nit_inici = datetime(1970, 1, 1, 22, 0)
        nit_final = datetime(1970, 1, 2, 06, 0)

        if inici <= nit_inici and final >= nit_final:
            if debug:
                print '1->',
            ret_val = (timedelta(hours=final.hour, minutes=final.minute) -
                       timedelta(hours=inici.hour, minutes=inici.minute))

            ret_val = nit_final - nit_inici

        elif final > nit_inici and inici < nit_inici:
            if debug:
                print '2->',
            ret_val = (timedelta(hours=final.hour, minutes=final.minute) -
                       timedelta(hours=nit_inici.hour, minutes=nit_inici.minute))

        elif inici >= nit_inici and final <= nit_final:
            if debug:
                print '3->',
            ret_val = (timedelta(hours=final.hour, minutes=final.minute) -
                       timedelta(hours=inici.hour, minutes=inici.minute))

        elif inici <= nit_final and inici >= nit_inici and final >= nit_final:
            if debug:
                print '4->',
            ret_val = (timedelta(hours=nit_final.hour, minutes=nit_final.minute) -
                       timedelta(hours=inici.hour, minutes=inici.minute))

        elif inici >= nit_inici and final >= nit_final:
            if debug:
                print '5->',
            ret_val = (timedelta(hours=nit_final.hour, minutes=nit_final.minute) -
                       timedelta(hours=inici.hour, minutes=inici.minute))

        # print inici, final, nit_inici, nit_final, ret_val
        if debug and type(ret_val) is not int:
            print inici, final, ret_val.seconds / 60 / 60, ret_val

        if ret_val:
            return ret_val.seconds

        return ret_val
    return 0
