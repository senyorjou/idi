# coding: utf-8
import copy
from datetime import datetime, date
import calendar
from django_xlwt import Workbook
import xlrd
import re

from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.db.models import Count
from django.contrib.auth.decorators import login_required
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages


from apps.settings_develop_marc import MEDIA_ROOT


from meta.models import User, MetaUnitatIdi, AuthUserProfile

from idirh.models import Moviment, Extra, PeriodesExtra, PeriodeTreballador,\
                         MovimentTreballador, ExtraTreballador, CalendariLaboral,\
                         ExtraTreballadorImpMensual

from idirh.forms import RangDatesForm, ReportExtraForm

from idirh.utils import omplir_calendari, daterange, get_hores_mix,\
                        get_treballadors_actius

# Nadal i Cap d'any
DATES_ESPECIALS = [(25, 12), (31, 12)]

@login_required
def moviments_treballador(request, unitat_id=None, output=None):
    table = []
    llegenda = []
    lleg = {}

    data_inici = '01-' + datetime.now().strftime('%m-%Y')
    darrer_dia = calendar.monthrange(datetime.now().year, datetime.now().month)[1]

    data_fi = '%d-' % darrer_dia + datetime.now().strftime('%m-%Y')

    form = RangDatesForm(user=request.user,
                         initial={'data_inici': data_inici,
                                  'data_fi': data_fi})
    output = None

    if request.method == 'POST':
        form = RangDatesForm(request.POST, user=request.user)

        if form.is_valid():
            usuaris = get_treballadors_actius()

            output = form.cleaned_data['output']
            try:
                data_inici = datetime.strptime(form.cleaned_data['data_inici'], '%d-%m-%Y')
                data_fi = datetime.strptime(form.cleaned_data['data_fi'], '%d-%m-%Y')
            except:
                pass

            if form.cleaned_data['unitat'] is not None:
                usuaris = usuaris.filter(authuserprofile__unitat_idi=form.cleaned_data['unitat'])

            # Inicialitzem a zero el diccionari amb els moviments de cada treballador
            # table_row = { moviment_id: 0, moviment_id: 0, moviment_id: ...}
            table_row = {}
            table_row[0] = ""
            table_data = {}
            table_data[0] = {0: ""}

            for moviment in Moviment.objects.all():
                table_data[0][moviment.id] = moviment.codi
                table_row[moviment.id] = 0
                llegenda.append((moviment.codi, moviment.nom))
                lleg[moviment.id] = moviment.nom

            for usuari in usuaris:
                mts = usuari.movimenttreballador_set.filter(
                                                    dia__range=(data_inici, data_fi)
                                                    ).values('moviment'
                                                    ).annotate(dcount=Count('moviment'))

                table_data[usuari.id] = copy.copy(table_row)
                table_data[usuari.id][0] = (usuari.id, usuari.get_full_name())
                if len(mts) != 0:
                    for mt in mts:
                        table_data[usuari.id][mt['moviment']] = (mt['dcount'],
                                                                 lleg[mt['moviment']])

            for key, value in table_data.iteritems():
                table.append(value)

        valors = [x for x in table[0] if x != 0]
        valides = [0]

        for column in valors:
            for row in table:
                if row[0]:
                    if row[column]:
                        if not column in valides:
                            valides.append(column)

        for row in table:
            for key in valors:
                if key not in valides:
                    del row[key]

    if table and output:
        wb = Workbook()
        ws = wb.add_sheet('Moviments')
        ws.write(0, 0, 'Llistat de Moviments per %s' % form.cleaned_data['unitat'])

        for i, row in enumerate(table):
            if i == 0:
                for key, item in row.items():
                    ws.write(2, key, item)
            else:
                for key, item in row.items():
                    if key == 0:
                        ws.write(2 + i, key, item[1])
                    else:
                        ws.write(2 + i, key, item)

        ws.write(5 + i, 0, 'Llegenda')
        for j, row in enumerate(llegenda):
            ws.write(6 + i + j, 0, row[0])
            ws.write(6 + i + j, 1, row[1])

        fname = 'Moviments_per_unitat_%s.xls' % form.cleaned_data['unitat']
        response = HttpResponse(mimetype="application/vnd.ms-excel")
        response['Content-Disposition'] = 'attachment; filename=%s' % fname

        wb.save(response)
        return response

    context = {'report_title': "Moviments per treballador",
               'form': form,
               'table': table,
               'llegenda': llegenda
               }

    return render_to_response('planilles/report_moviments.html', context,
                              context_instance=RequestContext(request))


@login_required
def activitat_extra(request, unitat_id=None, periode_id=0):
    periodes_exp = periodes_val = codis = users = movs_extra = users_dict = []
    load_array = False
    periode = None

    if unitat_id is not None:
        try:
            unitat = MetaUnitatIdi.objects.get(id=unitat_id)
        except:
            return redirect('report_activitat_extra')

        calendari = CalendariLaboral.objects.get(id=unitat.calendari_laboral.id)
        data_inici = '01-01-' + datetime.now().strftime('%Y')

        darrer_dia = calendar.monthrange(datetime.now().year, datetime.now().month)[1]
        data_fi = '%d-' % darrer_dia + datetime.now().strftime('%m-%Y')

        periodes_val = PeriodesExtra.objects.filter(unitat=unitat, exportat=False, validat=True)\
                                            .order_by('-data_fi')

        periodes_exp = PeriodesExtra.objects.filter(unitat=unitat, exportat=True)\
                                            .order_by('-data_inici')

        # if file is None:
        treballadors = User.objects.filter(authuserprofile__unitat_idi=unitat,
                                           authuserprofile__es_actiu=True)
        # else:
        #     treballadors = get_treballadors_from_excel()
        # Arrays
        movs_extra = Extra.objects.all().order_by('ordre_excel')
        movs_implicits = Extra.objects.filter(tipus='I')
        arr_extra = dict((key.codi, 0) for key in movs_extra)
        codis = [extra.codi for extra in movs_extra]
        users_dict = {}
        users = []
        file = accio_sortida = None
        permet_validacio = True

        if int(periode_id):
            periode = PeriodesExtra.objects.get(id=periode_id)
            data_inici = periode.data_inici.strftime('%d-%m-%Y')
            data_fi = periode.data_fi.strftime('%d-%m-%Y')

            load_array = True
        # else:
        #     periode = None

        form = ReportExtraForm(request.user,
                               initial={'data_fi': data_fi,
                                        'unitat': unitat,
                                        'periode': periode_id})
        data_inici = datetime.strptime(data_inici, '%d-%m-%Y')
        data_fi = datetime.strptime(data_fi, '%d-%m-%Y')

        try:
            periode_anterior = PeriodesExtra.objects\
                                            .filter(data_fi__lt=data_fi)\
                                            .order_by('-data_fi')[0]

            data_fi_periode_anterior = periode_anterior.data_fi
        except:
            data_fi_periode_anterior = datetime(1970, 1, 1).date()

    else:
        form = ReportExtraForm(request.user)

    # Llistar MOVIMENTS NO VALIDATS
    if request.method == 'POST':
        form = ReportExtraForm(request.user, request.POST, request.FILES)
        if form.is_valid():
            load_array = True
            unitat = form.cleaned_data['unitat']
            accio_sortida = form.cleaned_data['output']
            file = form.cleaned_data['output_file']

            try:
                data_fi = datetime.strptime(form.cleaned_data['data_fi'], '%d-%m-%Y')
            except:
                pass

            # Mirem si es pot validar un periode, ha de ser posterior a la
            # darrera validació
            if periodes_val:
                permet_validacio = date(data_fi.year, data_fi.month, data_fi.day) > periodes_val[0].data_fi or True

            data_accio = datetime.now()
            #  Desactivo això fins no resoldre el tema de reescriure un fitxer
            #  obert durant el mateix request
            if accio_sortida == 'nou-xls-entrada':
                treballadors, error, vals = get_treballadors_from_excel(request, file)
                if error:
                    msg = 'S\'ha cancelat la exportacio.'
                    msg += 'Treballador amb <strong>DNI %s</strong>, a la <strong>fila %s</strong> del full de calcul no es present a la Base de Dades' % (vals[0], vals[1] + 1)
                    messages.error(request, _(msg))
                    context = {'unitat': unitat_id,
                               'form': form,
                               'periodes_val': periodes_val,
                               'periodes_exp': periodes_exp,
                               'periode': periode,
                               'movs_extra': movs_extra,
                               'users_dict': users_dict,
                               'users': users,
                               'codis': codis}

                    return render_to_response('planilles/report_extraordinaria.html', context,
                                              context_instance=RequestContext(request))
    if load_array:
        for treballador in treballadors:
            # Generem UN CALENDARI per cada treballador
            #
            # Per generar els implicits hem de crear el calendari
            # Ho omplim de la mateixa manera que a planilla
            data_inici_sql = data_inici.strftime("%Y-%m-%d")
            data_fi_sql = data_fi.strftime("%Y-%m-%d")

            mysql_query = """
                SELECT * FROM plan_periode_treballador WHERE
                user_id = %(user)s
                AND (congelat = 1 OR congelat = 0)
                AND (
                    ((data_inici < '%(data_fi)s') AND (data_fi > '%(data_inici)s')))
                """ % dict(user=treballador.id, data_inici=data_inici_sql, data_fi=data_fi_sql)

            periodes = PeriodeTreballador.objects.raw(mysql_query)
            # De moment treiem el fltre de congelació
            moviments = MovimentTreballador.objects.filter(user=treballador)\
                                                   .filter(dia__gte=data_inici)\
                                                   .filter(dia__lte=data_fi)\
                                                   # .filter(congelat=True)

            extres = ExtraTreballador.objects.filter(user=treballador)\
                                             .filter(dia__year=data_inici.year)\
                                             .filter(extra__tipus='E')\
                                             .order_by('dia')

            cal_obj = calendari.get_calendari(int(data_inici.year))
            cal_data = calendari.get_any_resum(data_inici.year)
            omplir_calendari(treballador, periodes, moviments, extres, cal_obj, cal_data, [])

            row = []
            user_extra = arr_extra.copy()
            user_extra_virtual = arr_extra.copy()
            # Loop del periode de dates,
            # a cada dia es fa un loop als moviments implicits
            # if dia.weekday in dies_de_moviment --> get segons dintre moviment
            extres_base = treballador.extratreballador_set
            imp_base = treballador.extratreballadorimpmensual_set

            implicits = []
            #  Es compleix la condició si clicko a un periode
            if periode:
                # Si és exportat
                if periode.exportat:
                    extres = extres_base\
                             .filter(data_exportacio=periode.updated_at)

                    extres_eliminats = []
                    implicits = []

                else:  # periode validat
                    extres = extres_base\
                             .filter(data_validacio__lte=data_fi)\
                             .filter(data_validacio__gt=data_fi_periode_anterior)

                    extres_eliminats = extres_base\
                                       .filter(data_eliminacio__lte=data_fi)\
                                       .filter(data_eliminacio__gt=data_fi_periode_anterior)

                    implicits = imp_base\
                                .filter(data_mes=periode.data_fi)\
                                .filter(data_eliminacio__isnull=True)
                    for imp in implicits:
                        user_extra[imp.extra.codi] = imp.qty
            # Calcul dels moviments NO VALIDATS ==> Pendents
            else:
                extres = extres_base\
                         .filter(dia__range=(data_inici, data_fi))\
                         .filter(data_validacio__isnull=True)

                extres_eliminats = extres_base\
                                   .filter(dia__range=(data_inici, data_fi))\
                                   .filter(data_eliminacio__lte=data_fi)\
                                   .filter(data_validacio__isnull=True)

                implicits = carregar_implicits_treballador_periode(treballador,
                                                                   data_fi,
                                                                   cal_obj,
                                                                   movs_implicits)
                # informes = get_infromes_mateu(treballador, data_fi)
                # user_extra[CODI_DE_EXTRA_INFORMES] = informes

                for imp in implicits:
                    user_extra[imp] = implicits[imp]

            for extra in extres:
                # Aqui hi ha un procés la mar de cutre
                # Si el concepte extra son hores es sumen
                # si son desplaçaments/informes es resten
                # això ens permet després un tractament diferent
                if extra.extra.comput == 'H':
                    user_extra[extra.extra.codi] = user_extra.get(extra.extra.codi, 0) + extra.get_hores_totals()
                else:
                    user_extra[extra.extra.codi] = user_extra.get(extra.extra.codi, 0) + extra.despla


            for extra in extres_eliminats:
                if extra.extra.comput == 'H':
                    user_extra[extra.extra.codi] = user_extra.get(extra.extra.codi, 0) - extra.get_hores_totals()
                else:
                    user_extra[extra.extra.codi] = user_extra.get(extra.extra.codi, 0) - extra.despla


            users_dict[treballador.id] = user_extra
            row.append({'cognom': treballador.last_name,
                        'nom': treballador.first_name,
                        'id': treballador.id,
                        'username': treballador.username})

            for extra in movs_extra:
                if not user_extra[extra.codi]:
                    row.append(0)
                else:
                    if extra.comput == 'H':
                        row.append(('%.2f' % (float(user_extra[extra.codi]) / 60 / 60)))
                    else:
                        row.append(user_extra[extra.codi])
            users.append(row)

            # Si es demana validació, ho fem per cada extra__set
            # I es graven els implocits a la taula de control mensual
            if accio_sortida == 'validar' and permet_validacio:
                extres.update(data_validacio=data_fi)

                for mov in movs_implicits:
                    qty = user_extra.get(mov.codi, 0)
                    if qty:
                        row = ExtraTreballadorImpMensual(
                            user=treballador,
                            editor=request.user,
                            data_mes=data_fi,
                            mes=data_fi.month,
                            qty=qty,
                            extra=mov)
                        row.save()

                # GRAVAR INFORMES VALIDATS/TANCATS A LA TAULA DE EXTRA EXPLICITS

            if accio_sortida == 'tancar':
                extres.update(data_exportacio=data_accio)

        # I crear el registre de "validat en data..."
        # Nomes es permet validar si la data es correcte
        if accio_sortida == 'validar' and permet_validacio:
            pe = PeriodesExtra(created_at=data_accio,  # Es força el created_at
                               user_validacio=request.user,
                               unitat=unitat,
                               data_inici=data_inici,
                               data_fi=data_fi,
                               validat=True)
            pe.save()
            # el created_at no es fiable al insert, dona errors forçant-l'ho
            # és cutre gravar-l'ho dues vegades però no coincidien els datetimes
            pe.created_at = data_accio
            pe.save()

        if accio_sortida == 'tancar':
            # Això s'ha de millorar
            periode.exportat = True
            periode.save()

    if users_dict and accio_sortida == 'xls':
        wb = Workbook()
        ws = wb.add_sheet('Extraordinaria')
        ws.write(0, 0, 'Llistat d\'activitat extraordinaria per %s' % unitat)
        ws.write(1, 0,
                 'Periode de %s a %s'
                 % (data_inici.strftime('%d-%m-%Y'), data_fi.strftime('%d-%m-%Y')))

        for i, extra in enumerate(movs_extra):
            ws.write(3, i+2, extra.nom)

        for i, row in enumerate(users):
                for j, item in enumerate(row):
                    if j > 0:
                        if item:
                            ws.write(4 + i, j+1, item)
                        else:    
                            ws.write(4 + i, j+1, '')
                    else:
                        nom = item['nom'] + ' ' + item['cognom']
                        ws.write(4 + i, j+1, nom)

        # for i, row in enumerate(users_dict):
        #     for j, item in enumerate(users_dict[row]):
        #         ws.write(3 + i, j, users_dict[row][item])

        fname = 'Moviments_per_unitat_%s.xls' % unitat
        response = HttpResponse(mimetype="application/vnd.ms-excel")
        response['Content-Disposition'] = 'attachment; filename=%s' % fname

        wb.save(response)
        return response

    if users_dict and accio_sortida == 'nou-xls-entrada':
        wb = Workbook()
        ws = wb.add_sheet('Extraordinaria')
        ws.write(0, 0, 'Llistat d\'activitat extraordinaria')
        ws.write(1, 0,
                 'Periode de %s a %s'
                 % (data_inici.strftime('%d-%m-%Y'), data_fi.strftime('%d-%m-%Y')))

        for i, extra in enumerate(movs_extra):
            ws.write(3, i+2, extra.nom)

        for i, row in enumerate(users):
                for j, item in enumerate(row):
                    if j > 0:
                        ws.write(4 + i, j+1, item)
                    else:
                        nom = item['nom'] + ' ' + item['cognom']
                        ws.write(4 + i, j+1, nom)

        fname = 'Moviments__%s.xls' % unitat
        response = HttpResponse(mimetype="application/vnd.ms-excel")
        response['Content-Disposition'] = 'attachment; filename=%s' % fname

        wb.save(response)
        return response

    # DESHABILITAT DE MOMENT
    if users_dict and accio_sortida == 'xls-entrada':
        DNI = re.compile('(?P<dni>\d{8})(?P<lletra1>[A-Z])')
        CIF = re.compile('(?P<lletra1>[A-Z])(?P<dni>\d{7})(?P<lletra2>[0-9A-Z])')

        wb = Workbook()

        path = default_storage.save(file.name, ContentFile(file.read()))
        book = xlrd.open_workbook('%s/%s' % (MEDIA_ROOT, path))

        rs = book.sheet_by_index(0)
        ws = wb.add_sheet(rs.name)

        for row in range(rs.nrows):
            # values = []
            for col in range(rs.ncols):
                row_val = unicode(rs.cell(row, col).value)
                # A la columna 4 tinc DNI
                if col == 3:
                    if (DNI.match(row_val)) or (CIF.match(row_val)):
                        try:
                            treballador = User.objects.get(username=row_val[:8])
                        except User.DoesNotExist:
                            msg = 'S\'ha cancelat la exportacio.'
                            msg += 'Treballador amb <strong>DNI %s</strong>, a la <strong>fila %s</strong> del full de calcul no es present a la Base de Dades' % (vals[0], vals[1] + 1)
                            messages.error(request, _(msg))
                            context = {'unitat': unitat_id,
                                       'form': form,
                                       'periodes_val': periodes_val,
                                       'periodes_exp': periodes_exp,
                                       'periode': periode,
                                       'movs_extra': movs_extra,
                                       'users_dict': users_dict,
                                       'users': users,
                                       'codis': codis}

                            return render_to_response('planilles/report_extraordinaria.html', context,
                                                      context_instance=RequestContext(request))

                        # extra_treballador = get_extra_treballador(treballador)
                        # moviments = ExtraTreballador.objects.filter(user=treballador)\
                        #                                     .fiter(dia_year=2031)\
                        #                                     .filter(dia_month=02)

                        # user_extra = arr_extra.copy()
                        # extres = treballador.extratreballador_set.filter(dia__range=(data_inici, data_fi))
                        # if extres:
                        #     for extra in extres:
                        #         user_extra[extra.extra.codi] = user_extra.get(extra.extra.codi, 0) + extra.get_hores_totals()

                        # DNI / CIF
                        ws.write(row, col, row_val)
                        # NOM, haig de saltar una columna més
                        col += 1
                        row_val = rs.cell(row, col).value
                        ws.write(row, col, row_val)
                        col += 1
                        while(col < rs.ncols - 1):
                            for extra in arr_extra:
                                # ws.write(row, col, user_extra[extra])
                                # TODO, TODO
                                try:
                                    ws.write(row, col, users_dict[treballador.id][extra])
                                except:
                                    ws.write(row, col, '')
                                col += 1
                        break

                # values.append(unicode(rs.cell(row, col).value))

                ws.write(row, col, row_val)

        fname = 'Moviments Extraordinaris.xls'
        response = HttpResponse(mimetype="application/vnd.ms-excel")
        response['Content-Disposition'] = 'attachment; filename=%s' % fname

        wb.save(response)
        return response

    context = {'unitat': unitat_id,
               'form': form,
               'periodes_val': periodes_val,
               'periodes_exp': periodes_exp,
               'periode': periode,
               'movs_extra': movs_extra,
               'users_dict': users_dict,
               'users': users,
               'codis': codis}

    return render_to_response('planilles/report_extraordinaria.html', context,
                              context_instance=RequestContext(request))


def calcula_mesos_anterios(treballador, movs_implicits, mes_final):
    obj_movs = {}

    for mesos in range(6):
        mes = mes_final - (mesos - 1)
        if mes == 0:
            mes = 12

        for mov in movs_implicits:
            row = ExtraTreballadorImpMensual.objects.filter(user=treballador)\
                                                    .filter(extra=mov)\
                                                    .filter(mes=mes)
            if row:
                obj_movs[mov.codi] = row[0].qty



    return obj_movs


def activitat_extra_historic(request, treb_id=None, treb_codi=None):

    treballador = User.objects.get(id=treb_id)
    profile = AuthUserProfile.objects.get(user=treb_id)
    movs_extra = Extra.objects.all().order_by('ordre_excel')
    movs_imp = Extra.objects.all().order_by('ordre_excel').filter(tipus='I')

    mes_final = datetime.now().month

    treb_mesos = {}

    for mesos in range(6):
        mes = mes_final - (mesos - 1)
        if mes == 0:
            mes = 12

        treb_mesos[mes] = {}
        for mov in movs_extra:
            row = ExtraTreballadorImpMensual.objects.filter(user=treballador)\
                                                    .filter(extra=mov)\
                                                    .filter(mes=mes)

            if row:
                treb_mesos[mes][mov.codi] = row[0].qty



    context = {'treballador': treballador,
               'profile': profile,
               'movs_extra': movs_extra,
               'treb_mesos': treb_mesos}

    return render_to_response('planilles/report_extraordinaria_historic.html',
                              context, context_instance=RequestContext(request))


def carregar_implicits_treballador_periode(treballador, data_fi, cal_obj,
                                           movs_implicits):
    """
    Els implicits es calculen desde inici d'any
    01/01/data_fi.year --> data_fi


        implicits_gravats   --> query a la taula per data_mes <= data fi
        implicits_calculats --> calcul de range(data_fi - 6 mesos, data fi)

        implicits_darrer_mes --> calcul de range(primer dia mes, darrer dia mes)

        implicits = implicts_darrer_mes + implicits_gravats - implicits_calculats
    """

    data_inici_anteriors = date(data_fi.year, 1, 1)
    data_fi_anteriors = data_fi.date()
    if data_fi.month > 1:
        data_fi_anteriors = date(data_fi.year,
                                 data_fi.month - 1,
                                 calendar.monthrange(data_fi.year, data_fi.month - 1)[1])

    implicits_gravats = treballador.extratreballadorimpmensual_set\
                        .filter(data_mes__lte=data_fi_anteriors)\
                        .filter(data_eliminacio__isnull=True)

    user_extra_virtual = {}

    for implicit in implicits_gravats:
        if user_extra_virtual.get(implicit.extra.codi, 0):
            user_extra_virtual[implicit.extra.codi] -= implicit.qty
        else:
            user_extra_virtual[implicit.extra.codi] = -implicit.qty

    add_day = False
    for dia in daterange(data_inici_anteriors, data_fi.date(), end=True):
        dia_obj = cal_obj[dia.month - 1][1][dia.day - 1]
        if dia_obj.get('hora_inici'):
            for mov in movs_implicits:
                nadal = False
                if (mov.es_dia_especial) and (dia.day, dia.month) in DATES_ESPECIALS:
                    print dia,  'Nadal o cap d\'any'
                    nadal = True
                    add_day = True

                elif (mov.festiu_laboral == 'F' and
                      dia_obj['tipus'] == 'TE' and
                      dia_obj['obj'].tipus_dia in ['F', 'FL']):

                    add_day = True

                elif (str(dia.weekday()) in mov.dies_numero):
                    add_day = True

                if add_day:
                    user_extra_virtual[mov.codi] = user_extra_virtual.get(mov.codi, 0) +\
                                                   get_hores_mix(dia_obj.get('hora_inici'),
                                                                 dia_obj.get('hora_fi'),
                                                                 mov.hora_inici,
                                                                 mov.hora_fi)
                    if nadal:
                        print mov.codi, get_hores_mix(dia_obj.get('hora_inici'),dia_obj.get('hora_fi'),mov.hora_inici,mov.hora_fi)

                    add_day = False

    return user_extra_virtual


def carregar_informes_treballador_periode(treballador, data_fi, cal_obj):
    """
    Els implicits es calculen desde inici d'any
    01/01/data_fi.year --> data_fi


        implicits_gravats   --> query a la taula per data_mes <= data fi
        implicits_calculats --> calcul de range(data_fi - 6 mesos, data fi)

        implicits_darrer_mes --> calcul de range(primer dia mes, darrer dia mes)

        implicits = implicts_darrer_mes + implicits_gravats - implicits_calculats
    """

    extra = 0

    # informes = get_infromes_mateu() --> Llista d'informes del periode amb data i hora
    informes = []

    for informe in informes:
        if __get_treballador_tipus_hora(treb_id, informe.dia, informe.hora) == 'Extra':
            extra += 1

    return extra


def exportar_activitat_extra(request):

    pass


def __generar_extra_periode(request):
    pass


def get_treballadors_from_excel(request, fitxer):

    treballadors = []
    error = True

    if not fitxer:
        return (treballadors, not error, [])

    DNI = re.compile('(?P<dni>\d{8})(?P<lletra1>[A-Z])')
    CIF = re.compile('(?P<lletra1>[A-Z])(?P<dni>\d{7})(?P<lletra2>[0-9A-Z])')

    path = default_storage.save(fitxer.name, ContentFile(fitxer.read()))
    book = xlrd.open_workbook('%s/%s' % (MEDIA_ROOT, path))

    rs = book.sheet_by_index(0)

    for row in range(rs.nrows):
        # values = []
        for col in range(rs.ncols):
            row_val = unicode(rs.cell(row, col).value)
            # A la columna 4 tinc DNI
            if col == 3:
                if (DNI.match(row_val)) or (CIF.match(row_val)):
                    try:
                        treballador = User.objects.get(username=row_val[:8])
                        treballadors.append(treballador)
                    except User.DoesNotExist:
                        return ([], error, (row_val, row))

    # El fitxer queda obert i no es pot tancar usant un metode de  la classe
    # el del no funciona, book = None tampoc
    #
    # del book

    return (treballadors, not error, [])


#@login_required
#def ratios_unitat(request):
#
#    form = RangDatesForm(request.user)
#    if request.method == 'POST':
#        form = RangDatesForm(request.user, request.POST)
#
#    context = {'report_title': "Ratios per unitat",
#               'form': form}
#    return render_to_response('planilles/report.html', context,
#                              context_instance=RequestContext(request))
