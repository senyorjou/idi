# coding: utf-8
#
"""
Created on 05/11/2012
@author: mjou
"""

from django.conf.urls.defaults import url, patterns
from django.views.generic.simple import redirect_to

urlpatterns = patterns('apps.idirh',
    # Index per defecte al calendari personal
    url(r'^$', redirect_to, {'url': '/idirh/rrhh/planilla/'}),
    #url(r'^calendari_personal/$', 'views_calendari.calendari_personal', name='calendari_personal'),
    #url(r'^calendari_personal/(?P<anyo>\d+)/$', 'views_calendari.calendari_personal', name='calendari_personal'),
    # calendaris
    url(r'^calendari/$', 'views_calendari.calendaris', name='calendaris'),
    url(r'^calendari/(?P<calendari_id>\d+)/$', 'views_calendari.calendari', name='calendari'),
    url(r'^calendari/(?P<calendari_id>\d+)/(?P<anyo>\d+)/$', 'views_calendari.calendari', name='calendari'),
    url(r'^calendari_new/$', 'views_calendari.calendari'),
    url(r'^calendari/(?P<calendari_id>\d+)/(?P<anyo>\d+)/delete/$', 'views_calendari.calendari_delete'),
    url(r'^calendari_display/(?P<calendari_id>\d+)/$', 'views_calendari.calendari_display'),
    # dies
    url(r'^calendari/(?P<calendari_id>\d+)/(?P<dia_id>\d+)/$', 'views_calendari.calendari_dia'),
    url(r'^calendari/(?P<calendari_id>\d+)/save/$', 'views_calendari.calendari_dia_save'),
    url(r'^calendari/(?P<calendari_id>\d+)/(?P<anyo>\d+)/save/$', 'views_calendari.calendari_dia_save'),
    url(r'^calendari/(?P<calendari_id>\d+)/(?P<dia_id>\d+)/(?P<anyo>\d+)/delete/$', 'views_calendari.calendari_dia_delete', name='calendari_delete_dia'),

    # unitats i grups
    url(r'^unitats/$', 'views_unitat.index', name='unitats'),
    url(r'^unitats/(?P<unitat_id>\d+)/$', 'views_unitat.unitat', name='unitat'),

    # CRUD de Grups
    url(r'^unitats/(?P<unitat_id>\d+)/grups/$', 'views_unitat.index_grups', name='grups'),
    # url(r'^unitats/(?P<unitat_id>\d+)/(?P<grup_id>\d+)/$', 'views_unitat.grup', name='grup_unitat'),
    # url(r'^unitats/(?P<unitat_id>\d+)/(?P<grup_id>\d+)/delete/$', 'views_unitat.grup_unitat_delete', name='grup_unitat_delete'),
    # url(r'^unitats/(?P<unitat_id>\d+)/(?P<grup_id>\d+)/(?P<lloc_id>\d+)/$', 'views_unitat.grup_unitat_lloc', name='grup_unitat_lloc'),
    # url(r'^unitats/(?P<unitat_id>\d+)/(?P<grup_id>\d+)/(?P<seq_id>\d+)/(?P<cat_id>\d+)/delete/$', 'views_unitat.grup_lloc_treball_delete', name='grup_lloc_treball_delete'),

    url(r'^unitats/(?P<unitat_id>\d+)/grup/(?P<grup_id>\d+)/$', 'views_unitat.unitat_grup', name='unitat_grup'),
    url(r'^unitats/(?P<unitat_id>\d+)/grup/(?P<grup_id>\d+)/delete/$', 'views_unitat.unitat_grup_delete', name='unitat_grup_delete'),

    # CRUD de Equips
    url(r'^unitats/(?P<unitat_id>\d+)/grup/(?P<grup_id>\d+)/equip/(?P<equip_id>\d+)/$', 'views_unitat.unitat_grup_equip', name='unitat_grup_equip'),
    url(r'^unitats/(?P<unitat_id>\d+)/grup/(?P<grup_id>\d+)/equip/(?P<equip_id>\d+)/delete/$', 'views_unitat.unitat_grup_equip_delete'),
    url(r'^unitats/(?P<unitat_id>\d+)/grup/(?P<grup_id>\d+)/equip/(?P<equip_id>\d+)/add_periode/$', 'views_unitat.unitat_grup_equip_add_periode', name='unitat_grup_equip_periode'),
    url(r'^unitats/(?P<unitat_id>\d+)/grup/(?P<grup_id>\d+)/equip/(?P<equip_id>\d+)/periode/(?P<periode_id>\d+)/delete/$', 'views_unitat.unitat_grup_equip_delete_periode', ),
    url(r'^unitats/(?P<unitat_id>\d+)/grup/(?P<grup_id>\d+)/equip/(?P<equip_id>\d+)/incidencia/(?P<incidencia_id>\d+)/delete/$', 'views_unitat.unitat_grup_equip_delete_incidencia', ),
    url(r'^unitats/equips/$', 'views_unitat.maquines_index', name='maquines'),
    # url(r'^grups/$', 'views_unitat.index_grups', name='grups'),
    # url(r'^grups/(?P<grup_id>\d+)/$', 'views_unitat.grup', name='grup'),
    # url(r'^grups/(?P<grup_id>\d+)/grup_lloc_treball/$', 'views_unitat.grup_lloc_treball'),
    # url(r'^grups/(?P<grup_id>\d+)/(?P<seq_id>\d+)/(?P<cat_id>\d+)/grup_lloc_treball_delete/$', 'views_unitat.grup_lloc_treball_delete'),

    # Categories i metacategories
    url(r'^grups_categoria/$', 'views_grups_categoria.index_grups_categories', name='grups_categoria'),
    url(r'^grups_categoria/(?P<grup_cat_id>\d+)/$', 'views_grups_categoria.grup_categories', name='grup_categoria'),

    url(r'^grups_categoria/(?P<grup_cat_id>\d+)/save/$', 'views_grups_categoria.grup_categories_cat_save'),
    url(r'^grups_categoria/(?P<grup_cat_id>\d+)/delete/$', 'views_grups_categoria.grup_categories_delete'),
    url(r'^grups_categoria/(?P<grup_cat_id>\d+)/(?P<cat_id>\d+)/delete/$', 'views_grups_categoria.grup_categories_cat_delete'),

    # Màquines
    # url(r'^maquines/$', 'views_unitat.maquines_index', name='maquines'),
    # url(r'^maquines/(?P<maquina_id>\d+)/$', 'views_unitat.maquines_maquina', name='maquina'),
    # url(r'^maquines/(?P<maquina_id>\d+)/delete/$', 'views_unitat.maquines_maquina_delete'),
    # url(r'^maquines/(?P<maquina_id>\d+)/periode/$', 'views_unitat.maquines_periode_maquina'),
    # url(r'^maquines/(?P<maquina_id>\d+)/periode/(?P<seq_id>\d+)/delete/$', 'views_unitat.maquines_periode_maquina_delete'),

    # Rotacions i sequencies
    url(r'^rotacions/$', 'views_planilles.rotacions_index', name='rotacions'),
    url(r'^rotacions/(?P<rot_id>\d+)/$', 'views_planilles.rotacions_rotacio', name='rotacio'),
    url(r'^rotacions/(?P<rot_id>\d+)/delete/$', 'views_planilles.rotacions_rotacio_delete'),
    url(r'^rotacions/(?P<rot_id>\d+)/rotaciosequencia/$', 'views_planilles.rotacions_rotaciosequencia'),
    url(r'^rotacions/(?P<rot_id>\d+)/(?P<rotseq_id>\d+)/delete/$', 'views_planilles.rotacions_rotaciosequencia_delete'),
    #
    url(r'^sequencies/$', 'views_planilles.sequencies_index', name='sequencies'),
    url(r'^sequencies_equip/$', 'views_planilles.sequencies_equip_index', name='sequencies_equip'),
    # Hardcoded el tipus de sequencia, Personal/Maquina
    url(r'^sequencies/(?P<tipus_seq>[MP])?/$', 'views_planilles.sequencies_sequencia', name='sequencia'),
    url(r'^sequencies/(?P<seq_id>\d+)?/$', 'views_planilles.sequencies_sequencia', name='sequencia'),
    # url(r'^sequencies_equip/(?P<seq_id>\d+)/$', 'views_planilles.sequencies_sequencia_equip_index', name='sequencies_equip'),

    url(r'^sequencies/(?P<seq_id>\d+)/delete/$', 'views_planilles.sequencies_sequencia_delete'),
    url(r'^sequencies/(?P<seq_id>\d+)/franja/$', 'views_planilles.sequencies_franja'),
    url(r'^sequencies/(?P<seq_id>\d+)/(?P<dia>\d+)/delete/$', 'views_planilles.sequencies_franja_delete'),

    # Plantilla
    # Indexos
    url(r'^rrhh/$', 'views_rrhh.index', name='rrhh'),
    url(r'^rrhh/unitat/(?P<unitat_id>\d+)/$', 'views_rrhh.index', name='rrhh_unitat'),

    # Detall
    url(r'^rrhh/(?P<treb_id>\d+)/(?P<treb_codi>\w+)/$', 'views_rrhh.index_treballador', name='treballador'),
    url(r'^rrhh/(?P<treb_id>\d+)/$', 'views_rrhh.index_treballador', name='treballador'),
    url(r'^rrhh/(?P<treb_id>\d+)/(?P<treb_codi>\w+)/(?P<anyo>\d+)/$', 'views_rrhh.index_treballador_anyo', name='treballador_any'),
    url(r'^rrhh/(?P<treb_id>\d+)/(?P<treb_codi>\w+)/(?P<anyo>\d+)/canvi_jornada_anual/$',
        'views_rrhh.canvi_jornada_anual'),
    url(r'^rrhh/(?P<treb_id>\d+)/(?P<treb_codi>\w+)/(?P<anyo>\d+)/(?P<snap_anyo>\d+)/(?P<snap_mes>\d+)/(?P<snap_dia>\d+)/(?P<snap_hora>\d+)/(?P<snap_min>\d+)/$',
        'views_rrhh.index_treballador_anyo', name='treballador_snapshot'),

    # delete periode / treballador
    url(r'^rrhh/(?P<treb_id>\d+)/(?P<treb_codi>\w+)/(?P<anyo>\d+)/(?P<periode_id>\d+)/deleteperiode/$',
        'views_rrhh.index_treballador_periode_delete'),
    # delete moviment / treballador
    url(r'^rrhh/(?P<treb_id>\d+)/(?P<treb_codi>\w+)/(?P<anyo>\d+)/(?P<moviment_id>\d+)/deletemoviment/$',
        'views_rrhh.index_treballador_moviment_delete'),
    # delete hora extra / treballador
    url(r'^rrhh/(?P<treb_id>\d+)/(?P<treb_codi>\w+)/(?P<anyo>\d+)/(?P<extra_id>\d+)/deletehoraextra/$',
        'views_rrhh.treballador_horaextra_delete'),

    # EDITAR FAPT
    url(r'^rrhh/(?P<treb_id>\d+)/(?P<treb_codi>\w+)/(?P<anyo>\d+)/(?P<periode_id>\d+)/$',
        'views_rrhh.treballador_periode_edit', name='treballador_periode_edit'),
    # Editar FAPT -->POST
    url(r'^rrhh/(?P<treb_id>\d+)/(?P<treb_codi>\w+)/(?P<anyo>\d+)/(?P<periode_id>\d+)/franja/$',
        'views_rrhh.treballador_periode_edit_franja'),

    url(r'^rrhh/(?P<treb_id>\d+)/(?P<treb_codi>\w+)/(?P<anyo>\d+)/(?P<periode_id>\d+)/(?P<dia>\d+)/delete/$',
        'views_rrhh.treballador_fapdelete'),


    url(r'^rrhh/rotacio_form/(?P<rot_id>\d+)/', 'views_rrhh.form_partial_rotacio'),

    url(r'^rrhh/(?P<treb_id>\d+)/(?P<treb_codi>\w+)/(?P<anyo>\d+)/congelar/$', 'views_rrhh.congelar_treballador_anyo', name='congelar_treballador_any'),

    # Jornades
    url(r'^rrhh/jornades/$', 'views_rrhh.index_jornades_anuals', name='jornades'),
    url(r'^rrhh/jornades/(?P<jornada_id>\d+)/$', 'views_rrhh.jornada_anual', name='jornada'),
    url(r'^rrhh/jornades/(?P<jornada_id>\d+)/delete/$', 'views_rrhh.jornada_anual_delete'),

    # Planilla
    #(r'^articles/(\d{4})/(\d{2})/(\d+)/$', 'news.views.article_detail'),

    url(r'^rrhh/planilla/$', 'views_rrhh.planilla', name='planilles'),
    url(r'^rrhh/planilla/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/$', 'views_rrhh.planilla', name='planilles_data'),
    url(r'^rrhh/planilla/(?P<year>\d+)/(?P<month>\d+)/(?P<day>\d+)/(?P<unitat_id>\d+)/$', 'views_rrhh.planilla', name='planilles_data_unitat'),
    url(r'^rrhh/planilla/(?P<unitat_id>\d+)/$', 'views_rrhh.planilla_unitat', name='planilla_unitat'),
    url(r'^rrhh/planilla/(?P<unitat_id>\d+)/(?P<grup_id>\d+)/$', 'views_rrhh.planilla', name='planilla_grup'),

    # Extra
    url(r'^rrhh/extra/$', 'views_rrhh.index_extra', name='hores_extra'),
    url(r'^rrhh/extra/(?P<extra_codi>\w+)/$', 'views_rrhh.extra_extra', name='hora_extra'),
    url(r'^rrhh/extra/(?P<extra_codi>\w+)/delete/$', 'views_rrhh.extra_delete'),

    # Moviments
    url(r'^moviments/$', 'views_moviments.moviments_index', name='moviments'),
    url(r'^moviments/(?P<mov_id>\d+)/$', 'views_moviments.moviments_moviment', name='moviment'),
    url(r'^moviments/(?P<mov_id>\d+)/delete/$', 'views_moviments.moviments_moviment_delete'),
    url(r'^moviments/(?P<ambit>\w+)/$', 'views_moviments.moviments_index', name='moviments_filtrats'),

    # Incidencies
    url(r'^incidencies/$', 'views_moviments.incidencies_index', name='incidencies'),
    url(r'^incidencies/(?P<incidencia_id>\d+)/$', 'views_moviments.incidencies_incidencia', name='incidencia'),
    url(r'^incidencies/(?P<incidencia_id>\d+)/delete/$', 'views_moviments.incidencies_incidencia_delete'),

    # Generic
    url(r'^tools/update_session/(?P<key>\w+)/(?P<value>\w+)/$', 'views.update_session'),
    url(r'^tools/synchro_karat/$', 'views_rrhh.synchro_karat', name='synchro_karat'),

    # Reports
    # url(r'^reports/moviments_treballador/$', 'views_reports.moviments_treballador', name='report_moviments_treballador'),
    url(r'^reports/moviments_treballador/(?P<unitat_id>\d+)?$', 'views_reports.moviments_treballador', name='report_moviments_treballador'),
    #url(r'^reports/ratios_unitat/', 'views_reports.ratios_unitat', name='report_ratios_unitat'),
    url(r'^reports/moviments_treballador/(?P<unitat_id>\d+)?/(?P<output>\w+)/$', 'views_reports.moviments_treballador', name='report_moviments_treballador_xls'),
    url(r'^reports/activitat_extra/$', 'views_reports.activitat_extra', name='report_activitat_extra'),
    # url(r'^reports/activitat_extra/(?P<unitat_id>\d+)/$', 'views_reports.activitat_extra', name='report_activitat_extra'),
    url(r'^reports/activitat_extra/(?P<unitat_id>\w+)/$', 'views_reports.activitat_extra', name='report_activitat_extra'),
    url(r'^reports/activitat_extra/(?P<unitat_id>\d+)/(?P<periode_id>\d+)$', 'views_reports.activitat_extra', name='report_activitat_extra'),
    url(r'^reports/activitat_extra/validacio/(?P<periode_id>\d+)$', 'views_reports.exportar_activitat_extra', name='exportar_activitat_extra'),
    url(r'^reports/activitat_extra/historic/(?P<treb_id>\d+)/(?P<treb_codi>\w+)/$', 'views_reports.activitat_extra_historic', name='activitat_extra_historic'),
    url(r'^reports/query_treb_hora/$', 'views_rrhh.query_treballador_hora', name='query_treballador_hora'),

)

urlpatterns += patterns('',
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}, name="logout")
)
