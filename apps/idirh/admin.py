from django.contrib import admin
from idirh.models import CalendariLaboral, DiaCalendari, GrupUnitat

admin.site.register(CalendariLaboral)
admin.site.register(DiaCalendari)
admin.site.register(GrupUnitat)
