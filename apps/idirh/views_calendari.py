# -*- coding: utf-8 -*-
from datetime import datetime

from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib import messages

# from django.contrib.auth.models import User
from apps.meta.models import MetaUnitatIdi

from apps.idirh.models import DiaCalendari, CalendariLaboral,\
                                  PeriodeTreballador, MovimentTreballador,\
                                  ExtraTreballador

from apps.idirh.forms import CalendariLaboralForm

from apps.idirh.utils import permisos, omplir_calendari


@login_required
def calendaris(request):
    """
    Vista principal llistat de calendaris per any
    """
    calendaris = CalendariLaboral.objects.all().order_by('-es_mestre', 'nom')

    context = {
        'calendaris': calendaris,
        'anyo_def': datetime.now().year
    }

    return render_to_response('planilles/calendaris_index.html', context,
                              context_instance=RequestContext(request))


@login_required
def calendari(request, calendari_id=0, anyo=datetime.now().year):
    anyo = int(anyo)
    if int(calendari_id):
        calendari = CalendariLaboral.objects.get(id=calendari_id)
        dies = calendari.diacalendari_set.filter(dia__year=anyo).order_by('dia')
        anys = calendari.diacalendari_set.all().order_by('-dia').dates('dia', 'year')
    else:
        calendari = CalendariLaboral()
        anys = dies = []

    if request.method == 'POST':
        form = CalendariLaboralForm(request.POST, instance=calendari)
        if form.is_valid():
            form.save()
            calendari_id = calendari.id
            return redirect('calendari', calendari_id=calendari_id)

    else:
        form = CalendariLaboralForm(instance=calendari)

    context = {
        'calendari_id': calendari_id,
        'calendari': calendari,
        'dies': dies,
        'form': form,
        'anys': anys,
        'anyo_def': anyo,
        'anyo_resum': calendari.get_any_resum(anyo),
        'calendari_llista': calendari.get_calendari_llista(anyo),
        'calendari_cal': calendari.get_calendari(anyo),
        'dies_31': xrange(0, 32),
        'permis': permisos(request.user)  # permis == 'ROOT' || 'RRHH'...
    }

    return render_to_response('planilles/calendaris_calendari.html',
                              context,
                              context_instance=RequestContext(request))


@login_required
def calendari_delete(request, calendari_id):
    """
    Chequeijem que l'usuari tingui permisos
    """
    if (permisos(request.user) in ['ROOT', 'RRHH']):
        try:
            calendari = CalendariLaboral.objects.get(id=calendari_id)
            calendari.delete()
        except CalendariLaboral.DoesNotExist:
            pass

    return redirect('calendaris')


@login_required
def calendari_dia(request, calendari_id, dia_id=0):
    calendari = CalendariLaboral.objects.get(id=calendari_id)
    dies = calendari.diacalendari_set.all().order_by('dia')

    if int(dia_id):
        dia = DiaCalendari.objects.get(id=dia_id)
    else:
        dia = {}

    data = {
        'calendari': calendari,
        'dies': dies,
        'dia': dia
    }

    return render_to_response('planilles/calendaris_dia.html',
                              data,
                              context_instance=RequestContext(request))


@login_required
def calendari_dia_save(request, calendari_id, anyo=None):
    """ Grabar dia per calendari:
        [ x ] modificar data a format correcte
        [   ] check de data/any unic
        [   ] convertir entrada a forms
    """

    if request.method == 'POST':
        # Parse'n'convert del dia que em ve dd/mm/aaaa a MySQL's aaaa-mm-dd
        dia_mysql = datetime.strptime(request.POST.get('f_dia', ''),  '%d-%m-%Y')
        if DiaCalendari.objects.filter(dia__exact=datetime.date(dia_mysql)):
            # Si ja existeix el dia disparem error
            # Si forms override d'aixo
            messages.error(request, _('Aquest el dia ja existeix al calendari.'))
        else:
            # Si s'entra un dia de fora del actual calendari, el guardem, per visitar-l'ho
            anyo = request.POST.get('f_dia', '')[-4:]
            dia = DiaCalendari(dia=dia_mysql.strftime('%Y-%m-%d'),
                               nom=request.POST.get('f_nom', ''),
                               tipus_dia=request.POST.get('f_tipus', ''),
                               calendari=CalendariLaboral.objects.get(id=calendari_id))
            dia.save()

    return redirect('calendari', calendari_id=calendari_id, anyo=anyo)


@login_required
def calendari_dia_delete(request, calendari_id, dia_id, anyo):
    """
    Esbborrar dia de calendari, no es pot esborrar un dia del passat.
    """
    dia = DiaCalendari.objects.get(id=dia_id)
    dia.delete()

    return redirect('calendari', calendari_id=calendari_id, anyo=anyo)


@login_required
def calendari_display(request, calendari_id):
    """
    Renderitza la taula del calendari, per agafar-la via AJAX
    """
    calendari = CalendariLaboral.objects.get(id=calendari_id)

    context = {
        'calendari_llista': calendari.get_calendari_llista(2012),
        'calendari_cal': calendari.get_calendari(2012),
        'dies_31': xrange(0, 32)    # Thats cutre, but...
    }

    return render_to_response('planilles/calendari_display_calendari_any.html',
                              context,
                              context_instance=RequestContext(request))


@login_required
def calendari_personal(request, anyo=datetime.now().year):
    anyo = int(anyo)
    treballador = request.user
    context = {}

    unitat = MetaUnitatIdi.objects.get(id=treballador.get_profile().unitat_idi_id)

    if unitat is None or not unitat.actiu:
        # messages.error(request, _('No disposes de calendari personal per no estar assignat a una Unitat IDI'))
        context['error'] = _('La Unitat a la que estas assignat no disposa de calendari actiu.')
    else:
        try:
            calendari = CalendariLaboral.objects.get(id=unitat.calendari_laboral.id)
            periodes = PeriodeTreballador.objects.filter(user=treballador)\
                                             .filter(data_inici__year=anyo)\
                                             .filter(congelat=True)\
                                             .order_by('data_inici')

            moviments = MovimentTreballador.objects.filter(user=treballador)\
                                           .filter(dia__year=anyo)\
                                           .filter(congelat=True)\
                                           .order_by('dia')

            extres = ExtraTreballador.objects.filter(user=treballador)\
                                             .filter(dia__year=anyo)\
                                             .order_by('dia')

            anys = PeriodeTreballador.objects.filter(user=treballador)\
                                             .dates('data_inici', 'year')

            try:
                darrer_periode_congelat = PeriodeTreballador.objects\
                                                .filter(user=treballador)\
                                                .filter(data_inici__year=anyo)\
                                                .filter(congelat=True)\
                                                .order_by('-updated_at')[0]
            except:
                darrer_periode_congelat = None
    
            consolidat = {
                'laboral': 0,
                'hores_treballades_avui': 0,
                'hores_totals': treballador.get_profile().jornada_anual.hores * 60 * 60,
                'vacas_fetes': 0,
                'vacas_totals': 0,
                'lleure_fets': 0,
                'lleure_totals': 0
            }

            cal_obj = calendari.get_calendari(anyo)
            cal_data = calendari.get_any_resum(anyo)

            omplir_calendari(treballador, periodes, moviments, extres, cal_obj, cal_data, anyo, consolidat, congelat=True)

            # Si no esta determinada la vista per defecte la fixem
            if 'cal_vista' not in request.session:
                request.session['cal_vista'] = 'cal'

            context = {
                'calendari': calendari,
                'treballador': treballador,
                'unitat': unitat,
                'calendari_cal': cal_obj,
                'calendari_llista': calendari.get_calendari_llista(anyo),
                'dies_31': xrange(0, 32),
                'periodes': periodes,
                'moviments': moviments,
                'extres': extres,
                'consolidat': consolidat,
                'anys': anys,
                'anyo_def': anyo,
                'avui': datetime.now().date(),
                'darrer_periode_congelat': darrer_periode_congelat
            }

        except:
            context['error'] = _('La Unitat a la que estas assignat no disposa de calendari actiu.')

    return render_to_response('planilles/calendari_personal.html', context,
                              context_instance=RequestContext(request))
