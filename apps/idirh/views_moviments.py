# coding: utf-8

from django.utils.translation import ugettext_lazy as _

from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from django.db import IntegrityError

from apps.idirh.models import Moviment, Incidencia

from apps.idirh.forms import MovimentForm, IncidenciaForm

from apps.idirh.utils import permisos


@login_required
def moviments_index(request, ambit=None):
    """
    """
    if ambit == 'IDI':
        moviments = Moviment.objects.filter(es_idi=True).order_by('nom')
    elif ambit == 'ICS':
        moviments = Moviment.objects.filter(es_ics=True).order_by('nom')
    else:
        moviments = Moviment.objects.all().order_by('nom')

    context = {
        'ambit': ambit,
        'moviments': moviments,
        'permis': permisos(request.user)
    }

    return render_to_response('planilles/moviments_index.html', context,
                              context_instance=RequestContext(request))


@login_required
def moviments_moviment(request, mov_id=0):
    permis = permisos(request.user)
    if int(mov_id):
        try:
            moviment = Moviment.objects.get(id=mov_id)
        except Moviment.DoesNotExist:
            return redirect('moviments')
    else:
        moviment = Moviment()

    if request.method == 'POST':
        if permis in ['RRHH', 'ROOT']:
            form = MovimentForm(request.POST, instance=moviment)
            if form.is_valid():
                form.save()
        return redirect('moviment', moviment.id)
    else:
        form = MovimentForm(instance=moviment)

    context = {
        'mov_id': mov_id,
        'moviment': moviment,
        'form': form,
        'permis': permis
    }

    return render_to_response('planilles/moviments_moviment.html', context,
                              context_instance=RequestContext(request))


@login_required
def moviments_moviment_delete(request, mov_id=0):
    if int(mov_id):
        moviment = Moviment.objects.get(id=mov_id)
        try:
            moviment.delete()
            msg_info = 'Ja s\'ha esborrat el moviment'
            moviment_nom = '<strong>%s</strong>' % moviment.nom
            messages.info(request,
                       _('%(info)s %(moviment_nom)s')
                       % {'info': msg_info, 'moviment_nom': moviment_nom})

        except IntegrityError:
            msg_error = 'No es pot esborrar aquest moviment, ja esta aplicat a algun treballador'
            messages.error(request,
                       _('%(error)s')
                       % {'error': msg_error})
            return redirect('moviment', mov_id)

    return redirect('moviments')


@login_required
def incidencies_index(request):
    incidencies = Incidencia.objects.all().order_by('nom')

    context = {
        'incidencies': incidencies,
    }

    return render_to_response('planilles/incidencies_index.html', context,
                              context_instance=RequestContext(request))

    pass


@login_required
def incidencies_incidencia(request, incidencia_id=0):
    if int(incidencia_id):
        try:
            incidencia = Incidencia.objects.get(id=incidencia_id)
        except Incidencia.DoesNotExist:
            return redirect('incidencies')
    else:
        incidencia = Incidencia()

    # Si es un post es passa per aqui
    if request.method == 'POST':
        form = IncidenciaForm(request.POST, instance=incidencia)
        if form.is_valid():
            form.save()
        return redirect('incidencia', incidencia.id)
    else:
        form = IncidenciaForm(instance=incidencia)

    context = {
        'incidencia': incidencia,
        'form': form,
    }

    return render_to_response('planilles/incidencies_incidencia.html', context,
                              context_instance=RequestContext(request))


@login_required
def incidencies_incidencia_delete(request, incidencia_id=0):
    if int(incidencia_id):
        try:
            incidencia = Incidencia.objects.get(id=incidencia_id)
            incidencia.delete()
        except Incidencia.DoesNotExist:
            pass

    return redirect('incidencies')
