# coding: utf-8
#
import calendar
from datetime import datetime, timedelta, date, time

from django.utils.translation import ugettext_lazy as _

from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages

from django.db import connection
from django.db.models import Q

from apps.meta.models import MetaUnitatIdi, AuthUserProfile,\
                             MetaGrupCategoriaProfessional

from apps.idirh.models import CalendariLaboral, FAPTreballador,\
                              PeriodeTreballador, MovimentTreballador,\
                              Rotacio, DiaCalendari, RotacioAplicada,\
                              JornadaAnual, PeriodeEquipSequenciaPersonal,\
                              Extra, ExtraTreballador, Moviment

from apps.idirh.forms import TreballadorForm, FranjaForm, JornadaAnualForm,\
                             JornadaAnualAplicadaForm, ExtraForm, TrebHoraForm

from apps.idirh.models import DIES_SETMANA

from apps.idirh.utils import get_query, get_hores_en_format, daterange,\
                             get_dia, permisos, omplir_calendari,\
                             get_treballadors_actius

from apps.meta.common import sincronitza_karat_portal


@login_required
def index(request, unitat_id=0):
    """
    Vista principal de treballadors per unitat
    Nomes es veuen les unitats a les que es te acces
    I nomes les categories professionals que te assignades
    """
    query_string = ''
    treballadors = None
    unitat_id = int(unitat_id)

    unitats = request.user.get_profile().unitats_idi_permissos\
                                        .filter(actiu=True)\
                                        .order_by('centre_idi')

    categories_control = request.user.get_profile().categories_de_control.all()
    categories = MetaGrupCategoriaProfessional.objects.all()

    treballadors = get_treballadors_actius()
    treballadors = treballadors.filter(authuserprofile__categoria_profesional_karat__grup__in=categories_control)\
                               .order_by('authuserprofile__unitat_idi', 'last_name')


    if unitat_id:
        treballadors = treballadors.filter(authuserprofile__unitat_idi=unitat_id)
    else:
        treballadors = treballadors.filter(authuserprofile__unitat_idi__in=unitats)

    if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']
        entry_query = get_query(query_string, ['first_name', 'last_name', ])
        treballadors = treballadors.filter(entry_query)

    if ('c' in request.GET) and request.GET['c'].strip():
        treballadors = treballadors.filter(authuserprofile__categoria_profesional_karat__grup__in=request.GET['c'])

    context = {
        'query_string': query_string,
        'unitats': unitats,
        'unitat_id': unitat_id,
        'treballadors': treballadors,
        'categories_control': categories_control,
        'categories': categories
    }

    return render_to_response('planilles/rrhh_index.html',
                              context,
                              context_instance=RequestContext(request))


@login_required
def index_treballador(request, treb_id=0, treb_codi=0):
    anyo = datetime.now().year
    return redirect('treballador_any', treb_id, treb_codi, anyo)


@login_required
def index_treballador_anyo(request, treb_id=0, treb_codi=0,
                           anyo=datetime.now().year,
                           snap_anyo=0, snap_mes=0, snap_dia=0,
                           snap_hora=0, snap_min=0):
    """
    """
    anyo = int(anyo)

    treballador = User.objects.get(id=treb_id)
    treb_profile = AuthUserProfile.objects.get(user=treb_id)
    unitat = MetaUnitatIdi.objects.get(id=treballador.get_profile().unitat_idi_id)
    # Cas que un treballador estigui assignat a una Unitat que no dispoosa de
    # CalendariLaboral... redirect
    if unitat.calendari_laboral is not None:
        calendari = CalendariLaboral.objects.get(id=unitat.calendari_laboral.id)
    else:
        msg_treballador = '<strong>' +\
                          treballador.get_profile().get_nom_normalitzat()
        msg_treballador += ' ' +\
                           treballador.get_profile().get_cognom_normalitzat() +\
                           '</strong> '
        msg_unitat = '<strong>' + unitat.descripcio + '</strong>'
        messages.error(request,
                       _('%(treb)s que pertany a %(unitat)s no disposa de Calendari assignat.')
                       % {'treb': msg_treballador, 'unitat': msg_unitat})

        return redirect('rrhh')

    # Comprobacio de que el treballador està en una unitat on es poden assignar
    # dies, pq la unitat te assignats equips/periodes
    #
    # COMPROVACIÓ DESACTIVADA!!!!
    periodes_assignats = PeriodeEquipSequenciaPersonal.objects.filter(periode_equip__equip__grup__unitat=unitat,
                                                                      periode_equip__data_inici__year=anyo)\
                         or True

    if not periodes_assignats:
        msg_unitat = '<strong>' + unitat.descripcio + '</strong>'
        messages.error(request,
                       _('La unitat %(unitat)s no te definits els requisits funcionals per aquest any.')
                       % {'unitat': msg_unitat})

    # ens dona els dies laborables/festius
    dies_any = calendari.get_any_resum(anyo)
    periodes = PeriodeTreballador.objects.filter(user=treballador)\
                                         .filter(data_inici__year=anyo)\
                                         .order_by('id')

    moviments = MovimentTreballador.objects.filter(user=treballador)\
                                           .filter(dia__year=anyo)\
                                           .order_by('dia')

    extres = ExtraTreballador.objects.filter(user=treballador)\
                                     .filter(dia__year=anyo)\
                                     .filter(extra__tipus='E')\
                                     .filter(data_eliminacio__isnull=True)\
                                     .order_by('dia')
    congelacio = 0
    if (snap_anyo):
        congelacio = datetime(int(snap_anyo), int(snap_mes), int(snap_dia), int(snap_hora), int(snap_min), 59)
        periodes = periodes.filter(updated_at__lte=congelacio)
        moviments = moviments.filter(updated_at__lte=congelacio)
        extres = extres.filter(updated_at__lte=congelacio)

    anys = PeriodeTreballador.objects.filter(user=treballador)\
                                     .dates('data_inici', 'year')

    darrer_periode_congelat = PeriodeTreballador.objects\
                                                .filter(user=treballador)\
                                                .filter(data_inici__year=anyo)\
                                                .filter(congelat=True)\
                                                .order_by('-updated_at')
    if darrer_periode_congelat:
        darrer_periode_congelat = darrer_periode_congelat[0]

    congelacions = PeriodeTreballador.objects.filter(user=treballador)\
                                             .filter(data_inici__year=anyo)\
                                             .filter(congelat=True)\
                                             .dates('updated_at', 'day', order="DESC")

    mysql_query = """
    SELECT DISTINCT updated_at FROM (
        SELECT DISTINCT updated_at FROM plan_exraordinaria_treballador p_extra
            WHERE p_extra.user_id = %(user)s
        union
        SELECT DISTINCT updated_at FROM plan_moviment_treballador p_mov
            WHERE p_mov.user_id = %(user)s
        union
        SELECT DISTINCT updated_at FROM plan_periode_treballador p_periode
            WHERE p_periode.user_id = %(user)s
        ) tots ORDER BY updated_at DESC
    """ % dict(user=treballador.id)
    # Venga... old times, a saco
    cursor = connection.cursor()
    cursor.execute(mysql_query)
    congelacions = [row[0] for row in cursor.fetchall()]

    darrer_periode_congelat = congelacions[0] if congelacions else None

    # S'amaga la visió de del select d'hores anuals via view
    form_hores_anuals = None
    if permisos(request.user) in ['RRHH', 'ROOT'] and False:
        form_hores_anuals = JornadaAnualAplicadaForm(instance=treb_profile)
        form_hores_anuals.fields['jornada_anual'].queryset = JornadaAnual.objects.filter(actiu=True)

    consolidat = {
        'laboral': 0,
        'hores_treballades_avui': 0,
        'hores_totals': treballador.get_profile().jornada_anual.hores * 60 * 60,
        'vacas_fetes': 0,
        'vacas_totals': 0,
        'lleure_fets': 0,
        'lleure_totals': 0
    }

    # dies_setmana = []
    # Grabem la nova entrada en
    if request.method == 'POST':
        __save_entrada_calendari(request, treballador, unitat, anyo, calendari, periodes)

    # El calendari es crea incrementalment, primer
    # creem l'array del calendari...
    cal_obj = calendari.get_calendari(anyo)
    # A cal_data disposem de la info de l'any (dies, dies festius,
    # dies laborables...)
    cal_data = calendari.get_any_resum(anyo)

    omplir_calendari(treballador, periodes, moviments, extres, cal_obj, cal_data, anyo, consolidat)

    # s'han de comptar les vacances de l'any passat tambe
    # això està fora de periodes i moviments
    avui = datetime.now()
    vac_any_passat = MovimentTreballador.objects.filter(user=treballador)\
                                                .filter(dia__year=anyo + 1)\
                                                .filter(dia__lt=avui)\
                                                .filter(moviment__codi='VP')

    consolidat['vacas_fetes'] += len(vac_any_passat)
    consolidat['vacas_totals'] += len(vac_any_passat)

    form = TreballadorForm(unitat)
    # S'edita el queryset del form, per filtrar per moviments relatius al treb.
    if treballador.get_profile().es_idi:
        form.fields['moviment'].queryset = Moviment.objects.filter(es_idi=True)
    if treballador.get_profile().es_ics:
        form.fields['moviment'].queryset = Moviment.objects.filter(es_ics=True)


    # Si no està determinada la vista per defecte la fixem
    if 'cal_vista' not in request.session:
        request.session['cal_vista'] = 'cal'

    avui = datetime.now().date()

    # Es pot editar el mes passat si  estem abans del dia 20
    if avui.day > 20:
        primer_dia_editable = date(avui.year, avui.month, 21)
    else:
        # Si el dia del mes és inferior al dia 20
        # es pot editar el mes anterior
        if avui.month == 1:
            primer_dia_editable = date(avui.year - 1, 12, 1)
        else:
            primer_dia_editable = date(avui.year, avui.month - 1, 1)

    context = {
        'treballador': treballador,
        'unitat': unitat,
        'calendari_cal': cal_obj,
        'dies_31': xrange(0, 32),
        'form': form,
        'periodes': periodes,
        'moviments': moviments,
        'extres': extres,
        'consolidat': consolidat,
        'anys': anys,
        'anyo_def': anyo,
        'darrer_periode_congelat': darrer_periode_congelat,
        'congelacions': congelacions,
        'congelacio': congelacio,
        'form_hores_anuals': form_hores_anuals,
        'dies_any': dies_any,
        'avui': datetime.now().date(),  # S'usa per permetre esborrar events congelats del futur
        'primer_dia_editable': primer_dia_editable,
        'can_edit': not congelacio and periodes_assignats,
    }

    return render_to_response('planilles/rrhh_treballador.html',
                              context,
                              context_instance=RequestContext(request))


@login_required
def congelar_treballador_anyo(request, treb_id, treb_codi, anyo):
    """
    Es posa el congelat a true usant .update() al
    propi queryset, es congelen els no congelats nomes.
    Es força el updated at a un valor fixe,
    no sigui que entre update i update passin
    alguns segons
    """

    rigth_now = datetime.now()

    treballador = User.objects.get(id=treb_id)
    PeriodeTreballador.objects.filter(user=treballador)\
                              .filter(updated_at__year=anyo)\
                              .filter(congelat=False)\
                              .update(congelat=True, updated_at=rigth_now)

    MovimentTreballador.objects.filter(user=treballador)\
                               .filter(updated_at__year=anyo)\
                               .filter(congelat=False)\
                               .update(congelat=True, updated_at=rigth_now)

    ExtraTreballador.objects.filter(user=treballador)\
                            .filter(updated_at__year=anyo)\
                            .filter(congelat=False)\
                            .update(congelat=True, updated_at=rigth_now)

    return redirect('treballador_any', treb_id, treb_codi, anyo)


@login_required
def form_partial_rotacio(request, rot_id=0):
    """
    Es crida desde AJAX, retorna un html parcial per injectar
    """
    # això és una tonteria, però evita que es pugui cridar directament
    # i mostrar una pàgina lletja
    if not request.is_ajax():
        return redirect('calendari_personal')

    rotacio = Rotacio.objects.get(id=rot_id)
    sequencies = rotacio.rotaciosequencia_set.all().order_by('ordre')
    context = {
        'sequencies': sequencies
    }
    return render_to_response('planilles/rrhh_treballador_form_rotacions.html',
                              context,
                              context_instance=RequestContext(request))


@login_required
def treballador_periode_edit(request, treb_id, treb_codi, anyo, periode_id):
    # BUG, preguntem si no s'ha inhabilitat/esborrat el periode mentrestant
    try:
        periode_seq = RotacioAplicada.objects.get(id=periode_id)
    except RotacioAplicada.DoesNotExist:
        # Si no existeix el reenviem a pantalla calendari amb un message
        messages.error(request, _('No es pot editar el periode que havies seleccionat'))
        return redirect('treballador_any', treb_id, treb_codi, anyo)

    periode = periode_seq.periode
    sequencia_original = periode_seq.sequencia
    form = FranjaForm()

    # faps_treballador = periode.faptreballador_set.all()
    faps_treballador = periode_seq.faptreballador_set.all()

    total_setmana = 0
    dies = []
    for dia in xrange(7):
        franges = FAPTreballador.objects.filter(dia_setmana=dia,
                                                periode_treballador=periode_seq.periode,
                                                periode_seq=periode_seq)\
                                        .order_by('hora_inici')
        total_segons = 0
        hores = []
        abrev_torn = 'M'
        for franja in franges:
            hora_inici = timedelta(hours=franja.hora_inici.hour,
                                   minutes=franja.hora_inici.minute)
            hora_fi = timedelta(hours=franja.hora_fi.hour,
                                minutes=franja.hora_fi.minute)
            total = hora_fi - hora_inici
            total_segons += total.seconds
            abrev_torn = franja.abrev_torn
            hores.append({'hora_inici': franja.hora_inici,
                          'hora_fi': franja.hora_fi})

        dies.append({'dia_num': dia,
                     'dia_str': DIES_SETMANA[dia],
                     'hores': hores,
                     'abrev_torn': abrev_torn,
                     'hores_format': get_hores_en_format(total_segons)
                     })
        total_setmana += total_segons

    total_real = 0
    if periode.reduccio:
        total_real = (total_setmana * 100) / periode.reduccio

    # Un cas una mica marciano, un torn que no te hores però al que se
    # li aplica reducció
    hores_original = sequencia_original.get_hores_totals()
    if hores_original:
        # Mirar https://docs.djangoproject.com/en/dev/ref/contrib/humanize/
        pct_sobre_torn = '%.2f' % (total_setmana * 100.0 / hores_original)
    else:
        pct_sobre_torn = '--'

    context = {
        'form': form,
        'treb_id': treb_id,
        'treb_codi': treb_codi,
        'anyo': anyo,
        'periode': periode,
        'faps_treballador': faps_treballador,
        'DIES_SETMANA': DIES_SETMANA,
        'dies': dies,
        'pct_sobre_torn': pct_sobre_torn,
        'total_real': get_hores_en_format(total_real, format='llarg'),
        'total_setmana': get_hores_en_format(total_setmana, format='llarg'),
        'total_sequencia_original': sequencia_original.get_hores_format(),
        'tipus_torn': 'FAP'
    }

    return render_to_response('planilles/rrhh_treballador_faps_form.html',
                              context,
                              context_instance=RequestContext(request))


@login_required
def treballador_periode_edit_franja(request, treb_id, treb_codi, anyo, periode_id):
    periode_seq = RotacioAplicada.objects.get(id=periode_id)
    periode = periode_seq.periode

    if request.method == 'POST':
        form = FranjaForm(request.POST)
        # Al form treballem tant les franges com la data final, sense agobios
        # si s'ha entrat alguna cosa ok, si no... també
        if form.is_valid():
            #
            # Grabo la data final del perode
            if form.cleaned_data['data_fi_periode']:
                data_fi_periode = datetime.strptime(form.cleaned_data['data_fi_periode'], '%d-%m-%Y')
                if data_fi_periode >= datetime.now():
                    periode.data_fi = data_fi_periode.date()
                    periode.save()

            #
            # Grabo les franges
            dia_setmana = form.cleaned_data['dia_setmana']
            dies = xrange(dia_setmana, dia_setmana + 1)
            inici_1 = form.cleaned_data['inici_1']
            fi_1 = form.cleaned_data['fi_1']
            inici_2 = form.cleaned_data['inici_2']
            fi_2 = form.cleaned_data['fi_2']
            abrev_torn = form.cleaned_data['abrev_torn_detall']

            if (inici_1 or inici_1 == time(0, 0)) and (fi_1 or fi_1 == time(0, 0)):
                if not abrev_torn:
                    messages.error(request,
                                   ('S\'ha d\'escollir un Tipus de Torn pel dia/dies seleccionats.'))
                    return redirect('treballador_periode_edit', treb_id, treb_codi, anyo, periode_id)

                if dia_setmana == 10:  # es dilluns --> divendres
                    dies = xrange(5)
                if dia_setmana == 11:  # es cap de setmana
                    dies = xrange(5, 7)

                for dia in dies:
                    franja = FAPTreballador(
                        periode_treballador=periode,
                        franja_template=None,
                        dia_setmana=dia,
                        hora_inici=inici_1,
                        hora_fi=fi_1,
                        periode_seq=periode_seq,
                        abrev_torn=abrev_torn,
                        sequencia=periode_seq.sequencia)
                    franja.save()

                    if (inici_2 or inici_2 == time(0, 0)) and (fi_2 or fi_2 == time(0, 0)):
                        franja = FAPTreballador(
                            periode_treballador=periode,
                            franja_template=None,
                            dia_setmana=dia,
                            hora_inici=inici_2,
                            hora_fi=fi_2,
                            periode_seq=periode_seq,
                            abrev_torn=abrev_torn,
                            sequencia=periode_seq.sequencia)
                        franja.save()

                if not periode_seq.editat:
                    periode_seq.editat = True
                    periode_seq.save()

    return redirect('treballador_periode_edit', treb_id, treb_codi, anyo, periode_id)


@login_required
def planilla_unitat(request, unitat_id=0):
    year = datetime.now().year
    month = datetime.now().month
    day = datetime.now().day

    return redirect('planilles_data_unitat', year, month, day, unitat_id)


@login_required
def planilla(request, year=datetime.now().year, month=datetime.now().month,
             day=datetime.now().day, unitat_id=0, grup_id=0):
    data = datetime(int(year), int(month), int(day))

    data_inici = data - timedelta(days=7)
    data_final = data + timedelta(days=24)
    propera_data = data + timedelta(days=7)

    anyo = int(year)

    #  Aquí està la chapusa, es control·len sempre 2 anys
    anyo1 = data_inici.year
    anyo2 = data_final.year

    unitats = request.user.get_profile().unitats_idi_permissos.filter(actiu=True)

    if not unitats:
        # Si no hi ha unitat assignada, es mostra un error
        messages.error(request,
                       ('No s\'ha assignat cap Unitat sobre la que tinguis acces.'))
        context = {
        }

        return render_to_response('planilles/rrhh_planilla.html', context,
                                  context_instance=RequestContext(request))
    else:
        unitat_default = unitats[0].id

    if not int(unitat_id):
        return redirect('planilla_unitat', unitat_default)

    categories = request.user.get_profile().categories_de_control.all()

    unitat = MetaUnitatIdi.objects.get(id=unitat_id)

    treballadors = get_treballadors_actius()

    treballadors = treballadors.filter(authuserprofile__unitat_idi=unitat_id)\
                               .filter(authuserprofile__categoria_profesional_karat__grup__in=categories)\
                               .order_by('authuserprofile__categoria_profesional_karat__grup',
                                         'last_name')

    if unitat.calendari_laboral is not None:
        calendari = CalendariLaboral.objects.get(id=unitat.calendari_laboral.id)
    else:
        msg_unitat = '<strong>' + unitat.descripcio + '</strong>'
        messages.error(request,
                       ('La unitat %(unitat)s no disposa de Calendari assignat.')
                       % {'unitat': msg_unitat})

        context = {
            'unitats': unitats,
            'unitat_id': unitat_id,
            'unitat': unitat,
        }

        return render_to_response('planilles/rrhh_planilla.html', context,
                                  context_instance=RequestContext(request))

    # Pleno l'array de dies, anoto weekends i festius
    # s'usa la funcio daterange
    dies_header = []
    for data_i in daterange(data_inici, data_final, end=True):
        tipus = data_i.day
        try:
            dia_festiu = DiaCalendari.objects.get(calendari=calendari,
                                                  dia=data_i)
            tipus = dia_festiu.tipus_dia
        except DiaCalendari.DoesNotExist:
            if calendar.weekday(data_i.year, data_i.month, data_i.day) in (5, 6):
                tipus = 'W'

        dies_header.append(tipus)

    # l'array PLAN es on es guarda el periode per treballador
    #
    plan = []
    consolidat = []
    for treballador in treballadors:
        try:
            karat_grup = treballador.get_profile().categoria_profesional_karat.grup
        except:
            karat_grup = None

        if karat_grup is None:
            karat_grup = _('Sense grup assignat')

        dies_treb = ['' for x in range(1, 33)]

        # Agafo els periodes per reballador
        # Formatejo dates a mysql per no usar Q
        #
        data_inici_sql = data_inici.strftime("%Y-%m-%d")
        data_fi_sql = data_final.strftime("%Y-%m-%d")
        # SQL injection flaw here, ready to crash some day
        # Versió del Marc
        mysql_query = """
            SELECT * FROM plan_periode_treballador WHERE
            user_id = %(user)s
            AND (
                ((data_inici >= '%(data_inici)s') AND (data_inici <= '%(data_fi)s'))
                OR
                ((data_inici <=' %(data_inici)s') AND ((data_fi >= '%(data_inici)s') OR (data_fi is NULL)))
                )
            """ % dict(user=treballador.id, data_inici=data_inici_sql, data_fi=data_fi_sql)    # Dies que no vull mostrar a la impresio

        # Versió de l'Albert S
        # De moment treiem el filtre de congelació
        mysql_query = """
            SELECT * FROM plan_periode_treballador WHERE
            user_id = %(user)s
            AND (congelat = 1 OR congelat = 0)
            AND (
                ((data_inici < '%(data_fi)s') AND (data_fi > '%(data_inici)s')))
            """ % dict(user=treballador.id, data_inici=data_inici_sql, data_fi=data_fi_sql)

        periodes = PeriodeTreballador.objects.raw(mysql_query)



        periodes_1 = PeriodeTreballador.objects.filter(user=treballador)\
                                         .filter(data_inici__year=anyo1)\
                                         .order_by('data_inici', 'id')
        periodes_2 = PeriodeTreballador.objects.filter(user=treballador)\
                                         .filter(data_inici__year=anyo2)\
                                         .order_by('data_inici', 'id')



        # periodes = PeriodeTreballador.objects.filter(user=treballador)\
        #                                      .filter(data_inici__year=anyo)\
        #                                      .order_by('data_inici')
        # moviments = MovimentTreballador.objects.filter(user=treballador)\
        #                                .filter(dia__year=anyo)\
        #                                .order_by('dia')

        # De moment treiem el fltre de congelació
        moviments = MovimentTreballador.objects.filter(user=treballador)\
                                               .filter(dia__gte=data_inici)\
                                               .filter(dia__lte=data_final)\
                                               # .filter(congelat=True)


        # FIX THAT
        extres = ExtraTreballador.objects.filter(user=treballador)\
                                         .filter(dia__year=anyo)\
                                         .filter(extra__tipus='E')\
                                         .order_by('dia')

        cal_obj_1 = calendari.get_calendari(anyo1)
        cal_data_1 = calendari.get_any_resum(anyo1)
        omplir_calendari(treballador, periodes_1, moviments, extres, cal_obj_1, cal_data_1, anyo1, consolidat)

        cal_obj_2 = calendari.get_calendari(anyo2)
        cal_data_2 = calendari.get_any_resum(anyo2)
        omplir_calendari(treballador, periodes_2, moviments, extres, cal_obj_2, cal_data_2, anyo2, consolidat)

        # Pleno els 24 dies de manera directe
        for i, data_i in enumerate(daterange(data_inici, data_final, end=True)):
            if data_i.year == anyo1:
                dia_persona = cal_obj_1[data_i.month - 1][1][data_i.day - 1]
            else:
                dia_persona = cal_obj_2[data_i.month - 1][1][data_i.day - 1]
            dies_treb[i] = dia_persona

            # Aqui hi posarem tot el tema del consolidat, que ja tenim indexat
            # més amunt
            if dia_persona['tipus'] == 'T':
                pass

        plan.append({
            'inicilal': treballador.first_name[0],
            'cognom': treballador.get_profile().get_cognom_normalitzat(),
            'id': treballador.id,
            'codi': treballador.username,
            'dies': dies_treb,
            'karat_grup': karat_grup
        })

    context = {
        'treballadors': treballadors,
        'data_inici': data_inici,
        'data_final': data_final,
        'propera_data': propera_data,
        'dies': dies_header,
        'plan': plan,
        'unitats': unitats,
        'unitat_id': int(unitat_id),
        'calendari': calendari,
        'dies_no': ['W', 'F', 'FL']    # Dies que no vull mostrar a la impresio
    }

    return render_to_response('planilles/rrhh_planilla.html', context,
                              context_instance=RequestContext(request))


@login_required
def treballador_fapdelete(request, treb_id, treb_codi, anyo, periode_id, dia):
    """"""
    # periode = PeriodeTreballador.objects.get(id=periode_id)
    periode_seq = RotacioAplicada.objects.get(id=periode_id)

    if not periode_seq.editat:
        periode_seq.editat = True
        periode_seq.save()

    fap = FAPTreballador.objects.filter(periode_seq=periode_seq,
                                        periode_treballador=periode_seq.periode,
                                        dia_setmana=dia)
    if fap:
        fap.delete()

    return redirect('treballador_periode_edit', treb_id, treb_codi, anyo, periode_seq.id)


@login_required
def index_treballador_moviment_delete(request, treb_id, treb_codi,
                                      anyo=datetime.now().year, moviment_id=0):
    if (int(moviment_id)):
        try:
            treb_mov = MovimentTreballador.objects.get(id=moviment_id)
            treb_mov.delete()
        except MovimentTreballador.DoesNotExist:
            pass
    return redirect('treballador_any', treb_id, treb_codi, anyo)


@login_required
def index_treballador_periode_delete(request, treb_id=0, treb_codi=0,
                                     anyo=datetime.now().year, periode_id=0):
    if (int(periode_id)):
        try:
            treb_periode = PeriodeTreballador.objects.get(id=periode_id)
            # Pim, pam , django magic
            for rotacio in treb_periode.rotacioaplicada_set.all():
                rotacio.faptreballador_set.all().delete()
            treb_periode.rotacioaplicada_set.all().delete()
            treb_periode.delete()
        except PeriodeTreballador.DoesNotExist:
            pass

    return redirect('treballador_any', treb_id, treb_codi, anyo)


@login_required
def index_jornades_anuals(request):
    jornades = JornadaAnual.objects.filter(actiu=True)
    context = {'jornades': jornades}

    return render_to_response('planilles/jornades_anuals_index.html',
                              context,
                              context_instance=RequestContext(request))


@login_required
def jornada_anual(request, jornada_id=0):
    """"""
    if int(jornada_id):
        jornada = JornadaAnual.objects.get(id=jornada_id)
    else:
        jornada = JornadaAnual()

    if request.method == 'POST':
        form = JornadaAnualForm(request.POST, instance=jornada)
        if form.is_valid():
            # Hi ha d'haver minim 1 per defecte
            # Es posen tots a false. La casa es gran
            JornadaAnual.objects.all().update(es_default=False)
            form.save()
            # jornada_id = jornada.id
            #  Si era default, ho tornem a setejar
            if form.cleaned_data['es_default_hidden']:
                jornada.es_default = True
                jornada.save()
            return redirect('jornades')
    else:
        form = JornadaAnualForm(instance=jornada)

    context = {
        'jornada_id': jornada_id,
        'jornada': jornada,
        'form': form,
    }

    return render_to_response('planilles/jornades_anuals_jornada.html',
                              context_instance=RequestContext(request, context))


@login_required
def jornada_anual_delete(request, jornada_id=0):
    try:
        jornada = JornadaAnual.objects.get(id=jornada_id)
        jornada.actiu = False
        jornada.save()
    except JornadaAnual.DoesNotExist:
        pass

    return redirect('jornades')


@login_required
def canvi_jornada_anual(request, treb_id, treb_codi, anyo):
    """
    Això està fet amb el CUL, s'ha de caviar ASAP
    El problema sorgia per tenir MyIsam vs InnoDB
    """
    if request.method == 'POST':
        form = JornadaAnualAplicadaForm(request.POST)
        jornada_anual_id = form.data['jornada_anual']
        jornada_anual = JornadaAnual.objects.get(id=jornada_anual_id)
        user_profile = AuthUserProfile.objects.get(user=treb_id)
        user_profile.jornada_anual = jornada_anual
        user_profile.save()

    return redirect('treballador_any', treb_id, treb_codi, anyo)


@login_required
def index_extra(request):
    hores = Extra.objects.all()
    context = {'hores': hores}

    return render_to_response('planilles/hora_extra_index.html', context,
                              context_instance=RequestContext(request))


@login_required
def extra_extra(request, extra_codi=0):
    if int(extra_codi):
        extra = Extra.objects.get(id=extra_codi)
    else:
        extra = Extra()

    if request.method == 'POST':
        form = ExtraForm(request.POST, instance=extra)
        if form.is_valid():
            # convert checkboxes to decimalinteger
            extra.dies_numero = ','.join(request.POST.getlist('dies_setmana'))
            extra.save()
            return redirect('hores_extra')
    else:
        form = ExtraForm(instance=extra)

    context = {
        'extra_codi': extra_codi,
        'extra': extra,
        'form': form,
    }

    return render_to_response('planilles/hora_extra.html', context,
                              context_instance=RequestContext(request))


@login_required
def extra_delete(request, extra_codi=0):
    if int(extra_codi):
        Extra.objects.filter(id=extra_codi).delete()

    return redirect('hores_extra')


@login_required
def treballador_horaextra_delete(request, treb_id=0, treb_codi=0,
                                 anyo=datetime.now().year, extra_id=0):
    """
    La Hora Extra no s'esborra si està validada o exportada, se li aplica
    l'atribut corresponent
    """

    if int(extra_id):
        extra = ExtraTreballador.objects.get(id=extra_id)
        # Si ja ha estat validada la *marquem* com a esborrada
        if extra.data_validacio:
            extra.data_eliminacio = datetime.now()
            extra.save()
        else:
            extra.delete()

    return redirect('treballador_any', treb_id, treb_codi, anyo)


@login_required
def synchro_karat(request):

    resultat = -1
    # Si ees POST és que hem executat la synchro
    if request.method == 'POST':
        # result = sincronitza_karat_portal()
        resultat = 15  # Fake fake fake

    context = {
        'resultat': resultat,
    }

    return render_to_response('planilles/synchro_karat.html', context,
                              context_instance=RequestContext(request))


@login_required
def query_treballador_hora(request):

    tipus_hora = ''
    if request.method == 'POST':
        form = TrebHoraForm(request.POST)
        if form.is_valid():
            treballador = form.cleaned_data['treballador']
            try:
                dia = datetime.strptime(form.cleaned_data['dia'], '%d/%m/%Y')
                hora = datetime.strptime(form.cleaned_data['hora'], '%H:%M')
                tipus_hora = __get_treballador_tipus_hora(treballador, dia, hora)
            except:
                pass
    else:
        form = TrebHoraForm()

    context = {
        'form': form,
        'tipus_hora': tipus_hora
    }

    return render_to_response('planilles/query_treballador_hora.html', context,
                              context_instance=RequestContext(request))


def __get_treballador_tipus_hora(treb_id, dia, hora):
    """Retorna el tipus d'hora o False

        False -> Si no treballador/unitat/calendari
        'Ordinaria'
        'Extra'
        'No hora' --> Si é suna hora fora de qualsevol franja assignada
    """
    try:
        treballador = User.objects.get(id=treb_id)
        unitat = MetaUnitatIdi.objects.get(id=treballador.get_profile().unitat_idi_id)
    except:
        return False

    if unitat.calendari_laboral is not None:
        calendari = CalendariLaboral.objects.get(id=unitat.calendari_laboral.id)
    else:
        return False

    anyo = dia.year
    periodes = PeriodeTreballador.objects.filter(user=treballador)\
                                         .filter(data_inici__year=anyo)\
                                         .order_by('data_inici', 'id')

    moviments = MovimentTreballador.objects.filter(user=treballador)\
                                           .filter(dia__year=anyo)\
                                           .order_by('dia')

    extres = ExtraTreballador.objects.filter(user=treballador)\
                                     .filter(dia__year=anyo)\
                                     .filter(extra__tipus='E')\
                                     .order_by('dia')

    cal_obj = calendari.get_calendari(anyo)
    cal_data = calendari.get_any_resum(anyo)
    consolidat = []

    omplir_calendari(periodes, moviments, extres, cal_obj, cal_data, consolidat)

    dia_obj = cal_obj[dia.month-1][1][dia.day-1]

    hora = time(hour=hora.hour, minute=hora.minute)

    if dia_obj['hora_inici'] <= hora <= dia_obj['hora_fi']:
        return 'Ordinaria'
    elif dia_obj['hora_inici_extra'] <= hora <= dia_obj['hora_fi_extra']:
        return 'Extra'

    return 'No Hora'

def __save_entrada_calendari(request, treballador, unitat, anyo, calendari, periodes):
    form = TreballadorForm(unitat, request.POST)
    if form.is_valid():
        tipus_accio = form.cleaned_data['tipus_accio']
        seq_data_inici = form.cleaned_data['seq_data_inici']
        seq_data_fi = form.cleaned_data['seq_data_fi']
        reduccio = form.cleaned_data['reduccio']
        mov_hora_inici = form.cleaned_data['mov_hora_inici']
        mov_hora_final = form.cleaned_data['mov_hora_final']

        mov_comentari = form.cleaned_data['mov_comentari']
        extra_despla = form.cleaned_data['extra_despla']
        editor = request.user

        if not reduccio:
            reduccio = 0
        if not extra_despla:
            extra_despla = 0

        # TODO
        # Convertint dates amb martell, això s'ha de millorar
        try:
            data_inici = datetime.strptime(seq_data_inici, '%d-%m-%Y')
        except:
            data_inici = datetime.strptime('01-01-%s' % anyo, '%d-%m-%Y')

        try:
            data_fi = datetime.strptime(seq_data_fi, '%d-%m-%Y')
            permet_es_infinit = False
        except:
            data_fi = data_inici
            permet_es_infinit = True

        # La data final de periode no pot superar l'any de la data inicial
        if data_inici.year != data_fi.year:
            data_fi = datetime.strptime('31-12-%s' % anyo, '%d-%m-%Y')

        if tipus_accio == 'E' and form.cleaned_data['extra']:
            extra = form.cleaned_data['extra']
            dies = data_fi - data_inici
            missatge_error = 0
            for dia in xrange((dies.days) + 1):
                dia_i = data_inici + timedelta(days=dia)
                extra_treballador = ExtraTreballador(
                    user=treballador,
                    editor=editor,
                    tipus='E',  # E --> Explicita
                    dia=dia_i,
                    extra=extra,
                    hora_inici=mov_hora_inici,
                    hora_fi=mov_hora_final,
                    despla=extra_despla,
                    comentari=mov_comentari)
                extra_treballador.save()

        if tipus_accio == 'M' and form.cleaned_data['moviment']:
            moviment = form.cleaned_data['moviment']
            #  Si el moviment és es_infinit la data fi és 31/12/ANYO
            if moviment.es_infinit and permet_es_infinit:
                data_fi = datetime.strptime('31-12-%s' % anyo, '%d-%m-%Y')

            # Si es tracta d'aixecar un moviment la cosa va diferent
            # Primer cerquem el moviment que hi havia posat
            # per esborrar-l'ho fins a final d'any
            if moviment.es_terminator:
                moviment_original = None
                mto = MovimentTreballador.objects
                moviments_originals = mto.filter(dia=data_inici,
                                                 user=treballador)
                # Cerquem els que son BAIXA Médica
                for moviment in moviments_originals:
                    if moviment.moviment.codi in ['BM']:
                        moviment_original = moviment.moviment

                if moviment_original:
                    data_fi = datetime.strptime('31-12-%s' % anyo, '%d-%m-%Y')

                    mto.filter(dia__gt=data_inici,
                               dia__lte=data_fi,
                               moviment=moviment_original,
                               user=treballador)\
                       .delete()
            else:
                dies = data_fi - data_inici
                missatge_error = 0
                for dia in xrange((dies.days) + 1):
                    # Es crida a la funció get_dia
                    dia_i = data_inici + timedelta(days=dia)

                    # Si no és imputable sempre i el dia no és laborable... NYEEEC
                    if not moviment.es_imputable_sempre:
                        tipus = get_dia(treballador, dia_i, calendari, periodes)
                        if tipus['tipus'] != 'T':
                            missatge_error += 1
                            continue
                    # esborrem el moviment vell..
                    # try:
                    #     MovimentTreballador.objects.get(user=treballador,\
                    #                                     dia=dia_i)\
                    #                                 .delete()
                    # except MovimentTreballador.DoesNotExist:
                    #     pass

                    moviment_treballador = MovimentTreballador(
                        user=treballador,
                        editor=editor,
                        dia=dia_i,
                        moviment=moviment,
                        hora_inici=mov_hora_inici,
                        hora_fi=mov_hora_final,
                        comentari=mov_comentari)

                    moviment_treballador.save()

                if missatge_error:
                    messages.error(request, _('No s\'han assignat tots els moviments en el periode assignat, revisa la llista.'))

        if tipus_accio in ['R', 'S'] and (form.cleaned_data['rotacio'] or
                                          form.cleaned_data['sequencia']):
            if tipus_accio == 'R':
                rotacio = form.cleaned_data['rotacio']
                sequencies = [x.sequencia for x in rotacio.rotaciosequencia_set
                                                          .all()
                                                          .order_by('ordre')]
                # La sequencia inicial és un radio custom
                # es dona el index de la llista de sequencies
                # reordeno la llista de sequencies per agafar la primera
                # que ve del formulari
                #
                initial = int(request.POST.get('sequencia_inicial', 0))
                sequencies = (sequencies[initial:len(sequencies)] +
                              sequencies[0:initial])

            if tipus_accio == 'S':
                sequencies = [form.cleaned_data['sequencia']]
                rotacio = None

            # Quan s'entra un PeriodeTreballador s'han de crear els
            # FAPTreballador també
            # Primer entrem el Periode
            periode_treballador = PeriodeTreballador(
                user=treballador,
                editor=editor,
                data_inici=data_inici,
                data_fi=data_fi,
                rotacio=rotacio,
                # sequencia = sequencia,
                reduccio=reduccio)

            periode_treballador.save()

            for ordre, sequencia in enumerate(sequencies):
                periode_seq = RotacioAplicada(
                    periode=periode_treballador,
                    sequencia=sequencia,
                    ordre=ordre + 1  # Per tenir 1, 2, 3...
                )
                periode_seq.save()

            for periode_seq in periode_treballador.rotacioaplicada_set.all():
                for franja in periode_seq.sequencia.franjatemplate_set.all():
                    fap_treballador = FAPTreballador(
                        franja_template=franja,
                        dia_setmana=franja.dia_setmana,
                        hora_inici=franja.inici,
                        hora_fi=franja.fi,
                        sequencia=franja.sequencia,
                        periode_seq=periode_seq,
                        abrev_torn=franja.abrev_torn
                    )
                    fap_treballador.save()

    return
