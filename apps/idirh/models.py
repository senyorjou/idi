# coding: utf-8
#
import calendar
from datetime import timedelta, datetime, date, time
from itertools import chain
from django.utils.translation import ugettext_lazy as _
from django.db import models

from django.contrib.auth.models import User

from apps.idirh.utils import get_hores_en_format, daterange, interseccio_hores_mateix_dia, get_segons_dia_nit_mateix_dia
from django.db.models import Q

# Models de META que caldria importar,
# (es referancien directament per evitar imports circulars
# https://docs.djangoproject.com/en/dev/ref/models/fields/#foreignkey
# from apps.meta.models import MetaUnitatIdi

DIES_SETMANA = [_('Dilluns'),
                _('Dimarts'),
                _('Dimecres'),
                _('Dijous'),
                _('Divendres'),
                _('Dissabte'),
                _('Diumenge')]

MESOS = [_('Gener'), _('Febrer'), _('Mars'), _('Abril'), _('Maig'),
         _('Juny'), _('Juliol'), _('Agost'), _('Setembre'), _('Octubre'),
         _('Novembre'), _('Desembre')]

TIPUS_USUARI = [
    'INF',          # Informatics, acces root
    'RRHH',         # Recursos Humans , acces root refinat
    'CAP',          # Cap d'Unitat, acces root per unitat
    'USR'           # Luser, acces a 4 cosestes
]

TIPUS_DIA = (('L', _('Laborable')), ('F', _('Festiu')))

TIPUS_SEQ = (('P', _('Personal')), ('M', _('Maquina')))

ABREV_SEQ = (('M', _('Mati')), ('P', _('Torn Partit')),
             ('T', _('Tarda')), ('N', _('Nit')))

COLOR_SEQ = (('laboral', _('Laborable')),
             ('laboral-mati', _('Laborable Mati')),
             ('laboral-partit', _('Laborable Partit')),
             ('laboral-tarda', _('Laborable Tarda')),
             ('laboral-nit', _('Laborable Nit')),)

COLOR_LLETRA = (('#fff', _('Blanc')), ('#000', _('Negre')))

EXTRA_TIPUS = (('I', 'Implicit'), ('E', 'Explicit'))
EXTRA_COMPUT = (('H', 'Hores'), ('N', 'Nombre'))
EXTRA_VISTA = (('E', 'Escalar'), ('C', 'Calcul'))


class GrupUnitat(models.Model):
    """
    Grups per unitat
    """
    nom = models.CharField(max_length=150)
    descripcio = models.TextField(null=True, blank=True)
    unitat = models.ForeignKey('meta.MetaUnitatIdi')
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = u'plan_grups_unitat'
        ordering = ['nom']

    def __unicode__(self):
        return unicode(self.nom)


class Equip(models.Model):
    """Equip / Maquina, depenent de grup"""
    nom = models.CharField(max_length=150)
    descripcio = models.TextField(null=True, blank=True)
    grup = models.ForeignKey(GrupUnitat)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = u'plan_equip'
        ordering = ['nom']

    def __unicode__(self):
        return self.nom


class Rotacio(models.Model):
    """
        Conjunt de sequencies.
        Descripcio basica, per suport a classes relacionades
    """
    nom = models.CharField(max_length=150)
    unitat = models.ForeignKey('meta.MetaUnitatIdi')
    actiu = models.BooleanField(default=True)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = u'plan_rotacio'
        ordering = ['nom']

    def __unicode__(self):
        return self.nom

# Sequencia es el TornTemplate
class Sequencia(models.Model):
    """
    Conjunt de franjes, a client s'anomenen TORNS

    FK MetaUnitatIdi, donat que cada TORN és molt específic localment
    Cada una disposa d'un color, per al llistat de planilla setmanal
    """
    nom = models.CharField(max_length=150)
    rotacions = models.ManyToManyField(Rotacio, through='RotacioSequencia')
    tipus_seq = models.CharField(max_length=1, choices=TIPUS_SEQ)  # PM, Persona o Maquina
    unitat = models.ForeignKey('meta.MetaUnitatIdi')
    color = models.CharField(max_length=20, choices=COLOR_SEQ, default='laboral')
    actiu = models.BooleanField(default=True)
    # TEST, per assignar el tipus a la propia franja
    abrev_torn = models.CharField(max_length=1, choices=ABREV_SEQ, default='M')
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = u'plan_sequencia'
        ordering = ['nom']

    def __unicode__(self):
        return self.nom

    def get_hores_totals(self):
        """"""
        franges = self.franjatemplate_set.all()

        total_segons = 0
        for franja in franges:
            hora_inici = timedelta(hours=franja.inici.hour,
                                   minutes=franja.inici.minute)
            hora_fi = timedelta(hours=franja.fi.hour,
                                minutes=franja.fi.minute)
            total = hora_fi - hora_inici
            total_segons += total.seconds

        return total_segons

        # return get_hores_en_format(total_segons, format='llarg')

    def get_hores_format(self):
        return get_hores_en_format(self.get_hores_totals(), format='llarg')


class RotacioSequencia(models.Model):
    rotacio = models.ForeignKey(Rotacio)
    sequencia = models.ForeignKey(Sequencia)
    ordre = models.IntegerField(default=10, null=False, blank=False)
    #
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = u'plan_rotacio_sequencia'

    def __unicode__(self):
        return u'Rotacio %s, Sequencia %s ' %\
                (self.rotacio, self.sequencia)


class FranjaTemplate(models.Model):
    """
    Unitat minima de calendari
    """
    dia_setmana = models.IntegerField()
    inici = models.TimeField()
    fi = models.TimeField()
    sequencia = models.ForeignKey(Sequencia)
    abrev_torn = models.CharField(max_length=1, choices=ABREV_SEQ, default='M')
    #
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = u'plan_franja_template'
        ordering = ['dia_setmana']

    def __unicode__(self):
        return u'' + self.inici.strftime('%H:%M') + ' / '\
                   + self.fi.strftime('%H:%M')

    def get_seconds(self):
        inici = timedelta(hours=self.inici.hour,
                          minutes=self.inici.minute)
        fi = timedelta(hours=self.fi.hour,
                       minutes=self.fi.minute)

        return fi - inici


class LlocTreballGrup(models.Model):
    """
    FK's per definir les franjes horaries que integren
    un [lloc de treball d'especialista (grup cat)] / [grup (màquina)]
    Seqüencia a nivell informatiu
    """
    grup = models.ForeignKey(GrupUnitat)
    categoria = models.ForeignKey('meta.MetaGrupCategoriaProfessional')
    sequencia = models.ForeignKey(Sequencia)
    inici = models.DateField()
    fi = models.DateField(blank=True, null=True, default=None)
    #
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = u'plan_lloc_treball_grup'

    def __unicode__(self):
        return u'Grup:%s, Categoria:%s, Seqüencia: %s' %\
               (self.grup, self.categoria, self.sequencia)


class PeriodeTreballador(models.Model):
    """
    Periode aplicat a treballador.

    """
    user = models.ForeignKey(User)
    editor = models.ForeignKey(User, related_name='editorperiode')
    data_inici = models.DateField()
    data_fi = models.DateField(null=True)
    # sequencia = models.ForeignKey(Sequencia)
    rotacio = models.ForeignKey(Rotacio, null=True)
    sequencies = models.ManyToManyField(Sequencia, through='RotacioAplicada')
    # El camp es diu reducció però en realitat conté el % de jornada
    reduccio = models.DecimalField(default=0.0, decimal_places=2, max_digits=5)
    congelat = models.BooleanField(default=False)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        db_table = u'plan_periode_treballador'
        ordering = ['data_inici']

    def __unicode__(self):
        num_seq = len(self.sequencies.all())
        return u'Inici: %s, Fi: %s'\
               % (self.data_inici, self.data_fi)


class PeriodeEquip(models.Model):
    """
    Periode aplicat a Equip
    """
    equip = models.ForeignKey(Equip)
    data_inici = models.DateField()
    data_fi = models.DateField(null=True)
    sequencia = models.ForeignKey(Sequencia)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = u'plan_periode_equip'
        ordering = ['data_inici']

    def __unicode__(self):
        return u'Inici: %s, Fi: %s' % (self.data_inici, self.data_fi)

    def get_dies_periode(self):
        """
        Dies entre inici i fi, delta retorna la diferencia de dies,
        de manera que li hem de sumar 1
        """
        delta = self.data_fi - self.data_inici

        return delta.days + 1


class PeriodeEquipSequenciaPersonal(models.Model):
    periode_equip = models.ForeignKey(PeriodeEquip)
    categoria = models.ForeignKey('meta.MetaGrupCategoriaProfessional')
    personal = models.DecimalField(default='0.0', decimal_places=2, max_digits=5)

    class Meta:
        db_table = u'plan_periode_equip_sequencia__personal'
        ordering = ['periode_equip']

    def __unicode__(self):
        return u'Periode: %s, Cat: %s, Px: %s' %\
                (self.periode_equip, self.categoria, self.personal)

    def get_hores_periode_categoria(self):
        """
        Retorna string amb el nombre d'hores treballades
        Internament es calculen els segons iterant sobre el periode
        i sumant segons treballats en cada franja diaria
        """
        franges = [0] * 7
        for franja in self.periode_equip.fapequip_set.all():
            franges[franja.dia_setmana] += franja.get_seconds().seconds

        segons = 0
        for dia in daterange(self.periode_equip.data_inici, self.periode_equip.data_fi, end=True):
            segons += franges[dia.weekday()]

        return get_hores_en_format(segons, 'llarg')


class RotacioAplicada(models.Model):
    sequencia = models.ForeignKey(Sequencia)
    periode = models.ForeignKey(PeriodeTreballador)
    editat = models.BooleanField(default=False)
    ordre = models.IntegerField(default=1)

    class Meta:
        db_table = u'plan_periode_sequencies'
        ordering = ['ordre']

    def __unicode__(self):
        return u'Periode:%s, Sequencia:%s' % (self.periode, self.sequencia)

    def get_franges(self, weekday):
        fapt = self.faptreballador_set.all()
        fapt = fapt.filter(dia_setmana=weekday)
        return list(fapt)

    # Retorna la quantitat de segons que intersecciona un rang horari
    # en un dia d'un periode
    def segons_interseccio_mateix_dia(self, weekday, hora_inici, hora_fi):
        if not hora_inici or not hora_fi or weekday > 6:
            raise ValueError("Parametres invàlids")

        segons_dia = segons_nit = 0
        fsapt = self.get_franges(weekday)

        for f in fsapt:
            hinici, hfi = interseccio_hores_mateix_dia(f.hora_inici, f.hora_fi, hora_inici, hora_fi)
            if hinici and hfi:
                sdia, snit = get_segons_dia_nit_mateix_dia(hinici, hfi)
                segons_dia += sdia
                segons_nit += snit

        return segons_dia, segons_nit

    # Retorna la quantitat de segons que estan fora del torn d'un dia de la
    # setmana en un rang horari
    def get_segons_foratorn(self, weekday, hora_inici, hora_fi):
        if not hora_inici or not hora_fi:
            raise ValueError("Paràmetres invàlids")

        if hora_fi >= hora_inici:
            segons_inter_dia, segons_inter_nit = self.segons_interseccio_mateix_dia(
                                weekday, hora_inici, hora_fi)
        else:
            # El moviment dona la volta
            ultima_hora_nit = datetime.time(23, 59)
            primera_hora_dia = datetime.time(0, 0)
            segons_inter_dia, segons_inter_nit = self.segons_interseccio_mateix_dia(
                                weekday, hora_inici, ultima_hora_nit)
            si_dia, si_nit = self.segons_interseccio_mateix_dia(
                                weekday, primera_hora_dia, hora_fi)
            segons_inter_dia += si_dia
            segons_inter_nit += si_nit

        return segons_inter_dia, segons_inter_nit


class FAPTreballador(models.Model):
    """
    Franja Aplicada per Treballador
    """
    # TODO: Aquest camp lia, no caldria.
    #periode_treballador = models.ForeignKey(PeriodeTreballador)
    franja_template = models.ForeignKey(FranjaTemplate, blank=True, null=True, on_delete=models.SET_NULL)
    dia_setmana = models.IntegerField()
    hora_inici = models.TimeField()
    hora_fi = models.TimeField()
    sequencia = models.ForeignKey(Sequencia, null=True, on_delete=models.SET_NULL)
    periode_seq = models.ForeignKey(RotacioAplicada)
    abrev_torn = models.CharField(max_length=1, choices=ABREV_SEQ, default='M')
    #
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = u'plan_franja_aplicada_treballador'
        ordering = ['dia_setmana']

    def __unicode__(self):
        return u'Inici: %s, Fi: %s' % (self.hora_inici, self.hora_fi)

    def get_seconds(self):
        inici = timedelta(hours=self.hora_inici.hour,
                          minutes=self.hora_inici.minute)
        fi = timedelta(hours=self.hora_fi.hour,
                       minutes=self.hora_fi.minute)

        return fi - inici

    def get_seconds_nit(self):
        debug = False  # Yes, I know...
        ret_val = 0
        dia = 1

        # La franja nocturna esta definida aquí a manija
        # delta = timedelta(0, inici_fals * 60 * 60)

        # if self.hora_inici.hour < inici_fals:
        #     dia += 1
        inici = datetime(1970, 1, dia,
                         self.hora_inici.hour,
                         self.hora_inici.minute)

        
        if self.hora_fi.hour < self.hora_inici.hour and self.hora_inici.hour != 0:
            dia += 1

        final = datetime(1970, 1, dia,
                         self.hora_fi.hour,
                         self.hora_fi.minute)

        # if self.hora_fi.hour < self.hora_inici.hour:
        #     print 'Its zerozero', inici, final


        # Inici / fi franja nocturma
        nit_inici = datetime(1970, 1, 1, 22, 0)
        nit_final = datetime(1970, 1, 2, 8, 0)

        if inici <= nit_inici and final >= nit_final:
            if debug:
                print '1->',
            ret_val = (timedelta(hours=final.hour, minutes=final.minute) -
                       timedelta(hours=inici.hour, minutes=inici.minute))

            ret_val = nit_final - nit_inici

        elif final > nit_inici and inici < nit_inici:
            if debug:
                print '2->',
            ret_val = (timedelta(hours=final.hour, minutes=final.minute) -
                       timedelta(hours=nit_inici.hour, minutes=nit_inici.minute))

        elif inici >= nit_inici and final <= nit_final:
            if debug:
                print '3->',
            ret_val = (timedelta(hours=final.hour, minutes=final.minute) -
                       timedelta(hours=inici.hour, minutes=inici.minute))

        elif inici <= nit_final and inici >= nit_inici and final >= nit_final:
            if debug:
                print '4->',
            ret_val = (timedelta(hours=nit_final.hour, minutes=nit_final.minute) -
                       timedelta(hours=inici.hour, minutes=inici.minute))

        elif inici >= nit_inici and final >= nit_final:
            if debug:
                print '5->',
            ret_val = (timedelta(hours=nit_final.hour, minutes=nit_final.minute) -
                       timedelta(hours=inici.hour, minutes=inici.minute))

        # print inici, final, nit_inici, nit_final, ret_val
        if debug and type(ret_val) is not int:
            print inici, final, ret_val.seconds / 60 / 60, ret_val

        if ret_val:
            segons = ret_val.seconds
        else:
            segons = ret_val
        
        # Si no hi ha almenys una hora nocturna, no es considera com a tal
        if segons >= 60:
            return segons
        else:
            return 0


class FAPEquip(models.Model):
    """
    Franja Aplicada per Equip
    """
    periode_equip = models.ForeignKey(PeriodeEquip)
    franja_template = models.ForeignKey(FranjaTemplate)
    dia_setmana = models.IntegerField()
    hora_inici = models.TimeField()
    hora_fi = models.TimeField()
    sequencia = models.ForeignKey(Sequencia,
                                  limit_choices_to={'tipus_seq': 'M'})
    #
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = u'plan_franja_aplicada_equip'
        ordering = ['dia_setmana']

    def __unicode__(self):
        return u'Dia: %s, Inici: %s - Fi: %s' %\
               (self.dia_setmana, self.hora_inici, self.hora_fi)

    def get_seconds(self):
        inici = timedelta(hours=self.hora_inici.hour,
                          minutes=self.hora_inici.minute)
        fi = timedelta(hours=self.hora_fi.hour,
                       minutes=self.hora_fi.minute)

        return fi - inici


class FAPMaquina(models.Model):
    """
    Franja Aplicada per Màquina
    """
    franja_template = models.ForeignKey(FranjaTemplate)
    dia_setmana = models.IntegerField()
    inici = models.TimeField()
    fi = models.TimeField()
    sequencia = models.ForeignKey(Sequencia,
                                  limit_choices_to={'tipus_seq': 'M'})
    lloc_treball_grup = models.ForeignKey(LlocTreballGrup)
    #
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = u'plan_franja_aplicada_maquina'
        ordering = ['dia_setmana']

    def __unicode__(self):
        return self.lloc_treball_grup


class CalendariLaboral(models.Model):
    """
    Calendari de dies laborables.
    """
    nom = models.CharField(max_length=150)
    es_mestre = models.BooleanField(default=False)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = u'plan_calendari_laboral'
        ordering = ['nom']

    def __unicode__(self):
        return self.nom

    def get_anys_en_curs(self):
        """
        Retorna un iter amb els anys que contenen dies a la taula diacalendari
        """
        return self.diacalendari_set.all().order_by('-dia')\
                                          .dates('dia', 'year')

    def get_any_resum(self, anyo):
        """
        Retorna un dict de dades informatives sobre l'any consolidat amb l'any
        mestre
        """
        bisest = False
        dies_any = 365
        dies_laborables = dies_festius = 0
        dies_vacances = 22
        dies_lleure = 3

        if calendar.isleap(anyo):
            dies_any += 1
            bisest = True
        try:
            if not self.es_mestre:
                cal_mestre = CalendariLaboral.objects.filter(es_mestre=1)[0]
                dies_laborables += cal_mestre.get_any_resum(anyo)['laborables']
                dies_festius += cal_mestre.get_any_resum(anyo)['festius']
        except:
            pass

        dies_festius += self.diacalendari_set.filter(tipus_dia='F')\
                                             .filter(dia__year=anyo)\
                                             .count()
        # calculo els dies de weekend a la python
        data_inicial = date(anyo, 1, 1)
        data_final = date(anyo, 12, 31)

        daygenerator = (data_inicial + timedelta(x + 1) for x in xrange((data_final - data_inicial).days))
        weekends = sum(1 for day in daygenerator if day.weekday() > 4)

        dies_laborables = dies_any - dies_festius

        dies_descans = weekends + dies_festius + dies_vacances + dies_lleure

        data = {
            'bisest': bisest,
            'laborables': dies_laborables,
            'festius': dies_festius,
            'weekends': weekends,
            'dies_any': dies_any,
            'dies_descans': dies_descans,
            'dia_1_de_gener_gregoria': datetime.toordinal(data_inicial)
        }

        return data

    def get_calendari_llista(self, anyo):
        """
        Retona un queryset amb els dies festius consolidats amb l'any mestre
        """
        total_dies = dies = self.diacalendari_set.filter(dia__year=anyo)\
                                                 .order_by('dia')
        if not self.es_mestre:
            calendari_mestre = CalendariLaboral.objects.filter(es_mestre=True)
            if (calendari_mestre):
                dies_mestre = calendari_mestre[0].diacalendari_set\
                                                 .filter(dia__year=anyo)\
                                                 .order_by('dia')
                total_dies = list(chain(dies, dies_mestre))

        return total_dies

    def get_diesFestius(self, data_inici, data_fi):
        diesFestius = DiaCalendari.objects.filter(dia__range=(data_inici, data_fi))

        if not self.es_mestre:
            diesFestius = diesFestius.filter(Q(calendari__metaunitatidi__calendari_laboral=self)|
                         Q(calendari__es_mestre=True))

        return diesFestius

    def get_calendari(self, anyo):
        """
        Retorna una llista tots els objectes dia dins un diccionari que conte
        mes atributs, per mostrar en format de any:
           mes | dia 1 | dia 2 | dia...

        calendari = {
            tipus = 'W', '0', ...
            objdia = DiaCalendari()
        }
        """
        calendari = []
        i = 0
        for mes in MESOS:
            calendari.append([])
            calendari[i].append(mes)
            calendari[i].append([x for x in xrange(1, 32)])
            i += 1

        mes_i = 1
        for mes in calendari:
            for dia in mes[1]:
                # Priemr generem la data
                try:
                    dia_data = datetime.date(datetime(anyo, mes_i, dia))
                except ValueError:
                    dia_data = None
                # Creem un objecte DiaCalendari, per usar els seus atribus
                # I ho posem en un dict per disposar d'atributs extra
                dict_dia = {
                    'obj': DiaCalendari(dia=dia_data),
                    'tipus': '',
                }
                if dia_data:
                    # Cap de setmana
                    if (calendar.weekday(dia_data.year, dia_data.month, dia_data.day) in (5, 6)):
                        dict_dia['tipus'] = 'W'
                        dict_dia['tooltip'] = dia_data.strftime('%d de %B de %Y')\
                                              + '<br />'\
                                              + '<strong>Cap de Setmana</strong>'
                else:
                    dict_dia['tipus'] = '0'

                calendari[mes_i - 1][1][dia - 1] = dict_dia
            mes_i += 1

        total_dies = dies = self.diacalendari_set.filter(dia__year=anyo)

        if not self.es_mestre:
            calendari_mestre = CalendariLaboral.objects.filter(es_mestre=True)
            if (calendari_mestre):
                dies_mestre = calendari_mestre[0].diacalendari_set.filter(dia__year=anyo)
                total_dies = list(chain(dies, dies_mestre))

        # Agafem els dies i els inserim sobre l'array creat
        for dia in total_dies:
            if not self.es_mestre and dia.calendari == self:
                tipus = 'FL'
            else:
                tipus = 'F'
            dict_dia = {
                'obj': dia,
                'tipus': tipus,
                'tooltip': dia.dia.strftime('%d de %B de %Y') +\
                           '<br />' +\
                           '<strong>' + dia.nom + '</strong>'
            }
            calendari[dia.dia.month - 1][1][dia.dia.day - 1] = dict_dia

        return calendari


class DiaCalendari(models.Model):
    """docstring for DiaCalendari"""
    dia = models.DateField()
    calendari = models.ForeignKey(CalendariLaboral)
    nom = models.CharField(max_length=150)
    tipus_dia = models.CharField(max_length=1, choices=TIPUS_DIA)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = u'plan_dia_calendari'

    def __unicode__(self):
        return unicode(self.dia)


class Maquina(models.Model):
    """docstring for Maquina"""
    nom = models.CharField(max_length=150)
    grup = models.ForeignKey(GrupUnitat)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = u'plan_maquina'

    def __unicode__(self):
        return self.nom


class PeriodeMaquina(models.Model):
    """docstring for PeriodeMaquina"""
    maquina = models.ForeignKey(Maquina)
    sequencia = models.ForeignKey(Sequencia)
    data_inici = models.DateField()
    data_fi = models.DateField(blank=True, null=True, default=None)
    personal = models.IntegerField(default=1)
    #
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'plan_periode_maquina'

    def __unicode__(self):
        return 'Seqüencia %s, des de %s fins a %s' %\
                (self.sequencia, self.inici, self.fi)


class Moviment(models.Model):
    """
    Moviment de persona
    """
    nom = models.CharField(max_length=50)
    codi = models.CharField(max_length=2, default='T')
    # tipus_mov = models.CharField(max_length=1, choices=TIPUS_SEQ, default='P')
    color = models.CharField(max_length=20, default='#009900')
    color_text = models.CharField(max_length=10, choices=COLOR_LLETRA, default='#fff')
    imputa_hores = models.BooleanField(default=False)
    #
    es_computable = models.BooleanField(default=True)
    es_jornadaefectiva_afegida = models.BooleanField(default=False)
    es_excedencia = models.BooleanField(default=False)
    es_jornada = models.BooleanField(default=False)
    es_imputable_sempre = models.BooleanField(default=False)
    es_terminator = models.BooleanField(default=False)  # Elimina moviments identics
    es_any_passat = models.BooleanField(default=False)
    es_infinit = models.BooleanField(default=False)
    es_idi = models.BooleanField(default=False)
    es_ics = models.BooleanField(default=False)
    #
    calcul = models.CharField(max_length=50)
    text_suport = models.TextField(blank=True)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'plan_moviment'
        ordering = ['nom']

    def __unicode__(self):
        return self.nom

    def es_vacances(self):
        if not self.es_computable and\
           not self.es_excedencia and\
           not self.es_jornada:  # and self.codi in ('V', 'LL', 'VP'):
            return True

    def ambit(self):
        """
        Retorna un string amb IDI, ICS depenent del cas
        """
        ret_str = '<span class="muted">--</span>'
        if self.es_idi:
            ret_str = 'IDI'
        if self.es_ics:
            if self.es_idi:
                ret_str += ', ICS'
            else:
                ret_str = 'ICS'

        return ret_str


class DiaCalendariTreballador():
    def __init__(self, user, data, rotacioTreballador,
                 movimentsTreballador, diaFestiu, extresTreballador, 
                 segons_conveni_x_dia, rati_conveni_nocturn):
        self.user = user
        self.data = data
        self.rotacio = rotacioTreballador
        self.movimentsTreballador = movimentsTreballador
        self.extres = extresTreballador
        self.segons_conveni_x_dia = segons_conveni_x_dia
        self.rati_conveni_nocturn = rati_conveni_nocturn
        self.diaFestiu = diaFestiu

    # Retorna els segons del torn ignorant els moviments i altres
    def get_segons_torn(self):
        # Si és dia festiu, el torn s'anula i només queden els moviments.
        if self.diaFestiu:
            return 0, 0

        fsapt = self.rotacio.get_franges(self.data.weekday())
        segons_dia = segons_nit = 0

        for f in fsapt:
            if f.abrev_torn == 'N':
                segons_nit += f.get_seconds().seconds
            else:
                segons_dia += f.get_seconds().seconds

        return segons_dia, segons_nit

    def get_segons_efectius(self):
        segons_dia_torn, segons_nit_torn = self.get_segons_torn()
        segons_dia_moviment = segons_nit_moviment = 0
        for mt in self.movimentsTreballador:
            if mt.moviment.es_excedencia:
                # Zero segons efectius i deixem de mirar moviments
                segons_dia_torn = segons_nit_torn = 0
                break
            if mt.moviment.es_any_passat:
                # Aquest any no ens implica en res
                continue
            if mt.moviment.es_terminator:
                # Moviment que no afecta al calcul ja que no es crea
                continue
            # Es jornada efectiva
            if mt.moviment.es_computable:
                # Es canvi de jornada
                if mt.moviment.es_jornada:
                    segons_dia_torn = segons_nit_torn = 0
                    segons_dia_moviment += mt.get_segons()
                    segons_nit_moviment += mt.get_segons_nit()
                elif mt.moviment.imputa_hores and mt.moviment.es_jornadaefectiva_afegida:
                    # El moviment ha de sumar les hores que es passen de la jornada
                    # que té la persona aquell dia
                    segons_inter_dia = segons_inter_nit = 0
                    if self.rotacio and mt.hora_inici and mt.hora_fi:
                        segons_inter_dia, segons_inter_nit = self.rotacio.get_segons_foratorn(
                                                    self.data.weekday(), mt.hora_inici, mt.hora_fi)

                    segons_dia_moviment += mt.get_segons() - segons_inter_dia
                    segons_nit_moviment += mt.get_segons_nit() - segons_inter_nit
                else:
                    # Com que el moviment és computable, no hem de restar res.
                    pass
            # No és jornada efectiva
            else:
                # Es canvi de jornada
                if mt.moviment.es_jornada:
                    segons_dia_torn = segons_nit_torn = 0
                elif mt.moviment.imputa_hores:
                    # El moviment no és jornada efectiva de manera
                    # que hem de restar les hores que estan dins la jornada
                    # que té la persona aquell dia
                    segons_inter_dia = segons_inter_nit = 0
                    if self.rotacio and mt.hora_inici and mt.hora_fi:
                        segons_inter_dia, segons_inter_nit = self.rotacio.get_segons_foratorn(
                                                    self.data.weekday(), mt.hora_inici, mt.hora_fi)

                    segons_dia_moviment -= segons_inter_dia
                    segons_nit_moviment -= segons_inter_nit
                else:
                    # No és jornada i no s'imputa en hores per tant no li ha de
                    # computar tot el propi torn.
                    segons_dia_torn = segons_nit_torn = 0

        return segons_dia_torn + segons_dia_moviment, segons_nit_torn + segons_nit_moviment

    # TODO: Falta aplicar els segons de moviments any passat
    def get_segons_conveni(self):
        segons_dia, segons_nit = self.get_segons_efectius()
        pes_jornada = 1
        if self.rotacio.periode.reduccio != 0:
            pes_jornada = float(self.rotacio.periode.reduccio)/100

        return self.segons_conveni_x_dia * pes_jornada - self.rati_conveni_nocturn * segons_nit


# Classe asellares per abstreure tota la lògica de les vistes.
class CalendariTreballador():
    def __init__(self, user, data_inici, data_fi, congelat=False):
        self.user = user
        self.data_inici = data_inici
        self.data_fi = data_fi

        # TODO: Caldria agafar el calendari laboral de la unitat
        # en què estava el treballador en aquella data.
        unitat = user.get_profile().unitat_idi
        jornada_anual = user.get_profile().jornada_anual

        self.festius = unitat.calendari_laboral.get_diesFestius(
                            data_inici, data_fi)

        self.periodes = PeriodeTreballador.objects.filter(user=user
                            ).filter(
                                Q(data_inici__range=(data_inici, data_fi))
                                   |
                                Q(data_fi__range=(data_inici, data_fi))
                                   |
                                (Q(data_inici__lte=data_inici) & Q(data_fi__gte=data_fi))
                            ).order_by('data_inici')

        self.moviments = MovimentTreballador.objects.filter(user=user
                            ).filter(dia__range=(data_inici, data_fi)
                            ).filter(congelat=congelat
                            ).order_by('dia')

        self.extres = ExtraTreballador.objects.filter(user=user
                            ).filter(dia__range=(data_inici, data_fi)
                            ).order_by('dia')

        if congelat:
            self.periodes = self.periodes.filter(congelat=True)
            self.moviments = self.moviments.filter(congelat=True)
            # TODO: Faltaria fer algo similar al congelat=True

        # TODO: No fer servir 365. Calcular els dies de l'any en concret tenint
        # en compte que es pot inicialitzar el calendari amb dues dates podent 
        # ser entre dos anys diferents
        segons_conveni_x_dia = float(jornada_anual.hores * 60 * 60 / 365)
        rati_conveni_nocturn = float(jornada_anual.hores / jornada_anual.hores_nit) - 1

        self.dies = {}
        # TODO: Aquesta generació es pot fer d'una manera molt més òptima
        # només cal no fer un select per buscar el període de cada dia
        # sinó generant-los seqüencialment.
        for data in daterange(self.data_inici, self.data_fi, end=True):
            try:
                periodesTreballador = self.periodes.filter(data_inici__lte=data,
                                                          data_fi__gte=data
                                                          ).order_by('-created_at')[0]

                rotacioTreballador = []
                if periodesTreballador:
                    rotacions = periodesTreballador.rotacioaplicada_set.all(
                                                ).order_by('ordre')
                    setmana = data.isocalendar()[1] - 1
                    rotacioTreballador = rotacions[setmana % len(rotacions)]
                movimentsTreballador = self.moviments.filter(dia=data)
                extresTreballador = self.extres.filter(dia=data)

                diaFestiu = None
                for dia in self.festius.filter(dia=data):
                    diaFestiu = dia
                    # suposo que només n'hi trobarà un
                    break

                self.dies[str(data)] = DiaCalendariTreballador(user, data,
                    rotacioTreballador, movimentsTreballador, diaFestiu,
                    extresTreballador, segons_conveni_x_dia,
                    rati_conveni_nocturn)
            except Exception, e:
                pass
        a = 1

    # TODO: Opció congelat
    def get_segons_torn(self, data_inici, data_fi):
        if self.data_inici > data_inici or self.data_fi < data_fi:
            raise ValueError("Dates fora del rang en què s'ha inicialitzat el calendari")

        segons_dia = segons_nit = 0
        for data in daterange(data_inici, data_fi, end=True):
            # TODO: Mirar perquè a vegades no troba la clau. Hauria de ser-hi
            # sermpre
            try:
                segons_torn = self.dies[str(data)].get_segons_torn()
                segons_dia += segons_torn[0]
                segons_nit += segons_torn[1]
            except:
                pass

        return segons_dia, segons_nit

    # TODO: Opció congelat
    def get_segons_conveni(self, data_inici, data_fi):
        if self.data_inici > data_inici or self.data_fi < data_fi:
            raise ValueError("Dates fora del rang en què s'ha inicialitzat el calendari")

        segons_conveni = 0
        for data in daterange(data_inici, data_fi, end=True):
            try:
                segons_conveni += self.dies[str(data)].get_segons_conveni()
            except KeyError:
                pass

        return segons_conveni

    # TODO: Opció congelat
    def get_segons_efectius(self, data_inici, data_fi):
        if self.data_inici > data_inici or self.data_fi < data_fi:
            raise ValueError("Dates fora del rang en què s'ha inicialitzat el calendari")

        segons_efectius_dia = segons_efectius_nit = 0
        for data in daterange(data_inici, data_fi, end=True):
            try:
                segons_dia, segons_nit = self.dies[str(data)].get_segons_efectius()
                segons_efectius_dia += segons_dia
                segons_efectius_nit += segons_nit
            except KeyError:
                pass

        return segons_efectius_dia, segons_efectius_nit


class MovimentTreballador(models.Model):
    """"""
    user = models.ForeignKey(User)
    editor = models.ForeignKey(User, related_name='editormoviment')
    dia = models.DateField()
    hora_inici = models.TimeField(blank=True, null=True, default=None)
    hora_fi = models.TimeField(blank=True, null=True, default=None)
    all_day = models.BooleanField(default=True)
    comentari = models.TextField(blank=True)
    # tipus = models.CharField(max_length=2)
    moviment = models.ForeignKey(Moviment, on_delete=models.PROTECT)
    congelat = models.BooleanField(default=False)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        db_table = 'plan_moviment_treballador'

    def __unicode__(self):
        return 'Usuari %s, moviment %s dia %s'\
                % (self.user, self.moviment, self.dia)

    def get_dia_format(self):
        return self.dia.strftime('%d de %B de %Y')

    def get_franja_horaria_format(self):
        # Les 00:00 es considera NULL, WTF!!!
        if ((self.hora_inici or self.hora_inici == time(0, 0)) and (self.hora_fi or self.hora_fi == time(0, 0))):
            return (self.hora_inici.strftime('%H:%M') + ' - ' +
                    self.hora_fi.strftime('%H:%M'))

        return ''

    def get_segons(self):
        """
        Construeixo dues dates falses per calcular el periode en segons
        entre ambdues.
        data1 - data2 = timedelta (en segons)
        """
        if (self.hora_inici and self.hora_fi):
            dia_fals = 1
            if self.hora_fi <= self.hora_inici:
                dia_fals = 2

            inici = datetime(1970, 1, 1, self.hora_inici.hour, self.hora_inici.minute)
            fi = datetime(1970, 1, dia_fals, self.hora_fi.hour, self.hora_fi.minute)

            delta = fi - inici

            return delta.seconds
        return 0

    def get_segons_nit(self):
        if (self.hora_inici and self.hora_fi):
            debug = False  # Yes, I know...
            ret_val = 0
            dia = 1

            # La franja nocturna esta definida aquí a manija
            # delta = timedelta(0, inici_fals * 60 * 60)

            # if self.hora_inici.hour < inici_fals:
            #     dia += 1
            inici = datetime(1970, 1, dia,
                             self.hora_inici.hour,
                             self.hora_inici.minute)


            if self.hora_fi.hour < self.hora_inici.hour and self.hora_inici.hour != 0:
                dia += 1

            final = datetime(1970, 1, dia,
                             self.hora_fi.hour,
                             self.hora_inici.minute)

            # if self.hora_fi.hour < self.hora_inici.hour:
            #     print 'Its zerozero', inici, final


            # Inici / fi franja nocturma
            nit_inici = datetime(1970, 1, 1, 22, 0)
            nit_final = datetime(1970, 1, 2, 8, 0)

            if inici <= nit_inici and final >= nit_final:
                if debug:
                    print '1->',
                ret_val = (timedelta(hours=final.hour, minutes=final.minute) -
                           timedelta(hours=inici.hour, minutes=inici.minute))

                ret_val = nit_final - nit_inici

            elif final > nit_inici and inici < nit_inici:
                if debug:
                    print '2->',
                ret_val = (timedelta(hours=final.hour, minutes=final.minute) -
                           timedelta(hours=nit_inici.hour, minutes=nit_inici.minute))

            elif inici >= nit_inici and final <= nit_final:
                if debug:
                    print '3->',
                ret_val = (timedelta(hours=final.hour, minutes=final.minute) -
                           timedelta(hours=inici.hour, minutes=inici.minute))

            elif inici <= nit_final and inici >= nit_inici and final >= nit_final:
                if debug:
                    print '4->',
                ret_val = (timedelta(hours=nit_final.hour, minutes=nit_final.minute) -
                           timedelta(hours=inici.hour, minutes=inici.minute))

            elif inici >= nit_inici and final >= nit_final:
                if debug:
                    print '5->',
                ret_val = (timedelta(hours=nit_final.hour, minutes=nit_final.minute) -
                           timedelta(hours=inici.hour, minutes=inici.minute))

            # print inici, final, nit_inici, nit_final, ret_val
            if debug and type(ret_val) is not int:
                print inici, final, ret_val.seconds / 60 / 60, ret_val

            if ret_val:
                segons = ret_val.seconds
            else:
                segons = ret_val

            # Si no hi ha almenys una hora nocturna, no es considera com a tal
            if segons >= 60:
                return segons
            else:
                return 0
        return 0


class JornadaAnual(models.Model):
    """
    Nombre d'hores que fa cada personal
    """
    nom = models.CharField(max_length=150)
    hores = models.DecimalField(max_digits=6, decimal_places=1)
    hores_nit = models.DecimalField(max_digits=6, decimal_places=1)
    actiu = models.BooleanField(default=True)
    es_default = models.BooleanField(default=False)
    dies_vacances = models.PositiveSmallIntegerField(default=0)
    dies_vacances_nit = models.PositiveSmallIntegerField(default=0)
    dies_lleure = models.PositiveSmallIntegerField(default=0)
    dies_lleure_nit = models.PositiveSmallIntegerField(default=0)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta():
        db_table = 'plan_jornada_anual'

    def __unicode__(self):
        return u'%s, hores: %s' % (self.nom, self.hores)


class Incidencia(models.Model):
    """
    Incidencies d'Equip
    """
    nom = models.CharField(max_length=50)
    codi = models.CharField(max_length=2, default='T')
    color = models.CharField(max_length=20, default='#009900')
    color_text = models.CharField(max_length=10, choices=COLOR_LLETRA, default='#fff')
    text_suport = models.TextField(blank=True)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta():
        db_table = 'plan_incidencia'

    def __unicode__(self):
        return self.nom


class IncidenciaEquip(models.Model):
    """docstring for IncidenciaEquip"""
    equip = models.ForeignKey(Equip)
    dia = models.DateField()
    hora_inici = models.TimeField(blank=True, null=True, default=None)
    hora_fi = models.TimeField(blank=True, null=True, default=None)
    comentari = models.TextField(blank=True)
    incidencia = models.ForeignKey(Incidencia, on_delete=models.PROTECT)

    class Meta():
        db_table = 'plan_incidencia_equip'

    def __unicode__(self):
        return 'Usuari %s, moviment %s dia %s'\
                % (self.user, self.tipus, self.dia)


class Extra(models.Model):
    """"""
    codi = models.CharField(max_length=4, null=False)
    nom = models.CharField(max_length=150, null=True)
    descripcio = models.CharField(max_length=250, null=True)
    tipus = models.CharField(max_length=1, choices=EXTRA_TIPUS, default='I')
    comput = models.CharField(max_length=1, choices=EXTRA_COMPUT, default='H')
    preu = models.IntegerField(default=1)
    vista = models.CharField(max_length=1, choices=EXTRA_VISTA, default='E')  # E = Escalar, C = Calcul
    ordre_excel = models.IntegerField(default=1)  # S'ha de treure ordenat per al excel
    # Camps per la fórmula
    dies_numero = models.CommaSeparatedIntegerField(max_length=20, blank=True, null=True)
    es_dia_especial = models.BooleanField(default=False)
    hora_inici = models.TimeField(blank=True, null=True, default=None)
    hora_fi = models.TimeField(blank=True, null=True, default=None)
    festiu_laboral = models.CharField(max_length=1, choices=TIPUS_DIA, blank=True, default='L')
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        db_table = 'plan_extraordinaries'
        ordering = ['ordre_excel']

    def __unicode__(self):
        return u'%s: %s' % (self.codi, self.nom)


class ExtraTreballador(models.Model):
    """
    tipus = Implicit / Explicit
    """
    user = models.ForeignKey(User)
    editor = models.ForeignKey(User, related_name='editorextra')
    tipus = models.CharField(max_length=1, default='I')
    dia = models.DateField()
    hora_inici = models.TimeField(blank=True, null=True, default=None)
    hora_fi = models.TimeField(blank=True, null=True, default=None)
    despla = models.IntegerField(default=0)
    comentari = models.TextField(blank=True)
    extra = models.ForeignKey(Extra, on_delete=models.PROTECT)
    congelat = models.BooleanField(default=False)
    # Dates per l'algoritme de seleccio passada
    data_validacio = models.DateField(null=True, blank=True)
    data_exportacio = models.DateField(null=True, blank=True)
    data_eliminacio = models.DateField(null=True, blank=True)
    data_exportacio_eliminacio = models.DateField(null=True, blank=True)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        db_table = 'plan_exraordinaria_treballador'

    def __unicode__(self):
        return u'Usuari %s, extra %s dia %s'\
               % (self.user, self.extra, self.dia)

    def get_dia_format(self):
        return self.dia.strftime('%d de %B de %Y')

    def get_hora_list(self):
        if self.hora_inici:
            return u'%s - %s' %\
                   (self.hora_inici.strftime('%H:%M'),
                    self.hora_fi.strftime('%H:%M'))
        return u''

    def get_extra_tooltip(self):
        hora_inici = self.hora_inici.strftime('%H:%M') if self.hora_inici else ''
        hora_fi = self.hora_fi.strftime('%H:%M') if self.hora_fi else ''

        if hora_inici:
            return u'%s - %s - %s' % (
                   hora_inici, hora_fi, self.extra.nom)
        return u'%s' % self.extra.nom

    def get_hores_totals(self):
        """
        Retorna segons entre dues hores,
        si el moviment.extra.comput és tipus HORA
        """
        if self.extra.comput == 'H':
            hora_inici = timedelta(hours=self.hora_inici.hour,
                                   minutes=self.hora_inici.minute)
            hora_fi = timedelta(hours=self.hora_fi.hour,
                                minutes=self.hora_fi.minute)
            total = hora_fi - hora_inici

            return total.seconds
        else:
            return 0


class ExtraTreballadorImpMensual(models.Model):
    user = models.ForeignKey(User)
    editor = models.ForeignKey(User, related_name='editorextramensual')
    mes = models.IntegerField(default=1)
    data_mes = models.DateField()
    qty = models.IntegerField(default=0)
    extra = models.ForeignKey(Extra, on_delete=models.PROTECT)
    congelat = models.BooleanField(default=False)
    #
    data_validacio = models.DateTimeField(null=True, blank=True)
    data_exportacio = models.DateTimeField(null=True, blank=True)
    data_eliminacio = models.DateTimeField(null=True, blank=True)
    data_exportacio_eliminacio = models.DateTimeField(null=True, blank=True)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        db_table = 'plan_extraordianaria_treb_imp_mes'

    def __unicode__(self):
        return u'Usuari %s, extra %s mes %s qty %s'\
               % (self.user, self.extra, self.mes, self.qty)



class PeriodesExtra(models.Model):
    unitat = models.ForeignKey('meta.MetaUnitatIdi')
    user_validacio = models.ForeignKey(User)
    data_inici = models.DateField()
    data_fi = models.DateField()
    validat = models.BooleanField(default=False)
    exportat = models.BooleanField(default=False)
    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'plan_periodes_extra'

    def __unicode__(self):
        return u'Unitat: %s, data: %s'\
               % (self.unitat, self.created_at)
