# coding: utf-8
#
#from django.utils.translation import ugettext_lazy as _

from datetime import time

from django.shortcuts import render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib import messages


# from django.contrib.auth.models import User
from apps.idirh.models import Rotacio, Sequencia, RotacioSequencia,\
                              FranjaTemplate, DIES_SETMANA

from apps.idirh.forms import RotacioForm, SequenciaForm,\
                             RotacioSequenciaForm, FranjaForm

from apps.idirh.utils import get_hores_en_format

from apps.idirh.models import COLOR_SEQ, ABREV_SEQ

@login_required
def rotacions_index(request):
    """
    Vista principal llistat de de rotacions
    """
    unitats = request.user.get_profile().unitats_idi_permissos.filter(actiu=True)
    rotacions = Rotacio.objects.filter(unitat__in=unitats, actiu=True)

    context = {'rotacions': rotacions}

    return render_to_response('planilles/rotacions_index.html', context,
                              context_instance=RequestContext(request))

@login_required
def rotacions_rotacio(request, rot_id=0):
    if int(rot_id):
        rotacio = Rotacio.objects.get(id=rot_id)
        rotseq = rotacio.rotaciosequencia_set.all().order_by('ordre')
        unitats = [rotacio.unitat, ]
    else:
        rotseq = []
        rotacio = Rotacio()
        unitats = request.user.get_profile().unitats_idi_permissos\
                                            .all()\
                                            .order_by('centre_idi')

    if request.method == 'POST':
        form = RotacioForm(request.user, request.POST, instance=rotacio)
        if form.is_valid():
            form.save()
            rot_id = rotacio.id
            return redirect('rotacio', rot_id)
    else:
        form = RotacioForm(request.user, instance=rotacio)

    context = {
        'rot_id': rot_id,
        'rotacio': rotacio,
        'form': form,
        'sequencies': Sequencia.objects.filter(tipus_seq='P')
                                       .filter(unitat__in=unitats)
                                       .filter(actiu=True),
        'rotseq': rotseq,
        'DIES_SETMANA': DIES_SETMANA,
    }

    return render_to_response('planilles/rotacions_rotacio.html', context,
                              context_instance=RequestContext(request))

@login_required
def rotacions_rotacio_delete(request, rot_id=0):
    """
    No s'esborren rotacions.
    Es deixen incatives, perque FAPS estan lligades a la rotacio, tant
    sols per motius d'agrupació i de posar nom a la FAP
    """

    if int(rot_id):
        try:
            rotacio = Rotacio.objects.get(id=rot_id)
            rotacio.actiu = False
            rotacio.save()
        except Rotacio.DoesNotExist:
            pass

    return redirect('rotacions')

@login_required
def rotacions_rotaciosequencia(request, rot_id=0):
    if request.method == 'POST':
        form = RotacioSequenciaForm(request.POST)
        if form.is_valid():
            form.save()

    return redirect('rotacio', rot_id)

@login_required
def rotacions_rotaciosequencia_delete(request, rot_id=0, rotseq_id=0):
    if int(rotseq_id):
        rotseq = RotacioSequencia.objects.get(id=rotseq_id)
        rotseq.delete()

    return redirect('rotacio', rot_id)

@login_required
def sequencies_index(request):
    """
    Vista principal llistat de TORNS de Personal
    """
    unitats = request.user.get_profile().unitats_idi_permissos.all()
    sequencies = Sequencia.objects.filter(unitat__in=unitats, actiu=True)\
                                  .filter(tipus_seq='P')\
                                  .order_by('unitat', 'nom')

    context = {
        'sequencies': sequencies,
        'unitats_multiples': len(unitats) > 1,
        'tipus_torn': 'P'  # Personal
    }

    return render_to_response('planilles/sequencies_index.html', context,
                              context_instance=RequestContext(request))

@login_required
def sequencies_equip_index(request):
    """
    Vista principal llistat de TORNS de Maquina
    """
    unitats = request.user.get_profile().unitats_idi_permissos.all()
    sequencies = Sequencia.objects.filter(unitat__in=unitats, actiu=True)\
                                  .filter(tipus_seq='M')\
                                  .order_by('unitat', 'nom')

    context = {
        'sequencies': sequencies,
        'unitats_multiples': len(unitats) > 1,
        'tipus_torn': 'M'  # Maquina
    }

    return render_to_response('planilles/sequencies_index.html', context,
                              context_instance=RequestContext(request))

@login_required
def sequencies_sequencia(request, seq_id=0, tipus_seq='P'):
    """
    Les sequencies estan lligades a la unitat.
    v1: Es passa 'user' al form per permetre editar la unitat si
        l'usuari manega més d'una unitat

    v2: La rotació va lligada a la unitat, també, de manera que
        nomes s'han de mostrar les unitats de la rotació
    """
    unitats = request.user.get_profile().unitats_idi_permissos.filter(actiu=True)

    if not unitats:
        # Si no hi ha unitat assignada, es mostra un error i es redirigeix
        # a index segons M o P
        messages.error(request,
                       ('No es poden gestionar Torns perqué no disposes de permisos sobre cap Unitat.'))
        unitats = request.user.get_profile().unitats_idi_permissos.all()
        sequencies = Sequencia.objects.filter(unitat__in=unitats, actiu=True)\
                                      .filter(tipus_seq=tipus_seq)\
                                      .order_by('unitat', 'nom')

        context = {
            'sequencies': sequencies,
            'unitats_multiples': len(unitats) > 1,
            'tipus_torn': tipus_seq
        }

        return render_to_response('planilles/sequencies_index.html', context,
                                  context_instance=RequestContext(request))

    unitat_alone = unitats[0]

    if int(seq_id):
        sequencia = Sequencia.objects.get(id=seq_id)
        tipus_seq = sequencia.tipus_seq
    else:
        sequencia = Sequencia(tipus_seq=tipus_seq)

    if request.method == 'POST':
        # User passat al form
        form = SequenciaForm(request.user, request.POST, instance=sequencia)
        if form.is_valid():
            form.save()
            seq_id = sequencia.id
            return redirect('sequencia', seq_id)
    else:
        # User passat al form
        # Tractar de canviar el form.field.queryset
        form = SequenciaForm(request.user, instance=sequencia)
        form.fields['abrev_torn_detall'].initial = sequencia.abrev_torn

    # franges per sequencia
    total_setmana = 0
    if sequencia:
        dies = []
        for dia in xrange(7):
            franges = FranjaTemplate.objects.filter(dia_setmana=dia,
                                                    sequencia=sequencia)\
                                            .order_by('inici')
            total_segons = 0
            hores = []
            for franja in franges:
                total_segons += franja.get_seconds().seconds
                hores.append({'hora_inici': franja.inici, 'hora_fi': franja.fi})

            if franges:
                abrev_torn = franja.abrev_torn
            else:
                abrev_torn = sequencia.abrev_torn

            dies.append({'dia_num': dia,
                         'dia_str': DIES_SETMANA[dia],
                         'hores': hores,
                         'abrev_torn': abrev_torn,
                         'hores_format': get_hores_en_format(total_segons)})

            total_setmana += total_segons


    context = {
        'seq_id': seq_id,
        'sequencia': sequencia,
        'form': form,
        'DIES_SETMANA': DIES_SETMANA,
        'dies': dies,
        'total_setmana': get_hores_en_format(total_setmana, format='llarg'),
        'unitat_alone': unitat_alone,
        'unitats_multiples': len(unitats) > 1,
        'colors': COLOR_SEQ,
        'abrevs': ABREV_SEQ,
        'tipus_torn': tipus_seq
    }

    return render_to_response('planilles/sequencies_sequencia.html',
                              context,
                              context_instance=RequestContext(request))

@login_required
def sequencies_sequencia_delete(request, seq_id=0):
    """
    No s'esborren sequencies.
    Es deixen incatives, perque FAPS estan lligades a la sequencia, tant
    sols per motius d'agrupació i de posar nom a la FAP
    """
    if int(seq_id):
        try:
            sequencia = Sequencia.objects.get(id=seq_id)
            sequencia.actiu = False
            sequencia.save()
        except Sequencia.DoesNotExist:
            pass
    return redirect('sequencies')

@login_required
def sequencies_franja(request, seq_id=0):
    if request.method == 'POST':
        form = FranjaForm(request.POST)
        if form.is_valid():
            sequencia = Sequencia.objects.get(id=seq_id)

            dia_setmana = form.cleaned_data['dia_setmana']
            inici_1 = form.cleaned_data['inici_1']
            fi_1 = form.cleaned_data['fi_1']
            inici_2 = form.cleaned_data['inici_2']
            fi_2 = form.cleaned_data['fi_2']
            abrev_torn = form.cleaned_data['abrev_torn_detall']

            if (inici_1 or inici_1 == time(0, 0)) and (fi_1 or fi_1 == time(0, 0)):
                dies = xrange(dia_setmana, dia_setmana + 1)
                if dia_setmana == 10:  # es dilluns --> divendres
                    dies = xrange(5)
                if dia_setmana == 11:  # es cap de setmana
                    dies = xrange(5, 7)

                for dia in dies:
                    franja = FranjaTemplate(
                        dia_setmana=dia,
                        inici=inici_1,
                        fi=fi_1,
                        abrev_torn=abrev_torn,
                        sequencia=sequencia)
                    franja.save()

                    if (inici_2 or inici_2 == time(0, 0)) and (fi_2 or fi_2 == time(0, 0)):
                        franja = FranjaTemplate(
                            dia_setmana=dia,
                            inici=inici_2,
                            fi=fi_2,
                            abrev_torn=abrev_torn,
                            sequencia=sequencia)
                        franja.save()
    else:
        form = FranjaForm()

    return redirect('sequencia', seq_id)

@login_required
def sequencies_franja_delete(request, seq_id=0, dia=0):
    if int(seq_id):
        sequencia = Sequencia.objects.get(id=seq_id)
        franges = FranjaTemplate.objects\
                                .filter(sequencia=sequencia, dia_setmana=dia)
        franges.delete()

    return redirect('sequencia', seq_id)
