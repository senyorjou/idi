# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from apps.meta.models import MetaGrupCategoriaProfessional,\
                             MetaCategoriaProfesionalKarat

from apps.idirh.forms import GrupCatForm


@login_required
def index_grups_categories(request):
    """
    Llistat de categories
    """
    grups_categories = MetaGrupCategoriaProfessional.objects.all()\
                                                            .order_by('nom')

    context = {'grups_categories': grups_categories}

    return render_to_response('planilles/grups_categories_index.html',
                              context,
                              context_instance=RequestContext(request))


@login_required
def grup_categories(request, grup_cat_id=0):
    """
    Manteniment de grups de categories
    """
    if int(grup_cat_id):
        try:
            grup_cat = MetaGrupCategoriaProfessional.objects.get(id=grup_cat_id)
        except MetaGrupCategoriaProfessional.DoesNotExist:
            return redirect('grups_categoria')

        cats_in = grup_cat.metacategoriaprofesionalkarat_set.all()\
                                                            .order_by('descripcio')
    else:
        grup_cat = MetaGrupCategoriaProfessional()
        cats_in = []

    if request.method == 'POST':
        form = GrupCatForm(request.POST, instance=grup_cat)
        if form.is_valid():
            form.save()
            grup_cat_id = grup_cat.id
            return redirect('grup_categoria', grup_cat_id)
    else:
        form = GrupCatForm(instance=grup_cat)

    context = {
        'grup_cat_id': grup_cat_id,
        'cats_in': cats_in,
        'cats_out': MetaCategoriaProfesionalKarat.objects.all()
                                                         .order_by('descripcio'),
        'form': form
    }

    return render_to_response('planilles/grup_categories.html',
                              context,
                              context_instance=RequestContext(request))


@login_required
def grup_categories_cat_save(request, grup_cat_id):
    if request.method == 'POST':
        cat_id = request.POST.get('f_categories', '')
        if (cat_id):
            grup_categoria = MetaGrupCategoriaProfessional.objects.get(id=grup_cat_id)
            categoria = MetaCategoriaProfesionalKarat.objects.get(id=cat_id)
            categoria.grup = grup_categoria
            categoria.save()

    return redirect('grup_categoria', grup_cat_id=grup_cat_id)


@login_required
def grup_categories_delete(request, grup_cat_id):
    grup_categoria = MetaGrupCategoriaProfessional.objects.get(id=grup_cat_id)
    if (grup_categoria):
        grup_categoria.delete()

    return redirect('grups_categoria')


@login_required
def grup_categories_cat_delete(request, grup_cat_id, cat_id):
    categoria = MetaCategoriaProfesionalKarat.objects.get(id=cat_id)
    categoria.grup = None

    categoria.save()

    return redirect('grup_categoria', grup_cat_id=grup_cat_id)
