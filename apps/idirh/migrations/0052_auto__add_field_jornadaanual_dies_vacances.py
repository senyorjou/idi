# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'JornadaAnual.dies_vacances'
        db.add_column('plan_jornada_anual', 'dies_vacances',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'JornadaAnual.dies_vacances'
        db.delete_column('plan_jornada_anual', 'dies_vacances')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'idirh.calendarilaboral': {
            'Meta': {'ordering': "['nom']", 'object_name': 'CalendariLaboral', 'db_table': "u'plan_calendari_laboral'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'es_mestre': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'idirh.diacalendari': {
            'Meta': {'object_name': 'DiaCalendari', 'db_table': "u'plan_dia_calendari'"},
            'calendari': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.CalendariLaboral']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dia': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'tipus_dia': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'idirh.equip': {
            'Meta': {'ordering': "['nom']", 'object_name': 'Equip', 'db_table': "u'plan_equip'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'descripcio': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'grup': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.GrupUnitat']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'idirh.extra': {
            'Meta': {'ordering': "['ordre_excel']", 'object_name': 'Extra', 'db_table': "'plan_extraordinaries'"},
            'codi': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'comput': ('django.db.models.fields.CharField', [], {'default': "'H'", 'max_length': '1'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True'}),
            'dies_numero': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'es_dia_especial': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'festiu_laboral': ('django.db.models.fields.CharField', [], {'default': "'L'", 'max_length': '1', 'blank': 'True'}),
            'hora_fi': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'hora_inici': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True'}),
            'ordre_excel': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'preu': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'tipus': ('django.db.models.fields.CharField', [], {'default': "'I'", 'max_length': '1'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'vista': ('django.db.models.fields.CharField', [], {'default': "'E'", 'max_length': '1'})
        },
        'idirh.extratreballador': {
            'Meta': {'object_name': 'ExtraTreballador', 'db_table': "'plan_exraordinaria_treballador'"},
            'comentari': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'congelat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_eliminacio': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'data_exportacio': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'data_exportacio_eliminacio': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'data_validacio': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'despla': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'dia': ('django.db.models.fields.DateField', [], {}),
            'editor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'editorextra'", 'to': "orm['auth.User']"}),
            'extra': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Extra']", 'on_delete': 'models.PROTECT'}),
            'hora_fi': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'hora_inici': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tipus': ('django.db.models.fields.CharField', [], {'default': "'I'", 'max_length': '1'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'idirh.extratreballadorimpmensual': {
            'Meta': {'object_name': 'ExtraTreballadorImpMensual', 'db_table': "'plan_extraordianaria_treb_imp_mes'"},
            'congelat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_eliminacio': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'data_exportacio': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'data_exportacio_eliminacio': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'data_mes': ('django.db.models.fields.DateField', [], {}),
            'data_validacio': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'editor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'editorextramensual'", 'to': "orm['auth.User']"}),
            'extra': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Extra']", 'on_delete': 'models.PROTECT'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mes': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'qty': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'idirh.fapequip': {
            'Meta': {'ordering': "['dia_setmana']", 'object_name': 'FAPEquip', 'db_table': "u'plan_franja_aplicada_equip'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dia_setmana': ('django.db.models.fields.IntegerField', [], {}),
            'franja_template': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.FranjaTemplate']"}),
            'hora_fi': ('django.db.models.fields.TimeField', [], {}),
            'hora_inici': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'periode_equip': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.PeriodeEquip']"}),
            'sequencia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Sequencia']"})
        },
        'idirh.fapmaquina': {
            'Meta': {'ordering': "['dia_setmana']", 'object_name': 'FAPMaquina', 'db_table': "u'plan_franja_aplicada_maquina'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dia_setmana': ('django.db.models.fields.IntegerField', [], {}),
            'fi': ('django.db.models.fields.TimeField', [], {}),
            'franja_template': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.FranjaTemplate']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inici': ('django.db.models.fields.TimeField', [], {}),
            'lloc_treball_grup': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.LlocTreballGrup']"}),
            'sequencia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Sequencia']"})
        },
        'idirh.faptreballador': {
            'Meta': {'ordering': "['dia_setmana']", 'object_name': 'FAPTreballador', 'db_table': "u'plan_franja_aplicada_treballador'"},
            'abrev_torn': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dia_setmana': ('django.db.models.fields.IntegerField', [], {}),
            'franja_template': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.FranjaTemplate']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'hora_fi': ('django.db.models.fields.TimeField', [], {}),
            'hora_inici': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'periode_seq': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.PeriodeSequencies']"}),
            'periode_treballador': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.PeriodeTreballador']"}),
            'sequencia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Sequencia']", 'null': 'True', 'on_delete': 'models.SET_NULL'})
        },
        'idirh.franjatemplate': {
            'Meta': {'ordering': "['dia_setmana']", 'object_name': 'FranjaTemplate', 'db_table': "u'plan_franja_template'"},
            'abrev_torn': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dia_setmana': ('django.db.models.fields.IntegerField', [], {}),
            'fi': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inici': ('django.db.models.fields.TimeField', [], {}),
            'sequencia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Sequencia']"})
        },
        'idirh.grupunitat': {
            'Meta': {'ordering': "['nom']", 'object_name': 'GrupUnitat', 'db_table': "u'plan_grups_unitat'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'descripcio': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'unitat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaUnitatIdi']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'idirh.incidencia': {
            'Meta': {'object_name': 'Incidencia', 'db_table': "'plan_incidencia'"},
            'codi': ('django.db.models.fields.CharField', [], {'default': "'T'", 'max_length': '2'}),
            'color': ('django.db.models.fields.CharField', [], {'default': "'#009900'", 'max_length': '20'}),
            'color_text': ('django.db.models.fields.CharField', [], {'default': "'#fff'", 'max_length': '10'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'text_suport': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'idirh.incidenciaequip': {
            'Meta': {'object_name': 'IncidenciaEquip', 'db_table': "'plan_incidencia_equip'"},
            'comentari': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'dia': ('django.db.models.fields.DateField', [], {}),
            'equip': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Equip']"}),
            'hora_fi': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'hora_inici': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'incidencia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Incidencia']", 'on_delete': 'models.PROTECT'})
        },
        'idirh.jornadaanual': {
            'Meta': {'object_name': 'JornadaAnual', 'db_table': "'plan_jornada_anual'"},
            'actiu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dies_vacances': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'es_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hores': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '1'}),
            'hores_nit': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'idirh.lloctreballgrup': {
            'Meta': {'object_name': 'LlocTreballGrup', 'db_table': "u'plan_lloc_treball_grup'"},
            'categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaGrupCategoriaProfessional']"}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'fi': ('django.db.models.fields.DateField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'grup': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.GrupUnitat']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inici': ('django.db.models.fields.DateField', [], {}),
            'sequencia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Sequencia']"})
        },
        'idirh.maquina': {
            'Meta': {'object_name': 'Maquina', 'db_table': "u'plan_maquina'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'grup': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.GrupUnitat']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'idirh.moviment': {
            'Meta': {'ordering': "['nom']", 'object_name': 'Moviment', 'db_table': "'plan_moviment'"},
            'calcul': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'codi': ('django.db.models.fields.CharField', [], {'default': "'T'", 'max_length': '2'}),
            'color': ('django.db.models.fields.CharField', [], {'default': "'#009900'", 'max_length': '20'}),
            'color_text': ('django.db.models.fields.CharField', [], {'default': "'#fff'", 'max_length': '10'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'es_any_passat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'es_computable': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'es_excedencia': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'es_ics': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'es_idi': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'es_imputable_sempre': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'es_infinit': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'es_jornada': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'es_terminator': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imputa_hores': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'text_suport': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'idirh.movimenttreballador': {
            'Meta': {'object_name': 'MovimentTreballador', 'db_table': "'plan_moviment_treballador'"},
            'all_day': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'comentari': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'congelat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'dia': ('django.db.models.fields.DateField', [], {}),
            'editor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'editormoviment'", 'to': "orm['auth.User']"}),
            'hora_fi': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'hora_inici': ('django.db.models.fields.TimeField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'moviment': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Moviment']", 'on_delete': 'models.PROTECT'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'idirh.periodeequip': {
            'Meta': {'ordering': "['data_inici']", 'object_name': 'PeriodeEquip', 'db_table': "u'plan_periode_equip'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_fi': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'data_inici': ('django.db.models.fields.DateField', [], {}),
            'equip': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Equip']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sequencia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Sequencia']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'idirh.periodeequipsequenciapersonal': {
            'Meta': {'ordering': "['periode_equip']", 'object_name': 'PeriodeEquipSequenciaPersonal', 'db_table': "u'plan_periode_equip_sequencia__personal'"},
            'categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaGrupCategoriaProfessional']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'periode_equip': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.PeriodeEquip']"}),
            'personal': ('django.db.models.fields.DecimalField', [], {'default': "'0.0'", 'max_digits': '5', 'decimal_places': '2'})
        },
        'idirh.periodemaquina': {
            'Meta': {'object_name': 'PeriodeMaquina', 'db_table': "'plan_periode_maquina'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_fi': ('django.db.models.fields.DateField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'data_inici': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maquina': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Maquina']"}),
            'personal': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'sequencia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Sequencia']"})
        },
        'idirh.periodesequencies': {
            'Meta': {'ordering': "['ordre']", 'object_name': 'PeriodeSequencies', 'db_table': "u'plan_periode_sequencies'"},
            'editat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ordre': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'periode': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.PeriodeTreballador']"}),
            'sequencia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Sequencia']"})
        },
        'idirh.periodesextra': {
            'Meta': {'object_name': 'PeriodesExtra', 'db_table': "'plan_periodes_extra'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_fi': ('django.db.models.fields.DateField', [], {}),
            'data_inici': ('django.db.models.fields.DateField', [], {}),
            'exportat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unitat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaUnitatIdi']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'user_validacio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"}),
            'validat': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'idirh.periodetreballador': {
            'Meta': {'ordering': "['data_inici']", 'object_name': 'PeriodeTreballador', 'db_table': "u'plan_periode_treballador'"},
            'congelat': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data_fi': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'data_inici': ('django.db.models.fields.DateField', [], {}),
            'editor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'editorperiode'", 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reduccio': ('django.db.models.fields.DecimalField', [], {'default': '0.0', 'max_digits': '5', 'decimal_places': '2'}),
            'rotacio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Rotacio']", 'null': 'True'}),
            'sequencies': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['idirh.Sequencia']", 'through': "orm['idirh.PeriodeSequencies']", 'symmetrical': 'False'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'db_index': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'idirh.rotacio': {
            'Meta': {'ordering': "['nom']", 'object_name': 'Rotacio', 'db_table': "u'plan_rotacio'"},
            'actiu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'unitat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaUnitatIdi']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'idirh.rotaciosequencia': {
            'Meta': {'object_name': 'RotacioSequencia', 'db_table': "u'plan_rotacio_sequencia'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ordre': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'rotacio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Rotacio']"}),
            'sequencia': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.Sequencia']"})
        },
        'idirh.sequencia': {
            'Meta': {'ordering': "['nom']", 'object_name': 'Sequencia', 'db_table': "u'plan_sequencia'"},
            'abrev_torn': ('django.db.models.fields.CharField', [], {'default': "'M'", 'max_length': '1'}),
            'actiu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'color': ('django.db.models.fields.CharField', [], {'default': "'laboral'", 'max_length': '20'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'rotacions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['idirh.Rotacio']", 'through': "orm['idirh.RotacioSequencia']", 'symmetrical': 'False'}),
            'tipus_seq': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'unitat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaUnitatIdi']"}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'meta.metacentreidi': {
            'Meta': {'ordering': "['descripcio']", 'object_name': 'MetaCentreIdi', 'db_table': "u'meta_centre_idi'"},
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'meta.metagrupcategoriaprofessional': {
            'Meta': {'object_name': 'MetaGrupCategoriaProfessional', 'db_table': "u'meta_grup_categoria_professional'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'meta.metaunitatidi': {
            'Meta': {'ordering': "['descripcio']", 'object_name': 'MetaUnitatIdi', 'db_table': "u'meta_unitat_idi'"},
            'actiu': ('django.db.models.fields.IntegerField', [], {}),
            'calendari_laboral': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.CalendariLaboral']", 'null': 'True'}),
            'centre_idi': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCentreIdi']"}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unitatat_facturacio_id': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['idirh']