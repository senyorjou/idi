# coding: utf-8
# from django.forms import ModelForm, Form

from django.utils.translation import ugettext_lazy as _

from django import forms

from django.forms.widgets import CheckboxSelectMultiple

#from django.contrib.admin.widgets import FilteredSelectMultiple
from apps.idirh.models import CalendariLaboral, GrupUnitat, Rotacio,\
                                  Sequencia, RotacioSequencia, Maquina,\
                                  Moviment, JornadaAnual, Equip, Incidencia,\
                                  Extra, ExtraTreballador

from apps.meta.models import MetaGrupCategoriaProfessional, MetaUnitatIdi,\
                             AuthUserProfile

from apps.idirh.models import ABREV_SEQ

TIPUS_ACCIO = (('M', _('Moviment')),
               ('R', _('Rotacio')),
               ('S', _('Seqüencia')),)

FAVORITE_COLORS_CHOICES = (('blue', 'Blue'),
                           ('green', 'Green'),
                           ('black', 'Black'))

DIES_SETMANA = (('0', _('Dilluns')),
                ('1', _('Dimarts')),
                ('2', _('Dimecres')),
                ('3', _('Dijous')),
                ('4', _('Divendres')),
                ('5', _('Dissabte')),
                ('6', _('Diumenge')))


class CalendariLaboralForm(forms.ModelForm):
    """docstring for CalendariLaboralForm"""
    class Meta:
        model = CalendariLaboral


class MetaUnitatIdiForm(forms.ModelForm):
    """
    Al form de Unitat nomes es modifica el calendari en us
    """
    class Meta:
        model = MetaUnitatIdi
        fields = ('calendari_laboral',)


class LLocTreballGrupForm(forms.Form):
    # choices és una llista que conté els valors que podria arribar a tenir
    # Els select's necessiten una llisat de valors possibles
    var_choices = [(x.id, x.id) for x in
                   MetaGrupCategoriaProfessional.objects.all()]

    sequencies = forms.ModelChoiceField(queryset=Sequencia.objects.all(),
                                        empty_label="- - - - - -")
    inici = forms.CharField()
    fi = forms.CharField(required=False)
    especialistes = forms.ModelMultipleChoiceField(
                    queryset=MetaGrupCategoriaProfessional.objects.all(),
                    required=False)
    especialistes_seleccionats = forms.MultipleChoiceField(choices=var_choices)
    especialistes_pack = forms.CharField(required=False)

    # Això és per assignar un class al camp.
    # REVIEW!!! Em sembla molt barroer
    def __init__(self, user, *args, **kwargs):
        super(LLocTreballGrupForm, self).__init__(*args, **kwargs)
        unitats = user.get_profile().unitats_idi_permissos.all()
        sequencies = Sequencia.objects.filter(unitat__in=unitats)
        self.fields['sequencies'].queryset = sequencies


class GrupUnitatForm(forms.ModelForm):
    """docstring for GrupUnitatForm"""
    # subject=forms.ModelMultipleChoiceField(Subjects.objects.all(),widget=
    # FilteredSelectMultiple("Subjects",False,attrs={'rows':'10'}))
    # sequencies = forms.ModelChoiceField(queryset=Sequencia.objects.all(), empty_label="- - - - - -")
    # especialistes = forms.ModelMultipleChoiceField(queryset=MetaGrupCategoriaProfessional.objects.all())
    # especialistes_seleccionats = forms.CharField()

    class Meta:
        model = GrupUnitat


class UnitatGrupEquipForm(forms.ModelForm):
    """docstring for UnitatGrupEquipForm"""
    class Meta:
        model = Equip


class GrupCatForm(forms.ModelForm):
    """docstring for GrupCatForm"""
    class Meta:
        model = MetaGrupCategoriaProfessional


class SequenciaForm(forms.ModelForm):
    """docstring for SequenciaForm"""
    abrev_torn_detall = forms.ChoiceField(widget=forms.Select(),
                                          choices=ABREV_SEQ,
                                          required=False)

    class Meta:
        model = Sequencia
        exclude = ('actiu', 'rotacions', 'color', )

    # Això és per assignar un class al camp.
    # REVIEW!!! Em sembla molt barroer
    def __init__(self, user, *args, **kwargs):
        super(SequenciaForm, self).__init__(*args, **kwargs)
        self.fields['nom'].widget.attrs['class'] = 'input-xxlarge'
        unitats = user.get_profile().unitats_idi_permissos.filter(actiu=True)
        self.fields['unitat'].queryset = unitats


class RotacioForm(forms.ModelForm):
    """docstring for SequenciaForm"""
    class Meta:
        model = Rotacio
        exclude = ('actiu',)

    def __init__(self, user, *args, **kwargs):
        super(RotacioForm, self).__init__(*args, **kwargs)
        self.fields['nom'].widget.attrs['class'] = 'input-xxlarge'
        unitats = user.get_profile().unitats_idi_permissos.filter(actiu=True)
        self.fields['unitat'].queryset = unitats


class RotacioSequenciaForm(forms.ModelForm):
    """docstring for RotSeqForm"""
    class Meta:
        model = RotacioSequencia


class FranjaForm(forms.Form):
    """docstring for FranjaForm"""
    dia_setmana = forms.IntegerField()
    inici_1 = forms.TimeField(widget=forms.TimeInput(format='%H:%M'), required=False)
    fi_1 = forms.TimeField(widget=forms.TimeInput(format='%H:%M'), required=False)
    inici_2 = forms.TimeField(widget=forms.TimeInput(format='%H:%M'), required=False)
    fi_2 = forms.TimeField(widget=forms.TimeInput(format='%H:%M'), required=False)
    abrev_torn_detall = forms.ChoiceField(widget=forms.Select(),
                                          choices=(('', '- - - - - -'), ) + ABREV_SEQ,
                                          required=False)
    # S'afegeix poder modificar la data HASTA
    data_fi_periode = forms.CharField(required=False)

    def __init__(self, *args, **kwargs):
        super(FranjaForm, self).__init__(*args, **kwargs)
        self.fields['data_fi_periode'].widget.attrs['class'] = 'input-small'


class MaquinaForm(forms.ModelForm):
    """docstring for MaquinaForm"""
    class Meta:
        model = Maquina


class PeriodeMaquinaForm(forms.Form):
    """docstring for MaquinaForm"""
    sequencia = forms.ModelChoiceField(queryset=Sequencia.objects.filter(tipus_seq='M'),
                                       empty_label="- - - - - -")
    data_inici = forms.CharField()
    data_fi = forms.CharField(required=False)
    # personal = forms.CharField()


class EquipPeriodeForm(forms.Form):
    """docstring for TreballadorForm"""
    tipus_accio = forms.CharField(required=False)
    seq_data_inici = forms.CharField(required=False)
    seq_data_fi = forms.CharField(required=False)
    categories_personal = forms.CharField(required=False)

    # Cada franja Mati/tarda/nit disposa de la mateixa info
    # per tant repeteixo els camps
    #  M A T I
    sequencia_m = forms.ModelChoiceField(queryset=Sequencia.objects.filter(tipus_seq='M', actiu=True),
                                         empty_label="- - - - - -",
                                         required=False)
    categoria_m = forms.ModelChoiceField(queryset=MetaGrupCategoriaProfessional.objects.all(),
                                         empty_label="- - - - - -",
                                         required=False)
    personal_m = forms.DecimalField(required=False)

    # T A R D A
    sequencia_t = forms.ModelChoiceField(queryset=Sequencia.objects.filter(tipus_seq='M', actiu=True),
                                         empty_label="- - - - - -",
                                         required=False)
    categoria_t = forms.ModelChoiceField(queryset=MetaGrupCategoriaProfessional.objects.all(),
                                         empty_label="- - - - - -",
                                         required=False)
    personal_t = forms.DecimalField(required=False)
    # N I T
    sequencia_n = forms.ModelChoiceField(queryset=Sequencia.objects.filter(tipus_seq='M', actiu=True),
                                         empty_label="- - - - - -",
                                         required=False)
    categoria_n = forms.ModelChoiceField(queryset=MetaGrupCategoriaProfessional.objects.all(),
                                         empty_label="- - - - - -",
                                         required=False)
    personal_n = forms.DecimalField(required=False)

    incidencia = forms.ModelChoiceField(queryset=Incidencia.objects.all(),
                                        empty_label="- - - - - -",
                                        required=False)

    mov_hora_inici = forms.TimeField(required=False)
    mov_hora_final = forms.TimeField(required=False)

    mov_comentari = forms.CharField(required=False)

    def __init__(self, unitat, *args, **kwargs):
        super(EquipPeriodeForm, self).__init__(*args, **kwargs)
        self.fields['seq_data_inici'].widget.attrs['class'] = 'input-small'
        self.fields['seq_data_fi'].widget.attrs['class'] = 'input-small'
        self.fields['personal_m'].widget.attrs['class'] = 'span05'
        self.fields['personal_t'].widget.attrs['class'] = 'span05'
        self.fields['personal_n'].widget.attrs['class'] = 'span05'
        self.fields['mov_hora_inici'].widget.attrs['class'] = 'input-small'
        self.fields['mov_hora_final'].widget.attrs['class'] = 'input-small'
        self.fields['mov_comentari'].widget.attrs['class'] = 'input-xxlarge'

        sequencies = Sequencia.objects.filter(unitat=unitat,
                                              tipus_seq='M',
                                              actiu=True)
        self.fields['sequencia_m'].queryset = sequencies
        self.fields['sequencia_t'].queryset = sequencies
        self.fields['sequencia_n'].queryset = sequencies


class TreballadorForm(forms.Form):
    """docstring for TreballadorForm"""
    tipus_accio = forms.CharField(required=False)
    rotacio = forms.ModelChoiceField(
                    queryset=Rotacio.objects.filter(actiu=True),
                    empty_label="- - - - - -",
                    required=False)

    sequencia = forms.ModelChoiceField(
                    queryset=Sequencia.objects.filter(tipus_seq='P', actiu=True),
                    empty_label="- - - - - -",
                    required=False)

    seq_data_inici = forms.CharField(required=False)
    seq_data_fi = forms.CharField(required=False)

    moviment = forms.ModelChoiceField(
                    queryset=Moviment.objects.all(),
                    empty_label="- - - - - -",
                    required=False)

    extra = forms.ModelChoiceField(
                    queryset=Extra.objects.filter(tipus='E'),
                    empty_label="- - - - - -",
                    required=False)

    reduccio = forms.DecimalField(required=False, max_value=100, min_value=0)

    mov_hora_inici = forms.TimeField(required=False)
    mov_hora_final = forms.TimeField(required=False)
    mov_tot_el_dia = forms.NullBooleanField(widget=forms.CheckboxInput())
    mov_comentari = forms.CharField(required=False)
    extra_despla = forms.IntegerField(required=False)

    def __init__(self, unitat, *args, **kwargs):
        super(TreballadorForm, self).__init__(*args, **kwargs)
        self.fields['seq_data_inici'].widget.attrs['class'] = 'input-small'
        self.fields['seq_data_fi'].widget.attrs['class'] = 'input-small'
        self.fields['moviment'].widget.attrs['class'] = 'input-xlarge'
        self.fields['extra'].widget.attrs['class'] = 'input-xlarge'
        self.fields['rotacio'].widget.attrs['class'] = 'input-xlarge'
        self.fields['sequencia'].widget.attrs['class'] = 'input-xlarge'
        self.fields['mov_hora_inici'].widget.attrs['class'] = 'input-small'
        self.fields['mov_hora_final'].widget.attrs['class'] = 'input-small'
        self.fields['extra_despla'].widget.attrs['class'] = 'span1 tipus-desp'
        self.fields['mov_comentari'].widget.attrs['class'] = 'span6'
        self.fields['reduccio'].widget.attrs['class'] = 'span1'

        rotacions = Rotacio.objects.filter(unitat=unitat, actiu=True)
        self.fields['rotacio'].queryset = rotacions

        sequencies = Sequencia.objects.filter(unitat=unitat,
                                              tipus_seq='P',
                                              actiu=True)
        self.fields['sequencia'].queryset = sequencies


class JornadaAnualForm(forms.ModelForm):
    class Meta:
        model = JornadaAnual
        exclude = ('actiu',)

    es_default_hidden = forms.NullBooleanField()


class MovimentForm(forms.ModelForm):
    """docstring for MovimentForm"""
    class Meta:
        model = Moviment
        exclude = ('calcul', 'tipus')
        widgets = {
            'color_text': forms.RadioSelect(),
            'text_suport': forms.Textarea(attrs={'class': 'input-xxlarge',
                                                 'cols': 80,
                                                 'rows': 3}),
        }


class IncidenciaForm(forms.ModelForm):
    """docstring for MovimentForm"""
    class Meta:
        model = Incidencia
        widgets = {
            'color_text': forms.RadioSelect(),
            'text_suport': forms.Textarea(attrs={'cols': 80, 'rows': 3}),
        }


class RangDatesForm(forms.Form):
    output = forms.BooleanField(required=False)
    data_inici = forms.CharField(required=True)
    data_fi = forms.CharField(required=True)
    unitat = forms.ModelChoiceField(MetaUnitatIdi.objects.none(), required=False)

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(RangDatesForm, self).__init__(*args, **kwargs)
        self.base_fields['data_inici'].widget.attrs['class'] = 'input-small'
        self.base_fields['data_fi'].widget.attrs['class'] = 'input-small'
        unitats = []
        if user is not None:
            unitats = user.get_profile().unitats_idi_permissos.filter(actiu=True)
            self.base_fields['unitat'].queryset = unitats


class ReportExtraForm(forms.Form):
    output = forms.CharField(required=False)
    output_file = forms.FileField(required=False)
    # data_inici = forms.CharField(required=False)
    data_fi = forms.CharField(required=False)
    unitat = forms.ModelChoiceField(MetaUnitatIdi.objects.none(), required=False)
    periode = forms.CharField(required=False)

    def __init__(self, user, *args, **kwargs):
        super(ReportExtraForm, self).__init__(*args, **kwargs)
        # self.base_fields['data_inici'].widget.attrs['class'] = 'input-small'
        self.base_fields['data_fi'].widget.attrs['class'] = 'input-small'
        unitats = []
        if user is not None:
            unitats = user.get_profile().unitats_idi_permissos.filter(actiu=True)
            self.base_fields['unitat'].queryset = unitats


class JornadaAnualAplicadaForm(forms.ModelForm):
    class Meta:
        model = AuthUserProfile
        fields = ('jornada_anual', )

    def __init__(self, *args, **kwargs):
        super(JornadaAnualAplicadaForm, self).__init__(*args, **kwargs)
        self.fields['jornada_anual'].widget.attrs['class'] = 'input-xlarge'


class ExtraForm(forms.ModelForm):
    dies_setmana = forms.MultipleChoiceField(
                        required=False,
                        widget=CheckboxSelectMultiple(attrs={'class': 'special'}),
                        choices=DIES_SETMANA)

    class Meta:
        model = Extra

    def __init__(self, *args, **kwargs):
        super(ExtraForm, self).__init__(*args, **kwargs)
        self.fields['codi'].widget.attrs['class'] = 'input-small'
        self.fields['nom'].widget.attrs['class'] = 'input-xxlarge'
        self.fields['descripcio'].widget.attrs['class'] = 'input-xxlarge'


class TrebHoraForm(forms.Form):
    treballador = forms.CharField(required=True)
    dia = forms.CharField(required=True)
    hora = forms.CharField(required=True)
