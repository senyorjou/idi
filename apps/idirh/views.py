from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from apps.meta.models import MetaUnitatIdi, MetaCentreIdi

@login_required
def index(request):
    """
    Vista principal, dashboard amb menu d'accessos a la resta d'app's
    """
    usuaris = User.objects.all()
    unitats = MetaUnitatIdi.objects.filter(actiu=1)\
                                   .order_by('centre_idi')\
                                   .reverse()

    centres = MetaCentreIdi.objects.order_by('descripcio')

    context = {
        'usuaris': usuaris,
        'centres': centres,
        'unitats': unitats,
        'notificacions': 1
    }

    return render_to_response('planilles/index.html', context,
                              context_instance=RequestContext(request))


# TODO: Aviam que fem aqui amb @login_required quan el el request ve amb AJAX
def update_session(request, key, value):
    allowed_keys = ['cal_vista']
    if request.is_ajax() and key in allowed_keys:
        request.session[key] = value

    return HttpResponse('ok')
