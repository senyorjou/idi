# coding: utf-8
from django.db import models
from apps.meta.models import MetaUnitatIdi,AuthUser
from django.forms.models import BaseForm
from django.forms import ModelForm
from django.utils.encoding import smart_unicode
import datetime
#from django.core.mail import send_mail
from apps.meta.common import send_mail
from django.conf import settings

#class CauEstat(models.Model):
#    id = models.AutoField(primary_key=True) # Field name made lowercase.
#    estat = models.CharField(max_length=45, blank=False) # Field name made lowercase.
#    class Meta:
#        db_table = u'cau_estat'
#    def __unicode__(self):
#        return smart_unicode(self.estat)  


class CauCategoria(models.Model):
    id = models.AutoField(primary_key=True) # Field name made lowercase.
    categoria = models.CharField(max_length=45, blank=False) # Field name made lowercase.
    class Meta:
        db_table = u'cau_categoria'
        ordering = ['-categoria']
    def __unicode__(self):
        return smart_unicode(self.categoria)  

  
class CauPacs(models.Model):
    id = models.AutoField(primary_key=True) # Field name made lowercase.
    pacs = models.CharField(max_length=45, blank=False) # Field name made lowercase.
    url =  models.CharField(max_length=45, blank=False) # Field name made lowercase.
    
    class Meta:
        db_table = u'cau_pacs'
    def __unicode__(self):
        return str(self.pacs)
    def get_url(self):
        return unicode(self.url) + '/dcm4chee-web/foldersubmit.m'

PRIORITAT_VALUES = ( (1, 'Baixa'), (2, 'Normal'), (3, 'Alta'))
ESTAT_VALUES = ( (1, 'Nou'), (2, 'Resolt'), (3, 'Pendent'), (4, 'Esperant resposta'))      

class CauTicketComentari(models.Model):
    id = models.AutoField(primary_key=True)
    descripcio = models.TextField(max_length=3000, null=True,  blank=True)
    user = models.ForeignKey(AuthUser, null=True,  blank=True)    
    data = models.DateTimeField(default=datetime.datetime.now)
    ticket = models.ForeignKey('CauTicket', null=True,  blank=True)
    tipus = models.BooleanField(verbose_name="Comentari intern")
    class Meta:
        db_table = u'cau_ticket_comentari'
        ordering = ['-id']
    def __unicode__(self):
        return "Comentari #" + str(self.id)
    def notifica_responsable(self):
        if not self.tipus:
            self.ticket.notifica_responsable()
    def notifica_usuari(self):
        if not self.tipus:
            self.ticket.notifica_usuari()    
        
class CauTicket(models.Model):
    id = models.AutoField(primary_key=True)
    resum = models.CharField(max_length=135)
    descripcio = models.TextField(max_length=3000, null=True,  blank=True)
    categoria = models.ForeignKey(CauCategoria, blank=True, default=2)
    unitat_idi = models.ForeignKey(MetaUnitatIdi, blank=True, default=17)
    informat_per = models.ForeignKey(AuthUser, related_name="%(app_label)s_%(class)s_informat_per_related") # Field name made lowercase.
    prioritat = models.IntegerField(choices=PRIORITAT_VALUES, default=1, verbose_name="Prioritat demanada")
    estat = models.IntegerField(choices=ESTAT_VALUES, default=1)
    data_apertura = models.DateTimeField(default=datetime.datetime.now, verbose_name="Data obertura")
    assignat_a = models.ForeignKey(AuthUser, null=True, blank=True,  related_name="%(app_label)s_%(class)s_assignat_a_related", limit_choices_to = {'is_staff': True}) # Field name made lowercase.
    modificat_per =models.ForeignKey(AuthUser, null=True,  blank=True,related_name="%(app_label)s_%(class)s_modificat_per_related", limit_choices_to = {'is_staff': True}) # Field name made lowercase.
    data_modificacio = models.DateTimeField(null=True,  blank=True)
    tancat_per = models.ForeignKey(AuthUser, null=True,  blank=True,related_name="%(app_label)s_%(class)s_tancat_per_related", limit_choices_to = {'is_staff': True}) # Field name made lowercase.
    data_tancament = models.DateTimeField(null=True,  blank=True)
    telefon = models.CharField(max_length=45, verbose_name="Telèfon", null=True,  blank=True)
    adjunt  = models.FileField(upload_to="cau_files/%Y/%m/%d")

    class Meta:
        db_table = u'cau_ticket'
        #abstract = True
    def notifica_responsable(self):
        if self.informat_per.email != None:
            send_mail("[IDI] Tiquet #" + unicode(self.id) + " - " + unicode(self.get_estat_display()) + " - " + unicode(self.unitat_idi), 
                      "El tiquet #" + unicode(self.id) + " ha estat actualitzat. \n\n" +
                      "Resum: \n\n" + self.resum + "\n\n" +
                      "Comentaris: \n\n" + "".join(["["+unicode(x.data)+"]\n" + x.descripcio + "\n\n" for x in self.cauticketcomentari_set.all()]) + 
                      "Pots visualitzar-ne els detalls a l'apartat d'eines de la intranet.\n\n" + "\n" +
                      "http://web.idi-cat.org/cau/incidencia/"+ unicode(self.id) + "/\n\n", 
                      settings.DEFAULT_FROM_EMAIL, [self.assignat_a.email], fail_silently=False)
    def notifica_usuari(self):
        if self.assignat_a != None:
            send_mail("[IDI] Tiquet #" + unicode(self.id) + " - " + unicode(self.get_estat_display()), 
                      "El tiquet #" + unicode(self.id) + " ha estat actualitzat. \n\n" +
                      "Resum: \n\n" + self.resum + "\n\n" +
                      "Comentaris: \n\n" + "".join(["["+unicode(x.data)+"]\n" + x.descripcio + "\n\n" for x in self.cauticketcomentari_set.all()]) + "\n" +                     
                      "Pots visualitzar-ne els detalls a l'apartat d'eines de la intranet.\n\n" +
                      "http://web.idi-cat.org/cau/incidencia/"+ str(self.id) + "/\n\n",
                      settings.DEFAULT_FROM_EMAIL, [self.informat_per.email], fail_silently=False)
    def __unicode__(self):
        return   u'Tiket:' + str( self.id ) 
    
class CauTicketHistoricPacs(models.Model):
    id = models.AutoField(primary_key=True)
    resum = models.CharField(max_length=135)
    descripcio = models.CharField(max_length=1500, null=True,  blank=True)
    categoria = models.ForeignKey(CauCategoria,blank=True)# 17 es SSCC
    unitat_idi = models.ForeignKey(MetaUnitatIdi,blank=True,default=17)# 17 es SSCC
    informat_per = models.ForeignKey(AuthUser, related_name="%(app_label)s_%(class)s_informat_per_related") # Field name made lowercase.
    prioritat = models.IntegerField( )
    estat = models.IntegerField(choices=ESTAT_VALUES, default=1)
    data_apertura = models.DateTimeField()
    assignat_a = models.ForeignKey(AuthUser, null=True, blank=True,  related_name="%(app_label)s_%(class)s_assignat_a_related") # Field name made lowercase.
    modificat_per =models.ForeignKey(AuthUser, null=True,  blank=True,related_name="%(app_label)s_%(class)s_modificat_per_related") # Field name made lowercase.
    data_modificacio = models.DateTimeField()
    tancat_per = models.ForeignKey(AuthUser, null=True,  blank=True,related_name="%(app_label)s_%(class)s_tancat_per_related") # Field name made lowercase.
    data_tancament = models.DateTimeField( null=True,  blank=True)    
    #id = models.AutoField(primary_key=True,default=None)
    #cau_tiket = models.OneToOneField(CauTicket, unique=True )
    pacs = models.ForeignKey(CauPacs, null=False,  blank=False)
    id_pacient = models.BigIntegerField( )
    pacient_nom = models.CharField(max_length=135, null=True,  blank=True )
    pacient_cognom1 = models.CharField(max_length=135, null=True,  blank=True)
    pacient_cognom2 = models.CharField(max_length=135, null=True,  blank=True)
    data_i_hora_prestacio = models.DateField( verbose_name='Data i hora de prestació')
    accio_demanada = models.CharField(max_length=500, null=True,  blank=True , verbose_name='Descripció')
    accio_realitzada = models.CharField(max_length=500 ,null=True,  blank=True)
    class Meta:
        db_table = u'cau_ticket_historicpacs'
    def __unicode__(self):
        return u'Tiket:' 
            
