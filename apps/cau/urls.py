# coding: utf-8
from django.conf.urls.defaults import url,patterns 

urlpatterns = patterns('apps.cau.views',
    url(r'^$', 'index'), #
    #tiketAccesHistoricPACS
    #tiketAccesHistoricPACS
    url(r'^tiket-acces-historic-pacs/(?P<tiket_id>\d+)','nouTiketAccesHistoricPACS'),
    url(r'^recuperacio-estudis-offline/','recuperacioEstudisOffline'),
    url(r'^informatica/', 'incidencies', name="cau_informatica"),
    url(r'^incidencies/', 'index'),
    url(r'^incidencia/(.*)/', 'detallIncidencia'),
   )