# coding: utf8
#from apps import django_tables2 as tables
import django_tables2 as tables
from django.utils.safestring import mark_safe
from django_tables2.utils import A  # alias for Accessor
        
class HtmlColumn(tables.Column):
    def render(self, value):
        return mark_safe(value)
    
#todo: aixo es pot fer en una fucio que reotrni la clase perque son identiques pero canviant poques coses
class IncidenciesTable(tables.Table):
    #id = HtmlColumn(verbose_name="# Tiquet")
    id = tables.LinkColumn('apps.cau.views.detallIncidencia', args=[A('id')])
    resum = HtmlColumn(verbose_name='Resum')
    #descripcio = HtmlColumn(verbose_name='Descripció')
    categoria = HtmlColumn(verbose_name='Categoria')
    unitat_idi = HtmlColumn(verbose_name='Unitat IDI')
    estat = HtmlColumn(verbose_name='Estat')
    #informat_per = HtmlColumn(verbose_name='Creat per')
    prioritat = HtmlColumn(verbose_name='Prioritat')
    #data_apertura = HtmlColumn(verbose_name='Data creació')
    assignat_a = HtmlColumn(verbose_name='Assignat a')
    data_modificacio = HtmlColumn(verbose_name='Data Modificació')
    
    class Meta:
        attrs = {'class': 'paleblue'}
        order_by = '-id'
    def render_assignat_a(self, value):
        ret = ""
        if value == None:
            ret = "Sense assignar"
        else:
            ret = value
        return mark_safe(ret) 
    
    def render_data_modificacio(self, value, record):
        ret = ""
        if value == None:
            ret = record.data_apertura
        else:
            ret = value
        return mark_safe(ret) 
    