# coding: utf-8
# 

from apps.cau.models import CauTicket, CauCategoria, CauPacs, CauTicketHistoricPacs, CauTicketComentari
from django.contrib import admin
from apps.cau.forms import CauTicketComentariAdminForm
from apps.meta.models import AuthUser
import datetime

class CauTicketComentariInline(admin.StackedInline):
    model = CauTicketComentari
    #exclude = ['data']
    readonly_fields = ['data', 'user']
    can_delete = False
    form = CauTicketComentariAdminForm
    extra = 1
    def response_add(self, request, obj, post_url_continue=None):
        return False
        obj.user = AuthUser.objects.get(id=request.user.id)
        #obj.save()
        return super(CauTicketComentariInline, self).response_add(request, obj)
        

class CauTicketAdmin(admin.ModelAdmin):
    model = CauTicket
    list_filter = ['estat', 'assignat_a', 'unitat_idi']
    list_display = ['id', 'resum', 'unitat_idi', 'informat_per', 'assignat_a', 'data_apertura', 'data_modificacio', 'estat']
    #readonly_fields = ['informat_per', 'data_modificacio', 'data_tancament', 'modificat_per', 'tancat_per', 'resum', 'descripcio', 'data_apertura', 'categoria']    
    readonly_fields = ['informat_per', 'data_modificacio', 'data_tancament', 'modificat_per', 'tancat_per', 'data_apertura' ]
    fields = ['resum', 'descripcio', 'informat_per', 'categoria', 'unitat_idi', 'prioritat', 'estat', 
              'data_apertura', 'assignat_a', 'telefon', 
               'data_modificacio', 'data_tancament', 'modificat_per', 'tancat_per', ]
    inlines = [CauTicketComentariInline]
    search_fields = ['descripcio', 'resum']
    def has_add_permission(self, request):
        return False
    def response_add(self, request, obj, post_url_continue=None):
        super(CauTicketAdmin, self).response_add(request, obj)
        from django.http import HttpResponseRedirect
        return HttpResponseRedirect("/admin/cau/cauticket/?estat__exact=1&assignat_a__id__exact=%d" % request.user.id )
        
    def response_change(self, request, obj, post_url_continue=None):
        # Enviar sempre un mail a la persona que la té assignada i la persona que l'ha obert
        
        obj.modificat_per = AuthUser.objects.get(id=request.user.id)
        obj.data_modificacio = datetime.datetime.now()
        try:
            ob =  obj.cauticketcomentari_set.all()[0]
            ob.user_id = request.user.id#en algun moment hem de carregar el usuari 
            ob.save()
        except:
            pass
        
        if obj.estat == 2 and obj.tancat_per == None:
            obj.tancat_per = obj.modificat_per
            obj.data_tancament = datetime.datetime.now()
        obj.save()
        comentaris = obj.cauticketcomentari_set.all()
        intern = False
        try:
            if comentaris[0].tipus:
                intern = True
        except:
            pass
        
        if not intern:
            obj.notifica_usuari()
            obj.notifica_responsable()

        super(CauTicketAdmin, self).response_change(request, obj)
        from django.http import HttpResponseRedirect
        return HttpResponseRedirect("/admin/cau/cauticket/?estat__exact=1&assignat_a__id__exact=%d" % request.user.id )
    
    
admin.site.register(CauTicket, CauTicketAdmin)
admin.site.register(CauCategoria)
admin.site.register(CauPacs)
admin.site.register(CauTicketHistoricPacs)
