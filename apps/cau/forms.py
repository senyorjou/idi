# coding: utf-8
from apps.cau.models import CauTicketHistoricPacs, CauTicket, CauTicketComentari
from django.forms import ModelForm, Textarea, Select
from apps.meta.models import ROFormMixin
from django.contrib.admin import widgets 
from django import forms

class CauTicketComentariAdminForm(forms.ModelForm):
    
    descripcio = forms.CharField( widget=forms.Textarea )
    class Meta:
        model = CauTicketComentari

class CauTicketUserForm(ModelForm):
    class Meta:
        model = CauTicket 
        fields = ['categoria', 'prioritat', 'resum', 'telefon', 'descripcio', 'adjunt']
        widgets = {
            'descripcio': Textarea(attrs={'rows':10, 'cols':80}),
        }        
        exclude = ('adjunt')
    def __init__(self, *args, **kwargs):
        super(CauTicketUserForm, self).__init__(*args, **kwargs)
        self.fields['resum'].widget.attrs.update({'style' : 'width: 300px;'})
        
class CauTicketUserROForm(ModelForm):
    read_only = ('data')
    class Meta:
        model = CauTicket
        exclude = ('telefon') 
        #read_only = ('resum')
        widgets = {
            'descripcio': Textarea(attrs={'rows':10, 'cols':80}),          
        }
    def __init__(self, *args, **kwargs):
        super(CauTicketUserROForm, self).__init__(*args, **kwargs)
        self.fields['resum'].widget.attrs.update({'style' : 'width: 300px;'})
        for field in self.fields.itervalues():
            try:
                attrs = field.widget.attrs
                attrs['disabled'] = 'disabled'
                field.widget.attrs = attrs     
            except:
                pass 

class CauTicketComentariForm(ModelForm):

    class Meta:
        model = CauTicketComentari
        exclude = ('data', 'ticket', 'user')
        widgets = {
            'descripcio': Textarea(attrs={'rows':10, 'cols':80}),          
        }
    def __init__(self, *args, **kwargs):
        super(CauTicketComentariForm, self).__init__(*args, **kwargs)

                 
class CauTicketComentariROForm(ModelForm):
    class Meta:
        model = CauTicketComentari
        exclude = ('ticket')
        widgets = {
            'descripcio': Textarea(attrs={'rows':10, 'cols':80}),          
        }
    def __init__(self, *args, **kwargs):
        super(CauTicketComentariROForm, self).__init__(*args, **kwargs)
        self.fields['data'].widget.attrs.update({'style' : 'width: 130px;'})
        for field in self.fields.itervalues():
            try:
                attrs = field.widget.attrs
                attrs['disabled'] = 'disabled'
                field.widget.attrs = attrs     
            except:
                pass         
                    
class CauTicket_FormBase(ModelForm):
        class Meta:
            model = CauTicket
            fields = ['resum','descripcio','categoria' ,'unitat_idi' ,'informat_per','prioritat','estat' ,'data_apertura','assignat_a' ,'modificat_per' ,'data_modificacio','tancat_per' ,'data_tancament' ] 


class CauTicket_Form(CauTicket_FormBase ,ROFormMixin):
        read_only = ()
        fields = ['resum' , 'descripcio', 'categoria']
        
        def __init__(self, *args, **kwargs):
            #hereda de provaohdim i de ROmixin on fara que els camps defints ro siguin read only
            super(CauTicket_Form, self).__init__(*args, **kwargs) 
            #self.fields['data_i_hora_prestacio'].widget = widgets.AdminSplitDateTime()
            self.fields['descripcio'].widget = Textarea(attrs={'rows':10, 'cols':80})
            self.fields['resum'].widget.attrs.update({'style' : 'width: 300px;'})

class CauTicketHistoricPacs_FormBase(ModelForm):
        class Meta:
            model = CauTicketHistoricPacs
            fields = [  'pacs', 'id_pacient','pacient_nom','pacient_cognom1','pacient_cognom2','data_i_hora_prestacio','accio_demanada']

class CauTicketHistoricPacs_Form(CauTicketHistoricPacs_FormBase ,ROFormMixin):
        read_only = ()
        def __init__(self, *args, **kwargs):
            #hereda de provaohdim i de ROmixin on fara que els camps defints ro siguin read only
            super(CauTicketHistoricPacs_Form, self).__init__(*args, **kwargs) 
            #self.fields['data_i_hora_prestacio'].widget = widgets.AdminSplitDateTime()
            self.fields['data_i_hora_prestacio'].widget = widgets.AdminDateWidget()
            self.fields['accio_demanada'].widget = Textarea(attrs={'rows':4, 'cols':40})
                                




