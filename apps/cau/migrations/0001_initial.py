# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CauCategoria'
        db.create_table(u'cau_categoria', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('categoria', self.gf('django.db.models.fields.CharField')(max_length=45)),
        ))
        db.send_create_signal('cau', ['CauCategoria'])

        # Adding model 'CauPacs'
        db.create_table(u'cau_pacs', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pacs', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=45)),
        ))
        db.send_create_signal('cau', ['CauPacs'])

        # Adding model 'CauTicketComentari'
        db.create_table(u'cau_ticket_comentari', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('descripcio', self.gf('django.db.models.fields.TextField')(max_length=1500, null=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.AuthUser'], null=True, blank=True)),
            ('data', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('ticket', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cau.CauTicket'], null=True, blank=True)),
        ))
        db.send_create_signal('cau', ['CauTicketComentari'])

        # Adding model 'CauTicket'
        db.create_table(u'cau_ticket', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('resum', self.gf('django.db.models.fields.CharField')(max_length=135)),
            ('descripcio', self.gf('django.db.models.fields.TextField')(max_length=1500, null=True, blank=True)),
            ('categoria', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cau.CauCategoria'], blank=True)),
            ('unitat_idi', self.gf('django.db.models.fields.related.ForeignKey')(default=17, to=orm['meta.MetaUnitatIdi'], blank=True)),
            ('informat_per', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cau_cauticket_informat_per_related', to=orm['meta.AuthUser'])),
            ('prioritat', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('estat', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('data_apertura', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now)),
            ('assignat_a', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='cau_cauticket_assignat_a_related', null=True, to=orm['meta.AuthUser'])),
            ('modificat_per', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='cau_cauticket_modificat_per_related', null=True, to=orm['meta.AuthUser'])),
            ('data_modificacio', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('tancat_per', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='cau_cauticket_tancat_per_related', null=True, to=orm['meta.AuthUser'])),
            ('data_tancament', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('telefon', self.gf('django.db.models.fields.CharField')(max_length=45, null=True, blank=True)),
            ('adjunt', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('cau', ['CauTicket'])

        # Adding model 'CauTicketHistoricPacs'
        db.create_table(u'cau_ticket_historicpacs', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('resum', self.gf('django.db.models.fields.CharField')(max_length=135)),
            ('descripcio', self.gf('django.db.models.fields.CharField')(max_length=1500, null=True, blank=True)),
            ('categoria', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cau.CauCategoria'], blank=True)),
            ('unitat_idi', self.gf('django.db.models.fields.related.ForeignKey')(default=17, to=orm['meta.MetaUnitatIdi'], blank=True)),
            ('informat_per', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cau_cautickethistoricpacs_informat_per_related', to=orm['meta.AuthUser'])),
            ('prioritat', self.gf('django.db.models.fields.IntegerField')()),
            ('estat', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('data_apertura', self.gf('django.db.models.fields.DateTimeField')()),
            ('assignat_a', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='cau_cautickethistoricpacs_assignat_a_related', null=True, to=orm['meta.AuthUser'])),
            ('modificat_per', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='cau_cautickethistoricpacs_modificat_per_related', null=True, to=orm['meta.AuthUser'])),
            ('data_modificacio', self.gf('django.db.models.fields.DateTimeField')()),
            ('tancat_per', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='cau_cautickethistoricpacs_tancat_per_related', null=True, to=orm['meta.AuthUser'])),
            ('data_tancament', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('pacs', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cau.CauPacs'])),
            ('id_pacient', self.gf('django.db.models.fields.BigIntegerField')()),
            ('pacient_nom', self.gf('django.db.models.fields.CharField')(max_length=135, null=True, blank=True)),
            ('pacient_cognom1', self.gf('django.db.models.fields.CharField')(max_length=135, null=True, blank=True)),
            ('pacient_cognom2', self.gf('django.db.models.fields.CharField')(max_length=135, null=True, blank=True)),
            ('data_i_hora_prestacio', self.gf('django.db.models.fields.DateField')()),
            ('accio_demanada', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
            ('accio_realitzada', self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True)),
        ))
        db.send_create_signal('cau', ['CauTicketHistoricPacs'])


    def backwards(self, orm):
        # Deleting model 'CauCategoria'
        db.delete_table(u'cau_categoria')

        # Deleting model 'CauPacs'
        db.delete_table(u'cau_pacs')

        # Deleting model 'CauTicketComentari'
        db.delete_table(u'cau_ticket_comentari')

        # Deleting model 'CauTicket'
        db.delete_table(u'cau_ticket')

        # Deleting model 'CauTicketHistoricPacs'
        db.delete_table(u'cau_ticket_historicpacs')


    models = {
        'cau.caucategoria': {
            'Meta': {'ordering': "['-categoria']", 'object_name': 'CauCategoria', 'db_table': "u'cau_categoria'"},
            'categoria': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cau.caupacs': {
            'Meta': {'object_name': 'CauPacs', 'db_table': "u'cau_pacs'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pacs': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        'cau.cauticket': {
            'Meta': {'object_name': 'CauTicket', 'db_table': "u'cau_ticket'"},
            'adjunt': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'assignat_a': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cau_cauticket_assignat_a_related'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cau.CauCategoria']", 'blank': 'True'}),
            'data_apertura': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'data_modificacio': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'data_tancament': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'descripcio': ('django.db.models.fields.TextField', [], {'max_length': '1500', 'null': 'True', 'blank': 'True'}),
            'estat': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'informat_per': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cau_cauticket_informat_per_related'", 'to': "orm['meta.AuthUser']"}),
            'modificat_per': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cau_cauticket_modificat_per_related'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'prioritat': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'resum': ('django.db.models.fields.CharField', [], {'max_length': '135'}),
            'tancat_per': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cau_cauticket_tancat_per_related'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'telefon': ('django.db.models.fields.CharField', [], {'max_length': '45', 'null': 'True', 'blank': 'True'}),
            'unitat_idi': ('django.db.models.fields.related.ForeignKey', [], {'default': '17', 'to': "orm['meta.MetaUnitatIdi']", 'blank': 'True'})
        },
        'cau.cauticketcomentari': {
            'Meta': {'ordering': "['-id']", 'object_name': 'CauTicketComentari', 'db_table': "u'cau_ticket_comentari'"},
            'data': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'descripcio': ('django.db.models.fields.TextField', [], {'max_length': '1500', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ticket': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cau.CauTicket']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.AuthUser']", 'null': 'True', 'blank': 'True'})
        },
        'cau.cautickethistoricpacs': {
            'Meta': {'object_name': 'CauTicketHistoricPacs', 'db_table': "u'cau_ticket_historicpacs'"},
            'accio_demanada': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'accio_realitzada': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'assignat_a': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cau_cautickethistoricpacs_assignat_a_related'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cau.CauCategoria']", 'blank': 'True'}),
            'data_apertura': ('django.db.models.fields.DateTimeField', [], {}),
            'data_i_hora_prestacio': ('django.db.models.fields.DateField', [], {}),
            'data_modificacio': ('django.db.models.fields.DateTimeField', [], {}),
            'data_tancament': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '1500', 'null': 'True', 'blank': 'True'}),
            'estat': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_pacient': ('django.db.models.fields.BigIntegerField', [], {}),
            'informat_per': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cau_cautickethistoricpacs_informat_per_related'", 'to': "orm['meta.AuthUser']"}),
            'modificat_per': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cau_cautickethistoricpacs_modificat_per_related'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'pacient_cognom1': ('django.db.models.fields.CharField', [], {'max_length': '135', 'null': 'True', 'blank': 'True'}),
            'pacient_cognom2': ('django.db.models.fields.CharField', [], {'max_length': '135', 'null': 'True', 'blank': 'True'}),
            'pacient_nom': ('django.db.models.fields.CharField', [], {'max_length': '135', 'null': 'True', 'blank': 'True'}),
            'pacs': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cau.CauPacs']"}),
            'prioritat': ('django.db.models.fields.IntegerField', [], {}),
            'resum': ('django.db.models.fields.CharField', [], {'max_length': '135'}),
            'tancat_per': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cau_cautickethistoricpacs_tancat_per_related'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'unitat_idi': ('django.db.models.fields.related.ForeignKey', [], {'default': '17', 'to': "orm['meta.MetaUnitatIdi']", 'blank': 'True'})
        },
        'meta.authuser': {
            'Meta': {'object_name': 'AuthUser', 'db_table': "u'auth_user'"},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '225'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.IntegerField', [], {}),
            'is_staff': ('django.db.models.fields.IntegerField', [], {}),
            'is_superuser': ('django.db.models.fields.IntegerField', [], {}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '384'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '90'})
        },
        'meta.metacentreidi': {
            'Meta': {'ordering': "['descripcio']", 'object_name': 'MetaCentreIdi', 'db_table': "u'meta_centre_idi'"},
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'meta.metaunitatidi': {
            'Meta': {'ordering': "['descripcio']", 'object_name': 'MetaUnitatIdi', 'db_table': "u'meta_unitat_idi'"},
            'actiu': ('django.db.models.fields.IntegerField', [], {}),
            'calendari_laboral': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['planilles.CalendariLaboral']", 'null': 'True'}),
            'centre_idi': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCentreIdi']"}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unitatat_facturacio_id': ('django.db.models.fields.IntegerField', [], {})
        },
        'planilles.calendarilaboral': {
            'Meta': {'ordering': "['nom']", 'object_name': 'CalendariLaboral', 'db_table': "u'plan_calendari_laboral'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'es_mestre': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['cau']