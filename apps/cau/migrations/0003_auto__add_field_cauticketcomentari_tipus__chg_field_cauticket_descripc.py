# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'CauTicketComentari.tipus'
        db.add_column(u'cau_ticket_comentari', 'tipus',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


        # Changing field 'CauTicket.descripcio'
        db.alter_column(u'cau_ticket', 'descripcio', self.gf('django.db.models.fields.TextField')(max_length=3000, null=True))

    def backwards(self, orm):
        # Deleting field 'CauTicketComentari.tipus'
        db.delete_column(u'cau_ticket_comentari', 'tipus')


        # Changing field 'CauTicket.descripcio'
        db.alter_column(u'cau_ticket', 'descripcio', self.gf('django.db.models.fields.TextField')(max_length=1500, null=True))

    models = {
        'cau.caucategoria': {
            'Meta': {'ordering': "['-categoria']", 'object_name': 'CauCategoria', 'db_table': "u'cau_categoria'"},
            'categoria': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'cau.caupacs': {
            'Meta': {'object_name': 'CauPacs', 'db_table': "u'cau_pacs'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pacs': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '45'})
        },
        'cau.cauticket': {
            'Meta': {'object_name': 'CauTicket', 'db_table': "u'cau_ticket'"},
            'adjunt': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'assignat_a': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cau_cauticket_assignat_a_related'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'categoria': ('django.db.models.fields.related.ForeignKey', [], {'default': '2', 'to': "orm['cau.CauCategoria']", 'blank': 'True'}),
            'data_apertura': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'data_modificacio': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'data_tancament': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'descripcio': ('django.db.models.fields.TextField', [], {'max_length': '3000', 'null': 'True', 'blank': 'True'}),
            'estat': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'informat_per': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cau_cauticket_informat_per_related'", 'to': "orm['meta.AuthUser']"}),
            'modificat_per': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cau_cauticket_modificat_per_related'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'prioritat': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'resum': ('django.db.models.fields.CharField', [], {'max_length': '135'}),
            'tancat_per': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cau_cauticket_tancat_per_related'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'telefon': ('django.db.models.fields.CharField', [], {'max_length': '45', 'null': 'True', 'blank': 'True'}),
            'unitat_idi': ('django.db.models.fields.related.ForeignKey', [], {'default': '17', 'to': "orm['meta.MetaUnitatIdi']", 'blank': 'True'})
        },
        'cau.cauticketcomentari': {
            'Meta': {'ordering': "['-id']", 'object_name': 'CauTicketComentari', 'db_table': "u'cau_ticket_comentari'"},
            'data': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'descripcio': ('django.db.models.fields.TextField', [], {'max_length': '3000', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ticket': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cau.CauTicket']", 'null': 'True', 'blank': 'True'}),
            'tipus': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.AuthUser']", 'null': 'True', 'blank': 'True'})
        },
        'cau.cautickethistoricpacs': {
            'Meta': {'object_name': 'CauTicketHistoricPacs', 'db_table': "u'cau_ticket_historicpacs'"},
            'accio_demanada': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'accio_realitzada': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'assignat_a': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cau_cautickethistoricpacs_assignat_a_related'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cau.CauCategoria']", 'blank': 'True'}),
            'data_apertura': ('django.db.models.fields.DateTimeField', [], {}),
            'data_i_hora_prestacio': ('django.db.models.fields.DateField', [], {}),
            'data_modificacio': ('django.db.models.fields.DateTimeField', [], {}),
            'data_tancament': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '1500', 'null': 'True', 'blank': 'True'}),
            'estat': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_pacient': ('django.db.models.fields.BigIntegerField', [], {}),
            'informat_per': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cau_cautickethistoricpacs_informat_per_related'", 'to': "orm['meta.AuthUser']"}),
            'modificat_per': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cau_cautickethistoricpacs_modificat_per_related'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'pacient_cognom1': ('django.db.models.fields.CharField', [], {'max_length': '135', 'null': 'True', 'blank': 'True'}),
            'pacient_cognom2': ('django.db.models.fields.CharField', [], {'max_length': '135', 'null': 'True', 'blank': 'True'}),
            'pacient_nom': ('django.db.models.fields.CharField', [], {'max_length': '135', 'null': 'True', 'blank': 'True'}),
            'pacs': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cau.CauPacs']"}),
            'prioritat': ('django.db.models.fields.IntegerField', [], {}),
            'resum': ('django.db.models.fields.CharField', [], {'max_length': '135'}),
            'tancat_per': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cau_cautickethistoricpacs_tancat_per_related'", 'null': 'True', 'to': "orm['meta.AuthUser']"}),
            'unitat_idi': ('django.db.models.fields.related.ForeignKey', [], {'default': '17', 'to': "orm['meta.MetaUnitatIdi']", 'blank': 'True'})
        },
        'idirh.calendarilaboral': {
            'Meta': {'ordering': "['nom']", 'object_name': 'CalendariLaboral', 'db_table': "u'plan_calendari_laboral'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'es_mestre': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'meta.authuser': {
            'Meta': {'object_name': 'AuthUser', 'db_table': "u'auth_user'"},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '225'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.IntegerField', [], {}),
            'is_staff': ('django.db.models.fields.IntegerField', [], {}),
            'is_superuser': ('django.db.models.fields.IntegerField', [], {}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '384'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '90'})
        },
        'meta.metacentreidi': {
            'Meta': {'ordering': "['descripcio']", 'object_name': 'MetaCentreIdi', 'db_table': "u'meta_centre_idi'"},
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'meta.metaunitatidi': {
            'Meta': {'ordering': "['descripcio']", 'object_name': 'MetaUnitatIdi', 'db_table': "u'meta_unitat_idi'"},
            'actiu': ('django.db.models.fields.IntegerField', [], {}),
            'calendari_laboral': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['idirh.CalendariLaboral']", 'null': 'True'}),
            'centre_idi': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCentreIdi']"}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unitatat_facturacio_id': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['cau']