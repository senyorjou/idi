# coding: utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required,permission_required
import apps.meta.auditoria
from apps.meta.models import AuthUserProfile, AuthUser, MetaUnitatIdi
from django.shortcuts import redirect
from apps.cau.forms import CauTicket, CauTicketHistoricPacs, CauTicketHistoricPacs_Form, CauTicketUserForm, CauTicketUserROForm, CauTicketComentariROForm, CauTicketComentariForm, CauTicket_Form
from apps.cau.models import CauCategoria, CauPacs, CauTicketComentari
from apps.cau.tables import IncidenciesTable
import datetime,calendar,time
from django.core.mail import send_mail
import os
from apps.quadre_comandament.common import initFilters
from django.conf import settings

objectiu = "Peticions de suport"
indicadors = ["Informàtica", "Recuperació estudis offline"]

@login_required
#@permission_required('meta.guardies', login_url='/accounts/permisdenegat/')
def index(request):
    #apps.meta.auditoria.request_handler(request,12,'' )
    request.session['enabled_filters'] = {}
    return redirect('cau_informatica')
    #return render_to_response('cau/index.html', {"indicadors":indicadors, "objectiu": objectiu}, context_instance=RequestContext(request))

@login_required
def detallIncidencia(request, ticket_id):
    try:
        tiquet = CauTicket.objects.get(id=ticket_id)
    except:
        return redirect('/cau/informatica/')
            
    if tiquet.informat_per_id == request.user.id or request.user.is_staff == True:
        form = CauTicketUserROForm(instance=tiquet)
        
        if request.method == 'POST':
            comentari_form = CauTicketComentariForm(request.POST) # A form bound to the POST data
            if comentari_form.is_valid():
                descripcio = comentari_form.cleaned_data['descripcio']
                usuari = AuthUser.objects.get(username =str(request.user))
                c = CauTicketComentari(descripcio=descripcio, ticket=tiquet, user=usuari)
                c.save()
                if c.user == tiquet.informat_per:
                    c.notifica_responsable()
                elif c.user == tiquet.assignat_a:
                    c.notifica_usuari()
                else:
                    c.notifica_responsable()
                    c.notifica_usuari()
                
        comentari_form = CauTicketComentariForm() # An unbound form                    
        comentaris = []
        for comentari in tiquet.cauticketcomentari_set.all():
            comentaris.append(CauTicketComentariROForm(instance=comentari))
            
        
        return render_to_response('cau/detall_incidencia.html', 
                           {"indicadors":indicadors, 
                            "objectiu": objectiu,
                            "form": form,
                            "comentari_form": comentari_form,
                            "comentaris": comentaris,
                            "is_informatic": request.user.is_superuser,
                            "tiquet_id": tiquet.id},
                          context_instance=RequestContext(request))


@login_required
def incidencies(request):
    
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'incidencies' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'unitat': 0}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    unitatsel = request.session['enabled_filters']['unitat']

    if request.method == 'POST':        
        form = CauTicketUserForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            categoria = form.cleaned_data['categoria']
            resum = form.cleaned_data['resum']
            descripcio = form.cleaned_data['descripcio']
            telefon = form.cleaned_data['telefon']
            prioritat = form.cleaned_data['prioritat']
            usuari = AuthUser.objects.get(username="41084990")
            unitat = request.user.get_profile().unitat_idi
            if unitatsel != 0:
                unitat = MetaUnitatIdi.objects.get(id=unitatsel)
            c = CauTicket(resum=resum, descripcio=descripcio, categoria=categoria, prioritat=prioritat, estat=1,
                          telefon=telefon, informat_per_id=request.user.id, assignat_a=usuari, unitat_idi=unitat)
            c.save()
            c.notifica_responsable()
            
    form = CauTicketUserForm() # An unbound form
    tickets = CauTicket.objects.all().filter(informat_per__id=request.user.id)
    
    table = IncidenciesTable(tickets, order_by=request.GET.get('sort'))
    table.paginate(page=request.GET.get('page', 1), per_page=10)
    
    definicio = "Tot seguit apareix el llistat d'incidències informàtiques, juntament amb un formulari d'alta per crear-ne de noves."
    
    return render_to_response('cau/incidencies.html', 
                               {"indicadors":indicadors, 
                                "objectiu": objectiu,
                                "definicio": definicio,
                                "is_informatic": request.user.is_superuser,
                                "taula": table,
                                "form": form},
                              context_instance=RequestContext(request)) 

@login_required
def recuperacioEstudisOffline(request):
    
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'recuperacio-estudis-offline' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'unitat': 0}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]
    
    unitatsel = request.session['enabled_filters']['unitat']
    
    missatge = ""
    url_ret = ''
    llistat=[0,0,0,0,0,0,0,0,0,0]
    rows = []

    # TODO: Canviar la comprobació per una de més fidel a saber si som informàtics o no
    if AuthUserProfile.objects.get( user = AuthUser.objects.get(username =str(request.user)).id).categoria_profesional.id in (15,17,21,36):
        resp = CauTicketHistoricPacs.objects.all()
    else:# nems les que ha fet lusuari hara paer haram, s'ahurien de fer la del centre
        resp = CauTicketHistoricPacs.objects.all().filter(informat_per__username = str(request.user))

    if unitatsel != 0:
        resp = resp.filter(unitat_idi__id=unitatsel)
    
    for e in resp:
        llistat = ( '',
        str(e.id),
        #e.resum,
        e.id_pacient,
        e.pacient_cognom1 + ' ' + e.pacient_cognom2 + ', '  + e.pacient_nom, 
        e.data_i_hora_prestacio,
        e.pacs,
        e.accio_demanada,
        e.get_estat_display(),
        #e.accio_realitzada,
        "<a href='../tiket-acces-historic-pacs/" + str(e.id) + "' class='changelink'></a>"
        )
        rows.append(llistat)
   
    columns = ['Nº Tiket','Id Pacient','Pacient','Data','PACS','Acció','Estat','Edita' ]   
    
    definicio = "Llistat de peticions de recuperació d'estudis offline"
    
    return render_to_response('cau/recuperacioEstudisOffline.html', 
                               {
                                "indicadors": indicadors, 
                                "objectiu": objectiu,
                                "definicio": definicio,
                                "columns": columns,
                                "rows": rows,
                                'titol': "Tikets generats.",
                                                            },
                              context_instance=RequestContext(request)) 




@login_required
#@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def nouTiketAccesHistoricPACS (request, tiket_id):

    missatge = ""
    url_ret = ''
    form = None
    subform = None
    
    if 'HTTP_REFERER' in request.environ :
        if not 'modificar' in request.environ['HTTP_REFERER']:
            url_ret = request.environ['HTTP_REFERER']
            request.session['url_ret'] = url_ret
        else:
            if  'url_ret' in  request.session:
                if not 'modificar' in  request.session['url_ret']:
                    url_ret = request.session['url_ret']
            else:
                url_ret = '' 

    usuari = AuthUser.objects.get(username =str(request.user))
    data = datetime.datetime.now()   
    hubicacio =  AuthUserProfile.objects.get( user =usuari.id).unitat_idi
    es_informatic = AuthUserProfile.objects.get( user = usuari.id).categoria_profesional.id in (15,17,21,36) 
    #es_informatic = False #PER FER PROVESSSSSSSSSSS

    if request.method == 'POST':
        
        if tiket_id == '0':
            tiket_id = request.POST.get('tiket_id',0)
            
        if not CauTicketHistoricPacs.objects.filter(id=tiket_id).exists():
            #es nou tket amb dades passades:   
            subform = CauTicketHistoricPacs_Form(request.POST)#creem el objecte amb les dades del formulari

            if subform.is_valid():
                
                subobj = subform.save(commit=False)
                subobj.resum = 'Accés a prestacions del Historic PACS I.D.I.'
                subobj.descripcio = 'Accés a prestacions del Historic PACS I.D.I.'
                subobj.categoria = CauCategoria.objects.get(pk=1)
                subobj.unitat_idi = hubicacio
                subobj.informat_per = usuari 
                subobj.prioritat = 1                
                subobj.estat = 1
                subobj.data_apertura = data
                subobj.data_modificacio = data
                subobj.save()
                tiket_id = subobj.id
                #s'ah grabat i ja tenim un id nou,el recearreguem amb els nous valors al formulari
                subobjnou = subobj
                missatge = "s'ha creat un nou tiket!: '" + unicode(subobj.id) + "'   " + " I S'ha enviat un correu a la seva bustia amb les dades del tiket"
                #ENVIEM CORREUS 
                subject = 'tiket creat ' + str(tiket_id)
                message = unicode(subobjnou)
                sender =  settings.SERVER_EMAIL
                recipients = [settings.MAIL_INFORMATICA]
                if sender:# si te correu li enviem un correu
                    recipients.append(sender)
                #enviem si esta en producció sino no que s'ha de tenir configurat el servidor de correu i es un rollo
                if  str(os.environ['DJANGO_SETTINGS_MODULE']) == 'apps.settings':            
                    send_mail(subject, message, sender, recipients)
                else:
                    print 'subject: ' + subject + '; message: ' +  message + '; ender: ' +  sender  + ' ;recipients:' + str(recipients)
                        
        else:
            #estem editant un objecte existent i nomes em de fer update, amb les dades del post i les noves dades que volguem
            obj = CauTicketHistoricPacs.objects.get(pk=tiket_id)
            #nomes passem les dades del tiket si ets informatic , sino no , perque com els valors son ocults es possen a Nuls i kk
            if es_informatic:
                form =   CauTicket_Form(request.POST, instance=obj) # com ainformatics si que voldrem que es pugui editar
            else:
                form =   CauTicket_Form(instance=obj) 

            subform =   CauTicketHistoricPacs_Form(request.POST, instance=obj)
            #if es_informatic and form.is_valid():
             
                
            if  subform.is_valid() :
                
                obj_modificat = form.save(commit=False)# previ a guardar posarem el usuari que fa el update
                subobj_modificat = subform.save(commit=False)
                #Actulitzem qui modifica i quan. 
                obj_modificat.modificat_per = usuari 
                obj_modificat.data_modificacio = data
                
                if str(obj_modificat.estat) == 'assignat':#assignat
                    
                    #enviar correu al assigant i al interessat
                    message = "S'ha assignat aquest tiket a " + str(obj_modificat.assignat_a)  + '   Tiket:' +  str(subobj_modificat)
                    subject = 'tiket assignat '   + str(tiket_id)
                    recipients = ['informatica@idi.catalsut.cat']
                    recipients.append( obj_modificat.assignat_a.email )
                    sender = obj_modificat.informat_per.email
                    if sender:# si te correu li enviem un correu
                        recipients.append(sender)
                    #enviem si esta en producció sino no que s'ha de tenir configurat el servidor de correu i es un rollo
                    if  str(os.environ['DJANGO_SETTINGS_MODULE']) == 'apps.settings':            
                        send_mail(subject, message, sender, recipients)
                    else:
                        print 'ASSIGNAT: subject: ' + subject + '; message: ' +  message + '; sender: ' +  sender  + ' ;recipients:' + str(recipients)
                    
                                        
                if str(obj_modificat.estat) == 'tancat':
                    
                    obj_modificat.tancat_per = usuari
                    obj_modificat.data_tancament = data
                    subject = 'tiket tancat ' + str(tiket_id)
                    message = "S'ha tancat aquest tiket per " + str(usuari)  + '   Tiket:' + str(subobj_modificat)
                    recipients = ['informatica@idi.catalsut.cat']
                    recipients.append( usuari.email )
                    sender =  obj_modificat.informat_per.email
                    if sender:# si te correu li enviem un correu
                        recipients.append(sender)
                    #enviem si esta en producció sino no que s'ha de tenir configurat el servidor de correu i es un rollo
                    if  str(os.environ['DJANGO_SETTINGS_MODULE']) == 'apps.settings':            
                        send_mail(subject, message, sender, recipients)
                    else:
                        print 'TANCAT: subject: ' + subject + '; message: ' +  message + '; sender: ' +  sender  + ' ;recipients:' + str(recipients)
                    
                obj_modificat.save()
                subobj_modificat.save()
                #obj_modificat.save_m2m() #per gravar many to many es necessari al fer el commit = False, en aquest cas no tenim res many yo many
                form = CauTicket_Form(instance=obj_modificat)# per refrescar el canvi
                subform = CauTicketHistoricPacs_Form(instance=subobj_modificat)# per refrescar el canvi
                missatge = "s'han guardat els canvis! , el tiket està " + str(obj_modificat.estat)
    
    else:
        #mostrem el objecte si exxitreix en la bd        
        if CauTicketHistoricPacs.objects.using('default').filter(id=tiket_id).exists():
            obj = CauTicketHistoricPacs.objects.get(pk=tiket_id)
            form = CauTicket_Form(instance=obj)
            subform = CauTicketHistoricPacs_Form(instance=obj)  
                    
        else:#s'ha de crear pero no grabar, es dir mostrar la plantilla per pantalla , amb els valors per defecte:
            
            subform =   CauTicketHistoricPacs_Form( initial={})
            form =   CauTicket_Form( initial={ #'id', 
                                             'resum':'Accés a prestacions del Historic PACS I.D.I.' ,
                                              'descripcio':'Accés a prestacions del Historic PACS I.D.I.',
                                                 'categoria':1,
                                                  'unitat_idi' : hubicacio,
                                                  'informat_per': request.user,
                                                  'prioritat':1,
                                                  'estat':1,
                                                  'data_apertura':data,
                                                  #'assignat_a',
                                                  #'modificat_per',
                                                  'data_modificacio':data,
                                                  #'tancat_per',
                                                  #'data_tancament':data
                                                         })
    
 
    
            
    #auditoria.request_handler(request,8,'Tipus consulta: activitatTotaldetallModificar ; ' + str(request.session['enabled_filters']) + ' ,provaohdim_id:' + str(provaohdim_id) + ',codgen_id:' + str(codgen_id))
    return render_to_response("cau/tiketAccesHistoricPACS.html", 
                                                            { "form": form,
                                                              "subform": subform,  
                                                              "missatge":missatge,
                                                              "tiket_id":tiket_id,
                                                              "url_ret":url_ret,
                                                              "es_informatic":es_informatic
                                                             },
                                                                  context_instance=RequestContext(request)
                                                                )
