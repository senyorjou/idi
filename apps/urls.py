# coding: utf8
'''
Created on 26/07/2011
@author: amateu + asellares
'''

from django.conf.urls.defaults import patterns, include
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from django.views.generic import TemplateView
from django.views.generic.simple import redirect_to
from apps.meta.forms import ValidatingPasswordChangeForm, SetPasswordForm

urlpatterns = patterns('',
    (r'^$', 'apps.meta.views.main_index_view'),
    (r'^my_admin/jsi18n', 'django.views.i18n.javascript_catalog'),  # LLIBREIRA per possar datetime pikers etc
    (r'^meta/about/', TemplateView.as_view(template_name="meta/sobre.html")),
    (r'^admin/', include(admin.site.urls)),
    (r'^accounts/$', 'apps.meta.views.main_index_view'),
    (r'^accounts/login/', 'apps.meta.views.login'),  # https://docs.djangoproject.com/en/1.3/topics/auth/
    (r'^accounts/profile/', 'apps.meta.views.main_index_view'),  # https://docs.djangoproject.com/en/1.3/topics/auth/
    (r'^accounts/permisdenegat/', 'apps.meta.views.permisdenegat'),
    (r'^accounts/permisdenegat_intranet/', 'apps.meta.views.permisdenegat_intranet'),
    (r'^registration/password_reset/$', 'apps.meta.views.password_reset_idi'),
    (r'^registration/password_reset/done/$', 'django.contrib.auth.views.password_reset_done', {'template_name': 'registration/password_reset_done.html'}),
    (r'^registration/reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm', {'template_name': 'registration/password_reset_confirm.html', 'set_password_form': SetPasswordForm}),
    (r'^registration/reset/done/$', 'django.contrib.auth.views.password_reset_complete', {'template_name': 'registration/password_reset_complete.html'}),
    (r'^registration/password_change/$', 'django.contrib.auth.views.password_change', {'password_change_form': ValidatingPasswordChangeForm}),
    (r'^registration/logout/$', 'django.contrib.auth.views.logout'),
    (r'^registration/$', redirect_to, {'url': '/'}),
    (r'^password/done/$', 'django.contrib.auth.views.password_change_done'),
    (r'^historic_rm_vh/', include('apps.historic_rm_vh.urls')),
    (r'^historicRMVH/', include('apps.historic_rm_vh.urls')),  # PERQUE ALGU TE GUARDAT AQUESTA URL en el navegador ...
    (r'^historic_tctr_vh/', include('apps.historic_tctr_vh.urls')),
    (r'^quadre_comandament/', include('apps.quadre_comandament.urls')),
    #(r'^indicadors/', include('apps.indicadors.urls')),
    (r'^guardies/', include('apps.guardies.urls')),
    (r'^intranet/', include('apps.intranet.urls')),
    (r'^rrhh/', include('apps.rrhh.urls')),
    (r'^idirh/', include('apps.idirh.urls')),
    (r'^calaix/', include('apps.calaix.urls')),
    (r'^cau/', include('apps.cau.urls')),
    (r'^seguiment_personal/', include('apps.seguiment_personal.urls')),
    (r'^chaining/', include('apps.smart_selects.urls')),
    (r'^ajax_filtered_fields/', include('apps.ajax_filtered_fields.urls')),
    (r'^inventari/', redirect_to, {'url': '/admin/inventari/'}),
        
)
