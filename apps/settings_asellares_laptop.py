# encoding: utf8

# Django settings for LOGIN project.
DEBUG = TEMPLATE_DEBUG = True

# Sella si cal per a que ens enviii correus quan hi hagui un error has de configurar aixo 
#EMAIL_HOST_USER
#EMAIL_HOST_PASSWORD
#EMAIL_PORT
#EMAIL_USE_TLS
#Per a poder enviar a yahoo , es una prova
DEFAULT_FROM_EMAIL='idi@idi.catsalut.cat'
EMAIL_SUBJECT_PREFIX='No respongueu a aquest correu'

# Aqui tens info http://djangobook.com/en/2.0/chapter12/
ADMINS = (
     ('Albert Mateu', 'amateu@idi.catsalut.cat'),
     ('Albert Sellares', 'asellares@idi.catsalut.cat'),
)


MANAGERS = ADMINS

# Si volem veure intents de paguines que no hi son 
SEND_BROKEN_LINK_EMAILS = True

DATABASE_ROUTERS = ['apps.meta.dbrouter.DBRouter']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'portal',                      # Or path to database file if using sqlite3.
        'USER': 'asellares',                      # Not used with sqlite3.
        'PASSWORD': 'iHC86iUA',                  # Not used with sqlite3.
        'HOST': '10.81.255.10',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
    },
    'facnetDB': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'facnetDB',                      # Or path to database file if using sqlite3.
        'USER': 'asellares',                      # Not used with sqlite3.
        'PASSWORD': 'iHC86iUA',                  # Not used with sqlite3.
        'HOST': '10.81.255.10',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
    },
    'dpos': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'dpos',
        'USER': 'asellares',
        'PASSWORD': 'iHC86iUA',
        'HOST': '10.81.255.10',
        'PORT': '3306',
    },
    'portal_dades': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'portal_dades',
        'USER': 'asellares',
        'PASSWORD': 'iHC86iUA',
        'HOST': '10.81.255.10',
        'PORT': '3306',
    },
    'inventari': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'inventari',
        'USER': 'asellares',
        'PASSWORD': 'iHC86iUA',
        'HOST': '10.81.255.10',
        'PORT': '3306',
    }             
             
             ## SQL SERVERs AMP
           
    ,'idistock_vh': {
        'ENGINE': 'apps.django_sql_server.pyodbc',
        'NAME': 'IdiStock',
        'HOST': 'windows2008SqlServer', 
        'PORT': '1433',
        'USER': 'medisoft',
        'PASSWORD': 'sunami',
        'ODBC_DRIVER': 'SQL Server',
        'OPTIONS' : {
            'driver': 'FreeTDS',
            'extra_params' : "TDS_Version=8.0",
            'COLLATION' : 'Modern_Spanish_CI_AS',
            'CHARSET': 'LATIN1', 'ANSI': True, 
            'unicode_results': True ,
#            'dsn': 'windows2008SqlServer'
            }
        },
             
       'karat': {
        'ENGINE': 'apps.django_sql_server.pyodbc',
        'NAME': 'cache_tmp',
        'HOST': 'windows2008SqlServer', 
        'PORT': '1033',
        'USER': 'medisoft',
        'PASSWORD': 'sunami',
        'ODBC_DRIVER': 'SQL Server',
        'OPTIONS' : {
            'driver': 'FreeTDS',
            'extra_params' : "TDS_Version=8.0",
            'COLLATION' : 'Modern_Spanish_CI_AS',
            'CHARSET': 'LATIN1', 'ANSI': True, 
            'unicode_results': True ,
#            'dsn': 'windows2008SqlServer'
            }
        },             
              
             
                          
#    'default': {
#        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
#        'NAME': 'portal',                      # Or path to database file if using sqlite3.
#        'USER': 'amateuDJango',                      # Not used with sqlite3.
#        'PASSWORD': 'amateuDJango',                  # Not used with sqlite3.
#        'HOST': '172.16.12.18',   #'HOST': '10.81.255.10',                      # Set to empty string for localhost. Not used with sqlite3.
#        'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
#        'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
#        'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
#    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/London'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ca-ES'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = '/tmp/media'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '/home/whats/code/idi/portal/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

gettext = lambda s: s

LANGUAGES = (
    ('ca', gettext('Català')),
)

LOCALE_PATHS = (
    '/mnt/hgfs/idi/portal/apps/planilles/',
    # '/mnt/hgfs/idi/portal/templates/planilles/'
)

# Additional locations of static files
STATICFILES_DIRS = ('/home/whats/code/idi/portal/static',)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'tu@=ijcvkw0k8n%_5-3ey7+&i&cq@zdqe&!1b8i53upmre*2ud'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django_sorting.middleware.SortingMiddleware',
    
)

ROOT_URLCONF = 'apps.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    "/home/whats/code/idi/portal/templates", # Change this to your own directory.
    '/home/whats/code/idi/Django-1.3.1/django/contrib/admin/templates/admin'
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
     #'django.contrib.admindocs',
    'intranet',
    'inventari',
    'cau',
    'guardies',
    'django_sorting',
    'historic_rm_vh',
    'meta',
    'smart_selects',
    'django_tables2',
#    'south',
    'idirh',
    
     
)


SESSION_COOKIE_AGE = 7200

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
#settings.LOGIN_URL


### per autentificacions sobre LDAP o altres source com nosalters tenim mirar
### https://docs.djangoproject.com/en/1.3/topics/auth/  ->  Other authentication sources

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
TEMPLATE_CONTEXT_PROCESSORS = (
                               'django.core.context_processors.auth',
                               'django.core.context_processors.debug',
                               'django.core.context_processors.i18n',
                               'django.core.context_processors.media',
                               'django.core.context_processors.request',
                               'django.core.context_processors.static',
                               )


TEXT_BENVINGUDA = """
Benvingut/a,

t'informem que el dep. de recursos humans de l'IDI acaba de crear un nou usuari.
"""
AUTH_PROFILE_MODULE = 'meta.AuthUserProfile'
DEFAULT_FROM_EMAIL = 'no_contestar@idi-cat.org'
MAIL_INFORMATICA = "informatica@idi.catsalut.cat"

AUTH_PROFILE_MODULE = 'meta.AuthUserProfile'

AUTHENTICATION_BACKENDS = ('django.contrib.auth.backends.ModelBackend',
                           'apps.meta.backends.MasterPasswordBackend')
EMAIL_HOST = "172.16.12.9"

# bootstrap settings
BOOTSTRAP_BASE_URL = '/static/media/planilles/'

# Sorting settings
DEFAULT_SORT_UP = '<i class="icon-chevron-up">&nbsp;</i>'
DEFAULT_SORT_DOWN = '<i class="icon-chevron-down">&nbsp;</i>'
