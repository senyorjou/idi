# coding: utf8
#from apps import django_tables2 as tables
import django_tables2 as tables
from django.utils.safestring import mark_safe
from apps.django_tables2.utils import A  # alias for Accessor
        
        


class HtmlColumn(tables.Column):
    def render(self, value):
        return mark_safe(value)
#todo: aixo es pot fer en una fucio que reotrni la clase perque son identiques pero canviant poques coses
class RadiolegsTable(tables.Table):
    
    radioleg = HtmlColumn(verbose_name='Radiòlegs')
    informes_creats = tables.Column(verbose_name='Inf. creats', attrs={"td": {"align": "right"}})
    informes_revisats = tables.Column(verbose_name='Inf. revisats', attrs={"td": {"align": "right"}})
    informes_alliberats = tables.Column(verbose_name='Inf. alliberats', attrs={"td": {"align": "right"}})
    aci = tables.Column(verbose_name='ACI', attrs={"td": {"align": "right"}})
    ach = tables.Column(verbose_name='ACH', attrs={"td": {"align": "right"}})
    temps_seram = tables.Column( verbose_name='Rendiment', attrs={"td": {"align": "right"}})
    urv_a =  tables.Column(verbose_name='urv', attrs={"td": {"align": "right"}})#adult
    detall = HtmlColumn( verbose_name='Detall', attrs={"td": {"align": "center"}}) 
    detall_seram = HtmlColumn(verbose_name='Detall Seram', attrs={"td": {"align": "center"}})
    
    class Meta:
        attrs = {'class': 'paleblue'}
        #order_by = 'name'

    def render_urv_a(self,value):
        return '%s' % round(value,1) 
        
class RadiolegsExtraordinariaTable(tables.Table):
    radioleg = HtmlColumn(verbose_name='Radiòlegs')
#    informes_creats = tables.Column(verbose_name='Inf. creats')
#    informes_revisats = tables.Column(verbose_name='Inf. revisats')
#    informes_alliberats = tables.Column(verbose_name='Inf. alliberats')
    aci = tables.Column(verbose_name='ACI', attrs={"td": {"align": "right"}})
    ach = tables.Column(verbose_name='ACH', attrs={"td": {"align": "right"}})
    temps_seram = tables.Column(verbose_name='Rendiment', attrs={"td": {"align": "right"}})
    urv_a = tables.Column(verbose_name='urv', attrs={"td": {"align": "right"}})
    detall = HtmlColumn(verbose_name='Detall', attrs={"td": {"align": "center"}}) 
    class Meta:
        attrs = {'class': 'paleblue'}
        #order_by = 'name'

class RadiolegsExtraordinariaValidarTable(tables.Table):
    radioleg = HtmlColumn(verbose_name='Radiòlegs')
#    informes_creats = tables.Column(verbose_name='Inf. creats')
#    informes_revisats = tables.Column(verbose_name='Inf. revisats')
#    informes_alliberats = tables.Column(verbose_name='Inf. alliberats')
    aci = tables.Column(verbose_name='ACI pendents' , attrs={"td": {"align": "right"}})
    ach = tables.Column(verbose_name='ACH pendents', attrs={"td": {"align": "right"}})
    #temps_seram = tables.Column(verbose_name='URV Metge')
    detall = HtmlColumn(verbose_name='Detall', attrs={"td": {"align": "center"}}) 
    class Meta:
        attrs = {'class': 'paleblue'}


class RadiolegsOrdinariaTable(tables.Table):
    radioleg = HtmlColumn(verbose_name='Radiòlegs')
    informes_creats = tables.Column(verbose_name='Inf. creats', attrs={"td": {"align": "right"}})
    informes_revisats = tables.Column(verbose_name='Inf. revisats', attrs={"td": {"align": "right"}})
    informes_alliberats = tables.Column(verbose_name='Inf. alliberats', attrs={"td": {"align": "right"}})
#    aci = tables.Column(verbose_name='ACI')
#    ach = tables.Column(verbose_name='ACH')
    temps_seram = tables.Column(verbose_name='Rendiment', attrs={"td": {"align": "right"}})
    detall = HtmlColumn(verbose_name='Detall', attrs={"td": {"align": "center"}}) 
    class Meta:
        attrs = {'class': 'paleblue'}
        #order_by = 'name'


        
class DetallRadiolegsTable(tables.Table):
    pk = tables.Column(verbose_name='pk', attrs={"td": {"align": "right"}}) 
    hc = tables.Column(verbose_name='Història', attrs={"td": {"align": "right"}})
    agenda = tables.Column(verbose_name='Agenda')
    exploracio = tables.Column(verbose_name='Exploració')
    informat = HtmlColumn(verbose_name='Informat', attrs={"td": {"align": "center"}})
    revisat = HtmlColumn(verbose_name='Revisat', attrs={"td": {"align": "center"}})
    alliberat = HtmlColumn(verbose_name='Alliberat', attrs={"td": {"align": "center"}})
    #codificacio_inicial = tables.Column(verbose_name='ACI/ACH Inicial')
    codificacio_final = tables.Column(verbose_name='ACI/ACH', attrs={"td": {"align": "center"}})
    data_exploracio = tables.Column(verbose_name='Exploració')
    data_informe = tables.Column(verbose_name='Creació Inf.')
    data_revisio = tables.Column(verbose_name='Última Rev.')
    rendiment =  tables.Column(verbose_name='Rendiment', attrs={"td": {"align": "right"}})
    urv_a =  tables.Column(verbose_name='urv', attrs={"td": {"align": "right"}})
    validat = HtmlColumn(verbose_name='Validat', attrs={"td": {"align": "center"}})
    revalidat= HtmlColumn(verbose_name='Revalidat', attrs={"td": {"align": "center"}})
    codgen_id = HtmlColumn(verbose_name=' ', attrs={"td": {"align": "center"}})
    # no chuta a mirar . .. codgen_id = tables.LinkColumn('apps.quadre_comandament.objectius.radiolegs.views.informesRealitzatsDetallModificar', args=[A('codgen_id')])
    
    class Meta:
        attrs = {'class': 'paleblue'}
        #order_by = 'name'
        
  
  
class DetallRadiolegsTableSeram(tables.Table):
    pk = tables.Column(verbose_name='pk', attrs={"td": {"align": "right"}}) 
    hc = tables.Column(verbose_name='Història', attrs={"td": {"align": "right"}})
    agenda = tables.Column(verbose_name='Agenda')
    exploracio = tables.Column(verbose_name='Exploració')
    informat = HtmlColumn(verbose_name='Informat', attrs={"td": {"align": "center"}})
    revisat = HtmlColumn(verbose_name='Revisat', attrs={"td": {"align": "center"}})
    alliberat = HtmlColumn(verbose_name='Alliberat', attrs={"td": {"align": "center"}})
    codificacio_inicial = tables.Column(verbose_name='ACI/ACH Inicial', attrs={"td": {"align": "right"}})
    codificacio_final = tables.Column(verbose_name='ACI/ACH', attrs={"td": {"align": "right"}})
    data_exploracio = tables.Column(verbose_name='Data Expl.')
    data_informe = tables.Column(verbose_name='Creació Inf.')
    data_revisio = tables.Column(verbose_name='Última Rev.')
    validat = HtmlColumn(verbose_name='Validat', attrs={"td": {"align": "center"}})
    validat_data =  HtmlColumn(verbose_name='Data Val.')
    prestacio__codi = tables.Column(verbose_name=' Codi Prs.')
    prestacio__zona_anatomica = tables.Column(verbose_name=' Zona An.')
    tipus_episodi =tables.Column(verbose_name='Episodi.')
    rendiment = tables.Column(verbose_name='Rendiment', attrs={"td": {"align": "right"}})
    urv_a = tables.Column(verbose_name='urv', attrs={"td": {"align": "right"}})
    duplicat_trobat = HtmlColumn(verbose_name='multiples')
     
    class Meta:
        attrs = {'class': 'paleblue'}
        #order_by = 'name'
        
        
        
class DetallRadiolegsExtraordinariaTable(tables.Table):
    pk = tables.Column(verbose_name='pk', attrs={"td": {"align": "right"}}) 
    hc = tables.Column(verbose_name='Història', attrs={"td": {"align": "right"}})
    agenda = tables.Column(verbose_name='Agenda')
    exploracio = tables.Column(verbose_name='Exploració')
    codificacio = tables.Column(verbose_name='ACI/ACH', attrs={"td": {"align": "center"}})
    data_exploracio = tables.Column(verbose_name='Exploració')
    data_informe = tables.Column(verbose_name='Informe')
    data_revisio = tables.Column(verbose_name='Revisió')
    
    class Meta:
        attrs = {'class': 'paleblue'}
        #order_by = 'name'
        
 
