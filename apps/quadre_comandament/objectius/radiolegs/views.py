# coding: utf8

from __future__ import division# per fer divions i que retorniel reste /
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from apps.quadre_comandament.common import initFilters
from apps.quadre_comandament.models import  ProvaOhdim, Metge,ProvaOhdimCodificacioGenerica,ProvaOhdimCodificacioGenerica_Form 
from apps.quadre_comandament.objectius.radiolegs.tables import RadiolegsTable, RadiolegsOrdinariaTable,DetallRadiolegsTable,RadiolegsExtraordinariaTable,RadiolegsExtraordinariaValidarTable,DetallRadiolegsTableSeram
import datetime,calendar,time
from django.db.models import Q,Count,Sum
from apps.meta.models import AuthUser,AuthUserProfile
from apps.meta import auditoria
from django.db.models import F# https://docs.djangoproject.com/en/dev/topics/db/queries/#query-expressions
from django.db import IntegrityError
#from warnings import catch_warnings
#from django.db import connections

indicadors = ['Activitat Total', 'Activitat Ordinària', 'Activitat Extraordinària']
objectiu = "Gestió de l'activitat dels radiolegs"
objectiu_url = "radiolegs"

def get_indicadors(request):
    # Recordem que no es pot canviar una variable global ( per molts motius , multiples procesos ... etc)
    indicadors_local =  indicadors
    #mirem si te permisos per validar , si en te mostrem el boto i el formulari per poder fer-ho , 15 i 17 informatics, 26 cap unitat, 27 director unitat
    if AuthUserProfile.objects.get( user = AuthUser.objects.get(username =str(request.user)).id).categoria_profesional.id in [15,17,26,27,31]:
        if 'Activitat Extraordinària Validar' not in indicadors_local:
            indicadors_local.append('Activitat Extraordinària Validar')
#        #Programes            
#        if AuthUserProfile.objects.get( user = AuthUser.objects.get(username =str(request.user)).id).unitat_idi.id in[ 17,6 ]:#unitat 17 es SSCCC i el 6 es angio
#            if 'Programes Angio' not in indicadors_local:
#                indicadors_local.append('Programes Angio')
    else:
        if 'Activitat Extraordinària Validar' in indicadors_local:
            indicadors_local.remove('Activitat Extraordinària Validar')
        if 'Consum Angiograf' in indicadors_local:
            indicadors_local.remove('Consum Angiograf')
            
    
    if 'meta.revalidar' in  request.user.get_all_permissions():
        if 'Activitat Extraordinària Revalidar' not in indicadors_local:
            indicadors_local.append('Activitat Extraordinària Revalidar')
    else:
        if 'Activitat Extraordinària Revalidar' in indicadors_local:
            indicadors_local.remove('Activitat Extraordinària Revalidar')
#               
        
            
        
        
        
        
    return indicadors_local

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def index(request):
    request.session['enabled_filters'] = {}
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                          

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def radiolegs(request):
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                              'objectiu':objectiu,
                              'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request)) 


################################
# tots informes ralitzats ( creats , revisats i alliberats ) per radioleg ( ). Nomes es mostren els que l'usiari tingui permis en la unitat, es a dir
# surten els informes realitzats en la unitat. ( en el detall de l'activitat extraOrdinaria si apareixen els informes de altres unitats)
#
# El filtre mensual es fa per data de "document_revisat",  en general es quan el  docuement es tancat  i alliberat ( except per MN) 
#
##################################
#                             
#@login_required
#@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
#def activitatTotal(request):
#    
#    
#    #usuari = AuthUser.objects.get(username =str(request.user))
#    #es_informatic = AuthUserProfile.objects.get( user = usuari.id).categoria_profesional.id in (15,17,21,36)
#    es_informatic = True
#    
#    
#    
#    # Cal definir els filtres que volem per defecte
#    request.session['current_filters'] = 'radiolegs_filters' 
#    if not request.session['current_filters'] in request.session:
#        enabled_filters = {'any': int(time.strftime("%Y", time.gmtime())), 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
#        initFilters(request, enabled_filters, request.session['current_filters'])
#    request.session['enabled_filters'] = request.session[request.session['current_filters']]
#
#    
#    anys = request.session['enabled_filters']['any']
#    mes = request.session['enabled_filters']['mes']
#    unitatsel = request.session['enabled_filters']['unitat']
#    modalitatsel = request.session['enabled_filters']['modalitat']
#    
#    if anys == 0:# quan es sel·lecionen tots els anys
#        for filter_ in request.session['filters']:
#            if filter_['nom'] == "any":
#                any_primer = filter_['valors'][0][0]
#                any_ultim = filter_['valors'][-1][0]
#            else:
#                any_primer = 2000
#                any_ultim = 2800
#        if mes == 0:
#            start_date = datetime.date(int(any_primer), 1, 1)
#            end_date = datetime.date(int(any_ultim), 12, 31)
#        else: #tots els anys fins els mes sellecionat d'enguany!!
#            dies_mes = calendar.monthrange(any_ultim, mes)
#            start_date = datetime.date(any_primer, 1, 1)
#            end_date = datetime.date(any_ultim, mes, dies_mes[1])
#    else:
#        if mes == 0:
#                start_date = datetime.date(int(anys), 1, 1)
#                end_date = datetime.date(int(anys), 12, 31) 
#        else:
#            dies_mes = calendar.monthrange(anys, mes)
#            start_date = datetime.date(anys, mes, 1)
#            end_date = datetime.date(anys, mes, dies_mes[1])
#        
#        
#        
#        
#        
#    # Definim querys per defecte
#    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
#    
#                                                                        # no volem agenda ICS pero !!! si volem els metges que no tenen ralcio ( null )
#    master_queryset = ProvaOhdim.objects.using('portal_dades').exclude( 
#                                                                        Q(agenda__agenda_idi__es_ics=True ) & (
#                                                                        Q(metge_informa__es_idi=False,  metge_informa__es_idi__isnull=False ) |
#                                                                        Q(metge_revisa__es_idi=False,   metge_revisa__es_idi__isnull=False  ) |  
#                                                                        Q(metge_allibera__es_idi=False, metge_allibera__es_idi__isnull=False)
#                                                                        )
#                                                                         # NO volem els metges que son del ICS i que la agenda es del ICS!!!  ( LA Q es totalment necessaria sino peta ( fa la join i li canvia el nom a la taula i despres alferla sevir peta))
#                                                                       ).exclude (Q(agenda__agenda_idi__es_ics=True , metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True))
#                                                        
#
#    # AIXO FUNCIONA SI ES POSA EN LA MYSQL !!!!! NO HI HA UNAKLTRE FORAMA QUE CHUTI ( el Django no transalda bé la consulta !!!! unica sollucio es amb exclude )
#    # AND  NOT ( portal_dades.agenda_idi.es_ics  AND  m_informa.id is Null  AND m_revisa.id is Null AND m_allibera.id is Null )
#    # master_queryset = master_queryset.exclude(  agenda__agenda_idi__es_ics = True ,  metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True )
#    #cada vegada que es fa una assignacio fa una consulta a la mysql --> posa un "limit 21" imagino que per comprovar que funcionen els filtres 
#    
#    #volem les prestacions entre el rang de dades, i que estiguin concloses 
#    master_queryset =   master_queryset.filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
#    
#    if unitatsel != 0 and modalitatsel != 0:
#        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
#                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
#    elif unitatsel!= 0:
#        master_queryset = master_queryset.filter( agenda__agenda_idi__unitat_idi_id=unitatsel) 
#            
#    elif modalitatsel != 0:
#        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
#                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
#    else:   
#        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
#                                          agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))
#
#    import copy
#    init_object = [0,0,0,0,0,0,0,0,0,0]
#    
#    taula_series2_tmp = {}
#
#    queryset = master_queryset.values('metge_informa' ,'metge_informa__cognoms_nom','metge_informa__dni' ).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')
#
#    #NOM I COGNOMS i DETALL
#    for prova in queryset:
#        if  prova['metge_informa__cognoms_nom'] == None :
#            try:
#                if taula_series2_tmp[0] == None:
#                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: red;'>Informes amb errors</span>"
#                    taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../activitat-total-detall/0' class='addlink'></a>"
#                    if es_informatic:
#                        taula_series2_tmp[prova['metge_informa']][8]  = "<a href='../activitat-total-detall-seram/0' class='addlink'></a>"
#                    else:
#                        taula_series2_tmp[prova['metge_informa']][8]  = ""
#                    
#            except:
#                taula_series2_tmp[0] = copy.copy(init_object)
#                taula_series2_tmp[0][0]  = "<span style='color: red;'>Informes amb errors</span>"
#                taula_series2_tmp[0][7]  = "<a href='../activitat-total-detall/0' class='addlink'></a>"
#                if es_informatic:
#                    taula_series2_tmp[0][8]  = "<a href='../activitat-total-detall-seram/0' class='addlink'></a>"
#                else:
#                    taula_series2_tmp[0][8]  = ""
#  
#
#        else:
#            
#            taula_series2_tmp[prova['metge_informa']] = copy.copy(init_object)
#            
#            try:
#                user = AuthUser.objects.get(username =  prova['metge_informa__dni'][:8] )
#                if AuthUserProfile.objects.get( user = user).categoria_profesional.id in [4,5,6,26,27,30,31,]:
#                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: #31B404;'>" +  prova['metge_informa__cognoms_nom']+ "</span>"
#                else:#sin no es facultatiu o cap d¡untiat , es una scre o un tecnic i es un error
#                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: #B7A800;'>" +  prova['metge_informa__cognoms_nom']+ "</span>"
#            except AuthUser.DoesNotExist:
#                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: #B85F04;'>" + prova['metge_informa__cognoms_nom'] + "</span>"
#                
#            
#            taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../activitat-total-detall/%s' class='addlink'></a>" % prova['metge_informa']
#            if es_informatic:
#                taula_series2_tmp[prova['metge_informa']][8]  = "<a href='../activitat-total-detall-seram/%s' class='addlink'></a>" % prova['metge_informa']
#            else:
#                taula_series2_tmp[prova['metge_informa']][8]  = ""
#                
#    # INFORMES CREATS
#    queryset = master_queryset.values('metge_informa','metge_informa__cognoms_nom',).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')
#    for prova in queryset:
#        if  prova['metge_informa__cognoms_nom'] == None :
#            taula_series2_tmp[0][1] =  taula_series2_tmp[0][1] + prova['informes_creats'] # s'ha de su
#        else:
#            taula_series2_tmp[prova['metge_informa']][1] = prova['informes_creats']
#    # INFORMES REVISATS
#    queryset = master_queryset.values('metge_revisa','metge_revisa__cognoms_nom','metge_revisa__dni').annotate(informes_revisats=Count('id')).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
#        try:      
#            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
#        except KeyError:# no ha informat cap informe pero si revisat  o alliberat ?? els afegim , si te nom 
#            
#            if  prova['metge_revisa__cognoms_nom'] == None :
#                taula_series2_tmp[0][2] =  taula_series2_tmp[0][2] + prova['informes_revisats'] # s'hafegeixen els revistas al INFORMES DLATRES METGES
#            else:  
#                taula_series2_tmp[prova['metge_revisa']] = copy.copy(init_object) # PETA que no hi ha el metge que informa === a el afegim aqui
#                #taula_series2_tmp[prova['metge_revisa']][0]  = prova['metge_revisa__cognoms_nom']
#            
#                try:
#                    if  prova['metge_revisa__dni'] != None:
#                        user = AuthUser.objects.get(username =  prova['metge_revisa__dni'][:8] )
#                        if AuthUserProfile.objects.get( user = user).categoria_profesional.id in [4,5,6,26,27,30,31,]:
#                            taula_series2_tmp[prova['metge_revisa']][0]  = "<span style='color: #31B404;'>" +  prova['metge_revisa__cognoms_nom']+ "</span>"
#                        else:#sin no es facultatiu o cap d¡untiat , es una scre o un tecnic i es un error
#                            taula_series2_tmp[prova['metge_revisa']][0]  = "<span style='color: #B7A800;'>" +  prova['metge_revisa__cognoms_nom']+ "</span>"
#                    else:
#                        taula_series2_tmp[prova['metge_revisa']][0]  = "<span style='color: #B85F04;'>" +  '- ERROR SAP no tenim usuari - '+ "</span>"
#                except AuthUser.DoesNotExist:
#                    taula_series2_tmp[prova['metge_revisa']][0]  = "<span style='color: #B85F04;'>" + prova['metge_revisa__cognoms_nom'] + "</span>"
#
#                taula_series2_tmp[prova['metge_revisa']][1] = 0
#                taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
#                taula_series2_tmp[prova['metge_revisa']][7]  = "<a href='../activitat-total-detall/%s' class='addlink'></a>" % prova['metge_revisa']
#                if es_informatic:
#                    taula_series2_tmp[prova['metge_revisa']][8]  = "<a href='../activitat-total-detall-seram/%s' class='addlink'></a>" % prova['metge_revisa']
#                else:
#                    taula_series2_tmp[prova['metge_revisa']][8]  = ""
#            
#    # INFORMES ALLIBERATS
#    queryset = master_queryset.values('metge_allibera','metge_allibera__cognoms_nom','metge_allibera__dni').annotate(informes_alliberats=Count('id')).order_by('metge_allibera__cognoms_nom')   
#    for prova in queryset:
#        try:
#            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
#        except KeyError:
#
#            if  prova['metge_allibera__cognoms_nom'] == None :
#                taula_series2_tmp[0][3] =  taula_series2_tmp[0][3] + prova['informes_alliberats'] # s'hafegeixen els allibertas al "INFORMES DLATRES METGES"
#            else:  
#                taula_series2_tmp[prova['metge_allibera']] = copy.copy(init_object)
#                try:  
#                    if  prova['metge_allibera__dni'] != None:
#                        user = AuthUser.objects.get(username =  prova['metge_allibera__dni'][:8] )# si no existeix raise DoesNotExist
#                        if AuthUserProfile.objects.get( user = user).categoria_profesional.id in [4,5,6,26,27,30,31,]:#facultatius adjunts o caps de unitat
#                            taula_series2_tmp[prova['metge_allibera']][0]  = "<span style='color: #31B404;'>" +  prova['metge_allibera__cognoms_nom']+ "</span>"
#                        else:#sin no es facultatiu o cap d¡untiat , es una scre o un tecnic i es un error segur perque es del idi pero no es facultatiu
#                            taula_series2_tmp[prova['metge_allibera']][0]  = "<span style='color: #B7A800;'>" +  prova['metge_allibera__cognoms_nom']+ "</span>"
#                    else:# notenim el dni de l'ususari --> a vegades no tenim l'usuari directament
#                        taula_series2_tmp[prova['metge_allibera']][0]  = "<span style='color: #B85F04;'>" +  '- ERROR SAP no tenim usuari - '+ "</span>"
#                except AuthUser.DoesNotExist:# no estan donats dalta al portal == a resis o errors de sap
#                    taula_series2_tmp[prova['metge_allibera']][0]  = "<span style='color: #B85F04;'>" + prova['metge_allibera__cognoms_nom'] + "</span>"
#           
#            
#                taula_series2_tmp[prova['metge_allibera']][1] = 0
#                taula_series2_tmp[prova['metge_allibera']][2] = 0
#                taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
#                taula_series2_tmp[prova['metge_allibera']][7]  = "<a href='../activitat-total-detall/%s' class='addlink'></a>" % prova['metge_allibera']
#                if es_informatic:
#                    taula_series2_tmp[prova['metge_allibera']][8]  = "<a href='../activitat-total-detall-seram/%s' class='addlink'></a>" % prova['metge_allibera']
#                else:
#                    taula_series2_tmp[prova['metge_allibera']][8]  = ""
#        
#        
#    # INFORMES ACI
#    queryset = master_queryset.filter(provaohdimcodificaciogenerica__codificacio_generica_final=7,provaohdimcodificaciogenerica__validat=True ).values('metge_allibera').annotate(informes_alliberats=Count('id'))
#    for prova in queryset:       
#        taula_series2_tmp[prova['metge_allibera']][4] = prova['informes_alliberats']
#    # INFORMES ACH
#    queryset = master_queryset.filter(provaohdimcodificaciogenerica__codificacio_generica_final=8,provaohdimcodificaciogenerica__validat=True).values('metge_allibera').annotate(informes_alliberats=Count('id'))
#    for prova in queryset:           
#        taula_series2_tmp[prova['metge_allibera']][5] = prova['informes_alliberats']
#        
#    # SERAM 
#    #TODO: alguna cosa hem de fer per comprovar aquelles proves que no tenen codi seram , o assegurar-nos que totes les prestacions en tenen...
#    #nomes aquells que ha informat i ha alliberat el mateix radioleg
#    #queryset = master_queryset.values('metge_informa').filter(  metge_allibera = F('metge_informa')).annotate(temps_seram=Sum('prestacio__codi_seram__temps_metge')).order_by('metge_allibera__cognoms_nom')
#    
#    
#    #Tot els informes que crea el facultatiu i que revisa ell amteix -> el 100% del seu valor SERAM
#    queryset = master_queryset.values('metge_revisa').filter( metge_informa= F('metge_revisa')).annotate(temps_seram=Sum('prestacio__codi_seram__temps_metge')).order_by('metge_revisa__cognoms_nom')
#  
#    for prova in queryset:
#        if prova['temps_seram'] == None:
#            try:
#                taula_series2_tmp[prova['metge_revisa']][6] = 0
#            except KeyError:
#                tmp = None
#        else:
#            if prova['metge_revisa'] == None:
#                taula_series2_tmp[0][6] =  taula_series2_tmp[0][6] + prova['temps_seram']
#            else:
#                #enspodem trobar que no te nom i cognoms , es el [0] llavors
#                try:
#                    taula_series2_tmp[prova['metge_revisa']][6] = prova['temps_seram']
#                except KeyError:
#                    taula_series2_tmp[0][6] =  taula_series2_tmp[0][6] + prova['temps_seram']
#    
#    #hara sumem el 25% dels revisats i que no ha informat el mateix facultatiu 
#    #el resi crea i un adjunt allibera (i son diferents facultatius)
#    queryset = master_queryset.values('metge_revisa').exclude( metge_informa= F('metge_revisa')).annotate( temps_seram=Sum('prestacio__codi_seram__temps_metge') ).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
###       
#        if prova['temps_seram'] == None:
#                tmp = None
#        else: 
#                
#                taula_series2_tmp[prova['metge_revisa']][6] = taula_series2_tmp[prova['metge_revisa']][6] + (25*int(prova['temps_seram'])/100)
#                #if int(prova['metge_informa']) == 18001:
#                #    print(str(prova['metge_informa']) + ':  '  +  str((25*int(prova['temps_seram'])/100) ))
###                    
###    #ARa nomes queden els  errors ->
##    #-> El que crea es un resi i el que revisa es un adjunt , però no ... el allibera el resi!!! , COM que aqui ja estan inclossos els de dalt comentem la part de dalt fins que SAP no prïvieixi queels resis tanquin informes 
##    queryset = master_queryset.values('metge_revisa').exclude(Q(metge_revisa = F('metge_informa')) & Q(metge_revisa = F('metge_allibera'))).annotate( temps_seram=Sum('prestacio__codi_seram__temps_metge') ).order_by('metge_revisa__cognoms_nom')
##    for prova in queryset:
##        if not (prova['temps_seram'] == None):
##            if prova['metge_revisa'] == None:
##                tmp = None 
##            else: 
##                taula_series2_tmp[prova['metge_revisa']][6] = taula_series2_tmp[prova['metge_revisa']][6] + (25*int(prova['temps_seram'])/100)
##   
#
#
#
#
#
#
#    # URVs   SERAM 
#    #TODO: alguna cosa hem de fer per comprovar aquelles proves que no tenen codi seram , o assegurar-nos que totes les prestacions en tenen...
#    
#    #Tot els informes que crea el facultatiu que revisen el 100% del seu valor SERAM
#    queryset = master_queryset.values('metge_revisa').annotate(urv_a=Sum('prestacio__codi_seram__urv_a')).order_by('metge_revisa__cognoms_nom')
#  
#    for prova in queryset:
#        if prova['urv_a'] == None:
#            try:
#                taula_series2_tmp[prova['metge_revisa']][9] = 0
#            except KeyError:
#                tmp = None
#        else:
#            if prova['metge_revisa'] == None:
#                taula_series2_tmp[0][9] =  taula_series2_tmp[0][9] + prova['urv_a']
#            else:
#                #enspodem trobar que no te nom i cognoms , es el [0] llavors
#                try:
#                    taula_series2_tmp[prova['metge_revisa']][9] = prova['urv_a']
#                except KeyError:
#                    taula_series2_tmp[0][9] =  taula_series2_tmp[0][9] + prova['urv_a']
#    #hara sumem el 25% dels revisats 
#    #el resi crea i un adjunt allibera (i son diferents facultatius)
#    queryset = master_queryset.values('metge_revisa').exclude( metge_informa= F('metge_revisa')).annotate( urv_a=Sum('prestacio__codi_seram__urv_a') ).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
###       
#        if prova['urv_a'] == None:
#                tmp = None
#        else: 
#                
#                taula_series2_tmp[prova['metge_revisa']][9] =  float(taula_series2_tmp[prova['metge_revisa']][9]) + (25*float(prova['urv_a'])/100)
#                #if int(prova['metge_informa']) == 18001:
#                #    print(str(prova['metge_informa']) + ':  '  +  str((25*int(prova['temps_seram'])/100) ))
###                    
###    #ARa nomes queden els  errors ->
##    #-> El que crea es un resi i el que revisa es un adjunt , però no ... el allibera el resi!!! , COM que aqui ja estan inclossos els de dalt comentem la part de dalt fins que SAP no prïvieixi queels resis tanquin informes 
##    queryset = master_queryset.values('metge_revisa').exclude(Q(metge_revisa = F('metge_informa')) & Q(metge_revisa = F('metge_allibera'))).annotate( temps_seram=Sum('prestacio__codi_seram__temps_metge') ).order_by('metge_revisa__cognoms_nom')
##    for prova in queryset:
##        if not (prova['temps_seram'] == None):
##            if prova['metge_revisa'] == None:
##                tmp = None 
##            else: 
##                taula_series2_tmp[prova['metge_revisa']][6] = taula_series2_tmp[prova['metge_revisa']][6] + (25*int(prova['temps_seram'])/100)
##
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#    # Taula html - sort & filters
#    taula_series2 = [] #creem una tupla de metge i les dades
#    
#    
#    inf_alres_m = [0,0,0,0,0,0,0,0,0,0]
#    dades_metge_tmp=[0,0,0,0,0,0,0,0,0,0]
#    #Sumem la moralla , els que no estan a la taula metge i els metge que son null en el provaohdim
#    for dades_metge in taula_series2_tmp.items():
#        if dades_metge[1][0] == None or dades_metge[1][0] == 0:
#            if dades_metge[1][1] == None: dades_metge_tmp[0]= 0
#            else:dades_metge_tmp[0] = dades_metge[1][1] 
#            if dades_metge[1][2] == None: dades_metge_tmp[1]=0
#            else:dades_metge_tmp[1] = dades_metge[1][2]
#            if dades_metge[1][3] == None:  dades_metge_tmp[2]=0
#            else:dades_metge_tmp[2] = dades_metge[1][3]
#            if dades_metge[1][4] == None: dades_metge_tmp[3]=0
#            else:dades_metge_tmp[3] = dades_metge[1][4]
#            if dades_metge[1][5] == None:  dades_metge_tmp[4]=0
#            else:dades_metge_tmp[4] = dades_metge[1][5]
#            if dades_metge[1][6] == None: dades_metge_tmp[5]=0
#            else:dades_metge_tmp[5] = dades_metge[1][6]
#                
#            inf_alres_m = [ inf_alres_m[0],
#                            inf_alres_m[1] + int(dades_metge_tmp[0]) ,
#                            inf_alres_m[2] + int(dades_metge_tmp[1]),
#                            inf_alres_m[3] + int(dades_metge_tmp[2]),
#                            inf_alres_m[4] + int(dades_metge_tmp[3]),
#                            inf_alres_m[5] + int(dades_metge_tmp[4]),
#                            inf_alres_m[6] + int(dades_metge_tmp[5]),
#                            inf_alres_m[7], 
#                            inf_alres_m[8],
#                            inf_alres_m[9],
#                             ]
#        else:
#            if dades_metge[0]== 0:
#                inf_alres_m =  dades_metge[1]
#            else:
#                taula_series2.append((dades_metge[1][0], dades_metge[1]))
#                
#    if inf_alres_m[0] != 0:#afegim la suma dels que notenen nom i errors
#        taula_series2.append((inf_alres_m[0], inf_alres_m))
#        
#        
#    taula_series2 = sorted(taula_series2, key=lambda row: row[1][0])
#    new_table = []
#    for serie in taula_series2:
#        new_table.append({'radioleg':serie[0], 'informes_creats':serie[1][1], 'informes_revisats':serie[1][2], 'informes_alliberats':serie[1][3], 'aci':serie[1][4],'ach':serie[1][5] ,'temps_seram':serie[1][6], 'urv_a':serie[1][9] ,'detall':serie[1][7],'detall_seram':serie[1][8]})
#    table = RadiolegsTable(new_table, order_by=request.GET.get('sort'), exclude=('aci','ach'))
#    #table.paginate(page=request.GET.get('page', 1))
#    taula_series = table
#
#    # if request.GET.get('export','none') == "excel":   
#    # grafics
#    grafics = [
#               {'tipus': "taula2", 
#                'tamany': 100,                
#                'titol': "Detall d'informes per radiòleg",
#                'categories': [ 'Creats', 'Revisats', 'Alliberats','aci','ach','temps_seram','urv_a', 'Detall','Detall_Seram'],
#                'series': taula_series},              
#                ]
#    
#        # Definició
#    definicio = """
#        Suma de tota l'activitat informada, revisada i alliberada pels radiòlegs al centre seleccionat. 
#        En l'activitat, es tenen en compte les modificacions en l'activitat extraordinària.</br></br> 
#        En cas de triar "Tots", apareix l'activitat de totes les unitats que el teu usuari té assignades.</br></br>
#        Les dates de tall són per data d'informe alliberat.</br></br>
#        El rendiment és la suma de temps informe (definits per la SERAM) de les prestacions informades (revisat_per ) pel radiòleg a la unitat.
#        <p> </p>
#        <a>
#            <img src="/static/media/img/quadre_comandament/facultatius/taula_02_detall_colors.gif" alt="Definició colors" align ="middle"  />
#        </a>
#        """
#    auditoria.request_handler(request,8,'Tipus consulta: activitatTotal ; ' + str(request.session['enabled_filters']) + ' ')
#        
#    return render_to_response('quadre_comandament/indicador.html',
#                              {'indicadors':get_indicadors(request),
#                               'grafics':grafics,
#                               'objectiu':objectiu,
#                               'definicio':definicio,
#                               'objectiu_url':objectiu_url},
#                              context_instance=RequestContext(request))                      


################################
# Detall de  tots els informes ralitzats ( creats , revisats i alliberats ) per radioleg ( ). 
# apareixen els informes de altres unitats
#
# El filtre mensual es fa per data de "document_revisat",  en general es quan el  docuement es tancat  i alliberat ( except per MN)
# DEtall de infromes realitzats, es mostra tot el que ha fet el readioleg durant el periode sellecionat 
# ( informes revisat alliberts ) en MN i RM TC Lleida veuen el camp alliberts  
##################################


@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatTotalDetall(request, metge_id):

    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'radiolegs_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    anys = request.session['radiolegs_filters']['any']
    mes = request.session['radiolegs_filters']['mes']
    unitatsel = request.session['radiolegs_filters']['unitat']
    modalitatsel = request.session['radiolegs_filters']['modalitat']
    
           
    
    if anys == 0:# quan es sel·lecionen tots els anys
        for filter_ in request.session['filters']:
            if filter_['nom'] == "any":
                any_primer = filter_['valors'][0][0]
                any_ultim = filter_['valors'][-1][0]
            else:
                any_primer = 2000
                any_ultim = 2800
        if mes == 0:
            start_date = datetime.date(int(any_primer), 1, 1)
            end_date = datetime.date(int(any_ultim), 12, 31)
        else: #tots els anys fins els mes sellecionat d'enguany!!
            dies_mes = calendar.monthrange(any_ultim, mes)
            start_date = datetime.date(any_primer, 1, 1)
            end_date = datetime.date(any_ultim, mes, dies_mes[1])
    else:
        if mes == 0:
                start_date = datetime.date(int(anys), 1, 1)
                end_date = datetime.date(int(anys), 12, 31) 
        else:
            dies_mes = calendar.monthrange(anys, mes)
            start_date = datetime.date(anys, mes, 1)
            end_date = datetime.date(anys, mes, dies_mes[1])

    #TODO: AIXO S?HA DE TREURE PERQUE POTSER HI HAN DIGANOSTICAT INFORMES DALTRES CENTRES i els volem veure??? a l'estapera
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    
    if metge_id == '0':
        radioleg_nom_cognoms = "Informes amb errors"
        master_queryset = ProvaOhdim.objects.using('portal_dades').filter( agenda__agenda_idi__es_ics=False
                                                                        ).filter( Q(metge_informa__isnull=True) |
                                                                                  Q(metge_revisa__isnull=True) |
                                                                                  Q(metge_allibera__isnull=True)
                                                                         ).filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
    else:
        radioleg_nom_cognoms = Metge.objects.using('portal_dades').get(pk=long(metge_id)).cognoms_nom
        master_queryset = ProvaOhdim.objects.using('portal_dades').filter( Q(metge_informa=metge_id) |
                                                                           Q(metge_revisa=metge_id) |
                                                                           Q(metge_allibera=metge_id)
                                                                           ).filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
    if unitatsel != 0 and modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                                 agenda__agenda_idi__idtipoexploracion=modalitatsel)
    elif unitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel)  
            
    elif modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                                 agenda__agenda_idi__idtipoexploracion=modalitatsel)
    else:   
        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                                 agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))
                                
    proves_queryset_iterator = master_queryset.values( 'id', 
                                       'historia_clinica_id',
                                       'tipus_modalitat', 
                                       'agenda__agenda_idi__obs',
                                       'prestacio_id',
                                       'prestacio__descripcio',
                                       'metge_informa','metge_revisa','metge_allibera',
                                       'metge_informa__cognoms_nom','metge_revisa__cognoms_nom','metge_allibera__cognoms_nom', 
                                       'codificacio_generica__codi',
                                       'data_cita',
                                       'hora_cita',
                                       'data_document_informat',
                                       'hora_document_informat',
                                       'data_document_revisat',
                                       'hora_document_revisat'
                                       ).order_by('tipus_modalitat', 'agenda', 'historia_clinica_id', 'data_cita', 'hora_cita').iterator()

    import copy
    init_object = [0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    icona_oky= '<IMG SRC="/static/admin/img/admin/icon_success.gif" BORDER=0 ALT="Fet">'

    #NOM I COGNOMS i DETALL
    taula_proves = {}
    #rows_duplicats = copy.copy(proves_queryset_iterator)   No es pot ducplicar un iterador¿?
    taula_detall = []
    
    #i = len(proves_queryset) 263 i nomes fa el bucle de 100 !!!!!!!!!!!!!!!!!1
    # els querysets tenen un problema de cache i es queden nomes amb 100 registres -> !!!!!!!
    # http://jeffelmore.org/2010/09/25/smarter-caching-of-django-querysets/#comment-100
    # es mes eficient possar-ho en una named-tuple 
    # taula_detall_item = namedtuple('Detall', 'id historia_clinica_id tipus_modalitat agenda__agenda_idi__obs prestacio_id prestacio__descripcio metge_informa metge_revisa metge_allibera metge_informa__cognoms_nom metge_revisa__cognoms_nom metge_allibera__cognoms_nom codificacio_generica__codi data_cita hora_cita data_document_informat hora_document_informat data_document_revisat hora_document_revisat')

    for prova in proves_queryset_iterator:
        
        taula_proves[prova['id']] = copy.copy(init_object)

        if prova['metge_informa'] == long(metge_id):
            metge_informa = icona_oky 
        else: 
            metge_informa =  prova['metge_informa__cognoms_nom']

        if prova['metge_revisa'] == long(metge_id):
            metge_revisa = icona_oky 
        else: 
            metge_revisa =  prova['metge_revisa__cognoms_nom']
        if prova['metge_allibera'] == long(metge_id):
            metge_allibera = icona_oky 
        else: 
            metge_allibera =  prova['metge_allibera__cognoms_nom']
            
        taula_proves[prova['id']][1]  = prova
#        if (radioleg_nom_cognoms == '' and prova['metge_informa__cognoms_nom'] !=None ):
#            radioleg_nom_cognoms = prova['metge_informa__cognoms_nom']
        if prova['data_cita'] == None  or  prova['hora_cita'] == None: 
            data_i_hora_cita= '-'
        else:  
            data_i_hora_cita =  datetime.datetime(prova['data_cita'].year, prova['data_cita'].month, prova['data_cita'].day, prova['hora_cita'].hour, prova['hora_cita'].minute, prova['hora_cita'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_document_informat'] == None  or  prova['hora_document_informat'] == None:
            data_i_hora_doc_inf = '-'
        else:
            data_i_hora_doc_inf = datetime.datetime(prova['data_document_informat' ].year, prova['data_document_informat'].month, prova['data_document_informat'].day, prova['hora_document_informat'].hour, prova['hora_document_informat'].minute, prova['hora_document_informat'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_document_revisat'] == None  or  prova['hora_document_revisat'] == None:
            data_i_hora_doc_rev = '-'      
        else:
            data_i_hora_doc_rev = datetime.datetime(prova['data_document_revisat'].year, prova['data_document_revisat'].month, prova['data_document_revisat'].day, prova['hora_document_revisat'].hour, prova['hora_document_revisat'].minute, prova['hora_document_revisat'].second).strftime('%Y/%m/%d %H:%M')

        # Ara buscarem totes aquelles proves que tenen codificacio generica per posar la codificacio inicial i la final i si esta validat o no i per qui
        codgenerica_queryset = ProvaOhdimCodificacioGenerica.objects.using('portal_dades').filter(prova_ohdim__exact= prova['id'])#.values('codificacio_generica_inicial','codificacio_generica_final')
        # en prinicpi nomes pot ser un pero... retorna un queryset
        codificacio_inicial= ''
        codificacio_final= ''
        validat = ''
        revalidat = ''
        #pasem a la vista de modificar el pk de provaohdim per si no hi ha codificaciogenirica ( descuid del radioleg)
        modificar = "<a href='../activitat-total-detall-modificar/%s&%s' class='changelink'></a>" % (prova['id'],'0')
        #busqyuem les cod generiques fixades per la prestacio
        for codgen in codgenerica_queryset:
            #ls pk de prevaohdim_codificaio_geenrica   aixi el paser en un link
            modificar="<a href='../activitat-total-detall-modificar/%s&%s' class='changelink'></a>" % ( prova['id'],codgen.id)
            try:
                codificacio_inicial = str(codgen.codificacio_generica_inicial)
                if codificacio_inicial=='None' :codificacio_inicial =''
            except:
                codificacio_inicial = '' 
            try:
                codificacio_final = str(codgen.codificacio_generica_final)
                if codificacio_final=='None' :codificacio_final =''
            except:
                codificacio_final = ''
            if codgen.validat:
                validat =  icona_oky
                modificar = ''
            if codgen.validat == None:#per poder veure els errors debug 
                validat = '-'
            
            # si esta revalidat ja no es podra tornar a tocar mai mes !!!
            if codgen.revalidat:
                revalidat =  icona_oky
                modificar = ''

            
        duplicat_trobat=None
        #Les proves consecuteives  les marqeum amb marrro!!! 
#        for row_duplicats in rows_duplicats:
#            if (prova['historia_clinica_id'] == row_duplicats['historia_clinica_id'] ) and ( prova['data_cita'] == row_duplicats['data_cita']) and (prova['prestacio_id'] != row_duplicats['prestacio_id']):
#                duplicat_trobat='style=color:brown;'
          
        taula_detall.append((prova['id'],( prova['historia_clinica_id'],
                                           prova['agenda__agenda_idi__obs'],
                                           prova['prestacio__descripcio'] ,
                                           metge_informa , 
                                           metge_revisa, 
                                           metge_allibera,
                                           codificacio_inicial, 
                                           codificacio_final,
                                           data_i_hora_cita,
                                           data_i_hora_doc_inf,
                                           data_i_hora_doc_rev,
                                           validat,
                                           revalidat,
                                           modificar
                                           ), duplicat_trobat 
                             )
                            )
#    # if request.GET.get('export','none') == "excel":   

    # grafics
    new_table = []
    for row in taula_detall:
        new_table.append({'pk':row[0], 
                          'hc':row[1][0],
                          'agenda':row[1][1], 
                          'exploracio':row[1][2],
                          'informat':row[1][3], 
                          'revisat':row[1][4], 
                          'alliberat':row[1][5], 
                          'codificacio_inicial':row[1][6],
                          'codificacio_final':row[1][7],
                          'data_exploracio':row[1][8],
                          'data_informe':row[1][9],
                          'data_revisio':row[1][10],
                          'validat':row[1][11],
                          'revalidat':row[1][12],
                          'codgen_id':row[1][13]                          
                          })
        
    table = DetallRadiolegsTable(new_table, order_by=request.GET.get('sort'),exclude = ('rendiment','urv_a'))

    #table.paginate(page=request.GET.get('page', 1))
    
    
    taula_detall = table
    nregistres= str(len(new_table))
    
    grafics = [
              
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Detall d'informes realitzats/revisats/alliberats per: " + radioleg_nom_cognoms  + ".    Informes: " + nregistres,
               # 'categories': [ 'Història clínica','Exploració', 'Agenda','Informat', 'Revisat', 'Alliberat', 'Codificació', 'Data exploració', 'Data informe' ,'Data revisió' ,'validat','Modificar'],
                'series': taula_detall},
                ]


        # Definició
    definicio = """Detall dels tots els informes realitzats, revisats i alliberats en els que esta incolucrat el professional sel·leccionat 
     """
    auditoria.request_handler(request,8,'Tipus consulta: activitatTotaldetall ; ' + str(request.session['enabled_filters']) + ' ,metge_id:' + str(metge_id))
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request)) 






################################
# Detall de  tots els informes ralitzats ( creats , revisats i alliberats ) per radioleg ( ). 
# apareixen els informes de altres unitats
#
# El filtre mensual es fa per data de "document_revisat",  en general es quan el  docuement es tancat  i alliberat ( except per MN)
# DEtall de infromes realitzats, es mostra tot el que ha fet el readioleg durant el periode sellecionat 
# ( informes revisat alliberts ) en MN i RM TC Lleida veuen el camp alliberts  
##################################


   
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatTotalDetallModificar(request, provaohdim_id,codgen_id):

    missatge = ""
    url_ret = ''
    
    if 'HTTP_REFERER' in request.environ :
        if not 'modificar' in request.environ['HTTP_REFERER']:
            url_ret = request.environ['HTTP_REFERER']
            request.session['url_ret'] = url_ret
        else:
            if  'url_ret' in  request.session:
                if not 'modificar' in  request.session['url_ret']:
                    url_ret = request.session['url_ret']
            else:
                url_ret = '' 
        
    # TODO: -> s'ha de mirar si el user te permis per revalidar i el aci esta validat --> lalvors si hi ha un canvi guradem el codi _aci_final en el camp codificacio_generica_validada 
    # i enregistem qui i la data de qui revalida.. es el que al final s'ha pagat al facultatiu.
    te_revalidacio_permis ='meta.revalidar' in  request.user.get_all_permissions()
    
    if request.method == 'POST':
        
        #s'ha de buscar i si no existeix creem i guardem el objecte amb les dades modificades pel post'
        if not ProvaOhdimCodificacioGenerica.objects.filter(id=codgen_id).exists():
            form = ProvaOhdimCodificacioGenerica_Form(request.POST)#creem el objecte amb les dades del formulari
            obj_modificat = form.save(commit=False)#per poder canviar les variables -> sino al fer is_valid() el grava directament
            obj_modificat.prova_ohdim_id = provaohdim_id
            obj_modificat.modificat = True
            obj_modificat.modificat_per_id =  request.user.id
            obj_modificat.modificat_data =  datetime.datetime.now() 
            # si te permis de revalida llavors ja queda validat per ell però la revalidació es fa mes tard de tot en block en la solapa revalidar!!    
            if  te_revalidacio_permis:
                
                obj_modificat.validat = True
                obj_modificat.validat_per_id = request.user.id
                obj_modificat.validat_data = datetime.datetime.now()
                #gravem la eque es va validar en el seu moment pel cap de unitat, però que no es la pagada per RRHH                
                #obj_modificat.codificacio_generica_validada = None
            
            if form.is_valid():
                try:
                    obj_modificat.save()
                    #s'ah grabat i ja tenim un id nou,el recearreguem amb els nous valors al formulari
                    objnou = ProvaOhdimCodificacioGenerica.objects.get(prova_ohdim=provaohdim_id)
                    codgen_id = objnou.id
                    form =   ProvaOhdimCodificacioGenerica_Form( instance=objnou)#recarregem el formulari amb els camps afegits per codi i la BD com el timestamp
                    missatge = "s'ha afegit el canvi en la Base de dades!"
                except IntegrityError:
                    missatge = "Registre duplicat, no s'ha fet cap canvi!"
                    pass
        
        else:# estem editant un objecte codgenrica existent i nomes em de fer update, amb les dades del post
            
            obj = ProvaOhdimCodificacioGenerica.objects.get(pk=codgen_id)
            cod_gen_validada =obj.codificacio_generica_final # Guardem , habsns de agafar els canvis amb el form
            form =   ProvaOhdimCodificacioGenerica_Form(request.POST, instance=obj)#pasem les dades del objecte que esta en la BD mes las modificacions del usuari en el post
                        
            if form.is_valid():
                
                obj_modificat = form.save(commit=False) # previ a guardar posarem el usuari que fa el update

                if te_revalidacio_permis:
                    
                    # LA MAria no pot desvalidar nomes es guradarà el aci validar per tenir constancia a mes podria haber problemes al fer exportaicons de validats i es tornaria a reenviar el mateix ACI /ACH
                    
                    #obj_modificat.validat = not obj.validat #canviem el estat
                    #nomes gravem si era un de nou que no habia estat validat!!!
                    #if  obj_modificat.validat and  obj_modificat.validat_per_id is None: 
                    #    obj_modificat.validat_per_id = request.user.id
                    #    obj_modificat.validat_data = datetime.datetime.now()
                    #else: #desvalidem
                    #    obj_modificat.validat_per_id = None 
                    #    obj_modificat.validat_data = None
                        
                    #gravem la eque es va validar en el seu moment pel cap de unitat, però que no es la pagada per RRHH                
                    obj_modificat.codificacio_generica_validada = cod_gen_validada
                else:

                    obj_modificat.modificat_per_id =  request.user.id 
                    obj_modificat.modificat_data =  datetime.datetime.now()
                    obj_modificat.modificat = True
  
                missatge = "s'han guardat els canvis!"
                #obj_modificat.save_m2m() #per gravar many to many es necessari al fer el commit = False, en aquest cas no tenim res many yo many     
                obj_modificat.save()
                # per refrescar el canvi
                form = ProvaOhdimCodificacioGenerica_Form(instance=obj_modificat)
                  
    else:
        
        #mostrem el objecte si exxitreix en la bd        
        if ProvaOhdimCodificacioGenerica.objects.using('portal_dades').filter(id=codgen_id).exists():
            obj = ProvaOhdimCodificacioGenerica.objects.using('portal_dades').get(pk=codgen_id)
            form = ProvaOhdimCodificacioGenerica_Form(instance=obj)
                      
        else:#s'ha de crear , no te ACI ni ACH ni res (s'ha de crear un objecte prova_ohdim_codificaio_generica buit i despres el gravarem)
            data = datetime.datetime.now()  
            #: si te permis de revalida llavors afegim lse dades de validat per .. i tot lo altre perque si ho fa ell ja queda validat    
            if te_revalidacio_permis:
                form =   ProvaOhdimCodificacioGenerica_Form( initial ={'ProvaOhdim':provaohdim_id,'data':data ,
                                                                        'modificat':True ,'modificat_per': str(request.user.id) ,'modificat_data':data  
                                                                        ,'validat':True, 'validat_per': str(request.user.id) ,'validat_data':data
                                                                         })
            else:
                form =   ProvaOhdimCodificacioGenerica_Form( initial ={'ProvaOhdim':provaohdim_id,'data':data , 
                                                                       'modificat':True ,'modificat_per': str(request.user.id) ,'modificat_data':data   })
            
    auditoria.request_handler(request,8,'Tipus consulta: activitatTotaldetallModificar ; ' + str(request.session['enabled_filters']) + ' ,provaohdim_id:' + str(provaohdim_id) + ',codgen_id:' + str(codgen_id))
    return render_to_response("quadre_comandament/radiolegs/informesRealitzatsDetallModificar.html", 
                                                            { "form": form,
                                                              "objectiu":objectiu,
                                                              "objectiu_url":objectiu_url,
                                                              "missatge":missatge,
                                                              "provaohdim_id":provaohdim_id,
                                                              "codgen_id":codgen_id,
                                                              "url_ret":url_ret
                                                             },
                                                                
                                                                  context_instance=RequestContext(request)
                                                                )



#############################################################
# Activitat Extraordinaria 
#############################################################

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatExtraordinaria(request):
    
    
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'radiolegs_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    anys = request.session['enabled_filters']['any']
    mes = request.session['enabled_filters']['mes']
    unitatsel = request.session['enabled_filters']['unitat']
    modalitatsel = request.session['enabled_filters']['modalitat']

    if anys == 0:# quan es sel·lecionen tots els anys
        for filter_ in request.session['filters']:
            if filter_['nom'] == "any":
                any_primer = filter_['valors'][0][0]
                any_ultim = filter_['valors'][-1][0]
            else:
                any_primer = 2000
                any_ultim = 2800
        if mes == 0:
            start_date = datetime.date(int(any_primer), 1, 1)
            end_date = datetime.date(int(any_ultim), 12, 31)
        else: #tots els anys fins els mes sellecionat d'enguany!!
            dies_mes = calendar.monthrange(any_ultim, mes)
            start_date = datetime.date(any_primer, 1, 1)
            end_date = datetime.date(any_ultim, mes, dies_mes[1])
    else:
        if mes == 0:
                start_date = datetime.date(int(anys), 1, 1)
                end_date = datetime.date(int(anys), 12, 31) 
        else:
            dies_mes = calendar.monthrange(anys, mes)
            start_date = datetime.date(anys, mes, 1)
            end_date = datetime.date(anys, mes, dies_mes[1])

        
    # Definim querys per defecte
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    
                                                                        # no volem agenda ICS pero !!! si volem els metges que no tenen ralcio ( null )
    master_queryset = ProvaOhdim.objects.using('portal_dades').exclude( 
                                                                        Q(agenda__agenda_idi__es_ics=True ) & (
                                                                        Q(metge_informa__es_idi=False,  metge_informa__es_idi__isnull=False ) |
                                                                        Q(metge_revisa__es_idi=False,   metge_revisa__es_idi__isnull=False  ) |  
                                                                        Q(metge_allibera__es_idi=False, metge_allibera__es_idi__isnull=False)
                                                                        )
                                                                         # NO volem els metges que son del ICS i que la agenda es del ICS!!!  ( LA Q es totalment necessaria sino peta ( fa la join i li canvia el nom a la taula i despres alferla sevir peta))
                                                                       ).exclude (Q(agenda__agenda_idi__es_ics=True , metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True)
                                                                       # Ens quedem els que o bé no tenen realcio amb prova_ohdim_codificacio_generica o bé tenen un null possat perque al validar-lo es va treure el valor
                                                                       ).filter( 
                                                                                
                                                                                Q(provaohdimcodificaciogenerica__codificacio_generica_final__isnull=False) &
                                                                                Q(provaohdimcodificaciogenerica__codificacio_generica_final=8) | Q(provaohdimcodificaciogenerica__codificacio_generica_final=7))

    # AIXO FUNCIONA SI ES POSA EN LA MYSQL !!!!! NO HI HA UNAKLTRE FORAMA QUE CHUTI ( el Django no transalda bé la consulta !!!! unica sollucio es amb exclude )
    # AND  NOT ( portal_dades.agenda_idi.es_ics  AND  m_informa.id is Null  AND m_revisa.id is Null AND m_allibera.id is Null )
    # master_queryset = master_queryset.exclude(  agenda__agenda_idi__es_ics = True ,  metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True )
    #cada vegada que es fa una assignacio fa una consulta a la mysql --> posa un "limit 21" imagino que per comprovar que funcionen els filtres 
    
    #volem les prestacions entre el rang de dades, i que estiguin concloses 
    master_queryset =   master_queryset.filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
    
    if unitatsel != 0 and modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
    elif unitatsel!= 0:
        master_queryset = master_queryset.filter( agenda__agenda_idi__unitat_idi_id=unitatsel) 
            
    elif modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
    else:   
        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                          agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))

    import copy
    init_object = [0,0,0,0,0,0,0,0,0,0]
    
    taula_series2_tmp = {}

    queryset = master_queryset.values('metge_informa' ,'metge_informa__cognoms_nom' ).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')

    #NOM I COGNOMS i DETALL
    for prova in queryset:
        if  prova['metge_informa__cognoms_nom'] == None :
            try:
                if taula_series2_tmp[0] == None:
                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: red;'>Informes amb errors</span>"
                    taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../activitat-extraordinaria-detall/0' class='addlink'></a>"
            except:
                taula_series2_tmp[0] = copy.copy(init_object)
                taula_series2_tmp[0][0]  = "<span style='color: red;'>Informes amb errors</span>"
                taula_series2_tmp[0][7]  = "<a href='../activitat-extraordinaria-detall/0' class='addlink'></a>"

        else:
            taula_series2_tmp[prova['metge_informa']] = copy.copy(init_object)
            taula_series2_tmp[prova['metge_informa']][0]  = prova['metge_informa__cognoms_nom']
            taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../activitat-extraordinaria-detall/%s' class='addlink'></a>" % prova['metge_informa']

    # INFORMES CREATS
    queryset = master_queryset.values('metge_informa','metge_informa__cognoms_nom',).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')
    for prova in queryset:
        if  prova['metge_informa__cognoms_nom'] == None :
            taula_series2_tmp[0][1] = prova['informes_creats'] # s'ha de su
        else:
            taula_series2_tmp[prova['metge_informa']][1] = prova['informes_creats']
    # INFORMES REVISATS
    queryset = master_queryset.values('metge_revisa','metge_revisa__cognoms_nom').annotate(informes_revisats=Count('id')).order_by('metge_revisa__cognoms_nom')
    for prova in queryset:
        try:      
            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
        except KeyError:# no ha informat cap informe pero si revisat  o alliberat ?? els afegim
            taula_series2_tmp[prova['metge_revisa']] = copy.copy(init_object) # PETA que no hi ha el metge que informa === a el afegim aqui
            taula_series2_tmp[prova['metge_revisa']][0]  = prova['metge_revisa__cognoms_nom'] 
            taula_series2_tmp[prova['metge_revisa']][1] = 0
            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
            taula_series2_tmp[prova['metge_revisa']][7]  = "<a href='../activitat-extraordinaria-detall/%s' class='addlink'></a>" % prova['metge_revisa']
    # INFORMES ALLIBERATS
    queryset = master_queryset.values('metge_allibera','metge_allibera__cognoms_nom').annotate(informes_alliberats=Count('id')).order_by('metge_allibera__cognoms_nom')   
    for prova in queryset:
        try:
            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
        except KeyError:
            taula_series2_tmp[prova['metge_allibera']] = copy.copy(init_object)
            taula_series2_tmp[prova['metge_allibera']][1] = 0
            taula_series2_tmp[prova['metge_allibera']][2] = 0
            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
            taula_series2_tmp[prova['metge_allibera']][7]  = "<a href='../activitat-extraordinaria-detall/%s' class='addlink'></a>" % prova['metge_allibera']
        
    # INFORMES ACI
    queryset = master_queryset.filter(provaohdimcodificaciogenerica__codificacio_generica_final=7).values('metge_allibera').annotate(informes_alliberats=Count('id'))
    for prova in queryset:       
        taula_series2_tmp[prova['metge_allibera']][4] = prova['informes_alliberats']
    # INFORMES ACH
    queryset = master_queryset.filter(provaohdimcodificaciogenerica__codificacio_generica_final=8).values('metge_allibera').annotate(informes_alliberats=Count('id'))
    for prova in queryset:           
        taula_series2_tmp[prova['metge_allibera']][5] = prova['informes_alliberats']
    

  
  
    # SERAM temps_metge  Tot els informes que crea el facultatiu i que revisa ell amteix -> el 100% del seu valor SERAM
    factor_correctiu = 5 #Per tal que el valor del rendiment s'apropi mes a la realitat li summem a tot un valor

    #Tot els informes que crea el facultatiu i que revisa ell amteix -> el 100% del seu valor SERAM
    queryset = master_queryset.values('metge_revisa').filter( metge_informa= F('metge_revisa')).annotate(temps_seram=Sum('prestacio__codi_seram__temps_metge'), recomte=Count('prestacio__codi_seram__temps_metge')).order_by('metge_revisa__cognoms_nom')
  
    for prova in queryset:
        if prova['temps_seram'] == None:
            try:
                taula_series2_tmp[prova['metge_revisa']][6] = 0
            except KeyError:
                pass
        else:
            if prova['metge_revisa'] == None:
                taula_series2_tmp[0][6] =  taula_series2_tmp[0][6] + prova['temps_seram']
            else:
                #enspodem trobar que no te nom i cognoms , es el [0] llavors
                try:
                    taula_series2_tmp[prova['metge_revisa']][6] = prova['temps_seram'] + ( factor_correctiu *prova['recomte'])
                except KeyError:
                    taula_series2_tmp[0][6] =  taula_series2_tmp[0][6] + prova['temps_seram']
    

#    #hara sumem el 25% dels revisats i que no ha informat el mateix facultatiu 
#    #el resi crea i un adjunt allibera (i son diferents facultatius)
#    queryset = master_queryset.values('metge_revisa').exclude( metge_informa= F('metge_revisa')).annotate( temps_seram=Sum('prestacio__codi_seram__temps_metge') ).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
#        if prova['temps_seram'] <> None:
#                taula_series2_tmp[prova['metge_revisa']][6] = taula_series2_tmp[prova['metge_revisa']][6] + (25*int(prova['temps_seram'])/100)




##                    
##    #ARa nomes queden els  errors ->
#    #-> El que crea es un resi i el que revisa es un adjunt , però no ... el allibera el resi!!! , COM que aqui ja estan inclossos els de dalt comentem la part de dalt fins que SAP no prïvieixi queels resis tanquin informes 
#    queryset = master_queryset.values('metge_revisa').exclude(Q(metge_revisa = F('metge_informa')) & Q(metge_revisa = F('metge_allibera'))).annotate( temps_seram=Sum('prestacio__codi_seram__temps_metge') ).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
#        if not (prova['temps_seram'] == None):
#            if prova['metge_revisa'] == None:
#                tmp = None 
#            else: 
#                taula_series2_tmp[prova['metge_revisa']][6] = taula_series2_tmp[prova['metge_revisa']][6] + (25*int(prova['temps_seram'])/100)
#   

    #SERAM URVs    
    #Tot els informes que crea el facultatiu que revisen el 100% del seu valor SERAM
    queryset = master_queryset.values('metge_revisa').annotate(urv_a=Sum('prestacio__codi_seram__urv_a')).order_by('metge_revisa__cognoms_nom')
  
    for prova in queryset:
        if prova['urv_a'] == None:
            try:
                taula_series2_tmp[prova['metge_revisa']][9] = 0
            except KeyError:
                pass
        else:
            if prova['metge_revisa'] == None:
                taula_series2_tmp[0][9] =  taula_series2_tmp[0][9] + prova['urv_a']
            else:
                #enspodem trobar que no te nom i cognoms , es el [0] llavors
                try:
                    taula_series2_tmp[prova['metge_revisa']][9] = prova['urv_a']
                except KeyError:
                    taula_series2_tmp[0][9] =  taula_series2_tmp[0][9] + prova['urv_a']

    
#    #hara sumem el 25% dels revisats 
#    #el resi crea i un adjunt allibera (i son diferents facultatius)
#    queryset = master_queryset.values('metge_revisa').exclude( metge_informa= F('metge_revisa')).annotate( urv_a=Sum('prestacio__codi_seram__urv_a') ).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
#        if prova['urv_a'] <> None:
#            taula_series2_tmp[prova['metge_revisa']][9] =  float(taula_series2_tmp[prova['metge_revisa']][9]) + (25*float(prova['urv_a'])/100)



                    
##    #ARa nomes queden els  errors ->
#    #-> El que crea es un resi i el que revisa es un adjunt , però no ... el allibera el resi!!! , COM que aqui ja estan inclossos els de dalt comentem la part de dalt fins que SAP no prïvieixi queels resis tanquin informes 
#    queryset = master_queryset.values('metge_revisa').exclude(Q(metge_revisa = F('metge_informa')) & Q(metge_revisa = F('metge_allibera'))).annotate( temps_seram=Sum('prestacio__codi_seram__temps_metge') ).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
#        if not (prova['temps_seram'] == None):
#            if prova['metge_revisa'] == None:
#                tmp = None 
#            else: 
#                taula_series2_tmp[prova['metge_revisa']][6] = taula_series2_tmp[prova['metge_revisa']][6] + (25*int(prova['temps_seram'])/100)
#
    # Taula html - sort & filters
    taula_series2 = [] #creem una tupla de metge i les dades
    
    
    inf_alres_m = [0,0,0,0,0,0,0,0,0,0]
    dades_metge_tmp=[0,0,0,0,0,0,0,0,0,0]
    validat = False #, si hi ha algun validat ja no es podra tornar a validar
    #Sumem la moralla , els que no estan a la taula metge i els metge que son null en el provaohdim
    for dades_metge in taula_series2_tmp.items():
        if dades_metge[1][0] == None or dades_metge[1][0] == 0:
            if dades_metge[1][1] == None: dades_metge_tmp[0]= 0
            else:dades_metge_tmp[0] = dades_metge[1][1] 
            if dades_metge[1][2] == None: dades_metge_tmp[1]=0
            else:dades_metge_tmp[1] = dades_metge[1][2]
            if dades_metge[1][3] == None:  dades_metge_tmp[2]=0
            else:dades_metge_tmp[2] = dades_metge[1][3]
            if dades_metge[1][4] == None: dades_metge_tmp[3]=0
            else:dades_metge_tmp[3] = dades_metge[1][4]
            if dades_metge[1][5] == None:  dades_metge_tmp[4]=0
            else:dades_metge_tmp[4] = dades_metge[1][5]
            if dades_metge[1][6] == None: dades_metge_tmp[5]=0
            else:dades_metge_tmp[5] = dades_metge[1][6]

            if dades_metge[1][9] == None: dades_metge_tmp[8]=0
            else:dades_metge_tmp[8] = dades_metge[1][9]

                
            inf_alres_m = [ inf_alres_m[0],
                            inf_alres_m[1] + int(dades_metge_tmp[0]) ,
                            inf_alres_m[2] + int(dades_metge_tmp[1]),
                            inf_alres_m[3] + int(dades_metge_tmp[2]),
                            inf_alres_m[4] + int(dades_metge_tmp[3]),
                            inf_alres_m[5] + int(dades_metge_tmp[4]),
                            inf_alres_m[6] + int(dades_metge_tmp[5]),
                            inf_alres_m[7],
                            inf_alres_m[8],
                            inf_alres_m[9] + int(dades_metge_tmp[9]) ]
        else:
            if dades_metge[0]== 0:
                inf_alres_m =  dades_metge[1]
            else:
                taula_series2.append((dades_metge[1][0], dades_metge[1]))
                
    if inf_alres_m[0] != 0:#afegim la suma dels que notenen nom i errors
        taula_series2.append((inf_alres_m[0], inf_alres_m))
        
        
    taula_series2 = sorted(taula_series2, key=lambda row: row[1][0])
    new_table = []
    for serie in taula_series2:
        #'informes_creats':serie[1][1], 'informes_revisats':serie[1][2], 'informes_alliberats':serie[1][3]
        new_table.append({'radioleg':serie[0],  'aci':serie[1][4],'ach':serie[1][5] ,'temps_seram':serie[1][6],'urv_a':serie[1][9] ,'detall':serie[1][7]})
        if  serie[1][4] != 0 or serie[1][5] != 0:             
            validat = True
    table = RadiolegsExtraordinariaTable(new_table, order_by=request.GET.get('sort'))
    #table.paginate(page=request.GET.get('page', 1))
    taula_series = table

    # if request.GET.get('export','none') == "excel":   
    # grafics
    grafics = [
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Informes activitat extraordinaria",
                'categories': [ 'Creats', 'Revisats', 'Alliberats','aci','ach','temps_seram','urv_a', 'Detall'],
                'series': taula_series},              
                ]
    
        # Definició
    definicio = """
        Suma de tota l'activitat extraordinària alliberada pels radiòlegs al centre seleccionat.</br></br>
        En l'activitat, es tenen en compte les modificacions en l'activitat extraordinària.</br></br>  
        En cas de triar "Tots", apareix l'activitat de totes les unitats que el teu usuari té assignades.</br></br>
        """
    definicio =  definicio + """Sumem 5 min. a totes les prestacions per tal que el valor sigui mes real.</br></br>"""

    definicio =  definicio +  """        
        Les dates de tall són per data d'informe alliberat.</br></br>
        El rendiment és la suma de temps informe (definits per la SERAM) de les prestacions informades (revisades per) pel radiòleg a la unitat.
        """
        
    auditoria.request_handler(request,8,'Tipus consulta: activitatExtraordinaria ; ' + str(request.session['enabled_filters'])  )
    # hem heredat el indicador.html per poder afegir un boto de validacio
    return render_to_response('quadre_comandament/radiolegs/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url,
                               },
                              context_instance=RequestContext(request))                      


#############################################################
# Validacio de la Activitat Extraordinaria 
#############################################################

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatExtraordinariaPerValidar(request):
    ##
    # Mostrem les proves pendents de validar
    ##

    
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'radiolegs_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    anys = request.session['enabled_filters']['any']
    mes = request.session['enabled_filters']['mes']
    unitatsel = request.session['enabled_filters']['unitat']
    modalitatsel = request.session['enabled_filters']['modalitat']

    if anys == 0:# quan es sel·lecionen tots els anys
        for filter_ in request.session['filters']:
            if filter_['nom'] == "any":
                any_primer = filter_['valors'][0][0]
                any_ultim = filter_['valors'][-1][0]
            else:
                any_primer = 2000
                any_ultim = 2800
        if mes == 0:
            start_date = datetime.date(int(any_primer), 1, 1)
            end_date = datetime.date(int(any_ultim), 12, 31)
        else: #tots els anys fins els mes sellecionat d'enguany!!
            dies_mes = calendar.monthrange(any_ultim, mes)
            start_date = datetime.date(any_primer, 1, 1)
            end_date = datetime.date(any_ultim, mes, dies_mes[1])
    else:
        if mes == 0:
                start_date = datetime.date(int(anys), 1, 1)
                end_date = datetime.date(int(anys), 12, 31) 
        else:
            dies_mes = calendar.monthrange(anys, mes)
            start_date = datetime.date(anys, mes, 1)
            end_date = datetime.date(anys, mes, dies_mes[1])

    # Definim querys per defecte
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    
                                                                        # no volem agenda ICS pero !!! si volem els metges que no tenen ralcio ( null )
    master_queryset = ProvaOhdim.objects.using('portal_dades').exclude( 
                                                                        Q(agenda__agenda_idi__es_ics=True ) & (
                                                                        Q(metge_informa__es_idi=False,  metge_informa__es_idi__isnull=False ) |
                                                                        Q(metge_revisa__es_idi=False,   metge_revisa__es_idi__isnull=False  ) |  
                                                                        Q(metge_allibera__es_idi=False, metge_allibera__es_idi__isnull=False)
                                                                        )
                                                                         # NO volem els metges que son del ICS i que la agenda es del ICS!!!  ( LA Q es totalment necessaria sino peta ( fa la join i li canvia el nom a la taula i despres alferla sevir peta))
                                                                       ).exclude (Q(agenda__agenda_idi__es_ics=True , metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True)
                                                                       # Ens quedem els que o bé no tenen realcio amb prova_ohdim_codificacio_generica o bé tenen un null possat perque al validar-lo es va treure el valor
                                                                       ).filter( provaohdimcodificaciogenerica__codificacio_generica_final__isnull=False)

    # AIXO FUNCIONA SI ES POSA EN LA MYSQL !!!!! NO HI HA UNAKLTRE FORAMA QUE CHUTI ( el Django no transalda bé la consulta !!!! unica sollucio es amb exclude )
    # AND  NOT ( portal_dades.agenda_idi.es_ics  AND  m_informa.id is Null  AND m_revisa.id is Null AND m_allibera.id is Null )
    # master_queryset = master_queryset.exclude(  agenda__agenda_idi__es_ics = True ,  metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True )
    #cada vegada que es fa una assignacio fa una consulta a la mysql --> posa un "limit 21" imagino que per comprovar que funcionen els filtres 
    
    #volem les prestacions entre el rang de dades, i que estiguin concloses 
    master_queryset =   master_queryset.filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
    
    if unitatsel != 0 and modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
    elif unitatsel!= 0:
        master_queryset = master_queryset.filter( agenda__agenda_idi__unitat_idi_id=unitatsel) 
            
    elif modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
    else:   
        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                          agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))

    import copy
    init_object = [0,0,0,0,0,0,0,0]
    
    taula_series2_tmp = {}

    queryset = master_queryset.values('metge_informa' ,'metge_informa__cognoms_nom' ).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')

    #NOM I COGNOMS i DETALL
    for prova in queryset:
        if  prova['metge_informa__cognoms_nom'] == None :
            try:
                if taula_series2_tmp[0] == None:
                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: red;'>Informes amb errors</span>"
                    taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../activitat-extraordinaria-detall/0' class='addlink'></a>"
            except:
                taula_series2_tmp[0] = copy.copy(init_object)
                taula_series2_tmp[0][0]  = "<span style='color: red;'>Informes amb errors</span>"
                taula_series2_tmp[0][7]  = "<a href='../activitat-extraordinaria-detall/0' class='addlink'></a>"

        else:
            taula_series2_tmp[prova['metge_informa']] = copy.copy(init_object)
            taula_series2_tmp[prova['metge_informa']][0]  = prova['metge_informa__cognoms_nom']
            taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../activitat-extraordinaria-detall/%s' class='addlink'></a>" % prova['metge_informa']

    # INFORMES CREATS
    queryset = master_queryset.values('metge_informa','metge_informa__cognoms_nom',).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')
    for prova in queryset:
        if  prova['metge_informa__cognoms_nom'] == None :
            taula_series2_tmp[0][1] = prova['informes_creats'] # s'ha de su
        else:
            taula_series2_tmp[prova['metge_informa']][1] = prova['informes_creats']
    # INFORMES REVISATS
    queryset = master_queryset.values('metge_revisa','metge_revisa__cognoms_nom').annotate(informes_revisats=Count('id')).order_by('metge_revisa__cognoms_nom')
    for prova in queryset:
        try:      
            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
        except KeyError:# no ha informat cap informe pero si revisat  o alliberat ?? els afegim
            taula_series2_tmp[prova['metge_revisa']] = copy.copy(init_object) # PETA que no hi ha el metge que informa === a el afegim aqui
            taula_series2_tmp[prova['metge_revisa']][0]  = prova['metge_revisa__cognoms_nom'] 
            taula_series2_tmp[prova['metge_revisa']][1] = 0
            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
            taula_series2_tmp[prova['metge_revisa']][7]  = "<a href='../activitat-extraordinaria-detall/%s' class='addlink'></a>" % prova['metge_revisa']
    # INFORMES ALLIBERATS
    queryset = master_queryset.values('metge_allibera','metge_allibera__cognoms_nom').annotate(informes_alliberats=Count('id')).order_by('metge_allibera__cognoms_nom')   
    for prova in queryset:
        try:
            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
        except KeyError:
            taula_series2_tmp[prova['metge_allibera']] = copy.copy(init_object)
            taula_series2_tmp[prova['metge_allibera']][1] = 0
            taula_series2_tmp[prova['metge_allibera']][2] = 0
            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
            taula_series2_tmp[prova['metge_allibera']][7]  = "<a href='../activitat-extraordinaria-detall/%s' class='addlink'></a>" % prova['metge_allibera']
        
    # INFORMES ACI
    queryset = master_queryset.filter(Q(provaohdimcodificaciogenerica__codificacio_generica_final=7) &
                                      (Q(provaohdimcodificaciogenerica__validat=False) | Q(provaohdimcodificaciogenerica__validat__isnull=True) 
                                       )).values('metge_allibera').annotate(informes_alliberats=Count('id'))
    for prova in queryset:       
        taula_series2_tmp[prova['metge_allibera']][4] = prova['informes_alliberats']
    # INFORMES ACH
    queryset = master_queryset.filter(Q(provaohdimcodificaciogenerica__codificacio_generica_final=8) &
                                      (Q(provaohdimcodificaciogenerica__validat=False) | Q(provaohdimcodificaciogenerica__validat__isnull=True) 
                                       )).values('metge_allibera').annotate(informes_alliberats=Count('id'))
    
    
    for prova in queryset:           
        taula_series2_tmp[prova['metge_allibera']][5] = prova['informes_alliberats']
        


    # Taula html - sort & filters
    taula_series2 = [] #creem una tupla de metge i les dades
    
    
    inf_alres_m = [0,0,0,0,0,0,0,0]
    dades_metge_tmp=[0,0,0,0,0,0,0,0]
    validar = False #, si estan tots validats ja no es mostrara
    
    #Sumem la moralla , els que no estan a la taula metge i els metge que son null en el provaohdim
    for dades_metge in taula_series2_tmp.items():
        if dades_metge[1][0] == None or dades_metge[1][0] == 0:
            if dades_metge[1][1] == None: dades_metge_tmp[0]= 0
            else:dades_metge_tmp[0] = dades_metge[1][1] 
            if dades_metge[1][2] == None: dades_metge_tmp[1]=0
            else:dades_metge_tmp[1] = dades_metge[1][2]
            if dades_metge[1][3] == None:  dades_metge_tmp[2]=0
            else:dades_metge_tmp[2] = dades_metge[1][3]
            if dades_metge[1][4] == None: dades_metge_tmp[3]=0
            else:dades_metge_tmp[3] = dades_metge[1][4]
            if dades_metge[1][5] == None:  dades_metge_tmp[4]=0
            else:dades_metge_tmp[4] = dades_metge[1][5]
#            if dades_metge[1][6] == None: dades_metge_tmp[5]=0
#            else:dades_metge_tmp[5] = dades_metge[1][6]
                
            inf_alres_m = [ inf_alres_m[0],
                            inf_alres_m[1] + int(dades_metge_tmp[0]) ,
                            inf_alres_m[2] + int(dades_metge_tmp[1]),
                            inf_alres_m[3] + int(dades_metge_tmp[2]),
                            inf_alres_m[4] + int(dades_metge_tmp[3]),
                            inf_alres_m[5] + int(dades_metge_tmp[4]),
                            inf_alres_m[6] + int(dades_metge_tmp[5])
#                            ,inf_alres_m[7] 
                            ]
        else:
            if dades_metge[0]== 0:
                inf_alres_m =  dades_metge[1]
            else:
                taula_series2.append((dades_metge[1][0], dades_metge[1]))
                
#    if inf_alres_m[0] != 0:#afegim la suma dels que notenen nom i errors
#        taula_series2.append((inf_alres_m[0], inf_alres_m))
        
        
    taula_series2 = sorted(taula_series2, key=lambda row: row[1][0])
    new_table = []
    for serie in taula_series2:
        new_table.append({'radioleg':serie[0],  'aci':serie[1][4],'ach':serie[1][5] ,'detall':serie[1][7]})
        
        if  serie[1][4] != 0 or serie[1][5] != 0:             
            validar = True #si hi ha un aci o ach ja es mostrara
    table = RadiolegsExtraordinariaValidarTable(new_table, order_by=request.GET.get('sort'))
    #table.paginate(page=request.GET.get('page', 1))
    taula_series = table

    # if request.GET.get('export','none') == "excel":   
    # grafics
    grafics = [
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Informes activitat extraordinaria",
                'categories': [ 'Creats', 'Revisats', 'Alliberats','aci','ach', 'Detall'],
                'series': taula_series},              
                ]
    
        # Definició
    definicio = """
        Activitat realitzada pendent de validar pel responsable.
        """

    
    
    # seccio  validacio per part dels caps de unitat
    formulari = 'activitat_Extraordinaria_Validar_Centre'
#    validat = False
#    queryset = master_queryset.filter(
#                                     (Q(provaohdimcodificaciogenerica__codificacio_generica_final=8) | Q(provaohdimcodificaciogenerica__codificacio_generica_final=7))
#                                    & Q(provaohdimcodificaciogenerica__validat=True )).values('metge_allibera').annotate(validats=Count('id'))
#    for prova in queryset:           
#        if prova['validats'] and not validat:
#            validat = True#existeix algun validat i per tant no es mostrara el boto per a Validar
    
    #mirem si te permisos per validar , si en te mostrem el boto i el formulari per poder fer-ho , 15 i 17 informatics, 26 cap unitat, 27 director unitat
    if not AuthUserProfile.objects.get( user = AuthUser.objects.get(username =str(request.user)).id).categoria_profesional.id in [15,17,26,27,31]:
        validar = False
    # fi seccio validacio
    
    
    auditoria.request_handler(request,8,'Tipus consulta: acitivitatExtraordinariaPerValidar ; ' + str(request.session['enabled_filters']) )
    # hem heredat el indicador.html per poder afegir un boto de validacio
    return render_to_response('quadre_comandament/radiolegs/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url,
                               'forumlari':formulari, 
                               'validar':validar},
                              context_instance=RequestContext(request))                      





@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatExtraordinariaValidar(request):
    ###
    #executar la validacio de les perastcions selecionades
    ###
    missatge = ""
    url_ret = ''
    
    if 'HTTP_REFERER' in request.environ :
        if not 'modificar' in request.environ['HTTP_REFERER']:
            url_ret = request.environ['HTTP_REFERER']
            request.session['url_ret'] = url_ret
        else:
            if  'url_ret' in  request.session:
                if not 'modificar' in  request.session['url_ret']:
                    url_ret = request.session['url_ret']
            else:
                url_ret = '' 
    
    
    
    if request.method == 'POST':
        
 
        # Cal definir els filtres que volem per defecte
        request.session['current_filters'] = 'radiolegs_filters' 
        if not request.session['current_filters'] in request.session:
            enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
            initFilters(request, enabled_filters, request.session['current_filters'])
        request.session['enabled_filters'] = request.session[request.session['current_filters']]

        anys = request.session['enabled_filters']['any']
        mes = request.session['enabled_filters']['mes']
        unitatsel = request.session['enabled_filters']['unitat']
        modalitatsel = request.session['enabled_filters']['modalitat']

        if anys == 0:# quan es sel·lecionen tots els anys
            for filter_ in request.session['filters']:
                if filter_['nom'] == "any":
                    any_primer = filter_['valors'][0][0]
                    any_ultim = filter_['valors'][-1][0]
                else:
                    any_primer = 2000
                    any_ultim = 2800
            if mes == 0:
                start_date = datetime.date(int(any_primer), 1, 1)
                end_date = datetime.date(int(any_ultim), 12, 31)
            else: #tots els anys fins els mes sellecionat d'enguany!!
                dies_mes = calendar.monthrange(any_ultim, mes)
                start_date = datetime.date(any_primer, 1, 1)
                end_date = datetime.date(any_ultim, mes, dies_mes[1])
        else:
            if mes == 0:
                start_date = datetime.date(int(anys), 1, 1)
                end_date = datetime.date(int(anys), 12, 31) 
            else:
                dies_mes = calendar.monthrange(anys, mes)
                start_date = datetime.date(anys, mes, 1)
                end_date = datetime.date(anys, mes, dies_mes[1])

        
        # Definim querys per defecte
        user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    
                                                                        # no volem agenda ICS pero !!! si volem els metges que no tenen ralcio ( null )
        master_queryset = ProvaOhdim.objects.using('portal_dades').exclude( 
                                                                        Q(agenda__agenda_idi__es_ics=True ) & (
                                                                        Q(metge_informa__es_idi=False,  metge_informa__es_idi__isnull=False ) |
                                                                        Q(metge_revisa__es_idi=False,   metge_revisa__es_idi__isnull=False  ) |  
                                                                        Q(metge_allibera__es_idi=False, metge_allibera__es_idi__isnull=False)
                                                                        )
                                                                         # NO volem els metges que son del ICS i que la agenda es del ICS!!!  ( LA Q es totalment necessaria sino peta ( fa la join i li canvia el nom a la taula i despres alferla sevir peta))
                                                                       ).exclude (Q(agenda__agenda_idi__es_ics=True , metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True)
                                                                       # Ens quedem els que o bé no tenen realcio amb prova_ohdim_codificacio_generica o bé tenen un null possat perque al validar-lo es va treure el valor
                                                                       ).filter( provaohdimcodificaciogenerica__codificacio_generica_final__isnull=False)

    
        #volem les prestacions entre el rang de dades, i que estiguin concloses 
        master_queryset =   master_queryset.filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
    
        if unitatsel != 0 and modalitatsel != 0:
            master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
        elif unitatsel!= 0:
            master_queryset = master_queryset.filter( agenda__agenda_idi__unitat_idi_id=unitatsel) 
            
        elif modalitatsel != 0:
            master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
        else:   
            master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                          agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))


        master_queryset = master_queryset.filter(
                                     (Q(provaohdimcodificaciogenerica__codificacio_generica_final=8) | Q(provaohdimcodificaciogenerica__codificacio_generica_final=7))
                                    & ( Q(provaohdimcodificaciogenerica__validat=False ) | Q(provaohdimcodificaciogenerica__validat__isnull=True ) )  ).values('id')
 
        #s'ha de validar tot
        # no chuta -> perque es unaltre taula --> resultat = master_queryset.update(provaohdimcodificaciogenerica__validat=True)
        data = datetime.datetime.now()
        #nprestacions = master_queryset.annotate(Count('id')) fa una consulta la mysql un count
        nprestacions = 0
        for item in master_queryset:
            ProvaOhdimCodificacioGenerica.objects.select_related().filter(prova_ohdim=item['id']).update(
                                                                                                         validat = True,
                                                                                                         validat_per = str(request.user.id),
                                                                                                         validat_data=data,
                                                                                                         revalidat=False#sino possa un Null
                                                                                                         )
            nprestacions = nprestacions + 1
#            item.save()
        missatge = "s'han validat les " + str( nprestacions) + " prestacions filtrades! "  
        validar = True
          
    else:
        #mostrem el objecte si exxitreix en la bd
        validar = False
        missatge = "torneu enrrere si us plau! "
            
    # s'ha marcat validat ,
    definicio = 'Resultat de la operació'
    auditoria.request_handler(request,8,'Tipus consulta: activitatExtraordinariaValdiar update-> ; ' + str(request.session['enabled_filters']) )
    return render_to_response('quadre_comandament/radiolegs/indicador.html',
                              {'indicadors':get_indicadors(request),
                                "objectiu":objectiu,
                                "objectiu_url":objectiu_url,
                                 'definicio':definicio,
                                'url_ret':url_ret,
                               'validar': validar,
                               'missatge':missatge},
                              context_instance=RequestContext(request))


@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatExtraordinariaDetall(request, metge_id):
  
    # Cal definir els filtres que volem per defecte
        # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'radiolegs_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

#    request.session['enabled_filters'] = {}
    anys = request.session['radiolegs_filters']['any']
    mes = request.session['radiolegs_filters']['mes']
    unitatsel = request.session['radiolegs_filters']['unitat']
    modalitatsel = request.session['radiolegs_filters']['modalitat']

    if anys == 0:# quan es sel·lecionen tots els anys
        for filter_ in request.session['filters']:
            if filter_['nom'] == "any":
                any_primer = filter_['valors'][0][0]
                any_ultim = filter_['valors'][-1][0]
            else:
                any_primer = 2000
                any_ultim = 2800
        if mes == 0:
            start_date = datetime.date(int(any_primer), 1, 1)
            end_date = datetime.date(int(any_ultim), 12, 31)
        else: #tots els anys fins els mes sellecionat d'enguany!!
            dies_mes = calendar.monthrange(any_ultim, mes)
            start_date = datetime.date(any_primer, 1, 1)
            end_date = datetime.date(any_ultim, mes, dies_mes[1])
    else:
        if mes == 0:
                start_date = datetime.date(int(anys), 1, 1)
                end_date = datetime.date(int(anys), 12, 31) 
        else:
            dies_mes = calendar.monthrange(anys, mes)
            start_date = datetime.date(anys, mes, 1)
            end_date = datetime.date(anys, mes, dies_mes[1])

  
    #TODO: AIXO S?HA DE TREURE PERQUE POTSER HI HAN DIGANOSTICAT INFORMES DALTRES CENTRES i els volem veure??? SIiiii
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    
    # La Maria te permis per revalidar, es adir per cambiar el que ha valdiat el acp de unitat, es adir per possar el que RRHH paga finalmanet
    te_revalidacio_permis = 'meta.revalidar' in  request.user.get_all_permissions()   
    
    
    if metge_id == '0':
        radioleg_nom_cognoms = "Informes amb errors"
        master_queryset = ProvaOhdim.objects.using('portal_dades').filter( agenda__agenda_idi__es_ics=False
                                                                 ).filter( Q(metge_informa__isnull=True) |
                                                                           Q(metge_revisa__isnull=True) |
                                                                           Q(metge_allibera__isnull=True)
                                                                  ).filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
                                                                    
    else:
        radioleg_nom_cognoms = Metge.objects.using('portal_dades').get(pk=long(metge_id)).cognoms_nom
        master_queryset = ProvaOhdim.objects.using('portal_dades').filter( Q(metge_informa=metge_id) |
                                                                           Q(metge_revisa=metge_id) |
                                                                           Q(metge_allibera=metge_id)
                                                                 ).filter(Q(data_document_revisat__range=(start_date, end_date)) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))

    if unitatsel != 0 and modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                                 agenda__agenda_idi__idtipoexploracion=modalitatsel)
    elif unitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel)  
            
    elif modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                                 agenda__agenda_idi__idtipoexploracion=modalitatsel)
    else:   
        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                                 agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))


    # Ens quedem els que tenen  valor
    master_queryset = master_queryset.filter( Q(provaohdimcodificaciogenerica__codificacio_generica_final=7) | Q(provaohdimcodificaciogenerica__codificacio_generica_final=8))


                                
    proves_queryset_iterator = master_queryset.values( 'id', 
                                       'historia_clinica_id',
                                       'tipus_modalitat', 
                                       'agenda__agenda_idi__obs',
                                       'prestacio_id',
                                       'prestacio__descripcio',
                                       'prestacio__codi',
                                       'prestacio__codi_seram__temps_metge',
                                       'prestacio__codi_seram__urv_a',
                                       'metge_informa','metge_revisa','metge_allibera',
                                       'metge_informa__cognoms_nom','metge_revisa__cognoms_nom','metge_allibera__cognoms_nom', 
                                       'codificacio_generica__codi',
                                       'data_cita',
                                       'hora_cita',
                                       'data_document_informat',
                                       'hora_document_informat',
                                       'data_document_revisat',
                                       'hora_document_revisat'
                                       ).order_by('tipus_modalitat', 'agenda', 'historia_clinica_id', 'data_cita', 'hora_cita').iterator()




    import copy
    init_object = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    icona_oky= '<IMG SRC="/static/admin/img/admin/icon_success.gif" BORDER=0 ALT="Fet">'

    #NOM I COGNOMS i DETALL
    taula_proves = {}
    #rows_duplicats = copy.copy(proves_queryset_iterator)   No es pot ducplicar un iterador¿?
    taula_detall = []
    #radioleg_nom_cognoms = 'Desconegut'
    
    #i = len(proves_queryset) 263 i nomes fa el bucle de 100 !!!!!!!!!!!!!!!!!1
    # els querysets tenen un problema de cache i es queden nomes amb 100 registres -> !!!!!!!
    # http://jeffelmore.org/2010/09/25/smarter-caching-of-django-querysets/#comment-100
    # es mes eficient possar-ho en una named-tuple 
    #    taula_detall_item = namedtuple('Detall', 'id historia_clinica_id tipus_modalitat agenda__agenda_idi__obs prestacio_id prestacio__descripcio metge_informa metge_revisa metge_allibera metge_informa__cognoms_nom metge_revisa__cognoms_nom metge_allibera__cognoms_nom codificacio_generica__codi data_cita hora_cita data_document_informat hora_document_informat data_document_revisat hora_document_revisat')
    
    for prova in proves_queryset_iterator:
        
        taula_proves[prova['id']] = copy.copy(init_object)

        if prova['metge_informa'] == long(metge_id):
            metge_informa = icona_oky 
        else: 
            metge_informa =  prova['metge_informa__cognoms_nom']

        if prova['metge_revisa'] == long(metge_id):
            metge_revisa = icona_oky 
        else: 
            metge_revisa =  prova['metge_revisa__cognoms_nom']
        if prova['metge_allibera'] == long(metge_id):
            metge_allibera = icona_oky 
        else: 
            metge_allibera =  prova['metge_allibera__cognoms_nom']
            
        taula_proves[prova['id']][1]  = prova
        #if (radioleg_nom_cognoms == '' and prova['metge_informa__cognoms_nom'] !=None ):
        #    radioleg_nom_cognoms = prova['metge_informa__cognoms_nom']

        if prova['data_cita'] == None  or  prova['hora_cita'] == None: 
            data_i_hora_cita= '-'
        else:  
            data_i_hora_cita =  datetime.datetime(prova['data_cita'].year, prova['data_cita'].month, prova['data_cita'].day, prova['hora_cita'].hour, prova['hora_cita'].minute, prova['hora_cita'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_document_informat'] == None  or  prova['hora_document_informat'] == None:
            data_i_hora_doc_inf = '-'
        else:
            data_i_hora_doc_inf = datetime.datetime(prova['data_document_informat' ].year, prova['data_document_informat'].month, prova['data_document_informat'].day, prova['hora_document_informat'].hour, prova['hora_document_informat'].minute, prova['hora_document_informat'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_document_revisat'] == None  or  prova['hora_document_revisat'] == None:
            data_i_hora_doc_rev = '-'      
        else:
            data_i_hora_doc_rev = datetime.datetime(prova['data_document_revisat'].year, prova['data_document_revisat'].month, prova['data_document_revisat'].day, prova['hora_document_revisat'].hour, prova['hora_document_revisat'].minute, prova['hora_document_revisat'].second).strftime('%Y/%m/%d %H:%M')

        # Ara buscarem totes aquelles proves que tenen codificacio generica per posar la codificacio inicial i la final i si esta validat o no i per qui
        codgenerica_queryset = ProvaOhdimCodificacioGenerica.objects.using('portal_dades').filter(prova_ohdim__exact= prova['id'])#.values('codificacio_generica_inicial','codificacio_generica_final')
        # en prinicpi nomes pot ser un pero... retorna un queryset
        codificacio_inicial= ''
        codificacio_final= ''
        validat = ''
        revalidat = ''
        #pasem a la vista de modificar el pk de provaohdim per si no hi ha codificaciogenirica ( descuid del radioleg)
        modificar = "<a href='../activitat-total-detall-modificar/%s&%s' class='changelink'></a>" % (prova['id'],'0')
     
        for codgen in codgenerica_queryset:
            #ls pk de prevaohdim_codificaio_geenrica   aixi el paser en un link
            modificar="<a href='../activitat-total-detall-modificar/%s&%s' class='changelink'></a>" % ( prova['id'],codgen.id)
            try:
                codificacio_inicial = str(codgen.codificacio_generica_inicial)
                if codificacio_inicial=='None' :codificacio_inicial =''
            except:
                codificacio_inicial = '' 
            try:
                codificacio_final = str(codgen.codificacio_generica_final)
                if codificacio_final=='None' :codificacio_final =''
            except:
                codificacio_final = ''
            if codgen.validat:
                #T Si es algu que te permisos de revalidar .. pagat .. llavors validat = a icona_oky però es podra modificar -> en la fucnion miodificar hemd e fer 
                # els canvis pertinents -> si esta validat llavors revalidar . Guradem el valor que hi havia en un camp nou, la data i el user i deixem com a nou valor el que 
                #hi hagui posat dee  nou i una icona nova qu posa Revalidat
                validat =  icona_oky                
                if not te_revalidacio_permis:
                    modificar = ''

            if codgen.revalidat:
                revalidat = icona_oky
                #un cop revalidat ja no es opt tornar a editar
                modificar = ''
                    
                    
            
        
        #SERAM  temps_metge for prova_rev in queryset_revisats:
        rendiment =  prova['prestacio__codi_seram__temps_metge']#
        if unicode(prova['metge_revisa']) not in metge_id:
            rendiment = 0
            
        if   (unicode(prova['metge_revisa']) in metge_id )  and ( prova['metge_informa'] != prova['metge_revisa'] ) :
            if not (prova['prestacio__codi_seram__temps_metge'] == None):
                rendiment =  str((25*long(prova['prestacio__codi_seram__temps_metge']) / 100)).replace(".", ",")
                
                
                
                
        #SERAM URVs  for prova_rev in queryset_revisats:
        urv_a =  prova['prestacio__codi_seram__urv_a']#
        if unicode(prova['metge_revisa']) not in metge_id:
            urv_a = 0
            
        if   (unicode(prova['metge_revisa']) in metge_id )  and ( prova['metge_informa'] != prova['metge_revisa'] ) :
            if not (prova['prestacio__codi_seram__urv_a'] == None):
                urv_a =  str((25*long(prova['prestacio__codi_seram__urv_a']) / 100)).replace(".", ",")
                 

        
            
        duplicat_trobat=None
        #Les proves consecuteives  les marqeum amb marrro!!! 
#        for row_duplicats in rows_duplicats:
#            if (prova['historia_clinica_id'] == row_duplicats['historia_clinica_id'] ) and ( prova['data_cita'] == row_duplicats['data_cita']) and (prova['prestacio_id'] != row_duplicats['prestacio_id']):
#                duplicat_trobat='style=color:brown;'
            
        taula_detall.append((prova['id'],( prova['historia_clinica_id'],
                                           prova['agenda__agenda_idi__obs'],
                                           prova['prestacio__descripcio'] ,
                                           metge_informa , 
                                           metge_revisa, 
                                           metge_allibera,
                                           codificacio_inicial, 
                                           codificacio_final,
                                           data_i_hora_cita,
                                           data_i_hora_doc_inf,
                                           data_i_hora_doc_rev,
                                           validat,
                                           revalidat,
                                           modificar,
                                           rendiment,
                                           urv_a
                                           ), duplicat_trobat 
                             )
                            )
#    # if request.GET.get('export','none') == "excel":   

    # grafics
    new_table = []
    for row in taula_detall:
        new_table.append({'pk':row[0], 
                          'hc':row[1][0],
                          'agenda':row[1][1], 
                          'exploracio':row[1][2],
                          'informat':row[1][3], 
                          'revisat':row[1][4], 
                          'alliberat':row[1][5], 
                          'codificacio_inicial':row[1][6],
                          'codificacio_final':row[1][7],
                          'data_exploracio':row[1][8],
                          'data_informe':row[1][9],
                          'data_revisio':row[1][10],
                          'rendiment':row[1][13],
                          'urv_a':row[1][14],
                          'validat':row[1][11],
                          'revalidat':row[1][12],
                          'codgen_id':row[1][13]                           
                          })
    table = DetallRadiolegsTable(new_table, order_by=request.GET.get('sort'))
    #table.paginate(page=request.GET.get('page', 1))
    
    
    taula_detall = table
    nregistres= str(len(new_table))
    
    grafics = [
              
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Detall d'informes extraorinaris realitzats/revisats/alliberats per: " + radioleg_nom_cognoms  + ".    Informes: " + nregistres,
                'categories': [ 'Història clínica','Exploració', 'Agenda','Informat', 'Revisat', 'Alliberat', 'Codificació', 'Data exploració', 'Data informe' ,'Data revisió' ,'rendiment','urv_a','validat','revalidat'],
                'series': taula_detall},
                ]
    auditoria.request_handler(request,8,'Tipus consulta: acitivtatExtraorinaria ; ' + str(request.session['enabled_filters']) + ' ,metge_id:' + str(metge_id) )
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request)) 

#
##############################################################
## Activitat Ordinaria 
##############################################################
#
#@login_required
#@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
#def activitatOrdinaria(request):
#   
#
#
#    # Cal definir els filtres que volem per defecte
#    request.session['current_filters'] = 'radiolegs_filters' 
#    if not request.session['current_filters'] in request.session:
#        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
#        initFilters(request, enabled_filters, request.session['current_filters'])
#    request.session['enabled_filters'] = request.session[request.session['current_filters']]
#
#    anys = request.session['enabled_filters']['any']
#    mes = request.session['enabled_filters']['mes']
#    unitatsel = request.session['enabled_filters']['unitat']
#    modalitatsel = request.session['enabled_filters']['modalitat']
#
#    if anys == 0:# quan es sel·lecionen tots els anys
#        for filter_ in request.session['filters']:
#            if filter_['nom'] == "any":
#                any_primer = filter_['valors'][0][0]
#                any_ultim = filter_['valors'][-1][0]
#            else:
#                any_primer = 2000
#                any_ultim = 2800
#        if mes == 0:
#            start_date = datetime.date(int(any_primer), 1, 1)
#            end_date = datetime.date(int(any_ultim), 12, 31)
#        else: #tots els anys fins els mes sellecionat d'enguany!!
#            dies_mes = calendar.monthrange(any_ultim, mes)
#            start_date = datetime.date(any_primer, 1, 1)
#            end_date = datetime.date(any_ultim, mes, dies_mes[1])
#    else:
#        if mes == 0:
#                start_date = datetime.date(int(anys), 1, 1)
#                end_date = datetime.date(int(anys), 12, 31) 
#        else:
#            dies_mes = calendar.monthrange(anys, mes)
#            start_date = datetime.date(anys, mes, 1)
#            end_date = datetime.date(anys, mes, dies_mes[1])
#
#        
#    # Definim querys per defecte
#    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
#    
#                                                                        # no volem agenda ICS pero !!! si volem els metges que no tenen ralcio ( null )
#    master_queryset = ProvaOhdim.objects.using('portal_dades').exclude( 
#                                                                        Q(agenda__agenda_idi__es_ics=True ) & (
#                                                                        Q(metge_informa__es_idi=False,  metge_informa__es_idi__isnull=False ) |
#                                                                        Q(metge_revisa__es_idi=False,   metge_revisa__es_idi__isnull=False  ) |  
#                                                                        Q(metge_allibera__es_idi=False, metge_allibera__es_idi__isnull=False)
#                                                                        )
#                                                                         # NO volem els metges que son del ICS i que la agenda es del ICS!!!  ( LA Q es totalment necessaria sino peta ( fa la join i li canvia el nom a la taula i despres alferla sevir peta))
#                                                                       ).exclude (Q(agenda__agenda_idi__es_ics=True , metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True))
#                                                                                  
#    #Ens quedem els que o bé no tenen realcio amb prova_ohdim_codificacio_generica o bé tenen un null possat perque al validar-lo es va treure el valor
#    master_queryset = master_queryset.exclude( provaohdimcodificaciogenerica__codificacio_generica_final__in=[7,8])
#
#    #volem les prestacions entre el rang de dades, i que estiguin concloses 
#    master_queryset =   master_queryset.filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
#    
#    if unitatsel != 0 and modalitatsel != 0:
#        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
#                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
#    elif unitatsel!= 0:
#        master_queryset = master_queryset.filter( agenda__agenda_idi__unitat_idi_id=unitatsel) 
#            
#    elif modalitatsel != 0:
#        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
#                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
#    else:   
#        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
#                                          agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))
#
#    import copy
#    init_object = [0,0,0,0,0,0,0,0]
#    
#    taula_series2_tmp = {}
#
#    queryset = master_queryset.values('metge_informa' ,'metge_informa__cognoms_nom' ).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')
#
#    #NOM I COGNOMS i DETALL
#    for prova in queryset:
#        if  prova['metge_informa__cognoms_nom'] == None :
#            try:
#                if taula_series2_tmp[0] == None:
#                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: red;'>Informes amb errors</span>"
#                    taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../activitat-ordinaria-detall/0' class='addlink'></a>"
#            except:
#                taula_series2_tmp[0] = copy.copy(init_object)
#                taula_series2_tmp[0][0]  = "<span style='color: red;'>Informes amb errors</span>"
#                taula_series2_tmp[0][7]  = "<a href='../activitat-ordinaria-detall/0' class='addlink'></a>"
#
#        else:
#            taula_series2_tmp[prova['metge_informa']] = copy.copy(init_object)
#            taula_series2_tmp[prova['metge_informa']][0]  = prova['metge_informa__cognoms_nom']
#            taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../activitat-ordinaria-detall/%s' class='addlink'></a>" % prova['metge_informa']
#
#    # INFORMES CREATS
#    queryset = master_queryset.values('metge_informa','metge_informa__cognoms_nom',).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')
#    for prova in queryset:
#        if  prova['metge_informa__cognoms_nom'] == None :
#            taula_series2_tmp[0][1] = prova['informes_creats'] # s'ha de su
#        else:
#            taula_series2_tmp[prova['metge_informa']][1] = prova['informes_creats']
#    # INFORMES REVISATS
#    queryset = master_queryset.values('metge_revisa','metge_revisa__cognoms_nom').annotate(informes_revisats=Count('id')).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
#        try:      
#            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
#        except KeyError:# no ha informat cap informe pero si revisat  o alliberat ?? els afegim
#            taula_series2_tmp[prova['metge_revisa']] = copy.copy(init_object) # PETA que no hi ha el metge que informa === a el afegim aqui
#            taula_series2_tmp[prova['metge_revisa']][0]  = prova['metge_revisa__cognoms_nom'] 
#            taula_series2_tmp[prova['metge_revisa']][1] = 0
#            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
#            taula_series2_tmp[prova['metge_revisa']][7]  = "<a href='../activitat-ordinaria-detall/%s' class='addlink'></a>" % prova['metge_revisa']
#    # INFORMES ALLIBERATS
#    queryset = master_queryset.values('metge_allibera','metge_allibera__cognoms_nom').annotate(informes_alliberats=Count('id')).order_by('metge_allibera__cognoms_nom')   
#    for prova in queryset:
#        try:
#            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
#        except KeyError:
#            taula_series2_tmp[prova['metge_allibera']] = copy.copy(init_object)
#            taula_series2_tmp[prova['metge_allibera']][1] = 0
#            taula_series2_tmp[prova['metge_allibera']][2] = 0
#            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
#            taula_series2_tmp[prova['metge_allibera']][7]  = "<a href='../activitat-ordinaria-detall/%s' class='addlink'></a>" % prova['metge_allibera']
#        
#    # INFORMES ACI
#    queryset = master_queryset.filter(provaohdimcodificaciogenerica__codificacio_generica_final=7).values('metge_allibera').annotate(informes_alliberats=Count('id'))
#    for prova in queryset:       
#        taula_series2_tmp[prova['metge_allibera']][4] = prova['informes_alliberats']
#    # INFORMES ACH
#    queryset = master_queryset.filter(provaohdimcodificaciogenerica__codificacio_generica_final=8).values('metge_allibera').annotate(informes_alliberats=Count('id'))
#    for prova in queryset:           
#        taula_series2_tmp[prova['metge_allibera']][5] = prova['informes_alliberats']
#        
#    # SERAM 
#    #TODO: alguna cosa hem de fer per comprovar aquelles proves que no tenen codi seram , o assegurar-nos que totes les prestacions en tenen...
#    #nomes aquells que ha informat i ha alliberat el mateix radioleg
#    
#    queryset = master_queryset.values('metge_revisa').annotate(temps_seram=Sum('prestacio__codi_seram__temps_metge')).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
#        if prova['temps_seram'] == None:
#            taula_series2_tmp[prova['metge_revisa']][6] = 0
#        else:
#            taula_series2_tmp[prova['metge_revisa']][6] = prova['temps_seram']
#    
#    
##    queryset = master_queryset.values('metge_allibera').filter( metge_allibera = F('metge_informa')).annotate(temps_seram=Sum('prestacio__codi_seram__temps_metge')).order_by('metge_allibera__cognoms_nom')
##    for prova in queryset:
##        if prova['temps_seram'] == None:
##            taula_series2_tmp[prova['metge_allibera']][6] = 0
##        else:
##            taula_series2_tmp[prova['metge_allibera']][6] = prova['temps_seram']
##    #hara sumem el 25% dels revisats 
##    queryset = master_queryset.values('metge_allibera').exclude( metge_allibera = F('metge_informa')).annotate( temps_seram=Sum('prestacio__codi_seram__temps_metge') ).order_by('metge_allibera__cognoms_nom')
##    for prova in queryset:
##        if not (prova['temps_seram'] == None):
##            taula_series2_tmp[prova['metge_allibera']][6] = taula_series2_tmp[prova['metge_allibera']][6] + (25*int(prova['temps_seram'])/100)
#
#    # Taula html - sort & filters
#    taula_series2 = [] #creem una tupla de metge i les dades
#    
#    
#    inf_alres_m = [0,0,0,0,0,0,0,0]
#    dades_metge_tmp=[0,0,0,0,0,0,0,0]
#    #Sumem la moralla , els que no estan a la taula metge i els metge que son null en el provaohdim
#    for dades_metge in taula_series2_tmp.items():
#        if dades_metge[1][0] == None or dades_metge[1][0] == 0:
#            if dades_metge[1][1] == None: dades_metge_tmp[0]= 0
#            else:dades_metge_tmp[0] = dades_metge[1][1] 
#            if dades_metge[1][2] == None: dades_metge_tmp[1]=0
#            else:dades_metge_tmp[1] = dades_metge[1][2]
#            if dades_metge[1][3] == None:  dades_metge_tmp[2]=0
#            else:dades_metge_tmp[2] = dades_metge[1][3]
#            if dades_metge[1][4] == None: dades_metge_tmp[3]=0
#            else:dades_metge_tmp[3] = dades_metge[1][4]
#            if dades_metge[1][5] == None:  dades_metge_tmp[4]=0
#            else:dades_metge_tmp[4] = dades_metge[1][5]
#            if dades_metge[1][6] == None: dades_metge_tmp[5]=0
#            else:dades_metge_tmp[5] = dades_metge[1][6]
#                
#            inf_alres_m = [ inf_alres_m[0],
#                            inf_alres_m[1] + int(dades_metge_tmp[0]) ,
#                            inf_alres_m[2] + int(dades_metge_tmp[1]),
#                            inf_alres_m[3] + int(dades_metge_tmp[2]),
#                            inf_alres_m[4] + int(dades_metge_tmp[3]),
#                            inf_alres_m[5] + int(dades_metge_tmp[4]),
#                            inf_alres_m[6] + int(dades_metge_tmp[5]),
#                            inf_alres_m[7] ]
#        else:
#            if dades_metge[0]== 0:
#                inf_alres_m =  dades_metge[1]
#            else:
#                taula_series2.append((dades_metge[1][0], dades_metge[1]))
#                
#    if inf_alres_m[0] != 0:#afegim la suma dels que notenen nom i errors
#        taula_series2.append((inf_alres_m[0], inf_alres_m))
#        
#        
#    taula_series2 = sorted(taula_series2, key=lambda row: row[1][0])
#    new_table = []
#    for serie in taula_series2:
#        new_table.append({'radioleg':serie[0], 'informes_creats':serie[1][1], 'informes_revisats':serie[1][2], 'informes_alliberats':serie[1][3], 'aci':serie[1][4],'ach':serie[1][5] ,'temps_seram':serie[1][6],'detall':serie[1][7]})
#    table = RadiolegsOrdinariaTable(new_table, order_by=request.GET.get('sort'))
#    #table.paginate(page=request.GET.get('page', 1))
#    taula_series = table
#
#    # if request.GET.get('export','none') == "excel":   
#    # grafics
#    grafics = [
#               {'tipus': "taula2", 
#                'tamany': 100,                
#                'titol': "Informes activitat ordinaria",
#                'categories': [ 'Creats', 'Revisats', 'Alliberats','aci','ach','temps_seram', 'Detall'],
#                'series': taula_series},              
#                ]
#    
#        # Definició
#    definicio = """
#        Suma de tota l'activitat ordinària informada, revisada i alliberada pels radiòlegs al centre seleccionat.</br></br> 
#        En cas de triar "Tots", apareix l'activitat de totes les unitats que el teu usuari té assignades.</br></br>
#        Les dates de tall són per data d'informe alliberat.</br></br>
#        El rendiment és la suma de temps informe (definits per la SERAM) de les prestacions informades (reviades per ) pel radiòleg a la unitat.
#        """
#        
#        
#        
#    auditoria.request_handler(request,8,'Tipus consulta: acitivtatOrdinaria; ' + str(request.session['enabled_filters']) + ' ' )
#    return render_to_response('quadre_comandament/indicador.html',
#                              {'indicadors':get_indicadors(request),
#                               'grafics':grafics,
#                               'objectiu':objectiu,
#                               'definicio':definicio,
#                               'objectiu_url':objectiu_url},
#                              context_instance=RequestContext(request))                      


@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatOrdinariaDetall(request, metge_id):

    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'radiolegs_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    anys = request.session['radiolegs_filters']['any']
    mes = request.session['radiolegs_filters']['mes']
    unitatsel = request.session['radiolegs_filters']['unitat']
    modalitatsel = request.session['radiolegs_filters']['modalitat']

    if anys == 0:# quan es sel·lecionen tots els anys
        for filter_ in request.session['filters']:
            if filter_['nom'] == "any":
                any_primer = filter_['valors'][0][0]
                any_ultim = filter_['valors'][-1][0]
            else:
                any_primer = 2000
                any_ultim = 2800
        if mes == 0:
            start_date = datetime.date(int(any_primer), 1, 1)
            end_date = datetime.date(int(any_ultim), 12, 31)
        else: #tots els anys fins els mes sellecionat d'enguany!!
            dies_mes = calendar.monthrange(any_ultim, mes)
            start_date = datetime.date(any_primer, 1, 1)
            end_date = datetime.date(any_ultim, mes, dies_mes[1])
    else:
        if mes == 0:
                start_date = datetime.date(int(anys), 1, 1)
                end_date = datetime.date(int(anys), 12, 31) 
        else:
            dies_mes = calendar.monthrange(anys, mes)
            start_date = datetime.date(anys, mes, 1)
            end_date = datetime.date(anys, mes, dies_mes[1])

    #TODO: AIXO S?HA DE TREURE PERQUE POTSER HI HAN DIGANOSTICAT INFORMES DALTRES CENTRES i els volem veure??? SIiiii
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    
    if metge_id == '0':
        radioleg_nom_cognoms = "Informes amb errors"
        master_queryset = ProvaOhdim.objects.using('portal_dades').filter( agenda__agenda_idi__es_ics=False
                                                                 ).filter( Q(metge_informa__isnull=True) |
                                                                           Q(metge_revisa__isnull=True) |
                                                                           Q(metge_allibera__isnull=True)
                                                                  ).filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
    else:
        radioleg_nom_cognoms = Metge.objects.using('portal_dades').get(pk=long(metge_id)).cognoms_nom
        master_queryset = ProvaOhdim.objects.using('portal_dades').filter( Q(metge_informa=metge_id) |
                                                                           Q(metge_revisa=metge_id) |
                                                                           Q(metge_allibera=metge_id)
                                                                 ).filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))

    if unitatsel != 0 and modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                                 agenda__agenda_idi__idtipoexploracion=modalitatsel)
    elif unitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel)  
            
    elif modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                                 agenda__agenda_idi__idtipoexploracion=modalitatsel)
    else:   
        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                                 agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))

    # NO HI HA UANLTRE FORAM; ADE FER:_HO , amb lexlude es fa un lio i no fa bé la consulta mysql
    # treiem els que tenen valor en ACI ACH == ordinaria
    master_queryset = master_queryset.filter( ~Q(provaohdimcodificaciogenerica__codificacio_generica_final=7) & ~Q(provaohdimcodificaciogenerica__codificacio_generica_final=8))
                              
    proves_queryset_iterator = master_queryset.values( 'id', 
                                       'historia_clinica_id',
                                       'tipus_modalitat', 
                                       'agenda__agenda_idi__obs',
                                       'prestacio_id',
                                       'prestacio__descripcio',
                                       'metge_informa','metge_revisa','metge_allibera',
                                       'metge_informa__cognoms_nom','metge_revisa__cognoms_nom','metge_allibera__cognoms_nom', 
                                       'codificacio_generica__codi',
                                       'data_cita',
                                       'hora_cita',
                                       'data_document_informat',
                                       'hora_document_informat',
                                       'data_document_revisat',
                                       'hora_document_revisat'
                                       ).order_by('tipus_modalitat', 'agenda', 'historia_clinica_id', 'data_cita', 'hora_cita').iterator()

    import copy
    init_object = [0,0,0,0,0,0,0,0,0,0,0,0,0]
    icona_oky= '<IMG SRC="/static/admin/img/admin/icon_success.gif" BORDER=0 ALT="Fet">'

    #NOM I COGNOMS i DETALL
    taula_proves = {}
    #rows_duplicats = copy.copy(proves_queryset_iterator)   No es pot ducplicar un iterador¿?
    taula_detall = []
    #radioleg_nom_cognoms = 'Desconegut'
    
    #i = len(proves_queryset) 263 i nomes fa el bucle de 100 !!!!!!!!!!!!!!!!!1
    # els querysets tenen un problema de cache i es queden nomes amb 100 registres -> !!!!!!!
    # http://jeffelmore.org/2010/09/25/smarter-caching-of-django-querysets/#comment-100
    # es mes eficient possar-ho en una named-tuple 
    #    taula_detall_item = namedtuple('Detall', 'id historia_clinica_id tipus_modalitat agenda__agenda_idi__obs prestacio_id prestacio__descripcio metge_informa metge_revisa metge_allibera metge_informa__cognoms_nom metge_revisa__cognoms_nom metge_allibera__cognoms_nom codificacio_generica__codi data_cita hora_cita data_document_informat hora_document_informat data_document_revisat hora_document_revisat')

    for prova in proves_queryset_iterator:
        
        taula_proves[prova['id']] = copy.copy(init_object)

        if prova['metge_informa'] == long(metge_id):
            metge_informa = icona_oky 
        else: 
            metge_informa =  prova['metge_informa__cognoms_nom']

        if prova['metge_revisa'] == long(metge_id):
            metge_revisa = icona_oky 
        else: 
            metge_revisa =  prova['metge_revisa__cognoms_nom']
        if prova['metge_allibera'] == long(metge_id):
            metge_allibera = icona_oky 
        else: 
            metge_allibera =  prova['metge_allibera__cognoms_nom']
            
        taula_proves[prova['id']][1]  = prova
        #if (radioleg_nom_cognoms == '' and prova['metge_informa__cognoms_nom'] !=None ):
        #    radioleg_nom_cognoms = prova['metge_informa__cognoms_nom']

        if prova['data_cita'] == None  or  prova['hora_cita'] == None: 
            data_i_hora_cita= '-'
        else:  
            data_i_hora_cita =  datetime.datetime(prova['data_cita'].year, prova['data_cita'].month, prova['data_cita'].day, prova['hora_cita'].hour, prova['hora_cita'].minute, prova['hora_cita'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_document_informat'] == None  or  prova['hora_document_informat'] == None:
            data_i_hora_doc_inf = '-'
        else:
            data_i_hora_doc_inf = datetime.datetime(prova['data_document_informat' ].year, prova['data_document_informat'].month, prova['data_document_informat'].day, prova['hora_document_informat'].hour, prova['hora_document_informat'].minute, prova['hora_document_informat'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_document_revisat'] == None  or  prova['hora_document_revisat'] == None:
            data_i_hora_doc_rev = '-'      
        else:
            data_i_hora_doc_rev = datetime.datetime(prova['data_document_revisat'].year, prova['data_document_revisat'].month, prova['data_document_revisat'].day, prova['hora_document_revisat'].hour, prova['hora_document_revisat'].minute, prova['hora_document_revisat'].second).strftime('%Y/%m/%d %H:%M')

        # Ara buscarem totes aquelles proves que tenen codificacio generica per posar la codificacio inicial i la final i si esta validat o no i per qui
        codgenerica_queryset = ProvaOhdimCodificacioGenerica.objects.using('portal_dades').filter(prova_ohdim__exact= prova['id'])#.values('codificacio_generica_inicial','codificacio_generica_final')
        # en prinicpi nomes pot ser un pero... retorna un queryset
        codificacio_inicial= ''
        codificacio_final= ''
        validat = ''
        #pasem a la vista de modificar el pk de provaohdim per si no hi ha codificaciogenirica ( descuid del radioleg)
        modificar = "<a href='../activitat-total-detall-modificar/%s&%s' class='changelink'></a>" % (prova['id'],'0')
     
        for codgen in codgenerica_queryset:
            #ls pk de prevaohdim_codificaio_geenrica   aixi el paser en un link
            modificar="<a href='../activitat-total-detall-modificar/%s&%s' class='changelink'></a>" % ( prova['id'],codgen.id)
    
            try:
                codificacio_inicial = str(codgen.codificacio_generica_inicial)
                if codificacio_inicial=='None' :codificacio_inicial =''
            except:
                codificacio_inicial = '' 
            try:
                codificacio_final = str(codgen.codificacio_generica_final)
                if codificacio_final=='None' :codificacio_final =''
            except:
                codificacio_final = ''
            if codgen.validat:
                validat =  icona_oky                
                modificar = ''
            
            
        duplicat_trobat=None
        #Les proves consecuteives  les marqeum amb marrro!!! 
#        for row_duplicats in rows_duplicats:
#            if (prova['historia_clinica_id'] == row_duplicats['historia_clinica_id'] ) and ( prova['data_cita'] == row_duplicats['data_cita']) and (prova['prestacio_id'] != row_duplicats['prestacio_id']):
#                duplicat_trobat='style=color:brown;'
                
        taula_detall.append((prova['id'],( prova['historia_clinica_id'],
                                           prova['agenda__agenda_idi__obs'],
                                           prova['prestacio__descripcio'] ,
                                           metge_informa , 
                                           metge_revisa, 
                                           metge_allibera,
                                           codificacio_inicial, 
                                           codificacio_final,
                                           data_i_hora_cita,
                                           data_i_hora_doc_inf,
                                           data_i_hora_doc_rev,
                                           validat,
                                           modificar
                                           ), duplicat_trobat 
                             )
                            )
#    # if request.GET.get('export','none') == "excel":   

    # grafics
    new_table = []
    for row in taula_detall:
        new_table.append({'pk':row[0], 
                          'hc':row[1][0],
                          'agenda':row[1][1], 
                          'exploracio':row[1][2],
                          'informat':row[1][3], 
                          'revisat':row[1][4], 
                          'alliberat':row[1][5], 
                          'codificacio_inicial':row[1][6],
                          'codificacio_final':row[1][7],
                          'data_exploracio':row[1][8],
                          'data_informe':row[1][9],
                          'data_revisio':row[1][10],
                          'validat':row[1][11],
                          'codgen_id':row[1][12]                           
                          })
    
    table = DetallRadiolegsTable(new_table, order_by=request.GET.get('sort') ,exclude=('codificacio_final','validat','revalidat','rendiment','urv_a'))
    
    
    #table.paginate(page=request.GET.get('page', 1))
    
    
    taula_detall = table
    nregistres= str(len(new_table))
    
    grafics = [
              
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Detall d'informes ordinaris realitzats/revisats/alliberats per: " + radioleg_nom_cognoms  + ".    Informes: " + nregistres,
                'categories': [ 'Història clínica','Exploració', 'Agenda','Informat', 'Revisat', 'Alliberat', 'Codificació', 'Data exploració', 'Data informe' ,'Data revisió' ,'validat'],
                'series': taula_detall},
                ]

    auditoria.request_handler(request,8,'Tipus consulta: activitatOrdinariaDetall ; ' + str(request.session['enabled_filters']) + ' ,metge_id:' + str(metge_id) )
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request)) 
    
#    
#    
##############################################################
## SEgones opnions 
##############################################################
#
#@login_required
#@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
#def segonesOpinions(request):
#   
#
#
#    # Cal definir els filtres que volem per defecte
#    request.session['current_filters'] = 'radiolegs_filters' 
#    if not request.session['current_filters'] in request.session:
#        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
#        initFilters(request, enabled_filters, request.session['current_filters'])
#    request.session['enabled_filters'] = request.session[request.session['current_filters']]
#
#    anys = request.session['enabled_filters']['any']
#    mes = request.session['enabled_filters']['mes']
#    unitatsel = request.session['enabled_filters']['unitat']
#    modalitatsel = request.session['enabled_filters']['modalitat']
#
#    if mes != 0:
#        dies_mes = calendar.monthrange(anys, mes)
#        start_date = datetime.date(anys, mes, 1)
#        end_date = datetime.date(anys, mes, dies_mes[1])
#    else:
#        start_date = datetime.date(anys, 1, 1)
#        end_date = datetime.date(anys, 12, 31) 
#        
#    # Definim querys per defecte
#    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
#    
#                                                                        # no volem agenda ICS pero !!! si volem els metges que no tenen ralcio ( null )
#    master_queryset = ProvaOhdim.objects.using('portal_dades').exclude( 
#                                                                        Q(agenda__agenda_idi__es_ics=True ) & (
#                                                                        Q(metge_informa__es_idi=False,  metge_informa__es_idi__isnull=False ) |
#                                                                        Q(metge_revisa__es_idi=False,   metge_revisa__es_idi__isnull=False  ) |  
#                                                                        Q(metge_allibera__es_idi=False, metge_allibera__es_idi__isnull=False)
#                                                                        )
#                                                                         # NO volem els metges que son del ICS i que la agenda es del ICS!!!  ( LA Q es totalment necessaria sino peta ( fa la join i li canvia el nom a la taula i despres alferla sevir peta))
#                                                                       ).exclude (Q(agenda__agenda_idi__es_ics=True , metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True))
#                                                                                  
#    #Ens quedem els que o bé no tenen realcio amb prova_ohdim_codificacio_generica o bé tenen un null possat perque al validar-lo es va treure el valor
#    master_queryset = master_queryset.exclude( provaohdimcodificaciogenerica__codificacio_generica_final__in=[7,8])
#
#    #volem les prestacions entre el rang de dades, i que estiguin concloses 
#    master_queryset =   master_queryset.filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
#    
#    
#    #perestacions realitzades en altres centres , segones opinions
#    master_queryset =   master_queryset.filter(prestacio=18)
#    
#    
#    if unitatsel != 0 and modalitatsel != 0:
#        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
#                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
#    elif unitatsel!= 0:
#        master_queryset = master_queryset.filter( agenda__agenda_idi__unitat_idi_id=unitatsel) 
#            
#    elif modalitatsel != 0:
#        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
#                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
#    else:   
#        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
#                                          agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))
#
#    import copy
#    init_object = [0,0,0,0,0,0,0,0]
#    
#    taula_series2_tmp = {}
#
#    queryset = master_queryset.values('metge_informa' ,'metge_informa__cognoms_nom' ).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')
#
#    #NOM I COGNOMS i DETALL
#    for prova in queryset:
#        if  prova['metge_informa__cognoms_nom'] == None :
#            try:
#                if taula_series2_tmp[0] == None:
#                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: red;'>Informes amb errors</span>"
#                    taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../segones-opinions-detall/0' class='addlink'></a>"
#            except:
#                taula_series2_tmp[0] = copy.copy(init_object)
#                taula_series2_tmp[0][0]  = "<span style='color: red;'>Informes amb errors</span>"
#                taula_series2_tmp[0][7]  = "<a href='../activitat-ordinaria-detall/0' class='addlink'></a>"
#
#        else:
#            taula_series2_tmp[prova['metge_informa']] = copy.copy(init_object)
#            taula_series2_tmp[prova['metge_informa']][0]  = prova['metge_informa__cognoms_nom']
#            taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../segones-opinions-detall/%s' class='addlink'></a>" % prova['metge_informa']
#
#    # INFORMES CREATS
#    queryset = master_queryset.values('metge_informa','metge_informa__cognoms_nom',).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')
#    for prova in queryset:
#        if  prova['metge_informa__cognoms_nom'] == None :
#            taula_series2_tmp[0][1] = prova['informes_creats'] # s'ha de su
#        else:
#            taula_series2_tmp[prova['metge_informa']][1] = prova['informes_creats']
#    # INFORMES REVISATS
#    queryset = master_queryset.values('metge_revisa','metge_revisa__cognoms_nom').annotate(informes_revisats=Count('id')).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
#        try:      
#            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
#        except KeyError:# no ha informat cap informe pero si revisat  o alliberat ?? els afegim
#            taula_series2_tmp[prova['metge_revisa']] = copy.copy(init_object) # PETA que no hi ha el metge que informa === a el afegim aqui
#            taula_series2_tmp[prova['metge_revisa']][0]  = prova['metge_revisa__cognoms_nom'] 
#            taula_series2_tmp[prova['metge_revisa']][1] = 0
#            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
#            taula_series2_tmp[prova['metge_revisa']][7]  = "<a href='../segones-opinions-detall/%s' class='addlink'></a>" % prova['metge_revisa']
#    # INFORMES ALLIBERATS
#    queryset = master_queryset.values('metge_allibera','metge_allibera__cognoms_nom').annotate(informes_alliberats=Count('id')).order_by('metge_allibera__cognoms_nom')   
#    for prova in queryset:
#        try:
#            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
#        except KeyError:
#            taula_series2_tmp[prova['metge_allibera']] = copy.copy(init_object)
#            taula_series2_tmp[prova['metge_allibera']][1] = 0
#            taula_series2_tmp[prova['metge_allibera']][2] = 0
#            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
#            taula_series2_tmp[prova['metge_allibera']][7]  = "<a href='../segones-opinions-detall/%s' class='addlink'></a>" % prova['metge_allibera']
#        
#    # INFORMES ACI
#    queryset = master_queryset.filter(provaohdimcodificaciogenerica__codificacio_generica_final=7).values('metge_allibera').annotate(informes_alliberats=Count('id'))
#    for prova in queryset:       
#        taula_series2_tmp[prova['metge_allibera']][4] = prova['informes_alliberats']
#    # INFORMES ACH
#    queryset = master_queryset.filter(provaohdimcodificaciogenerica__codificacio_generica_final=8).values('metge_allibera').annotate(informes_alliberats=Count('id'))
#    for prova in queryset:           
#        taula_series2_tmp[prova['metge_allibera']][5] = prova['informes_alliberats']
#        
#    # SERAM 
#    #TODO: alguna cosa hem de fer per comprovar aquelles proves que no tenen codi seram , o assegurar-nos que totes les prestacions en tenen...
#    #nomes aquells que ha informat i ha alliberat el mateix radioleg
#    queryset = master_queryset.values('metge_allibera').filter( metge_allibera = F('metge_informa')).annotate(temps_seram=Sum('prestacio__codi_seram__temps_metge')).order_by('metge_allibera__cognoms_nom')
#    for prova in queryset:
#        if prova['temps_seram'] == None:
#            taula_series2_tmp[prova['metge_allibera']][6] = 0
#        else:
#            taula_series2_tmp[prova['metge_allibera']][6] = prova['temps_seram']
#    #hara sumem el 25% dels revisats 
#    queryset = master_queryset.values('metge_allibera').exclude( metge_allibera = F('metge_informa')).annotate( temps_seram=Sum('prestacio__codi_seram__temps_metge') ).order_by('metge_allibera__cognoms_nom')
#    for prova in queryset:
#        if not (prova['temps_seram'] == None):
#            taula_series2_tmp[prova['metge_allibera']][6] = taula_series2_tmp[prova['metge_allibera']][6] + (25*int(prova['temps_seram'])/100)
#
#    # Taula html - sort & filters
#    taula_series2 = [] #creem una tupla de metge i les dades
#    
#    
#    inf_alres_m = [0,0,0,0,0,0,0,0]
#    dades_metge_tmp=[0,0,0,0,0,0,0,0]
#    #Sumem la moralla , els que no estan a la taula metge i els metge que son null en el provaohdim
#    for dades_metge in taula_series2_tmp.items():
#        if dades_metge[1][0] == None or dades_metge[1][0] == 0:
#            if dades_metge[1][1] == None: dades_metge_tmp[0]= 0
#            else:dades_metge_tmp[0] = dades_metge[1][1] 
#            if dades_metge[1][2] == None: dades_metge_tmp[1]=0
#            else:dades_metge_tmp[1] = dades_metge[1][2]
#            if dades_metge[1][3] == None:  dades_metge_tmp[2]=0
#            else:dades_metge_tmp[2] = dades_metge[1][3]
#            if dades_metge[1][4] == None: dades_metge_tmp[3]=0
#            else:dades_metge_tmp[3] = dades_metge[1][4]
#            if dades_metge[1][5] == None:  dades_metge_tmp[4]=0
#            else:dades_metge_tmp[4] = dades_metge[1][5]
#            if dades_metge[1][6] == None: dades_metge_tmp[5]=0
#            else:dades_metge_tmp[5] = dades_metge[1][6]
#                
#            inf_alres_m = [ inf_alres_m[0],
#                            inf_alres_m[1] + int(dades_metge_tmp[0]) ,
#                            inf_alres_m[2] + int(dades_metge_tmp[1]),
#                            inf_alres_m[3] + int(dades_metge_tmp[2]),
#                            inf_alres_m[4] + int(dades_metge_tmp[3]),
#                            inf_alres_m[5] + int(dades_metge_tmp[4]),
#                            inf_alres_m[6] + int(dades_metge_tmp[5]),
#                            inf_alres_m[7] ]
#        else:
#            if dades_metge[0]== 0:
#                inf_alres_m =  dades_metge[1]
#            else:
#                taula_series2.append((dades_metge[1][0], dades_metge[1]))
#                
#    if inf_alres_m[0] != 0:#afegim la suma dels que notenen nom i errors
#        taula_series2.append((inf_alres_m[0], inf_alres_m))
#        
#        
#    taula_series2 = sorted(taula_series2, key=lambda row: row[1][0])
#    new_table = []
#    for serie in taula_series2:
#        new_table.append({'radioleg':serie[0], 'informes_creats':serie[1][1], 'informes_revisats':serie[1][2], 'informes_alliberats':serie[1][3], 'aci':serie[1][4],'ach':serie[1][5] ,'temps_seram':serie[1][6],'detall':serie[1][7]})
#    table = RadiolegsOrdinariaTable(new_table, order_by=request.GET.get('sort'))
#    #table.paginate(page=request.GET.get('page', 1))
#    taula_series = table
#
#    # if request.GET.get('export','none') == "excel":   
#    # grafics
#    grafics = [
#               {'tipus': "taula2", 
#                'tamany': 100,                
#                'titol': "Informes d'estudis realitzats en altres centres. ( segones opnions )",
#                'categories': [ 'Creats', 'Revisats', 'Alliberats','aci','ach','temps_seram', 'Detall'],
#                'series': taula_series},              
#                ]
#    
#        # Definició
#    definicio = """
#        Suma de tota l'activitat realitzada en altres centres ( segones opnions )</br></br> 
#        Nomes es compten en els casos que el facultatiu a desglossat en al prestació SAP "segones opinions".</br></br>
#        """
#    auditoria.request_handler(request,8,'Tipus consulta: acitivtatOrdinaria; ' + str(request.session['enabled_filters']) + ' ' )
#    return render_to_response('quadre_comandament/indicador.html',
#                              {'indicadors':get_indicadors(request),
#                               'grafics':grafics,
#                               'objectiu':objectiu,
#                               'definicio':definicio,
#                               'objectiu_url':objectiu_url},
#                              context_instance=RequestContext(request))                      
#
#
#
#
#
#@login_required
#@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
#def segonesOpinionsDetall(request, metge_id):
#
#    # Cal definir els filtres que volem per defecte
#    request.session['current_filters'] = 'radiolegs_filters' 
#    if not request.session['current_filters'] in request.session:
#        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
#        initFilters(request, enabled_filters, request.session['current_filters'])
#    request.session['enabled_filters'] = request.session[request.session['current_filters']]
#
#    #request.session['enabled_filters'] = {}
#    anys = request.session['radiolegs_filters']['any']
#    mes = request.session['radiolegs_filters']['mes']
#    unitatsel = request.session['radiolegs_filters']['unitat']
#    modalitatsel = request.session['radiolegs_filters']['modalitat']
#
#    if mes != 0:
#        dies_mes = calendar.monthrange(anys, mes)
#        start_date = datetime.date(anys, mes, 1)
#        end_date = datetime.date(anys, mes, dies_mes[1])
#    else:
#        start_date = datetime.date(anys, 1, 1)
#        end_date = datetime.date(anys, 12, 31) 
#  
#    #TODO: AIXO S?HA DE TREURE PERQUE POTSER HI HAN DIGANOSTICAT INFORMES DALTRES CENTRES i els volem veure??? SIiiii
#    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
#    
#    if metge_id == '0':
#        radioleg_nom_cognoms = "Informes amb errors"
#        master_queryset = ProvaOhdim.objects.using('portal_dades').filter( agenda__agenda_idi__es_ics=False
#                                                                 ).filter( Q(metge_informa__isnull=True) |
#                                                                           Q(metge_revisa__isnull=True) |
#                                                                           Q(metge_allibera__isnull=True)
#                                                                  ).filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
#    else:
#        radioleg_nom_cognoms = Metge.objects.using('portal_dades').get(pk=long(metge_id)).cognoms_nom
#        master_queryset = ProvaOhdim.objects.using('portal_dades').filter( Q(metge_informa=metge_id) |
#                                                                           Q(metge_revisa=metge_id) |
#                                                                           Q(metge_allibera=metge_id)
#                                                                 ).filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
#
#    if unitatsel != 0 and modalitatsel != 0:
#        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
#                                                 agenda__agenda_idi__idtipoexploracion=modalitatsel)
#    elif unitatsel != 0:
#        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel)  
#            
#    elif modalitatsel != 0:
#        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
#                                                 agenda__agenda_idi__idtipoexploracion=modalitatsel)
#    else:   
#        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
#                                                 agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))
#
#    # NO HI HA UANLTRE FORAM; ADE FER:_HO , amb lexlude es fa un lio i no fa bé la consulta mysql
#    # treiem els que tenen valor en ACI ACH == ordinaria
#    master_queryset = master_queryset.filter( ~Q(provaohdimcodificaciogenerica__codificacio_generica_final=7) & ~Q(provaohdimcodificaciogenerica__codificacio_generica_final=8))
#    
##perestacions realitzades en altres centres , segones opinions
#    master_queryset =   master_queryset.filter(prestacio=18)
#                              
#    proves_queryset_iterator = master_queryset.values( 'id', 
#                                       'historia_clinica_id',
#                                       'tipus_modalitat', 
#                                       'agenda__agenda_idi__obs',
#                                       'prestacio_id',
#                                       'prestacio__descripcio',
#                                       'metge_informa','metge_revisa','metge_allibera',
#                                       'metge_informa__cognoms_nom','metge_revisa__cognoms_nom','metge_allibera__cognoms_nom', 
#                                       'codificacio_generica__codi',
#                                       'data_cita',
#                                       'hora_cita',
#                                       'data_document_informat',
#                                       'hora_document_informat',
#                                       'data_document_revisat',
#                                       'hora_document_revisat'
#                                       ).order_by('tipus_modalitat', 'agenda', 'historia_clinica_id', 'data_cita', 'hora_cita').iterator()
#
#    import copy
#    init_object = [0,0,0,0,0,0,0,0,0,0,0,0,0]
#    icona_oky= '<IMG SRC="/static/admin/img/admin/icon_success.gif" BORDER=0 ALT="Fet">'
#
#    #NOM I COGNOMS i DETALL
#    taula_proves = {}
#    #rows_duplicats = copy.copy(proves_queryset_iterator)   No es pot ducplicar un iterador¿?
#    taula_detall = []
#    #radioleg_nom_cognoms = 'Desconegut'
#    
#    #i = len(proves_queryset) 263 i nomes fa el bucle de 100 !!!!!!!!!!!!!!!!!1
#    # els querysets tenen un problema de cache i es queden nomes amb 100 registres -> !!!!!!!
#    # http://jeffelmore.org/2010/09/25/smarter-caching-of-django-querysets/#comment-100
#    # es mes eficient possar-ho en una named-tuple 
#    #    taula_detall_item = namedtuple('Detall', 'id historia_clinica_id tipus_modalitat agenda__agenda_idi__obs prestacio_id prestacio__descripcio metge_informa metge_revisa metge_allibera metge_informa__cognoms_nom metge_revisa__cognoms_nom metge_allibera__cognoms_nom codificacio_generica__codi data_cita hora_cita data_document_informat hora_document_informat data_document_revisat hora_document_revisat')
#
#    for prova in proves_queryset_iterator:
#        
#        taula_proves[prova['id']] = copy.copy(init_object)
#
#        if prova['metge_informa'] == long(metge_id):
#            metge_informa = icona_oky 
#        else: 
#            metge_informa =  prova['metge_informa__cognoms_nom']
#
#        if prova['metge_revisa'] == long(metge_id):
#            metge_revisa = icona_oky 
#        else: 
#            metge_revisa =  prova['metge_revisa__cognoms_nom']
#        if prova['metge_allibera'] == long(metge_id):
#            metge_allibera = icona_oky 
#        else: 
#            metge_allibera =  prova['metge_allibera__cognoms_nom']
#            
#        taula_proves[prova['id']][1]  = prova
#        #if (radioleg_nom_cognoms == '' and prova['metge_informa__cognoms_nom'] !=None ):
#        #    radioleg_nom_cognoms = prova['metge_informa__cognoms_nom']
#
#        if prova['data_cita'] == None  or  prova['hora_cita'] == None: 
#            data_i_hora_cita= '-'
#        else:  
#            data_i_hora_cita =  datetime.datetime(prova['data_cita'].year, prova['data_cita'].month, prova['data_cita'].day, prova['hora_cita'].hour, prova['hora_cita'].minute, prova['hora_cita'].second).strftime('%Y/%m/%d %H:%M')
#        if prova['data_document_informat'] == None  or  prova['hora_document_informat'] == None:
#            data_i_hora_doc_inf = '-'
#        else:
#            data_i_hora_doc_inf = datetime.datetime(prova['data_document_informat' ].year, prova['data_document_informat'].month, prova['data_document_informat'].day, prova['hora_document_informat'].hour, prova['hora_document_informat'].minute, prova['hora_document_informat'].second).strftime('%Y/%m/%d %H:%M')
#        if prova['data_document_revisat'] == None  or  prova['hora_document_revisat'] == None:
#            data_i_hora_doc_rev = '-'      
#        else:
#            data_i_hora_doc_rev = datetime.datetime(prova['data_document_revisat'].year, prova['data_document_revisat'].month, prova['data_document_revisat'].day, prova['hora_document_revisat'].hour, prova['hora_document_revisat'].minute, prova['hora_document_revisat'].second).strftime('%Y/%m/%d %H:%M')
#
#        # Ara buscarem totes aquelles proves que tenen codificacio generica per posar la codificacio inicial i la final i si esta validat o no i per qui
#        codgenerica_queryset = ProvaOhdimCodificacioGenerica.objects.using('portal_dades').filter(prova_ohdim__exact= prova['id'])#.values('codificacio_generica_inicial','codificacio_generica_final')
#        # en prinicpi nomes pot ser un pero... retorna un queryset
#        codificacio_inicial= ''
#        codificacio_final= ''
#        validat = ''
#        #pasem a la vista de modificar el pk de provaohdim per si no hi ha codificaciogenirica ( descuid del radioleg)
#        modificar = "<a href='../activitat-total-detall-modificar/%s&%s' class='changelink'></a>" % (prova['id'],'0')
#     
#        for codgen in codgenerica_queryset:
#            #ls pk de prevaohdim_codificaio_geenrica   aixi el paser en un link
#            modificar="<a href='../activitat-total-detall-modificar/%s&%s' class='changelink'></a>" % ( prova['id'],codgen.id)
#    
#            try:
#                codificacio_inicial = str(codgen.codificacio_generica_inicial)
#                if codificacio_inicial=='None' :codificacio_inicial =''
#            except:
#                codificacio_inicial = '' 
#            try:
#                codificacio_final = str(codgen.codificacio_generica_final)
#                if codificacio_final=='None' :codificacio_final =''
#            except:
#                codificacio_final = ''
#            if codgen.validat:
#                validat =  icona_oky                
#                modificar = ''
#            
#            
#        duplicat_trobat=None
#        #Les proves consecuteives  les marqeum amb marrro!!! 
##        for row_duplicats in rows_duplicats:
##            if (prova['historia_clinica_id'] == row_duplicats['historia_clinica_id'] ) and ( prova['data_cita'] == row_duplicats['data_cita']) and (prova['prestacio_id'] != row_duplicats['prestacio_id']):
##                duplicat_trobat='style=color:brown;'
#                
#        taula_detall.append((prova['id'],( prova['historia_clinica_id'],
#                                           prova['agenda__agenda_idi__obs'],
#                                           prova['prestacio__descripcio'] ,
#                                           metge_informa , 
#                                           metge_revisa, 
#                                           metge_allibera,
#                                           codificacio_inicial, 
#                                           codificacio_final,
#                                           data_i_hora_cita,
#                                           data_i_hora_doc_inf,
#                                           data_i_hora_doc_rev,
#                                           validat,
#                                           modificar
#                                           ), duplicat_trobat 
#                             )
#                            )
##    # if request.GET.get('export','none') == "excel":   
#
#    # grafics
#    new_table = []
#    for row in taula_detall:
#        new_table.append({'pk':row[0], 
#                          'hc':row[1][0],
#                          'agenda':row[1][1], 
#                          'exploracio':row[1][2],
#                          'informat':row[1][3], 
#                          'revisat':row[1][4], 
#                          'alliberat':row[1][5], 
#                          'codificacio_inicial':row[1][6],
#                          'codificacio_final':row[1][7],
#                          'data_exploracio':row[1][8],
#                          'data_informe':row[1][9],
#                          'data_revisio':row[1][10],
#                          'validat':row[1][11],
#                          'codgen_id':row[1][12]                           
#                          })
#    table = DetallRadiolegsTable(new_table, order_by=request.GET.get('sort'))
#    #table.paginate(page=request.GET.get('page', 1))
#    
#    
#    taula_detall = table
#    nregistres= str(len(new_table))
#    
#    grafics = [
#              
#               {'tipus': "taula2", 
#                'tamany': 100,                
#                'titol': "Detall d'informes ordinaris realitzats/revisats/alliberats per: " + radioleg_nom_cognoms  + ".    Informes: " + nregistres,
#                'categories': [ 'Història clínica','Exploració', 'Agenda','Informat', 'Revisat', 'Alliberat', 'Codificació', 'Data exploració', 'Data informe' ,'Data revisió' ,'validat'],
#                'series': taula_detall},
#                ]
#
#    auditoria.request_handler(request,8,'Tipus consulta: activitatOrdinariaDetall ; ' + str(request.session['enabled_filters']) + ' ,metge_id:' + str(metge_id) )
#    return render_to_response('quadre_comandament/indicador.html',
#                              {'indicadors':get_indicadors(request),
#                               'grafics':grafics,
#                               'objectiu':objectiu,
#                               'objectiu_url':objectiu_url},
#                              context_instance=RequestContext(request)) 
#    
#    
#    





    
    
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatTotal(request):
    
    return activitatProcedure(request, get_indicadors(request),tipus_consulta = 'total')
   
   

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatTotalDetallSeram(request, metge_id):
    
    return activitatDetallSeramProcedure(request, metge_id,get_indicadors(request),es_seguiment_personal = False,tipus_consulta='total')
   
   

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatOrdinaria(request):
    
    return activitatProcedure(request, get_indicadors(request),tipus_consulta = 'ordinaria')

      
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatOrdinariaDetallSeram(request,metge_id):
    
    return activitatDetallSeramProcedure(request,metge_id, get_indicadors(request),es_seguiment_personal = False,tipus_consulta='ordinaria')



def activitatProcedure(request, indicadors_var, tipus_consulta):

    
    #usuari = AuthUser.objects.get(username =str(request.user))
    #es_informatic = AuthUserProfile.objects.get( user = usuari.id).categoria_profesional.id in (15,17,21,36)
    es_informatic = True
    

    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'radiolegs_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': int(time.strftime("%Y", time.gmtime())), 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    
    anys = request.session['enabled_filters']['any']
    mes = request.session['enabled_filters']['mes']
    unitatsel = request.session['enabled_filters']['unitat']
    modalitatsel = request.session['enabled_filters']['modalitat']
    
    if anys == 0:# quan es sel·lecionen tots els anys
        for filter_ in request.session['filters']:
            if filter_['nom'] == "any":
                any_primer = filter_['valors'][0][0]
                any_ultim = filter_['valors'][-1][0]
            else:
                any_primer = 2000
                any_ultim = 2800
        if mes == 0:
            start_date = datetime.date(int(any_primer), 1, 1)
            end_date = datetime.date(int(any_ultim), 12, 31)
        else: #tots els anys fins els mes sellecionat d'enguany!!
            dies_mes = calendar.monthrange(any_ultim, mes)
            start_date = datetime.date(any_primer, 1, 1)
            end_date = datetime.date(any_ultim, mes, dies_mes[1])
    else:
        if mes == 0:
                start_date = datetime.date(int(anys), 1, 1)
                end_date = datetime.date(int(anys), 12, 31) 
        else:
            dies_mes = calendar.monthrange(anys, mes)
            start_date = datetime.date(anys, mes, 1)
            end_date = datetime.date(anys, mes, dies_mes[1])

    # Definim querys per defecte
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    
                                                                        # no volem agenda ICS pero !!! si volem els metges que no tenen ralcio ( null )
    master_queryset = ProvaOhdim.objects.using('portal_dades').exclude( 
                                                                        Q(agenda__agenda_idi__es_ics=True ) & (
                                                                        Q(metge_informa__es_idi=False,  metge_informa__es_idi__isnull=False ) |
                                                                        Q(metge_revisa__es_idi=False,   metge_revisa__es_idi__isnull=False  ) |  
                                                                        Q(metge_allibera__es_idi=False, metge_allibera__es_idi__isnull=False)
                                                                        )
                                                                         # NO volem els metges que son del ICS i que la agenda es del ICS!!!  ( LA Q es totalment necessaria sino peta ( fa la join i li canvia el nom a la taula i despres alferla sevir peta))
                                                                       ).exclude (Q(agenda__agenda_idi__es_ics=True , metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True))
                                                        

    # AIXO FUNCIONA SI ES POSA EN LA MYSQL !!!!! NO HI HA UNAKLTRE FORAMA QUE CHUTI ( el Django no transalda bé la consulta !!!! unica sollucio es amb exclude )
    # AND  NOT ( portal_dades.agenda_idi.es_ics  AND  m_informa.id is Null  AND m_revisa.id is Null AND m_allibera.id is Null )
    # master_queryset = master_queryset.exclude(  agenda__agenda_idi__es_ics = True ,  metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True )
    #cada vegada que es fa una assignacio fa una consulta a la mysql --> posa un "limit 21" imagino que per comprovar que funcionen els filtres 
    
    #volem les prestacions entre el rang de dades, i que estiguin concloses 
    master_queryset =   master_queryset.filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
    
    if tipus_consulta == 'ordinaria':
        # Ens quedem els que o bé no tenen realcio amb prova_ohdim_codificacio_generica o bé tenen un null possat perque al validar-lo es va treure el valor
        master_queryset = master_queryset.exclude( provaohdimcodificaciogenerica__codificacio_generica_final__in=[7,8])

    
    
    if unitatsel != 0 and modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
    elif unitatsel!= 0:
        master_queryset = master_queryset.filter( agenda__agenda_idi__unitat_idi_id=unitatsel) 
            
    elif modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
    else:   
        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                          agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))

    import copy
    init_object = [0,0,0,0,0,0,0,0,0,0]
    
    taula_series2_tmp = {}

    queryset = master_queryset.values('metge_informa' ,'metge_informa__cognoms_nom','metge_informa__dni' ).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')

    #NOM I COGNOMS i DETALL
    for prova in queryset:
        if  prova['metge_informa__cognoms_nom'] == None :
            try:
                if taula_series2_tmp[0] == None:
                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: red;'>Informes amb errors</span>"
                    taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../activitat-"+ tipus_consulta +"-detall/0' class='addlink'></a>"
                    if es_informatic:
                        taula_series2_tmp[prova['metge_informa']][8]  = "<a href='../activitat-total-"+ tipus_consulta +"-seram/0' class='addlink'></a>"
                    else:
                        taula_series2_tmp[prova['metge_informa']][8]  = ""
                    
            except:
                taula_series2_tmp[0] = copy.copy(init_object)
                taula_series2_tmp[0][0]  = "<span style='color: red;'>Informes amb errors</span>"
                taula_series2_tmp[0][7]  = "<a href='../activitat-"+ tipus_consulta +"-detall/0' class='addlink'></a>"
                if es_informatic:
                    taula_series2_tmp[0][8]  = "<a href='../activitat-"+ tipus_consulta +"-detall-seram/0' class='addlink'></a>"
                else:
                    taula_series2_tmp[0][8]  = ""
  

        else:
            
            taula_series2_tmp[prova['metge_informa']] = copy.copy(init_object)
            
            try:
                user = AuthUser.objects.get(username =  prova['metge_informa__dni'][:8] )
                if AuthUserProfile.objects.get( user = user).categoria_profesional.id in [4,5,6,26,27,30,31,]:
                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: #31B404;'>" +  prova['metge_informa__cognoms_nom']+ "</span>"
                else:#sin no es facultatiu o cap d¡untiat , es una scre o un tecnic i es un error
                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: #B7A800;'>" +  prova['metge_informa__cognoms_nom']+ "</span>"
            except AuthUser.DoesNotExist:
                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: #B85F04;'>" + prova['metge_informa__cognoms_nom'] + "</span>"
            except  AuthUserProfile.DoesNotExist:                
                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: #B85F04;'>" + prova['metge_informa__cognoms_nom'] + "</span>"
            
            taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../activitat-"+ tipus_consulta +"-detall/%s' class='addlink'></a>" % prova['metge_informa']
            if es_informatic:
                taula_series2_tmp[prova['metge_informa']][8]  = "<a href='../activitat-"+ tipus_consulta +"-detall-seram/%s' class='addlink'></a>" % prova['metge_informa']
            else:
                taula_series2_tmp[prova['metge_informa']][8]  = ""
                
    # INFORMES CREATS
    queryset = master_queryset.values('metge_informa','metge_informa__cognoms_nom',).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')
    for prova in queryset:
        if  prova['metge_informa__cognoms_nom'] == None :
            taula_series2_tmp[0][1] =  taula_series2_tmp[0][1] + prova['informes_creats'] # s'ha de su
        else:
            taula_series2_tmp[prova['metge_informa']][1] = prova['informes_creats']
    # INFORMES REVISATS
    queryset = master_queryset.values('metge_revisa','metge_revisa__cognoms_nom','metge_revisa__dni').annotate(informes_revisats=Count('id')).order_by('metge_revisa__cognoms_nom')
    for prova in queryset:
        try:      
            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
        except KeyError:# no ha informat cap informe pero si revisat  o alliberat ?? els afegim , si te nom 
            
            if  prova['metge_revisa__cognoms_nom'] == None :
                taula_series2_tmp[0][2] =  taula_series2_tmp[0][2] + prova['informes_revisats'] # s'hafegeixen els revistas al INFORMES DLATRES METGES
            else:  
                taula_series2_tmp[prova['metge_revisa']] = copy.copy(init_object) # PETA que no hi ha el metge que informa === a el afegim aqui
                #taula_series2_tmp[prova['metge_revisa']][0]  = prova['metge_revisa__cognoms_nom']
            
                try:
                    if  prova['metge_revisa__dni'] != None:
                        user = AuthUser.objects.get(username =  prova['metge_revisa__dni'][:8] )
                        if AuthUserProfile.objects.get( user = user).categoria_profesional.id in [4,5,6,26,27,30,31,]:
                            taula_series2_tmp[prova['metge_revisa']][0]  = "<span style='color: #31B404;'>" +  prova['metge_revisa__cognoms_nom']+ "</span>"
                        else:#sin no es facultatiu o cap d¡untiat , es una scre o un tecnic i es un error
                            taula_series2_tmp[prova['metge_revisa']][0]  = "<span style='color: #B7A800;'>" +  prova['metge_revisa__cognoms_nom']+ "</span>"
                    else:
                        taula_series2_tmp[prova['metge_revisa']][0]  = "<span style='color: #B85F04;'>" +  '- ERROR SAP no tenim usuari - '+ "</span>"
                except AuthUser.DoesNotExist:
                    taula_series2_tmp[prova['metge_revisa']][0]  = "<span style='color: #B85F04;'>" + prova['metge_revisa__cognoms_nom'] + "</span>"

                taula_series2_tmp[prova['metge_revisa']][1] = 0
                taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
                taula_series2_tmp[prova['metge_revisa']][7]  = "<a href='../activitat-"+ tipus_consulta +"-detall/%s' class='addlink'></a>" % prova['metge_revisa']
                if es_informatic:
                    taula_series2_tmp[prova['metge_revisa']][8]  = "<a href='../activitat-"+ tipus_consulta +"-detall-seram/%s' class='addlink'></a>" % prova['metge_revisa']
                else:
                    taula_series2_tmp[prova['metge_revisa']][8]  = ""
            
    # INFORMES ALLIBERATS
    queryset = master_queryset.values('metge_allibera','metge_allibera__cognoms_nom','metge_allibera__dni').annotate(informes_alliberats=Count('id')).order_by('metge_allibera__cognoms_nom')   
    for prova in queryset:
        try:
            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
        except KeyError:

            if  prova['metge_allibera__cognoms_nom'] == None :
                taula_series2_tmp[0][3] =  taula_series2_tmp[0][3] + prova['informes_alliberats'] # s'hafegeixen els allibertas al "INFORMES DLATRES METGES"
            else:  
                taula_series2_tmp[prova['metge_allibera']] = copy.copy(init_object)
                try:  
                    if  prova['metge_allibera__dni'] != None:
                        user = AuthUser.objects.get(username =  prova['metge_allibera__dni'][:8] )# si no existeix raise DoesNotExist
                        if AuthUserProfile.objects.get( user = user).categoria_profesional.id in [4,5,6,26,27,30,31,]:#facultatius adjunts o caps de unitat
                            taula_series2_tmp[prova['metge_allibera']][0]  = "<span style='color: #31B404;'>" +  prova['metge_allibera__cognoms_nom']+ "</span>"
                        else:#sin no es facultatiu o cap d¡untiat , es una scre o un tecnic i es un error segur perque es del idi pero no es facultatiu
                            taula_series2_tmp[prova['metge_allibera']][0]  = "<span style='color: #B7A800;'>" +  prova['metge_allibera__cognoms_nom']+ "</span>"
                    else:# notenim el dni de l'ususari --> a vegades no tenim l'usuari directament
                        taula_series2_tmp[prova['metge_allibera']][0]  = "<span style='color: #B85F04;'>" +  '- ERROR SAP no tenim usuari - '+ "</span>"
                except AuthUser.DoesNotExist:# no estan donats dalta al portal == a resis o errors de sap
                    taula_series2_tmp[prova['metge_allibera']][0]  = "<span style='color: #B85F04;'>" + prova['metge_allibera__cognoms_nom'] + "</span>"
           
            
                taula_series2_tmp[prova['metge_allibera']][1] = 0
                taula_series2_tmp[prova['metge_allibera']][2] = 0
                taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
                taula_series2_tmp[prova['metge_allibera']][7]  = "<a href='../activitat-"+ tipus_consulta +"-detall/%s' class='addlink'></a>" % prova['metge_allibera']
                if es_informatic:
                    taula_series2_tmp[prova['metge_allibera']][8]  = "<a href='../activitat-"+ tipus_consulta +"-detall-seram/%s' class='addlink'></a>" % prova['metge_allibera']
                else:
                    taula_series2_tmp[prova['metge_allibera']][8]  = ""
        
        
    # INFORMES ACI
    queryset = master_queryset.filter(provaohdimcodificaciogenerica__codificacio_generica_final=7,provaohdimcodificaciogenerica__validat=True ).values('metge_allibera').annotate(informes_alliberats=Count('id'))
    for prova in queryset:       
        taula_series2_tmp[prova['metge_allibera']][4] = prova['informes_alliberats']
    # INFORMES ACH
    queryset = master_queryset.filter(provaohdimcodificaciogenerica__codificacio_generica_final=8,provaohdimcodificaciogenerica__validat=True).values('metge_allibera').annotate(informes_alliberats=Count('id'))
    for prova in queryset:           
        taula_series2_tmp[prova['metge_allibera']][5] = prova['informes_alliberats']
        
    # SERAM 
    #TODO: alguna cosa hem de fer per comprovar aquelles proves que no tenen codi seram , o assegurar-nos que totes les prestacions en tenen...
    #nomes aquells que ha informat i ha alliberat el mateix radioleg
    #queryset = master_queryset.values('metge_informa').filter(  metge_allibera = F('metge_informa')).annotate(temps_seram=Sum('prestacio__codi_seram__temps_metge')).order_by('metge_allibera__cognoms_nom')
    
   
    factor_correctiu = 5 #Per tal que el valor del rendiment s'apropi mes a la realitat li summem a tot un valor
    
    #Tot els informes que crea el facultatiu i que revisa ell amteix -> el 100% del seu valor SERAM
    queryset = master_queryset.values('metge_revisa').filter( metge_informa= F('metge_revisa')).annotate(temps_seram=Sum('prestacio__codi_seram__temps_metge'), recomte=Count('prestacio__codi_seram__temps_metge')).order_by('metge_revisa__cognoms_nom')
    
   
  
    for prova in queryset:
        if prova['temps_seram'] == None:
            try:
                taula_series2_tmp[prova['metge_revisa']][6] = 0
            except KeyError:
                pass#tan se val sumarem zero als erronis == 0
        else:
            if prova['metge_revisa'] == None:
                taula_series2_tmp[0][6] =  taula_series2_tmp[0][6] + prova['temps_seram']
            else:
                #enspodem trobar que no te nom i cognoms , es el [0] llavors
                try:
                    taula_series2_tmp[prova['metge_revisa']][6] =  str(( prova['temps_seram'] + ( factor_correctiu *prova['recomte']) ))  
                    #  % de jornada realitzada del total teóric segons torn diurn 1666 hores * , per a tots  
                    # taula_series2_tmp[prova['metge_revisa']][6] = (( prova['temps_seram'] + ( factor_correctiu *prova['recomte']) ) / 97920) * 100 
                     
                except KeyError:
                    taula_series2_tmp[0][6] =  taula_series2_tmp[0][6] + prova['temps_seram'] 
    
    #hara sumem el 25% dels revisats i que no ha informat el mateix facultatiu 
    #el resi crea i un adjunt allibera (i son diferents facultatius)
#    queryset = master_queryset.values('metge_revisa').exclude( metge_informa= F('metge_revisa')).annotate( temps_seram=Sum('prestacio__codi_seram__temps_metge') ).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
#
#        if prova['temps_seram'] <> None:
#
#            try:
#                #taula_series2_tmp[prova['metge_revisa']][6] = taula_series2_tmp[prova['metge_revisa']][6] + (25*int(prova['temps_seram'])/100)
#             
#                #Per possar el % de jornada realitzada del total teóric segons torn diurn 1666 hores * , per a tots
#                taula_series2_tmp[prova['metge_revisa']][6] = taula_series2_tmp[prova['metge_revisa']][6] + (((25*int(prova['temps_seram'])/100)) / 97920 ) * 100 
#                
#            except KeyError:
#                taula_series2_tmp[0][6] =  taula_series2_tmp[0][6] + (25*int(prova['temps_seram'])/100)
#                #if int(prova['metge_informa']) == 18001:
#                #    print(str(prova['metge_informa']) + ':  '  +  str((25*int(prova['temps_seram'])/100) ))

    
##                    
##    #ARa nomes queden els  errors ->
#    #-> El que crea es un resi i el que revisa es un adjunt , però no ... el allibera el resi!!! , COM que aqui ja estan inclossos els de dalt comentem la part de dalt fins que SAP no prïvieixi queels resis tanquin informes 
#    queryset = master_queryset.values('metge_revisa').exclude(Q(metge_revisa = F('metge_informa')) & Q(metge_revisa = F('metge_allibera'))).annotate( temps_seram=Sum('prestacio__codi_seram__temps_metge') ).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
#        if not (prova['temps_seram'] == None):
#            if prova['metge_revisa'] == None:
#                tmp = None 
#            else: 
#                taula_series2_tmp[prova['metge_revisa']][6] = taula_series2_tmp[prova['metge_revisa']][6] + (25*int(prova['temps_seram'])/100)
#   


    # URVs   SERAM 
    #TODO: alguna cosa hem de fer per comprovar aquelles proves que no tenen codi seram , o assegurar-nos que totes les prestacions en tenen...
    
    #Tot els informes que crea el facultatiu que revisen el 100% del seu valor SERAM
    queryset = master_queryset.values('metge_revisa').annotate(urv_a=Sum('prestacio__codi_seram__urv_a')).order_by('metge_revisa__cognoms_nom')
  
    for prova in queryset:
        if prova['urv_a'] == None:
            try:
                taula_series2_tmp[prova['metge_revisa']][9] = 0
            except KeyError:
                tmp = None
        else:
            if prova['metge_revisa'] == None:
                taula_series2_tmp[0][9] =  taula_series2_tmp[0][9] + prova['urv_a']
            else:
                #enspodem trobar que no te nom i cognoms , es el [0] llavors
                try:
                    taula_series2_tmp[prova['metge_revisa']][9] = prova['urv_a']
                except KeyError:
                    taula_series2_tmp[0][9] =  taula_series2_tmp[0][9] + prova['urv_a']

    
#    #hara sumem el 25% dels revisats 
#    #el resi crea i un adjunt allibera (i son diferents facultatius)
#    queryset = master_queryset.values('metge_revisa').exclude( metge_informa= F('metge_revisa')).annotate( urv_a=Sum('prestacio__codi_seram__urv_a') ).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
#        if prova['urv_a'] <> None:
#            try:
#                taula_series2_tmp[prova['metge_revisa']][9] =  float(taula_series2_tmp[prova['metge_revisa']][9]) + (25*float(prova['urv_a'])/100)
#            except KeyError:
#                    taula_series2_tmp[0][9] =  float(taula_series2_tmp[0][9]) + (25*float(prova['urv_a'])/100)



                    
##    #ARa nomes queden els  errors ->
#    #-> El que crea es un resi i el que revisa es un adjunt , però no ... el allibera el resi!!! , COM que aqui ja estan inclossos els de dalt comentem la part de dalt fins que SAP no prïvieixi queels resis tanquin informes 
#    queryset = master_queryset.values('metge_revisa').exclude(Q(metge_revisa = F('metge_informa')) & Q(metge_revisa = F('metge_allibera'))).annotate( temps_seram=Sum('prestacio__codi_seram__temps_metge') ).order_by('metge_revisa__cognoms_nom')
#    for prova in queryset:
#        if not (prova['temps_seram'] == None):
#            if prova['metge_revisa'] == None:
#                tmp = None 
#            else: 
#                taula_series2_tmp[prova['metge_revisa']][6] = taula_series2_tmp[prova['metge_revisa']][6] + (25*int(prova['temps_seram'])/100)
#


    # Taula html - sort & filters
    taula_series2 = [] #creem una tupla de metge i les dades
    
    
    inf_alres_m = [0,0,0,0,0,0,0,0,0,0]
    dades_metge_tmp=[0,0,0,0,0,0,0,0,0,0]
    #Sumem la moralla , els que no estan a la taula metge i els metge que son null en el provaohdim
    for dades_metge in taula_series2_tmp.items():
        if dades_metge[1][0] == None or dades_metge[1][0] == 0:
            if dades_metge[1][1] == None: dades_metge_tmp[0]= 0
            else:dades_metge_tmp[0] = dades_metge[1][1] 
            if dades_metge[1][2] == None: dades_metge_tmp[1]=0
            else:dades_metge_tmp[1] = dades_metge[1][2]
            if dades_metge[1][3] == None:  dades_metge_tmp[2]=0
            else:dades_metge_tmp[2] = dades_metge[1][3]
            if dades_metge[1][4] == None: dades_metge_tmp[3]=0
            else:dades_metge_tmp[3] = dades_metge[1][4]
            if dades_metge[1][5] == None:  dades_metge_tmp[4]=0
            else:dades_metge_tmp[4] = dades_metge[1][5]
            if dades_metge[1][6] == None: dades_metge_tmp[5]=0
            else:dades_metge_tmp[5] = dades_metge[1][6]
                
            inf_alres_m = [ inf_alres_m[0],
                            inf_alres_m[1] + int(dades_metge_tmp[0]) ,
                            inf_alres_m[2] + int(dades_metge_tmp[1]),
                            inf_alres_m[3] + int(dades_metge_tmp[2]),
                            inf_alres_m[4] + int(dades_metge_tmp[3]),
                            inf_alres_m[5] + int(dades_metge_tmp[4]),
                            inf_alres_m[6] + int(dades_metge_tmp[5]),
                            inf_alres_m[7], 
                            inf_alres_m[8],
                            inf_alres_m[9],
                             ]
        else:
            if dades_metge[0]== 0:
                inf_alres_m =  dades_metge[1]
            else:
                taula_series2.append((dades_metge[1][0], dades_metge[1]))
                
    if inf_alres_m[0] != 0:#afegim la suma dels que notenen nom i errors
        taula_series2.append((inf_alres_m[0], inf_alres_m))
        
        
    taula_series2 = sorted(taula_series2, key=lambda row: row[1][0])
    new_table = []
    for serie in taula_series2:
        new_table.append({'radioleg':serie[0], 'informes_creats':serie[1][1], 'informes_revisats':serie[1][2], 'informes_alliberats':serie[1][3], 'aci':serie[1][4],'ach':serie[1][5] ,'temps_seram':serie[1][6], 'urv_a':serie[1][9] ,'detall':serie[1][7],'detall_seram':serie[1][8]})
    table = RadiolegsTable(new_table, order_by=request.GET.get('sort'), exclude=('aci','ach'))
    #table.paginate(page=request.GET.get('page', 1))
    taula_series = table

    # if request.GET.get('export','none') == "excel":   
    # grafics
    grafics = [
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Resum informes  "+ tipus_consulta +" per radiòleg",
                'categories': [ 'Creats', 'Revisats', 'Alliberats','aci','ach','temps_seram','urv_a', 'Detall','Detall_Seram'],
                'series': taula_series},              
                ]
    
        # Definició
    definicio = """
        Suma de l'activitat """+ tipus_consulta +""" informada, revisada i alliberada pels radiòlegs al centre seleccionat. 
        """ 
    if tipus_consulta == 'total':
        definicio = definicio + "En l'activitat, es tenen en compte les modificacions en l'activitat extraordinària.</br></br>"
        definicio = definicio + "Sumem 5 min. a totes les prestacions per tal que el valor sigui mes real.</br></br>"
    
    definicio = definicio +  """En cas de triar "Tots", apareix l'activitat de totes les unitats que el teu usuari té assignades.</br></br>
        Les dates de tall són per data d'informe alliberat.</br></br>
        El rendiment és la suma de temps informe (definits per la SERAM) de les prestacions informades (revisat_per ) pel radiòleg a la unitat.
        <p> </p>
        <a>
            <img src="/static/media/img/quadre_comandament/facultatius/taula_02_detall_colors.gif" alt="Definició colors" align ="middle"  />
        </a>
        """
    auditoria.request_handler(request,8,'Tipus consulta: activitat '+ tipus_consulta +' ; ' + str(request.session['enabled_filters']) + ' ')
        
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))      


def activitatDetallSeramProcedure(request, metge_ids,indicadors_var,es_seguiment_personal,tipus_consulta):

    metge_id = []
    if type(metge_ids)==unicode:
        metge_id.append(metge_ids)
    else:
        metge_id = metge_ids
    
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'radiolegs_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    anys = request.session['radiolegs_filters']['any']
    mes = request.session['radiolegs_filters']['mes']
    unitatsel = request.session['radiolegs_filters']['unitat']
    modalitatsel = request.session['radiolegs_filters']['modalitat']

    if anys == 0:# quan es sel·lecionen tots els anys
        for filter_ in request.session['filters']:
            if filter_['nom'] == "any":
                any_primer = filter_['valors'][0][0]
                any_ultim = filter_['valors'][-1][0]
            else:
                any_primer = 2000
                any_ultim = 2800
        if mes == 0:
            start_date = datetime.date(int(any_primer), 1, 1)
            end_date = datetime.date(int(any_ultim), 12, 31)
        else: #tots els anys fins els mes sellecionat d'enguany!!
            dies_mes = calendar.monthrange(any_ultim, mes)
            start_date = datetime.date(any_primer, 1, 1)
            end_date = datetime.date(any_ultim, mes, dies_mes[1])
    else:
        if mes == 0:
                start_date = datetime.date(int(anys), 1, 1)
                end_date = datetime.date(int(anys), 12, 31) 
        else:
            dies_mes = calendar.monthrange(anys, mes)
            start_date = datetime.date(anys, mes, 1)
            end_date = datetime.date(anys, mes, dies_mes[1])

  
    #TODO: AIXO potser S?HA DE TREURE PERQUE POTSER HI HAN DIGANOSTICAT INFORMES DALTRES CENTRES i els volem veure ??? -> per a els metges que consulten la seves propies prestacions s'ignora 
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))


    if metge_id == '0':
        radioleg_nom_cognoms = "Informes amb errors"
        master_queryset = ProvaOhdim.objects.using('portal_dades').filter( agenda__agenda_idi__es_ics=False
                                                                        ).filter( Q(metge_informa__isnull=True) |
                                                                                  Q(metge_revisa__isnull=True) |
                                                                                  Q(metge_allibera__isnull=True)
                                                                         ).filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
    else:
        
        
        radioleg_nom_cognoms = Metge.objects.using('portal_dades').get(pk=long(metge_id[0])).cognoms_nom
        
        
        master_queryset = ProvaOhdim.objects.using('portal_dades').filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
        
        query_tmp = Q(metge_informa = -1) 
        
        for metge_id_item in metge_id:
            query_tmp = (query_tmp |  ( Q(metge_informa=metge_id_item) | Q(metge_revisa=metge_id_item) |  Q(metge_allibera=metge_id_item) ))
        
        master_queryset = master_queryset.filter(query_tmp)                                                               
                                                                       
                                                                          
    if unitatsel != 0 and modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                                 agenda__agenda_idi__idtipoexploracion=modalitatsel)
    elif unitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel)  
            
    elif modalitatsel != 0:
        if es_seguiment_personal:
            master_queryset = master_queryset.filter( agenda__agenda_idi__idtipoexploracion=modalitatsel)
        else:
            master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                                 agenda__agenda_idi__idtipoexploracion=modalitatsel)

    else:   
        
        if es_seguiment_personal:
            master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False)
        else:
            master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                                 agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))
     
    if tipus_consulta == 'ordinaria':
        # Ens quedem els que o bé no tenen realcio amb prova_ohdim_codificacio_generica o bé tenen un null possat perque al validar-lo es va treure el valor
        master_queryset = master_queryset.exclude( provaohdimcodificaciogenerica__codificacio_generica_final__in=[7,8])

                              
    proves_queryset_iterator = master_queryset.values( 'id', 
                                       'historia_clinica_id',
                                       'tipus_modalitat', 
                                       'agenda__agenda_idi__obs',
                                       'prestacio_id',
                                       'prestacio__codi',
                                       'prestacio__codi_seram__temps_metge',
                                       'prestacio__codi_seram__urv_a',
                                       'prestacio__descripcio',
                                       'prestacio__zona_anatomica_idi__descripcio', 
                                       'metge_informa','metge_revisa','metge_allibera',
                                       'metge_informa__cognoms_nom','metge_revisa__cognoms_nom','metge_allibera__cognoms_nom', 
                                       'codificacio_generica__codi',
                                       'data_cita',
                                       'hora_cita',
                                       'data_document_informat',
                                       'hora_document_informat',
                                       'data_document_revisat',
                                       'hora_document_revisat',
                                       'tipus_episodi'
                                       ).order_by('tipus_modalitat', 'agenda', 'historia_clinica_id', 'data_cita', 'hora_cita').iterator()

    import copy
    init_object = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    icona_oky= '<IMG SRC="/static/admin/img/admin/icon_success.gif" BORDER=0 ALT="Fet">'
    icona_duplicat= '<IMG SRC="/static/media/img/quadre_comandament/facultatius/estudi.gif" BORDER=0 ALT="Estudi">'
    icona_duplicats = '<IMG SRC="/static/media/img/quadre_comandament/facultatius/estudis.gif" BORDER=0 ALT="Desgloçat">'
    #NOM I COGNOMS i DETALL
    taula_proves = {}
    #rows_duplicats = copy.copy(proves_queryset_iterator)   No es pot ducplicar un iterador¿?
    taula_detall = []
    
    #i = len(proves_queryset) 263 i nomes fa el bucle de 100 !!!!!!!!!!!!!!!!!1
    # els querysets tenen un problema de cache i es queden nomes amb 100 registres -> !!!!!!!
    # http://jeffelmore.org/2010/09/25/smarter-caching-of-django-querysets/#comment-100
    # es mes eficient possar-ho en una named-tuple 
    # taula_detall_item = namedtuple('Detall', 'id historia_clinica_id tipus_modalitat agenda__agenda_idi__obs prestacio_id prestacio__descripcio metge_informa metge_revisa metge_allibera metge_informa__cognoms_nom metge_revisa__cognoms_nom metge_allibera__cognoms_nom codificacio_generica__codi data_cita hora_cita data_document_informat hora_document_informat data_document_revisat hora_document_revisat')
 
    for prova in proves_queryset_iterator:
        
        taula_proves[prova['id']] = copy.copy(init_object)

        if prova['metge_informa'] in  metge_id:
            metge_informa = icona_oky 
        else: 
            metge_informa =  prova['metge_informa__cognoms_nom']

        if prova['metge_revisa'] in metge_id:
            metge_revisa = icona_oky 
        else: 
            metge_revisa =  prova['metge_revisa__cognoms_nom']
        if prova['metge_allibera'] in  metge_id:
            metge_allibera = icona_oky 
        else: 
            metge_allibera =  prova['metge_allibera__cognoms_nom']
            
        taula_proves[prova['id']][1]  = prova
#        if (radioleg_nom_cognoms == '' and prova['metge_informa__cognoms_nom'] !=None ):
#            radioleg_nom_cognoms = prova['metge_informa__cognoms_nom']
        if prova['data_cita'] == None  or  prova['hora_cita'] == None: 
            data_i_hora_cita= '-'
        else:  
            data_i_hora_cita =  datetime.datetime(prova['data_cita'].year, prova['data_cita'].month, prova['data_cita'].day, prova['hora_cita'].hour, prova['hora_cita'].minute, prova['hora_cita'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_document_informat'] == None  or  prova['hora_document_informat'] == None:
            data_i_hora_doc_inf = '-'
        else:
            data_i_hora_doc_inf = datetime.datetime(prova['data_document_informat' ].year, prova['data_document_informat'].month, prova['data_document_informat'].day, prova['hora_document_informat'].hour, prova['hora_document_informat'].minute, prova['hora_document_informat'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_document_revisat'] == None  or  prova['hora_document_revisat'] == None:
            data_i_hora_doc_rev = '-'      
        else:
            data_i_hora_doc_rev = datetime.datetime(prova['data_document_revisat'].year, prova['data_document_revisat'].month, prova['data_document_revisat'].day, prova['hora_document_revisat'].hour, prova['hora_document_revisat'].minute, prova['hora_document_revisat'].second).strftime('%Y/%m/%d %H:%M')

        # Ara buscarem totes aquelles proves que tenen codificacio generica per posar la codificacio inicial i la final i si esta validat o no i per qui
        codgenerica_queryset = ProvaOhdimCodificacioGenerica.objects.using('portal_dades').filter(prova_ohdim__exact= prova['id'])#.values('codificacio_generica_inicial','codificacio_generica_final')
        # en prinicpi nomes pot ser un pero... retorna un queryset
        codificacio_inicial= ''
        codificacio_final= ''
        validat = ''
        validat_data = ''
        #pasem a la vista de modificar el pk de provaohdim per si no hi ha codificaciogenirica ( descuid del radioleg)
        modificar = "<a href='../activitat-total-detall-modificar/%s&%s' class='changelink'></a>" % (prova['id'],'0')
        #busqyuem les cod generiques fixades per la prestacio
        for codgen in codgenerica_queryset:
            #ls pk de prevaohdim_codificaio_geenrica   aixi el paser en un link
            modificar="<a href='../activitat-total-detall-modificar/%s&%s' class='changelink'></a>" % ( prova['id'],codgen.id)
            try:
                codificacio_inicial = str(codgen.codificacio_generica_inicial)
                if codificacio_inicial=='None' :codificacio_inicial =''
            except:
                codificacio_inicial = '' 
            try:
                codificacio_final = str(codgen.codificacio_generica_final)
                if codificacio_final=='None' :codificacio_final =''
            except:
                codificacio_final = ''
            if codgen.validat:
                validat =  icona_oky
                modificar = ''
                validat_data = codgen.validat_data
            else:
                validat = ''
                validat_data = ''
        
        #SERAM  temps_metge for prova_rev in queryset_revisats:
        rendiment =  prova['prestacio__codi_seram__temps_metge']#
        if unicode(prova['metge_revisa']) not in metge_id:
            rendiment = 0
            
        if   (unicode(prova['metge_revisa']) in metge_id )  and ( prova['metge_informa'] != prova['metge_revisa'] ) :
            if not (prova['prestacio__codi_seram__temps_metge'] == None):
                rendiment =  str((25*long(prova['prestacio__codi_seram__temps_metge']) / 100)).replace(".", ",")
                
                
                
                
        #SERAM URVs  for prova_rev in queryset_revisats:
        urv_a =  prova['prestacio__codi_seram__urv_a']#
        if unicode(prova['metge_revisa']) not in metge_id:
            urv_a = 0
            
        if   (unicode(prova['metge_revisa']) in metge_id )  and ( prova['metge_informa'] != prova['metge_revisa'] ) :
            if not (prova['prestacio__codi_seram__urv_a'] == None):
                urv_a =  str((25*long(prova['prestacio__codi_seram__urv_a']) / 100)).replace(".", ",")
                 
             
        duplicat_trobat=False
        #Les proves consecuteives  les marqeum 
#        for row_duplicats in taula_detall:
#            if (prova['historia_clinica_id'] == row_duplicats[1][0] ) and ( data_i_hora_cita[0:10] == row_duplicats[1][8]) and (prova['prestacio__codi'] != row_duplicats[1][9]):
#                duplicat_trobat=True

                
#        if duplicat_trobat:
#            taula_detall.append((prova['id'],('','',
#                                           prova['prestacio__descripcio'] ,'','','',
#                                           codificacio_inicial, 
#                                           codificacio_final,
#                                           '',
#                                           prova['prestacio__codi'],
#                                           seram 
#                                           ), duplicat_trobat))
#        else:
                        
        taula_detall.append((prova['id'],( prova['historia_clinica_id'],
                                           prova['agenda__agenda_idi__obs'],
                                           prova['prestacio__descripcio'] ,
                                           metge_informa , 
                                           metge_revisa, 
                                           metge_allibera,
                                           codificacio_inicial, 
                                           codificacio_final,
                                           str(data_i_hora_cita),
                                           data_i_hora_doc_inf,
                                           data_i_hora_doc_rev,
                                           validat,
                                           validat_data,
                                           prova['prestacio__codi'],
                                           prova['tipus_episodi'],
                                           rendiment,
                                           prova['prestacio__zona_anatomica_idi__descripcio'],
                                           urv_a
                                           
                                           ), duplicat_trobat))
        



    # grafics
    new_table = []
    nAcis = 0
    nAchs = 0
    for row in taula_detall:
        new_table.append({'pk':row[0], 
                          'hc':row[1][0],
                          'agenda':row[1][1], 
                          'exploracio':row[1][2],
                          'informat':row[1][3], 
                          'revisat':row[1][4], 
                          'alliberat':row[1][5], 
                          'codificacio_inicial':row[1][6],
                          'codificacio_final':row[1][7],
                          'data_exploracio':row[1][8],
                          'data_informe':row[1][9],
                          'data_revisio':row[1][10],
                          'validat':row[1][11],
                          'validat_data':row[1][12],
                          'prestacio__codi':row[1][13],
                          'tipus_episodi':row[1][14],
                          'rendiment':row[1][15],
                          'prestacio__zona_anatomica': row[1][16],
                          'duplicat_trobat': row[2],
                          'urv_a':row[1][17],
                         
                                                   
                          })
        if str(row[1][7]) == 'ACI': 
            nAcis = nAcis +1
        else:
            if str(row[1][7]) == 'ACH':
                nAchs = nAchs +1        
        

    if es_seguiment_personal:# es la   part de seguiment-personal de cada facultatiu
        table = DetallRadiolegsTableSeram(new_table, order_by=request.GET.get('sort') ,
                                           exclude=('pk','codificacio_inicial','prestacio__codi', 'data_informe','tipus_episodi','duplicat_trobat') ,
                                           template='django_tables2/table_duplicats.html' )
        
    else:
        table = DetallRadiolegsTableSeram(new_table, order_by=request.GET.get('sort') ,
                                           exclude=('codificacio_inicial','data_informe','data_revisio','validat','validat_data','duplicat_trobat','prestacio_zona_anatomica') , 
                                            template='django_tables2/table_duplicats.html' )
    
    
    
    
    taula_detall = table
    nregistres= str(len(new_table))
    

    # Definició
    if es_seguiment_personal:#
        
        grafics = [
          
           {'tipus': "taula2", 
            'tamany': 100,                
            'titol': "Detall informes realitzats per: " + radioleg_nom_cognoms  + ". <BR>   Num. Informes: " + str(nregistres) + ", Num. ACI: " + str(nAcis) + ",  Num ACH: " + str(nAchs), 
           # 'categories': [ 'Història clínica','Exploració', 'Agenda','Informat', 'Revisat', 'Alliberat', 'Codificació', 'Data exploració', 'Data informe' ,'Data revisió' ,'validat','Modificar'],
            'series': taula_detall},
            ]
        
        definicio =  """
        Detall de tots els informes que ha creat, revisat i alliberat. Aquests informes apareixen al mes en què s'ha realitzat l'última versió de l'informe, és a dir, una exploració feta al maig i alliberada al juny, apareixerà comptabilitzada al mes de Juny.
        <br></br>
        Les columnes informat, revisat i alliberat mostren l'usuari SAP que consta que les ha creat, revisat i alliberat. En cas que sigui el mateix radiòleg, apareix un símbol verd, en cas de ser un altre radiòleg, apareix el nom d'aquest altre radiòleg.
        <br></br>
        Validat mostra si la prova és extraordinària i s'ha validat pel responsable. Totes les proves validades són les què el departament d'informàtica passa automàticament al departament de rrhh el dia 20 de cada mes.
        <br></br>
        El rendiment és la suma de temps SERAM de totes les proves que consten revisades. Les prestacions que son informades per un altre facultatiu sumen un 25% 
        """
        
        auditoria.request_handler(request,8,'Tipus consulta: Seguiement_faculatiu ; ' + str(request.session['enabled_filters']) + ' ,metge_id:' + str(metge_id[0]))
        
        objectiu_url = '../seguiment_personal'
        
    else:
        
        grafics = [
          
           {'tipus': "taula2", 
            'tamany': 100,                
            'titol': "Detall d'informes realitzats/revisats/alliberats per: " + radioleg_nom_cognoms, #  + ". <BR>  Nº Informes: " + nregistres + ", Nº ACI: " + str(nAcis) + ",  Nº ACH: " + str(nAchs), 
           # 'categories': [ 'Història clínica','Exploració', 'Agenda','Informat', 'Revisat', 'Alliberat', 'Codificació', 'Data exploració', 'Data informe' ,'Data revisió' ,'validat','Modificar'],
            'series': taula_detall},
            ]
    
        definicio = """Detall dels tots els informes realitzats, revisats i alliberats en els que esta involucrat el professional sel·leccionat</br></br> 
        Quan el faculatiu que crea l'informe (resident) es difernt al facultatiu selecionat el valor del reniment es el 25% del seu valor total.
        """
        objectiu_url = 'radiolegs'
        
        auditoria.request_handler(request,8,'Tipus consulta: activitatTotaldetall ; ' + str(request.session['enabled_filters']) + ' ,metge_id:' + str(metge_id))
        
  
         
    
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors_var,
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request)) 
    








































#############################################################
# Validacio de la Activitat Extraordinaria 
#############################################################

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatExtraordinariaPerReValidar(request):
    ##
    # Mostrem les proves pendents de validar
    ##

    
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'radiolegs_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    anys = request.session['enabled_filters']['any']
    mes = request.session['enabled_filters']['mes']
    unitatsel = request.session['enabled_filters']['unitat']
    modalitatsel = request.session['enabled_filters']['modalitat']

    if anys == 0:# quan es sel·lecionen tots els anys
        for filter_ in request.session['filters']:
            if filter_['nom'] == "any":
                any_primer = filter_['valors'][0][0]
                any_ultim = filter_['valors'][-1][0]
            else:
                any_primer = 2000
                any_ultim = 2800
        if mes == 0:
            start_date = datetime.date(int(any_primer), 1, 1)
            end_date = datetime.date(int(any_ultim), 12, 31)
        else: #tots els anys fins els mes sellecionat d'enguany!!
            dies_mes = calendar.monthrange(any_ultim, mes)
            start_date = datetime.date(any_primer, 1, 1)
            end_date = datetime.date(any_ultim, mes, dies_mes[1])
    else:
        if mes == 0:
                start_date = datetime.date(int(anys), 1, 1)
                end_date = datetime.date(int(anys), 12, 31) 
        else:
            dies_mes = calendar.monthrange(anys, mes)
            start_date = datetime.date(anys, mes, 1)
            end_date = datetime.date(anys, mes, dies_mes[1])

    # Definim querys per defecte
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    
                                                                        # no volem agenda ICS pero !!! si volem els metges que no tenen ralcio ( null )
    master_queryset = ProvaOhdim.objects.using('portal_dades').exclude( 
                                                                        Q(agenda__agenda_idi__es_ics=True ) & (
                                                                        Q(metge_informa__es_idi=False,  metge_informa__es_idi__isnull=False ) |
                                                                        Q(metge_revisa__es_idi=False,   metge_revisa__es_idi__isnull=False  ) |  
                                                                        Q(metge_allibera__es_idi=False, metge_allibera__es_idi__isnull=False)
                                                                        )
                                                                         # NO volem els metges que son del ICS i que la agenda es del ICS!!!  ( LA Q es totalment necessaria sino peta ( fa la join i li canvia el nom a la taula i despres alferla sevir peta))
                                                                       ).exclude (Q(agenda__agenda_idi__es_ics=True , metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True)
                                                                       # Ens quedem els que o bé no tenen realcio amb prova_ohdim_codificacio_generica o bé tenen un null possat perque al validar-lo es va treure el valor
                                                                       ).filter( provaohdimcodificaciogenerica__codificacio_generica_final__isnull=False)

    # AIXO FUNCIONA SI ES POSA EN LA MYSQL !!!!! NO HI HA UNAKLTRE FORAMA QUE CHUTI ( el Django no transalda bé la consulta !!!! unica sollucio es amb exclude )
    # AND  NOT ( portal_dades.agenda_idi.es_ics  AND  m_informa.id is Null  AND m_revisa.id is Null AND m_allibera.id is Null )
    # master_queryset = master_queryset.exclude(  agenda__agenda_idi__es_ics = True ,  metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True )
    #cada vegada que es fa una assignacio fa una consulta a la mysql --> posa un "limit 21" imagino que per comprovar que funcionen els filtres 
    
    #volem les prestacions entre el rang de dades, i que estiguin concloses 
    master_queryset =   master_queryset.filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
    
    if unitatsel != 0 and modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
    elif unitatsel!= 0:
        master_queryset = master_queryset.filter( agenda__agenda_idi__unitat_idi_id=unitatsel) 
            
    elif modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
    else:   
        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                          agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))

    import copy
    init_object = [0,0,0,0,0,0,0,0]
    
    taula_series2_tmp = {}

    queryset = master_queryset.values('metge_informa' ,'metge_informa__cognoms_nom' ).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')

    #NOM I COGNOMS i DETALL
    for prova in queryset:
        if  prova['metge_informa__cognoms_nom'] == None :
            try:
                if taula_series2_tmp[0] == None:
                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: red;'>Informes amb errors</span>"
                    taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../activitat-extraordinaria-detall/0' class='addlink'></a>"
            except:
                taula_series2_tmp[0] = copy.copy(init_object)
                taula_series2_tmp[0][0]  = "<span style='color: red;'>Informes amb errors</span>"
                taula_series2_tmp[0][7]  = "<a href='../activitat-extraordinaria-detall/0' class='addlink'></a>"

        else:
            taula_series2_tmp[prova['metge_informa']] = copy.copy(init_object)
            taula_series2_tmp[prova['metge_informa']][0]  = prova['metge_informa__cognoms_nom']
            taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../activitat-extraordinaria-detall/%s' class='addlink'></a>" % prova['metge_informa']

    # INFORMES CREATS
    queryset = master_queryset.values('metge_informa','metge_informa__cognoms_nom',).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')
    for prova in queryset:
        if  prova['metge_informa__cognoms_nom'] == None :
            taula_series2_tmp[0][1] = prova['informes_creats'] # s'ha de su
        else:
            taula_series2_tmp[prova['metge_informa']][1] = prova['informes_creats']
    # INFORMES REVISATS
    queryset = master_queryset.values('metge_revisa','metge_revisa__cognoms_nom').annotate(informes_revisats=Count('id')).order_by('metge_revisa__cognoms_nom')
    for prova in queryset:
        try:      
            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
        except KeyError:# no ha informat cap informe pero si revisat  o alliberat ?? els afegim
            taula_series2_tmp[prova['metge_revisa']] = copy.copy(init_object) # PETA que no hi ha el metge que informa === a el afegim aqui
            taula_series2_tmp[prova['metge_revisa']][0]  = prova['metge_revisa__cognoms_nom'] 
            taula_series2_tmp[prova['metge_revisa']][1] = 0
            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
            taula_series2_tmp[prova['metge_revisa']][7]  = "<a href='../activitat-extraordinaria-detall/%s' class='addlink'></a>" % prova['metge_revisa']
    # INFORMES ALLIBERATS
    queryset = master_queryset.values('metge_allibera','metge_allibera__cognoms_nom').annotate(informes_alliberats=Count('id')).order_by('metge_allibera__cognoms_nom')   
    for prova in queryset:
        try:
            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
        except KeyError:
            taula_series2_tmp[prova['metge_allibera']] = copy.copy(init_object)
            taula_series2_tmp[prova['metge_allibera']][1] = 0
            taula_series2_tmp[prova['metge_allibera']][2] = 0
            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
            taula_series2_tmp[prova['metge_allibera']][7]  = "<a href='../activitat-extraordinaria-detall/%s' class='addlink'></a>" % prova['metge_allibera']
        
    # INFORMES ACI
    queryset = master_queryset.filter(Q(provaohdimcodificaciogenerica__codificacio_generica_final=7) &
                                      (Q(provaohdimcodificaciogenerica__validat=True) &  ( Q(provaohdimcodificaciogenerica__revalidat__isnull=True ) |  Q(provaohdimcodificaciogenerica__revalidat = False ) ) 
                                       )).values('metge_allibera').annotate(informes_alliberats=Count('id'))
    for prova in queryset:       
        taula_series2_tmp[prova['metge_allibera']][4] = prova['informes_alliberats']
    # INFORMES ACH
    queryset = master_queryset.filter(Q(provaohdimcodificaciogenerica__codificacio_generica_final=8) &
                                      (Q(provaohdimcodificaciogenerica__validat=True) &  ( Q(provaohdimcodificaciogenerica__revalidat__isnull=True ) |  Q(provaohdimcodificaciogenerica__revalidat = False ) ) 
                                       )).values('metge_allibera').annotate(informes_alliberats=Count('id'))
    
    
    for prova in queryset:           
        taula_series2_tmp[prova['metge_allibera']][5] = prova['informes_alliberats']
        


    # Taula html - sort & filters
    taula_series2 = [] #creem una tupla de metge i les dades
    
    
    inf_alres_m = [0,0,0,0,0,0,0,0]
    dades_metge_tmp=[0,0,0,0,0,0,0,0]
    revalidar = False #, si estan tots revalidats ja no es mostrara
    
    #Sumem la morralla , els que no estan a la taula metge i els metge que son null en el provaohdim
    for dades_metge in taula_series2_tmp.items():
        if dades_metge[1][0] == None or dades_metge[1][0] == 0:
            if dades_metge[1][1] == None: dades_metge_tmp[0]= 0
            else:dades_metge_tmp[0] = dades_metge[1][1] 
            if dades_metge[1][2] == None: dades_metge_tmp[1]=0
            else:dades_metge_tmp[1] = dades_metge[1][2]
            if dades_metge[1][3] == None:  dades_metge_tmp[2]=0
            else:dades_metge_tmp[2] = dades_metge[1][3]
            if dades_metge[1][4] == None: dades_metge_tmp[3]=0
            else:dades_metge_tmp[3] = dades_metge[1][4]
            if dades_metge[1][5] == None:  dades_metge_tmp[4]=0
            else:dades_metge_tmp[4] = dades_metge[1][5]
#            if dades_metge[1][6] == None: dades_metge_tmp[5]=0
#            else:dades_metge_tmp[5] = dades_metge[1][6]
                
            inf_alres_m = [ inf_alres_m[0],
                            inf_alres_m[1] + int(dades_metge_tmp[0]) ,
                            inf_alres_m[2] + int(dades_metge_tmp[1]),
                            inf_alres_m[3] + int(dades_metge_tmp[2]),
                            inf_alres_m[4] + int(dades_metge_tmp[3]),
                            inf_alres_m[5] + int(dades_metge_tmp[4]),
                            inf_alres_m[6] + int(dades_metge_tmp[5])
#                            ,inf_alres_m[7] 
                            ]
        else:
            if dades_metge[0]== 0:
                inf_alres_m =  dades_metge[1]
            else:
                taula_series2.append((dades_metge[1][0], dades_metge[1]))
                
#    if inf_alres_m[0] != 0:#afegim la suma dels que notenen nom i errors
#        taula_series2.append((inf_alres_m[0], inf_alres_m))
        
        
    taula_series2 = sorted(taula_series2, key=lambda row: row[1][0])
    new_table = []
    for serie in taula_series2:
        new_table.append({'radioleg':serie[0],  'aci':serie[1][4],'ach':serie[1][5] ,'detall':serie[1][7]})
        
        if  serie[1][4] != 0 or serie[1][5] != 0:             
            revalidar = True #si hi ha un aci o ach ja es mostrara
    table = RadiolegsExtraordinariaValidarTable(new_table, order_by=request.GET.get('sort'))
    #table.paginate(page=request.GET.get('page', 1))
    taula_series = table

    # if request.GET.get('export','none') == "excel":   
    # grafics
    grafics = [
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Informes activitat extraordinaria per revalidar",
                'categories': [ 'Creats', 'Revisats', 'Alliberats','aci','ach', 'Detall'],
                'series': taula_series},              
                ]
    
        # Definició
    definicio = """
        Activitat realitzada pendent de revalidar per Serveis Centrals del I.D.I 
        """

    
    
    # seccio  validacio per part dels caps de unitat
    formulari = 'activitat_Extraordinaria_Revalidar_Centre'
    
    #mirem si te permisos per RE validar un altre cop
    if not ('meta.revalidar' in  request.user.get_all_permissions()):
        revalidar = False
    # fi seccio validacio
    
    
    auditoria.request_handler(request,8,'Tipus consulta: acitivitatExtraordinariaPerReValidar ; ' + str(request.session['enabled_filters']) )
    # hem heredat el indicador.html per poder afegir un boto de validacio
    return render_to_response('quadre_comandament/radiolegs/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url,
                               'forumlari':formulari, 
                               'validar':revalidar},
                              context_instance=RequestContext(request))                      





@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def activitatExtraordinariaReValidar(request):
    ###
    #executar la validacio de les perastcions selecionades
    ###
    missatge = ""
    url_ret = ''
    
    if 'HTTP_REFERER' in request.environ :
        if not 'modificar' in request.environ['HTTP_REFERER']:
            url_ret = request.environ['HTTP_REFERER']
            request.session['url_ret'] = url_ret
        else:
            if  'url_ret' in  request.session:
                if not 'modificar' in  request.session['url_ret']:
                    url_ret = request.session['url_ret']
            else:
                url_ret = '' 
    
    
    
    if request.method == 'POST':
        
 
        # Cal definir els filtres que volem per defecte
        request.session['current_filters'] = 'radiolegs_filters' 
        if not request.session['current_filters'] in request.session:
            enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
            initFilters(request, enabled_filters, request.session['current_filters'])
        request.session['enabled_filters'] = request.session[request.session['current_filters']]

        anys = request.session['enabled_filters']['any']
        mes = request.session['enabled_filters']['mes']
        unitatsel = request.session['enabled_filters']['unitat']
        modalitatsel = request.session['enabled_filters']['modalitat']

        if anys == 0:# quan es sel·lecionen tots els anys
            for filter_ in request.session['filters']:
                if filter_['nom'] == "any":
                    any_primer = filter_['valors'][0][0]
                    any_ultim = filter_['valors'][-1][0]
                else:
                    any_primer = 2000
                    any_ultim = 2800
            if mes == 0:
                start_date = datetime.date(int(any_primer), 1, 1)
                end_date = datetime.date(int(any_ultim), 12, 31)
            else: #tots els anys fins els mes sellecionat d'enguany!!
                dies_mes = calendar.monthrange(any_ultim, mes)
                start_date = datetime.date(any_primer, 1, 1)
                end_date = datetime.date(any_ultim, mes, dies_mes[1])
        else:
            if mes == 0:
                start_date = datetime.date(int(anys), 1, 1)
                end_date = datetime.date(int(anys), 12, 31) 
            else:
                dies_mes = calendar.monthrange(anys, mes)
                start_date = datetime.date(anys, mes, 1)
                end_date = datetime.date(anys, mes, dies_mes[1])

        
        # Definim querys per defecte
        user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    
                                                                        # no volem agenda ICS pero !!! si volem els metges que no tenen ralcio ( null )
        master_queryset = ProvaOhdim.objects.using('portal_dades').exclude( 
                                                                        Q(agenda__agenda_idi__es_ics=True ) & (
                                                                        Q(metge_informa__es_idi=False,  metge_informa__es_idi__isnull=False ) |
                                                                        Q(metge_revisa__es_idi=False,   metge_revisa__es_idi__isnull=False  ) |  
                                                                        Q(metge_allibera__es_idi=False, metge_allibera__es_idi__isnull=False)
                                                                        )
                                                                         # NO volem els metges que son del ICS i que la agenda es del ICS!!!  ( LA Q es totalment necessaria sino peta ( fa la join i li canvia el nom a la taula i despres alferla sevir peta))
                                                                       ).exclude (Q(agenda__agenda_idi__es_ics=True , metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True)
                                                                       # Ens quedem els que o bé no tenen realcio amb prova_ohdim_codificacio_generica o bé tenen un null possat perque al validar-lo es va treure el valor
                                                                       ).filter( provaohdimcodificaciogenerica__codificacio_generica_final__isnull=False)

    
        #volem les prestacions entre el rang de dades, i que estiguin concloses 
        master_queryset =   master_queryset.filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
    
        if unitatsel != 0 and modalitatsel != 0:
            master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
        elif unitatsel!= 0:
            master_queryset = master_queryset.filter( agenda__agenda_idi__unitat_idi_id=unitatsel) 
            
        elif modalitatsel != 0:
            master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
        else:   
            master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                          agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))


        master_queryset = master_queryset.filter(
                         ( Q(provaohdimcodificaciogenerica__codificacio_generica_final=8) | Q(provaohdimcodificaciogenerica__codificacio_generica_final=7) )
                       & ( Q(provaohdimcodificaciogenerica__validat=True )
                       & ( Q(provaohdimcodificaciogenerica__revalidat__isnull=True ) | Q(provaohdimcodificaciogenerica__revalidat=False ) ) ) ).values('id')
 
        # s'ha de revalidar tot
        data = datetime.datetime.now()
        #nprestacions = master_queryset.annotate(Count('id')) fa una consulta la mysql un count
        nprestacions = 0
        for item in master_queryset:
            ProvaOhdimCodificacioGenerica.objects.select_related().filter(prova_ohdim=item['id']).update(
                                                                                                         revalidat = True,
                                                                                                         revalidat_per = str(request.user.id),
                                                                                                         revalidat_data=data
                                                                                                         )
            nprestacions = nprestacions + 1
#            item.save()
        missatge = "S'han revalidat les " + str( nprestacions) + " prestacions filtrades! "  
        revalidar = True
          
    else:
        #mostrem el objecte si exitreix en la bd
        revalidar = False
        missatge = "Torneu enrrere si us plau! "
            
    # s'ha marcat validat ,
    definicio = 'Resultat de la operació'
    auditoria.request_handler(request,8,'Tipus consulta: activitatExtraordinariaRevalidar update-> ; ' + str(request.session['enabled_filters']) )
    return render_to_response('quadre_comandament/radiolegs/indicador.html',
                              {'indicadors':get_indicadors(request),
                                "objectiu":objectiu,
                                "objectiu_url":objectiu_url,
                                 'definicio':definicio,
                                'url_ret':url_ret,
                               'validar': revalidar,
                               'missatge':missatge},
                              context_instance=RequestContext(request))
