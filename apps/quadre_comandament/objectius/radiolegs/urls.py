from django.conf.urls.defaults import url, patterns

urlpatterns = patterns('apps.quadre_comandament.objectius.radiolegs.views',
    url(r'^$', 'index'), #    
    url(r'^radiolegs/$', 'radiolegs'),
    url(r'^activitat-total/$', 'activitatTotal'),
 
    url(r'^activitat-total-detall/(?P<metge_id>\d+)', 'activitatTotalDetall'),
    url(r'^activitat-total-detall/', 'activitatTotal'),
    url(r'^activitat-total-detall-seram/(?P<metge_id>\d+)', 'activitatTotalDetallSeram'),
    ##
    url(r'^activitat-total-detall-modificar/(?P<provaohdim_id>\d+)&(?P<codgen_id>\d+)','activitatTotalDetallModificar'),
    url(r'^activitat-total-detall-modificar/','activitatTotalDetallModificar'),
    ## 
    url(r'^activitat-ordinaria/$', 'activitatOrdinaria'),
    url(r'^activitat-ordinaria-detall/(?P<metge_id>\d+)', 'activitatOrdinariaDetall'),
    url(r'^activitat-ordinaria-detall/', 'activitatOrdinaria'),
    url(r'^activitat-ordinaria-detall-seram/(?P<metge_id>\d+)', 'activitatOrdinariaDetallSeram'),
    ##
    url(r'^activitat-extraordinaria-validar/$', 'activitatExtraordinariaPerValidar'),
    url(r'^activitat-extraordinaria-validar/validar/', 'activitatExtraordinariaValidar'),
    #
    url(r'^activitat-extraordinaria/$', 'activitatExtraordinaria'),
    url(r'^activitat-extraordinaria-detall/(?P<metge_id>\d+)', 'activitatExtraordinariaDetall'),
    url(r'^activitat-extraordinaria-detall/', 'activitatExtraordinaria'),
    #
    url(r'^activitat-extraordinaria-revalidar/$', 'activitatExtraordinariaPerReValidar'),
    url(r'^activitat-extraordinaria-revalidar/validar/', 'activitatExtraordinariaReValidar'),


    
   # url(r'^segones-opinions/$', 'segonesOpinions'),
   # url(r'^segones-opinions-detall/(?P<metge_id>\d+)', 'segonesOpinionsDetall'),
   # url(r'^segones-opinions-detall/', 'segonesOpinions'),
#
#    url(r'^programes-angio/$', 'programesAngiograf'),
#    url(r'^programes-angio-detall/(?P<metge_id>\d+)', 'programesAngiografDetall'),
#    url(r'^programes-angio-detall/', 'programesAngiograf'),
#    url(r'^programes-angio-intervencio-detall/(?P<intervencio_id>\d+)', 'programesAngiografIntervencioDetall'),
#    url(r'^programes-angio-intervencio-detall/', 'programesAngiograf'),


    
  
)
