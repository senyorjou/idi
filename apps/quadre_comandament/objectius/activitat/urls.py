from django.conf.urls.defaults import url, patterns

urlpatterns = patterns('apps.quadre_comandament.objectius.activitat.views',
    url(r'^$', 'index'), #   
    url(r'^servei-peticionari/$', 'serveiPeticionari'),
    url(r'^tipus-de-prova/$', 'tipusProva'),
    url(r'^tipus-de-prova-per-entitat/$', 'entitat'), 
    url(r'^prestacions-desglossades/$', 'prestacionsDesglossades'),

)