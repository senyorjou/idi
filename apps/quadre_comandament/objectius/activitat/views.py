# coding: utf8
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from django.db import connections
from apps.quadre_comandament.common import initFilters
import time
import apps.meta.auditoria
from apps.django_xlwt.Row import Row

# TODO: fer un altre formatge quan hi hagin grafiques amb un altres més gran dep 40%

indicadors = ['Servei peticionari', 'Tipus de prova', 'Tipus de prova per entitat','Prestacions desglossades' ]
objectiu = "Gestió de l'activitat facturada"
objectiu_url = "activitat"

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def index(request):
    request.session['enabled_filters'] = {}
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                          

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def serveiPeticionari(request):
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'servei_peticionari_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))-1}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]
        
    # id centre facturacio , Id_unitat ( MN RM ) , idtipoexploracion, mes , any )   
    query = 'call Facturat_Peticionari(0,'
    if request.user.authuserprofile.unitat_idi_id == 17: 
        query = query + str(request.session['enabled_filters']['unitat'])
    else:
        query += str(request.user.authuserprofile.unitat_idi_id)
    query = query + ',' + str(request.session['enabled_filters']['modalitat'])
    query = query + ',' + str(request.session['enabled_filters']['mes'])
    query = query + ",%s);" % str(request.session['enabled_filters']['any'])
 
    cursor = connections['facnetDB'].cursor()
    cursor.execute(query, [])
    resultat = cursor.fetchall()   
            
    taula_series = []
    barres_series = []
    
    formatge_series_mes_actual = []
    formatge_series_acumulat = []
    taula_totals = ("<b>TOTAL</b>", [0, 0, 0, 0])
    
    items = 0
    altres_formatge = 0
    #altres_barres = 0
    
    for row in resultat:
        taula_series_item = (row[0], row[1:])
        taula_totals = ("<b>TOTAL</b>", [taula_totals[1][0] + row[1], 
                                         taula_totals[1][1] + row[2], 
                                         taula_totals[1][2] + row[3], 
                                         taula_totals[1][3] + row[4]])
        taula_series.append(taula_series_item)
        if items < 15:
            if row[2] > 0:
                formatge_series_item = (row[0], row[2])
                formatge_series_mes_actual.append(formatge_series_item)
            if row[4] > 0:
                formatge_series_item = (row[0], row[4])
                formatge_series_acumulat.append(formatge_series_item)
            if int(row[1]) > 0:
                barres_series_item = (row[0], [row[1]])
                barres_series.append(barres_series_item)
        else:
            altres_formatge += row[2]
            #altres_barres += row[1]
        items += 1
    taula_series.append(taula_totals)    
        
    if altres_formatge > 0:
        formatge_series_item = ("Altres", altres_formatge)
        formatge_series_mes_actual.append(formatge_series_item)
        formatge_series_acumulat.append(formatge_series_item) 
    #if altres_barres > 0:        
    #    barres_series_item = ("Altres", [altres_barres])
    #    barres_series.append(barres_series_item)
              
    
    grafics = [
               {'tipus': "formatge", 
                'tamany': 100,
                'titol': "Facturació/mes",
                'descripcio': "Facturació del mes a l'hospital de referència segons servei peticionari",
                'series': formatge_series_mes_actual },
                              
               {'tipus': "formatge", 
                'tamany': 100,
                'titol': "Facturació acumulada",
                'descripcio': "Facturació acumulada a l'hospital de referència segons servei peticionari",                
                'series': formatge_series_acumulat },
                                
               {'tipus': "barres", 
                'tamany': 90,                
                'titol': "Proves/mes",
                'descripcio': "Quantitat de proves facturades a l'hospital de referència durant el mes.",                
                'categories': ['Serveis peticionaris'],
                'titol_y': "# de proves",
                'series': barres_series},
              
               {'tipus': "taula", 
                'tamany': 90,                
                'titol': "Activitat facturada per servei peticionari",
                'categories': ['Proves mes', 'Facturat mes (€)', 'Proves acumulades', 'Facturat acumulat (€)'],
                'series': taula_series},
                ]
    
    # Definició
    definicio = """Activitat facturada al propi hospital classificada per servei peticionari. En aquesta activitat
    no hi apareix l'activitat realitzada per altres entitats com l'ICO.
    """
    
    apps.meta.auditoria.request_handler(request,6,'Tipus consulta:' + 'serveiPeticionari' + '; Consulta: ' + str(query ))
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))


@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')   
def activitat(request):
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                              'objectiu':objectiu},
                              context_instance=RequestContext(request))                          
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def tipusProva(request):
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'tipus_prova_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))-1}
        initFilters(request, enabled_filters, 'tipus_prova_filters')
    request.session['enabled_filters'] = request.session[request.session['current_filters']]
        
    # id centre facturacio , Id_unitat ( MN RM ) , idtipoexploracion, mes , any )   
    query = 'call Facturat_Exploracio_HR(0,' 
    if request.user.authuserprofile.unitat_idi_id == 17: 
        query = query + str(request.session['enabled_filters']['unitat'])
    else:
        query += str(request.user.authuserprofile.unitat_idi_id)
    query = query + ', ' + str(request.session['enabled_filters']['modalitat'])
    query = query +',' + str(request.session['enabled_filters']['mes'])
    query = query + ",%s);" % str(request.session['enabled_filters']['any'])

    cursor = connections['facnetDB'].cursor()
    cursor.execute(query, [])
    resultat = cursor.fetchall()   
    
    
    
    
         # MES anterior 
#    

    query_mes_anterior = 'call Facturat_Exploracio_HR(0,' 
    if request.user.authuserprofile.unitat_idi_id == 17: 
        query_mes_anterior += str(request.session['enabled_filters']['unitat'])
    else:
        query_mes_anterior += str(request.user.authuserprofile.unitat_idi_id)
    query_mes_anterior += ', ' + str(request.session['enabled_filters']['modalitat'])
    
    # posem el mateix , TOTS
    if request.session['enabled_filters']['mes'] == 0:
        query_mes_anterior +=',' + str(request.session['enabled_filters']['mes'])
        query_mes_anterior += ",%s);" % str(request.session['enabled_filters']['any'] ) 
    #posem el desembre del any anterior si seleccionem el gener
    else:
        if request.session['enabled_filters']['mes'] == 1:
            query_mes_anterior +=',' + str(12)
            query_mes_anterior += ",%s);" % str(request.session['enabled_filters']['any'] -1 ) 
        else:
            query_mes_anterior +=',' + str(request.session['enabled_filters']['mes']-1)
            query_mes_anterior += ",%s);" % str(request.session['enabled_filters']['any'])

    cursor = None
    cursor = connections['facnetDB'].cursor()
    cursor.execute(query_mes_anterior, [])
    resultat_m_anterior = cursor.fetchall()      
#    
       
    
    
   

            
    taula_series = []
    barres_series = []
    
    formatge_series_mes_actual = []
    formatge_series_acumulat = []
    taula_totals = ("<b>TOTAL</b>", [0, 0,0, 0,0 ,0])
    
    items = 0
    altres_formatge = 0
    #altres_barres = 0
    
    for row in resultat:
        
        trobat = False
        for row_m_a in resultat_m_anterior:
            
            if row[0] == row_m_a[0]:
                trobat = True
                row = row[1:]
                taula_series_item = (row[0],( row[1],row_m_a[2],row[2],row_m_a[3],row[3],row[4]))
                taula_totals = ("<b>TOTAL</b>", [taula_totals[1][0] + row[1],
                                                taula_totals[1][1] + row_m_a[2],
                                                taula_totals[1][2] + row[2],
                                                taula_totals[1][3] + row_m_a[3],
                                                taula_totals[1][4] + row[3], 
                                                taula_totals[1][5] + row[4]])
                taula_series.append(taula_series_item)
                if items < 15:
                    if int(row[2]) > 0:
                        formatge_series_item = (row[0], row[2])
                        formatge_series_mes_actual.append(formatge_series_item)
                    if int(row[4]) > 0:
                        formatge_series_item = (row[0], row[4])
                        formatge_series_acumulat.append(formatge_series_item)
                    if int(row[1]) > 0:
                        barres_series_item = (row[0], [row[1]])
                        barres_series.append(barres_series_item)
                else:
                    altres_formatge += row[2]
                    #altres_barres += row[1]
                items += 1    

        if not trobat:       
            row = row[1:]
            taula_series_item = (row[0],( row[1],0,row[2],0,row[3],row[4]))
            taula_totals = ("<b>TOTAL</b>", [taula_totals[1][0] + row[1],
                                             
                                             taula_totals[1][1]  + 0,
                                              
                                         taula_totals[1][2] + row[2], 
                                         
                                             taula_totals[1][3]  + 0,
                                         taula_totals[1][4] + row[3], 
                                         taula_totals[1][5] + row[4]])
            
            taula_series.append(taula_series_item)
            if items < 15:
                if int(row[2]) > 0:
                    formatge_series_item = (row[0], row[2])
                    formatge_series_mes_actual.append(formatge_series_item)
                if int(row[4]) > 0:
                    formatge_series_item = (row[0], row[4])
                    formatge_series_acumulat.append(formatge_series_item)
                if int(row[1]) > 0:
                    barres_series_item = (row[0], [row[1]])
                    barres_series.append(barres_series_item)
            else:
                altres_formatge += row[2]
                #altres_barres += row[1]
            items += 1
    
    taula_series.append(taula_totals)    
        
    if altres_formatge > 0:
        formatge_series_item = ("Altres", altres_formatge)
        formatge_series_mes_actual.append(formatge_series_item)
        formatge_series_acumulat.append(formatge_series_item) 
    #if altres_barres > 0:        
    #    barres_series_item = ("Altres", [altres_barres])
    #    barres_series.append(barres_series_item)
              
    
    
    


            
    taula_series_mes_anterior = []
    barres_series_mes_anterior = []
    formatge_series_mes_anterior = []
    #formatge_series_acumulat = []
    #taula_totals = ("<b>TOTAL</b>", [0, 0, 0, 0])
    
    items = 0
    altres_formatge = 0
    
    
    for row in resultat_m_anterior:
        row = row[1:]
        taula_series_item = (row[0], row[1:])
        taula_totals = ("<b>TOTAL</b>", [taula_totals[1][0] + row[1], 
                                          taula_totals[1][1] + row[2], 
                                          taula_totals[1][2] + row[3], 
                                          taula_totals[1][3] + row[4]])
        taula_series_mes_anterior.append(taula_series_item)
        if items < 15:
            if int(row[2]) > 0:
                formatge_series_item = (row[0], row[2])
                formatge_series_mes_anterior.append(formatge_series_item)
            if int(row[4]) > 0:
                formatge_series_item = (row[0], row[4])
                #formatge_series_acumulat.append(formatge_series_item)
            if int(row[1]) > 0:
                barres_series_item = (row[0], [row[1]])
                barres_series_mes_anterior.append(barres_series_item)
        else:
            altres_formatge += row[2]
            #altres_barres += row[1]
        items += 1
    taula_series_mes_anterior.append(taula_totals)    
        
    if altres_formatge > 0:
        formatge_series_item = ("Altres", altres_formatge)
        formatge_series_mes_anterior.append(formatge_series_item)
        #formatge_series_acumulat.append(formatge_series_item) 
    #if altres_barres > 0:
                

    
        
    
    grafics = [
               {'tipus': "formatge", 
                'tamany': 100,
                'titol': "Facturació/mes",
                'descripcio': "Facturació del mes a l'hospital de referència segons tipus de prova",
                'series': formatge_series_mes_actual },
               
               
                {'tipus': "formatge", 
                'tamany': 100,
                'titol': "Facturació/mes anterior",
                'descripcio': "Facturació del mes anterior a l'hospital de referència segons tipus de prova",
                'series': formatge_series_mes_anterior },
               
               
                              
               {'tipus': "formatge", 
                'tamany': 100,
                'titol': "Facturació acumulada",
                'descripcio': "Facturació acumulada a l'hospital de referència segons tipus de prova",                
                'series': formatge_series_acumulat },
                  
                  
                                
               {'tipus': "barres", 
                'tamany': 90,                
                'titol': "Proves/mes",
                'descripcio': "Quantitat de proves facturades a l'hospital de referència durant el mes.",                
                'categories': ['Tipus de prova'],
                'titol_y': "# de proves",
                'series': barres_series},
              
              
              
              
              
               {'tipus': "taula", 
                'tamany': 90,                
                'titol': "Activitat facturada per tipus de prova",
                'categories': ['Proves mes', 'Proves mes anterior' , 'Facturat mes (€)','Facturat mes ant.(€)' ,  'Proves acumulades', 'Facturat acumulat (€)'],
                'series': taula_series},
                ]
    
    # Definició
    definicio = """Activitat facturada al propi hospital classificada per tipus de prova. En aquesta activitat
    no hi apareix l'activitat realitzada per altres entitats com l'ICO.
    """
    apps.meta.auditoria.request_handler(request,6,'Tipus consulta:' + 'tipusProva' + '; Consulta: ' + str(query ))
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                         

# TODO: En aquest indicador mostrar un formatge amb el percentatge de facturació per client.
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def entitat(request):
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'entitat_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'centre':"0", 'unitat': "0", 'entitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))-1}
        initFilters(request, enabled_filters, 'entitat_filters')
    request.session['enabled_filters'] = request.session[request.session['current_filters']]
        
    # id centre facturacio , Id_unitat ( MN RM ) , idtipoexploracion, mes , any )
    # TODO: Permetre a nserra triar el centre   
    
    if request.user.authuserprofile.unitat_idi_id == 17:
        query = "call Facturat_Client(%s," % request.session['enabled_filters']['centre']
        query = query + str(request.session['enabled_filters']['unitat'])
    else:
        query = "call Facturat_Client(0," 
        query += str(request.user.authuserprofile.unitat_idi_id)
    query = query + ', ' + str(request.session['enabled_filters']['entitat'])
    query = query +',' + str(request.session['enabled_filters']['mes'])
    query = query + ",%s);" % str(request.session['enabled_filters']['any'])

    cursor = connections['facnetDB'].cursor()
    cursor.execute(query, [])
    resultat = cursor.fetchall()   
            
    taula_series = []
    barres_series = []
    
    formatge_series_mes_actual = []
    formatge_series_acumulat = []
    taula_totals = ("<b>TOTAL</b>", [0, 0, 0, 0])
    
    items = 0
    altres_formatge = 0
    #altres_barres = 0
    
    for row in resultat:
        row = row[1:]
        taula_series_item = (row[0], row[1:])
        taula_totals = ("<b>TOTAL</b>", [taula_totals[1][0] + row[1], 
                                         taula_totals[1][1] + row[2], 
                                         taula_totals[1][2] + row[3], 
                                         taula_totals[1][3] + row[4]])
        taula_series.append(taula_series_item)
        if items < 15:
            if int(row[2]) > 0:
                formatge_series_item = (row[0], row[2])
                formatge_series_mes_actual.append(formatge_series_item)
            if int(row[4]) > 0:
                formatge_series_item = (row[0], row[4])
                formatge_series_acumulat.append(formatge_series_item)
            if int(row[1]) > 0:
                barres_series_item = (row[0], [row[1]])
                barres_series.append(barres_series_item)
        else:
            altres_formatge += row[2]
            #altres_barres += row[1]
        items += 1
    taula_series.append(taula_totals)    
        
    if altres_formatge > 0:
        formatge_series_item = ("Altres", altres_formatge)
        formatge_series_mes_actual.append(formatge_series_item)
        formatge_series_acumulat.append(formatge_series_item) 
    #if altres_barres > 0:        
    #    barres_series_item = ("Altres", [altres_barres])
    #    barres_series.append(barres_series_item)
    
    # Gràfics a mostrar només en cas que no es seleccioni una unitat 
#    grafics = []          
#    if request.session['enabled_filters']['entitat'] == 0:
#        grafic = [{'tipus': "formatge", 
#                  'tamany': 100,
#                  'titol': "Facturació segons entitat entitats/mes",
#                  'descripcio': "Facturació del mes seleccionat segons entitat.",
#                  'series': },
#                  
#                  {'tipus': "formatge", 
#                  'tamany': 100,
#                  'titol': "Facturació acomulada segons entitats",
#                  'descripcio': "Facturació acomulada al mes seleccionat segons entitat.",
#                  'series': }
#                  }
#        grafics.extend(grafic)
    
    grafics = [               
               {'tipus': "formatge", 
                'tamany': 100,
                'titol': "Facturació/mes",
                'descripcio': "Facturació del mes seleccionat a l'entitat seleccionada.",
                'series': formatge_series_mes_actual },
                              
               {'tipus': "formatge", 
                'tamany': 100,
                'titol': "Facturació acumulada",
                'descripcio': "Facturació acumulada al mes seleccionat a l'entitat seleccionada.",                
                'series': formatge_series_acumulat },
                                
               {'tipus': "barres", 
                'tamany': 90,                
                'titol': "Proves/mes",
                'descripcio': "Quantitat de proves facturades a l'entitat al mes seleccionat.",                
                'categories': ['Tipus de prova'],
                'titol_y': "# de proves",
                'series': barres_series},
              
               {'tipus': "taula", 
                'tamany': 90,                
                'titol': "Facturació al mes seleccionat a l'entitat.",
                'categories': ['Proves mes', 'Facturat mes (€)', 'Proves acumulades', 'Facturat acumulat (€)'],
                'series': taula_series},
                ]
    
    # Definició
    definicio = """Activitat facturada total per tipus de prova. En aquesta activitat
    SI que hi apareix l'activitat realitzada per altres entitats com l'ICO.
    """    
    apps.meta.auditoria.request_handler(request,6,'Tipus consulta:' + 'entitat' + '; Consulta: ' + str(query ))
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                         
    







@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def prestacionsDesglossades(request):
    
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'tipus_prova_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))-1}
        initFilters(request, enabled_filters, 'tipus_prova_filters')
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    
    mes =  int(request.session['enabled_filters']['mes'])
    any =  int(request.session['enabled_filters']['any'])
    amdinici = str(any*10000 + (mes) *100)
    amdfi = str(any*10000 + (mes +1) *100)
    if mes == 0:
        amdfi = 0
    amd = any*10000
    amd = str(amd)

    modalitat =  str(request.session['enabled_filters']['modalitat'])
    if request.user.authuserprofile.unitat_idi_id == 17: 
        unitat = str(request.session['enabled_filters']['unitat'])
    else:
        unitat = str(request.user.authuserprofile.unitat_idi_id)
    
    query =  '''
        SELECT
              count(ndeproves) as recompte ,
              ndeproves as nproves_pacient  ,
              sum(no_facturades) n_prestacions_no_facturades
        FROM (
            SELECT
                count(c.NHistoria) as ndeproves,
                if(f.IdFacturaDetalle is null,1,0) as no_facturades,
                c.`CodAgenda`,  a.`Obs`
            FROM citasap c
               left join facturadetalle f on c.IdCitaSap = f.idcitasap
               left join agenda a on a.codagenda = c.CodAgenda
            WHERE
                   c.`amdAgenda` > %s 
                   AND if( %s = 0, ( c.`amdAgenda` < concat( %s , month(now())*1000)) , ( c.`amdAgenda` < %s ))
                   AND if( %s = 0, ( a.idtipoexploracion <> %s ), (a.idtipoexploracion = %s))
                   AND if( %s = 0, ( IdUnidadFac <> %s ), (IdUnidadFac = %s))
                   AND c.`STATUS` <> 'AN'
            GROUP BY c.NHistoria,c.amdAgenda,IdTipoExploracion, no_facturades
            ) aux  group by ndeproves; '''
            
    cursor = connections['facnetDB'].cursor()
    cursor.execute(query, [amdinici,amdfi,any,amdfi,modalitat,modalitat,modalitat,unitat,unitat,unitat])
    resultat = cursor.fetchall() 

    ### Mes anterior    
    mes =  int(request.session['enabled_filters']['mes'])
    any =  int(request.session['enabled_filters']['any'])
    if mes == 1:#gener -> any anteiror
        amdinici = str((any-1)*10000 + (12) *100)
        amdfi = str(any*10000 + (mes) *100)
    else:
        amdinici = str(any*10000 + (mes-1) *100)
        amdfi = str(any*10000 + (mes) *100)
    if mes == 0: amdfi = 0
    amd = any*10000
    amd = str(amd)
    
    cursor = None
    cursor = connections['facnetDB'].cursor()
    cursor.execute(query, [amdinici,amdfi,any,amdfi,modalitat,modalitat,modalitat,unitat,unitat,unitat])
    resultat_m_anterior = cursor.fetchall()      
    
    taula_series = []
    barres_series = []
    #taula_totals = ("<b>TOTAL</b>", [0, 0, 0, 0])
    
    
    taula_totals = ("<b>TOTAL</b>", [0, 0, 0, 0])
    
    
    
    
    for row in resultat:
        
        trobat = False
        for row_m_a in resultat_m_anterior:

            if row[1] == row_m_a[1]:
                trobat = True
                
                taula_series_item = (str(row[1]) + ' prestacions per estudi', ( row[0],row_m_a[0],row[2],row_m_a[2]))
                taula_series.append(taula_series_item)
                
                barres_series_item = (str(row[1]) + ' prestacions per estudi', ( row[0],row_m_a[0]))
                barres_series.append(barres_series_item)
                
                taula_totals = ("<b>TOTAL</b>", [taula_totals[1][0] + (row[1]*row[0]), 
                                         taula_totals[1][1] + (row_m_a[1]*row_m_a[0]), 
                                         taula_totals[1][2] + row[2], 
                                         taula_totals[1][3] + row_m_a[2]
                                         ])
                
                
        if not trobat:       

            taula_series_item = (str(row[1]) + ' prestacions per estudi', ( row[0],row[2]))
            taula_series.append(taula_series_item)
            barres_series_item = (str(row[1]) + ' prestacions per estudi', ( row[0],))
            barres_series.append(barres_series_item)
            
            taula_totals = ("<b>TOTAL</b>", [taula_totals[1][0] +  (row[1]*row[0]), 
                                         taula_totals[1][1] + 0, 
                                         taula_totals[1][2] +  row[2], 
                                         taula_totals[1][3] + 0])


    taula_series.append(taula_totals)
    
    grafics = [               
         
              
                {'tipus': "barres", 
                'tamany': 90,                
                'titol': "Estudis/mes",
                'descripcio': "Quantitat d'estudis agrupats segons la quantitat de prestacions que el componen.",                
                'categories': ["Nº d estudis per pacient", "Nº d estudis per pacient mes anterior"],
                'titol_y': "# d'estudis",
                'series': barres_series},

               {'tipus': "taula", 
                'tamany': 90,                
                'titol': "Facturació al mes seleccionat a l'entitat.",
                'categories': ['# Estudis', '# Estudis mes anterior.','# Estudis no facturades', '# Estudis no facturades mes anterior'],
                'series': taula_series},
                ]
    
            
            
            
    definicio = """Aquest indicador mostra quants estudis s'han facturat segons la quantitat de prestacions per estudi.</br></br>
    Es considera un estudi com totes les prestacions realitzades a una mateixa persona d'una mateixa modalitat.
    """    
    apps.meta.auditoria.request_handler(request,6,'Tipus consulta:' + 'total' + '; Consulta: ' + str(query )[:250])
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                         
        