# coding: utf8

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from apps.quadre_comandament.common import initFilters
from apps.quadre_comandament.models import  ProvaOhdim,UsuariSAP,Prestacio
from apps.quadre_comandament.objectius.tecnics_infermers.tables import ComplexitatDetallTable,ComplexitatDetallDueterTable
import datetime,calendar,time
from django.db.models import Q,Count
#from apps.meta.models import AuthUser,AuthUserProfile
from apps.meta import auditoria




indicadors = ['Complexitat','Complexitat due-ter']
objectiu = "Complexitat de la activitat dels DUE/TER"
objectiu_url = "tecnics_infermers"


@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def index(request):
    request.session['enabled_filters'] = {}
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                          

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def tecnicsInfermers(request):
    request.session['enabled_filters'] = {}
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                          

 
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def complexitatTaulaDefinicio(request):
    request.session['enabled_filters'] = {}
    url_ret = ''
    
    if 'HTTP_REFERER' in request.environ :
        if not 'modificar' in request.environ['HTTP_REFERER']:
            url_ret = request.environ['HTTP_REFERER']
            request.session['url_ret'] = url_ret
        else:
            if  'url_ret' in  request.session:
                if not 'modificar' in  request.session['url_ret']:
                    url_ret = request.session['url_ret']
            else:
                url_ret = '' 
     
    return render_to_response('quadre_comandament/tecnics_infermers/definicio_complexitat.html',
                              {'indicadors':indicadors,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url,
                               'url_ret':url_ret},
                              context_instance=RequestContext(request))                          

def getFilters(request,tipus_consulta):
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'tecnicinfermers_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    anys = request.session['enabled_filters']['any']
    mes = request.session['enabled_filters']['mes']
    unitatsel = request.session['enabled_filters']['unitat']
    modalitatsel = request.session['enabled_filters']['modalitat']

    prestacions_excloses = Prestacio.objects.filter(
            codi__in = [ 'RA00225',#'Esterotaxia')
                         'RA00909',#'Estudi ecogràfic vascular)
                         'RA01004',#'Revisió catèter venós central /revisió catèter venós disfuncionant)
                         'RA01005',#'Repermeabilització de catèter ocluït
                         'RA01007',#'Recol·locació catèter disfuncionant
                         'RA01009',#'Retirada de catèter tunelitzat
                         'RA01010',#'Retirada de reservori
                         'AD00002',#'Visita sucesiva
                         'AD00041' #'Visita cura enfermeria
                        ]).values_list('id', flat=True )# values_list retorna una llista encoptes de un ducicionari

    if mes != 0:
        if int(anys) <> 0:
            dies_mes = calendar.monthrange(anys, mes)
            start_date = datetime.date(anys, mes, 1)
            end_date = datetime.date(anys, mes, dies_mes[1])
        else:
            dies_mes = calendar.monthrange(datetime.datetime.now().year, mes)
            start_date = datetime.date(2000, 1, 1)
            end_date = datetime.date(datetime.datetime.now().year, mes, dies_mes)
    else:
        
        if int(anys) == 0:
            start_date = datetime.date(2000, 1, 1)
            end_date = datetime.date(datetime.datetime.now().year, 12, 31)
        else:
            start_date = datetime.date(anys, 1, 1)
            end_date = datetime.date(anys, 12, 31)
        
    #TODO: potser cal fer un data inici data fi ( per a que puguin filtrar  les dates)
    #start_date = '2012-01-01'
    #end_date = '2012-03-31'
        
    # Definim querys per defecte
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    
                                                                        # no volem agenda ICS
    master_queryset = ProvaOhdim.objects.using('portal_dades').exclude( Q(agenda__agenda_idi__es_ics=True ) 
                                                                      )
    #volem les prestacions entre el rang de dades, i que estiguin concloses 
    master_queryset =   master_queryset.filter(data_fi_prestacio__range=(start_date, end_date) , status_prestacio_codi='CO')
    
    # apliquem els altres filtes
    if unitatsel != 0 and modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
    elif unitatsel!= 0:
        master_queryset = master_queryset.filter( agenda__agenda_idi__unitat_idi_id=unitatsel) 
            
    elif modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
    else:   
        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                          agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))
    # les prestacions = 18 son informes de prestacions realitzades en altres centres === a que la RM s'ha fet en un alte lloc
    master_queryset = master_queryset.exclude(prestacio = 18 )
    
    #TODO: potser hemd e filtrar nomes el due/ter si volem bé les dades pero per poderles netjar i obrir incidencies a SAP cal poder veureles
    #cat_prof_dueter_ids = AuthUserProfile.Objects.categoria_profesional_id__in( 7 or = 12)
    #master_queryset = master_queryset.filter(tecnic_realitza =
    
    #les prestacions que son sedacions o anestesies -> en la prestacio que en depen ja posen la complexitat que te anestesia ( el 5 ) per tant no cal afegir unaltre provca
    #l afegeixen perque es factura a apart ( el anaestesista )
    master_queryset = master_queryset.exclude(prestacio__descripcio__icontains='sedacio')
    master_queryset = master_queryset.exclude(prestacio__descripcio__icontains='anestesia')
    #Excloem un seguit de prestacions que formen part de intervencionisme on el Du/ter no intervé  (de manera radiológica ) perque son extraccións ,etc ( valors donats per Nuria Albero)
    master_queryset = master_queryset.exclude(prestacio__in = prestacions_excloses)
            

    return master_queryset


@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def complexitat(request):
    ################################
    # Els técnics i informes marquen  totes les proves que realitzen amb una complexitat segons una taula ( si  elpacient colabora, intubat, neonatal ...)
    # aquesta informacio la quagdren en el camp de SAP nº de plaques que ens ve en el MDO pel mateix nom  
    ################################
    #    taula_complexitat = {1:'molt sencill',2:'sencill',3:'normal',4:'complexe',5:'molt complexe'}
    #    if taula_complexitat.has_key(3):
    #        print(taula_complexitat[3][1])
 
    ##
    ## Mes  selecionat 
    ##
 
    master_queryset = getFilters(request,'mes')
    
# AQUEST POT SER EL DETALL DELS TÉCNICS RESUMINT QUIBNES COMPLEXITATS TENEN I QUIN NUMERO DE PROVES FAN
# queryset = master_queryset.values('numero_plaques' ,'tecnic_realitza__cognoms_nom' ).annotate(informes_creats=Count('id')).order_by('tecnic_realitza__cognoms_nom')
    
    queryset = master_queryset.values('numero_plaques' ).annotate(recompte=Count('id')).order_by('numero_plaques')

    formatge_series_mes_actual = []
    items = 0
    altres = 0
    barres_series = []
    total = 0
    
    for row in queryset:
        
        complexitat =  row['numero_plaques']
        recompte = row['recompte']
        if complexitat == None:
            complexitat = 0
        if complexitat in (1,2,3,4,5):
            if recompte  > 0:
                formatge_series_item = (complexitat, recompte )
                formatge_series_mes_actual.append(formatge_series_item)
                barres_series_item = (complexitat, [recompte ])
                barres_series.append(barres_series_item)
        else:
            altres += recompte 
            items += 1
        total += recompte
        
    if altres > 0:
        formatge_series_item = ("Erronis", altres)
        formatge_series_mes_actual.append(formatge_series_item)
        barres_series_item = ("Erronis", [ altres ])
        barres_series.append(barres_series_item)
        
    ##
    ## ACUMULAT anual fins el mes selecionat
    ##

    master_queryset = getFilters(request,'acumulat')
    queryset = master_queryset.values('numero_plaques' ).annotate(recompte=Count('id')).order_by('numero_plaques')

    items = 0
    total_acumulat = 0
    altres_formatge = 0
    formatge_series_acumulat = []
    
    for row in queryset:
        complexitat =  row['numero_plaques']
        recompte = row['recompte']
        if complexitat == None:
            complexitat = 0
        
        if complexitat in (1,2,3,4,5):
            if recompte  > 0:
                formatge_series_item = (complexitat, recompte )
                formatge_series_acumulat.append(formatge_series_item)
#            if int(recompte ) > 0:
#                barres_series_item = (complexitat, [recompte ])
#                barres_series.append(barres_series_item)
        else:
            altres_formatge += recompte 
            items += 1
        total_acumulat += recompte
        
    if altres_formatge > 0:
        formatge_series_item = ("Erronis", altres_formatge)
        formatge_series_acumulat.append(formatge_series_item)
              
    
    grafics = [
               {'tipus': "formatge", 
                'tamany': 100,
                'titol': "Complexitat/mes",
                'descripcio': "% de complexitat realtizada de " + str(total) + " prestacions. Els erronis poden ser no marcats o incorrectes",
                'series': formatge_series_mes_actual },
                              
               {'tipus': "formatge", 
                'tamany': 100,
                'titol': "Complexitat acumulada",
                'descripcio': "% de complexitat realtizada de " + str(total_acumulat) + " prestacions. Els erronis poden ser no marcats o incorrectes",                
                'series': formatge_series_acumulat },
#                               
               {'tipus': "barres", 
                'tamany': 90,                
                'titol': "Complexitat/mes",
                'descripcio': "Quantitat de proves / complexitat ",                
                'categories': ['Complexitats'],
                'titol_y': "# de proves",
                'series': barres_series},
              

                ]
    
    # Definició
    definicio = """ Complexitat realitzada pels tècnics en radiodiagnostic i els diplomats en infermeria segons la taula de complexitat del 1 al 5.
    Nota: Queden excloses totes les prestacions marcades com a "Valoracio proves altres centres" , les sedacions i prestacions de recovery que no hi ha lloc on enregistrar la Complexitat.
    Taula: <a href="../complexitat-taula-definicio/" class="link">Taula definició de comlpexitat</a>
    
    """
    
    auditoria.request_handler(request,9,'Tipus consulta: Complexitat ; ' + str(request.session['enabled_filters'])  )
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))





@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def complexitatDetall(request):
    ################################
    # Els técnics i informes marquen  totes les proves que realitzen amb una complexitat segons una taula ( si  elpacient colabora, intubat, neonatal ...)
    # aquesta informacio la quagdren en el camp de SAP nº de plaques que ens ve en el MDO pel mateix nom  
    ################################
 
    
    master_queryset = getFilters(request,'mes')
    queryset = master_queryset.values('numero_plaques' , 'tecnic_realitza','tecnic_realitza__cognoms_nom').annotate(recompte=Count('id')).order_by('tecnic_realitza__cognoms_nom')
 
    import copy
    init_object = [0,0,0,0,0,0,0,0,0]
    taula_series_tmp = {}

    #NOM I COGNOMS i DETALL
    for prova in queryset:
        
        complexitat =  prova['numero_plaques']
        recompte = prova['recompte']
        dueter = prova['tecnic_realitza__cognoms_nom']  
        
        if dueter == None :
            try:
                if taula_series_tmp[0] == None:
                    taula_series_tmp[dueter][0]  = "<span style='color: red;'>Ningu</span>"
                    taula_series_tmp[dueter][8]  = "<a href='../complexitat-detall-dueter/0' class='addlink'></a>"
            except:
                taula_series_tmp[0] = copy.copy(init_object)
                taula_series_tmp[0][0]  = "<span style='color: red;'>Errors</span>"
                taula_series_tmp[0][8]  = "<a href='../complexitat-detall-dueter/0' class='addlink'></a>"

        else:
            if not dueter in taula_series_tmp:
                taula_series_tmp[dueter] = copy.copy(init_object)
            taula_series_tmp[dueter][0] = dueter
            if complexitat in (1,2,3,4,5):
                taula_series_tmp[dueter][complexitat] = recompte# complexitat
            else:#els marcats incorrecatment
                taula_series_tmp[dueter][6] += recompte
            #recompte total, esl que estan bé i els malament
            taula_series_tmp[dueter][7] += recompte
            taula_series_tmp[dueter][8]  = "<a href='../complexitat-detall-dueter/%s' class='addlink'></a>" % prova['tecnic_realitza']
    

    # Taula html - sort & filters    
    taula_series = [] #creem una tupla de metge i les dades
    
    inf_alres_m = [0,0,0,0,0,0,0,0]
    dades_metge_tmp=[0,0,0,0,0,0,0,0]
    #Sumem la moralla , els que no estan a la taula metge i els metge que son null en el provaohdim
    for dades_metge in taula_series_tmp.items():
        if dades_metge[1][0] == None or dades_metge[1][0] == 0:
            if dades_metge[1][1] == None: dades_metge_tmp[0]= 0
            else:dades_metge_tmp[0] = dades_metge[1][1] 
            if dades_metge[1][2] == None: dades_metge_tmp[1]=0
            else:dades_metge_tmp[1] = dades_metge[1][2]
            if dades_metge[1][3] == None:  dades_metge_tmp[2]=0
            else:dades_metge_tmp[2] = dades_metge[1][3]
            if dades_metge[1][4] == None: dades_metge_tmp[3]=0
            else:dades_metge_tmp[3] = dades_metge[1][4]
            if dades_metge[1][5] == None:  dades_metge_tmp[4]=0
            else:dades_metge_tmp[4] = dades_metge[1][5]
            if dades_metge[1][6] == None: dades_metge_tmp[5]=0
            else:dades_metge_tmp[5] = dades_metge[1][6]
                
            inf_alres_m = [ inf_alres_m[0],
                            inf_alres_m[1] + int(dades_metge_tmp[0]) ,
                            inf_alres_m[2] + int(dades_metge_tmp[1]),
                            inf_alres_m[3] + int(dades_metge_tmp[2]),
                            inf_alres_m[4] + int(dades_metge_tmp[3]),
                            inf_alres_m[5] + int(dades_metge_tmp[4]),
                            inf_alres_m[6] + int(dades_metge_tmp[5]),
                            inf_alres_m[7] ]
        else:
            if dades_metge[0]== 0:
                inf_alres_m =  dades_metge[1]
            else:
                taula_series.append((dades_metge[1][0], dades_metge[1]))
                
    if inf_alres_m[0] != 0:#afegim la suma dels que notenen nom i errors
        taula_series.append((inf_alres_m[0], inf_alres_m))
        
        
    taula_series = sorted(taula_series, key=lambda row: row[1][0])
    new_table = []
    for serie in taula_series:
        informats =float(serie[1][7] - serie[1][6])
        totals =  serie[1][7]
        if  totals != 0:
            percentatge ='{:.2%}'.format(informats/totals)
        else:
            percentatge = 0
        
        new_table.append({'dueter':serie[0], 'c1':serie[1][1], 'c2':serie[1][2], 'c3':serie[1][3], 'c4':serie[1][4],'c5':serie[1][5] ,'errors':serie[1][6],'total':serie[1][7],'percentatge':percentatge,'detall':serie[1][8] })
    table = ComplexitatDetallTable(new_table, order_by=request.GET.get('sort'))
    #table.paginate(page=request.GET.get('page', 1))
    taula_series_final = table

    # if request.GET.get('export','none') == "excel":   
    # grafics
    grafics = [
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Resum prestacions DUE/TER",
                'categories': [ 'dueter', 'c1', 'c2','c3','c4','c5', 'errros','total','emplenats','percentatge','detall'],
                'series': taula_series_final},              
                ]
 
    # Definició
    definicio = """ Compelxitat realitzada pels tècnics en radiodiagnostic i els diplomats en infermeria per tal de computar el acompliment de dades
    Nota: Queden excloses totes les prestacions marcades com a "Valoracio proves altres centres" , les sedacions i prestacions de recovery que no hi ha lloc on enregistrar la Complexitat.</P>
    <P>Taula: <a href="../complexitat-taula-definicio/" class="link">Taula deifnició comlpexitat</a></P>
    """
    
    auditoria.request_handler(request,9,'Tipus consulta: Complexitat ; ' + str(request.session['enabled_filters'])  )
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))



@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def complexitatDetallDueter(request,dueter_id):
    ################################
    # Els técnics i infermeres marquen  totes les proves que realitzen amb una complexitat segons una taula ( si  elpacient colabora, intubat, neonatal ...)
    # aquesta informacio la quagdren en el camp de SAP nº de plaques que ens ve en el MDO pel mateix nom
    # Detall de les prestacions marcades, per mostrar els errors, etc   
    ################################

    master_queryset = getFilters(request,'mes')
    
    if dueter_id == '0':
        tecnic_nom_cognoms = "Altres"
        master_queryset = master_queryset.filter( tecnic_realitza__isnull=True) 
    else:
        tecnic_nom_cognoms = UsuariSAP.objects.get(pk=long(dueter_id)).cognoms_nom
        master_queryset = master_queryset.filter( tecnic_realitza=dueter_id)
    
    proves_queryset_iterator = master_queryset.values( 
                                       
                                       'id', 
                                       'historia_clinica_id',
                                       'tipus_modalitat', 
                                       'agenda__agenda_idi__obs',
                                       'prestacio_id',
                                       'prestacio__descripcio',
                                       'metge_informa__cognoms_nom', 
                                       'tecnic_realitza',
                                       'tecnic_realitza__cognoms_nom',
                                       'data_cita',
                                       'hora_cita',
                                       'data_inici_prestacio',
                                       'hora_inici_prestacio',
                                       'data_fi_prestacio',
                                       'hora_fi_prestacio',
                                       'numero_plaques',
                                       #'numero_projeccions',
                                       #'quantitat',
                                       #'numero_projeccions_erronies'
                                       
                                       ).order_by('tipus_modalitat', 'agenda', 'historia_clinica_id', 'data_cita', 'hora_cita').iterator()

    import copy
    init_object = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    #icona_oky= '<IMG SRC="/static/admin/img/admin/icon_success.gif" BORDER=0 ALT="Fet">'
    taula_proves = {}
    taula_detall = []

    for prova in proves_queryset_iterator:
        
        taula_proves[prova['id']] = copy.copy(init_object)
        taula_proves[prova['id']][1]  = prova

        if prova['data_cita'] == None  or  prova['hora_cita'] == None: 
            data_i_hora_cita= '-'
        else:  
            data_i_hora_cita =  datetime.datetime(prova['data_cita'].year, prova['data_cita'].month, prova['data_cita'].day, prova['hora_cita'].hour, prova['hora_cita'].minute, prova['hora_cita'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_inici_prestacio'] == None  or  prova['hora_inici_prestacio'] == None:
            data_i_hora_inici_prestacio = '-'
        else:
            data_i_hora_inici_prestacio = datetime.datetime(prova['data_inici_prestacio' ].year, prova['data_inici_prestacio'].month, prova['data_inici_prestacio'].day, prova['hora_inici_prestacio'].hour, prova['hora_inici_prestacio'].minute, prova['hora_inici_prestacio'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_fi_prestacio'] == None  or  prova['hora_fi_prestacio'] == None:
            data_i_hora_fi_prestacio = '-'      
        else:
            data_i_hora_fi_prestacio = datetime.datetime(prova['data_fi_prestacio'].year, prova['data_fi_prestacio'].month, prova['data_fi_prestacio'].day, prova['hora_fi_prestacio'].hour, prova['hora_fi_prestacio'].minute, prova['hora_fi_prestacio'].second).strftime('%Y/%m/%d %H:%M')

        taula_detall.append((prova['id'],( prova['historia_clinica_id'],
                                           prova['agenda__agenda_idi__obs'],
                                           prova['prestacio__descripcio'] ,
                                           prova['metge_informa__cognoms_nom'],
                                           data_i_hora_cita,
                                           data_i_hora_inici_prestacio,
                                           data_i_hora_fi_prestacio,
                                           prova['numero_plaques']
                                           #,prova['numero_projeccions'],
                                           #prova['quantitat'],
                                           #prova['numero_projeccions_erronies']
                                           ) 
                            ))
    # grafics
    new_table = []
    for row in taula_detall:
        new_table.append({'pk':row[0], 
                          'hc':row[1][0],
                          'agenda':row[1][1], 
                          'prestacio':row[1][2],
                          'metge_informa':row[1][3], 
                          'data_i_hora_cita':row[1][4], 
                          'data_i_hora_inici_prestacio':row[1][5], 
                          'data_i_hora_fi_prestacio':row[1][6],
                          'numero_plaques':row[1][7]
                        #  ,'numero_projeccions':row[1][8],
                        #  'quantitat':row[1][9],
                        #  'numero_projeccions_erronies':row[1][9],
                          })
    table = ComplexitatDetallDueterTable(new_table, order_by=request.GET.get('sort'))
    #table.paginate(page=request.GET.get('page', 1))
    
    taula_detall = table
    nregistres= str(len(new_table))
    
    grafics = [
              
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Detall prestacions realitzades per: " + tecnic_nom_cognoms  + ".    Informes: " + nregistres,
               # 'categories': [ 'Història clínica','Exploració', 'Agenda','Informat', 'Revisat', 'Alliberat', 'Codificació', 'Data exploració', 'Data informe' ,'Data revisió' ,'validat','Modificar'],
                'series': taula_detall},
                ]


        # Definició
    definicio = """Detall dels totes les prestacions que ha realitzat el professional sel·leccionat. 
    <BR>Nota: Queden excloses totes les prestacions marcades com a "Valoracio proves altres centres" , les sedacions i prestacions de recovery que no hi ha lloc on enregistrar la Complexitat.</BR>
    <BR>Taula: <a href="../complexitat-taula-definicio/" class="link">Taula deifnició comlpexitat</a></BR>
    """
    auditoria.request_handler(request,9,'Tipus consulta: Complexitat detall; ' + str(request.session['enabled_filters']) + ' ,dueter_id:' + str(dueter_id))
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request)) 
