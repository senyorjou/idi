from django.conf.urls.defaults import url, patterns

urlpatterns = patterns('apps.quadre_comandament.objectius.tecnics_infermers.views',
    url(r'^$', 'index'), #    
    url(r'^tecnics_infermers/$', 'tecnicsInfermers'),
    url(r'^complexitat/$', 'complexitat'),
    url(r'^complexitat-due-ter/$', 'complexitatDetall'),
    url(r'^complexitat-detall-dueter/(?P<dueter_id>\d+)', 'complexitatDetallDueter'),
    
    url(r'^complexitat-taula-definicio/$', 'complexitatTaulaDefinicio'),
    
)
