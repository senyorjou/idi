# coding: utf8
#from apps import django_tables2 as tables
import django_tables2 as tables
from django.utils.safestring import mark_safe
from apps.django_tables2.utils import A  # alias for Accessor
        

class HtmlColumn(tables.Column):
    def render(self, value):
        return mark_safe(value)
        
class ComplexitatDetallTable(tables.Table):
    
    dueter = HtmlColumn(verbose_name='Due/Ter')
    c1 = tables.Column(verbose_name='1')
    c2 = tables.Column(verbose_name='2')
    c3 = tables.Column(verbose_name='3')
    c4 = tables.Column(verbose_name='4')
    c5 = tables.Column(verbose_name='5')
    errors = tables.Column(verbose_name='Errors')
    total = tables.Column(verbose_name='Total')
    percentatge = tables.Column(verbose_name='Percentatge')
    detall = HtmlColumn(verbose_name='Detall') 
    class Meta:
        attrs = {'class': 'paleblue'}

        
class ComplexitatDetallDueterTable(tables.Table):
    pk = tables.Column(verbose_name='pk') 
    hc = tables.Column(verbose_name='Història')
    agenda = tables.Column(verbose_name='Agenda')
    prestacio = tables.Column(verbose_name='Exploració')
    data_i_hora_cita = tables.Column(verbose_name='Data cita')
    data_i_hora_inici_prestacio = tables.Column(verbose_name='Data inici prest.')
    data_i_hora_fi_prestacio = tables.Column(verbose_name='Data fi prest.')
    numero_plaques = tables.Column(verbose_name='Complexitat')
    class Meta:
        attrs = {'class': 'paleblue'}
        #order_by = 'name'
         