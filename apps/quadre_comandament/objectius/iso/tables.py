# coding: utf8
#from apps import django_tables2 as tables
import django_tables2 as tables
from django.utils.safestring import mark_safe
from apps.django_tables2.utils import A  # alias for Accessor
        
        


class HtmlColumn(tables.Column):
    def render(self, value):
        return mark_safe(value)
#todo: aixo es pot fer en una fucio que reotrni la clase perque son identiques pero canviant poques coses

class DetallProvesRealitzadesDetall(tables.Table):
    pk = tables.Column(verbose_name='pk') 
    hc = tables.Column(verbose_name='Història')
    agenda = tables.Column(verbose_name='Agenda')
    exploracio = tables.Column(verbose_name='Exploració')
    informat =  tables.Column(verbose_name='Informat per')
    data_sollicitud_prestacio =  tables.Column(verbose_name='Solicitat')
    data_exploracio = tables.Column(verbose_name='Explorat')
    data_informe = tables.Column(verbose_name='Informat')
    hospitalitzat = HtmlColumn(verbose_name='Ingressat')
    tipus_episodi = tables.Column(verbose_name='T. episodi')
    dif_hores = tables.Column(verbose_name='temps. hores')
    # no chuta a mirar . .. codgen_id = tables.LinkColumn('apps.quadre_comandament.objectius.radiolegs.views.informesRealitzatsDetallModificar', args=[A('codgen_id')])
   
  
    
    class Meta:
        attrs = {'class': 'paleblue'}
        #order_by = 'name'
        
    