from django.conf.urls.defaults import url, patterns

urlpatterns = patterns('apps.quadre_comandament.objectius.iso.views',
    url(r'^$', 'index'), #    
    url(r'^proves-realitzades/$', 'proves_realitzades'),
    url(r'^detall/$', 'proves_detall'),
    url(r'^t-sollicitud-i-exploracio/$', 'temps_entre_sollicitud_i_exploracio'),
    url(r'^t-exploracio-i-informe/$', 'temps_entre_exploracio_i_informe'),
    url(r'^t-sollicitud-i-informe/$', 'temps_entre_sollicitud_i_informe'),
    
      
)
