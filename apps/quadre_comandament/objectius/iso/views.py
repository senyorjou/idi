# coding: utf8
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from apps.quadre_comandament.common import initFilters
from apps.quadre_comandament.models import ProvaOhdim
from apps.quadre_comandament.objectius.iso.tables import DetallProvesRealitzadesDetall
import calendar
import datetime
import apps.meta.auditoria
from django.db.models import Q,Count


indicadors = ['Proves realitzades', 'T. sol·licitud i exploració', 'T. exploració i informe' ,'T. sol·licitud i informe']
indicadors.append('Detall')    
        
objectiu = "Indicadors de qualitat ISO"
objectiu_url = "iso"

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def index(request):
    request.session['enabled_filters'] = {}
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                          

# TODO: Falta controlar els permisos perquè els tots sigui el tots dels permisos que tenen els usuaris.
# TODO: Canvi data finalització prestació .
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def proves_realitzades(request):
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'iso' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2011, 'unitat': 0, 'modalitat': 0 }
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]
    
    # Define master query
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    proves = ProvaOhdim.objects.using('portal_dades'
                            ).filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                     agenda__agenda_idi__es_ics=False,
                                     data_fi_prestacio__isnull=False
                            ).filter( Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE'))
                              
    if request.session['enabled_filters']['unitat'] != 0:
        proves = proves.filter(agenda__agenda_idi__unitat_idi_id = request.session['enabled_filters']['unitat'])

    if request.session['enabled_filters']['modalitat'] != 0:
        proves = proves.filter(agenda__agenda_idi__idtipoexploracion=request.session['enabled_filters']['modalitat'])
    
    
    taula_series = []
    any_sel = int(request.session['enabled_filters']['any'])

#TODO: podem feraixó vaiogoi mes rapid ara fau n subselesnt!!!! , podem fer una selct i despres sobre aquestos el singressats!! per soft!!     
    
    # Pacients totals
    row = ("<b>Prestacions totals</b>", [])
    for mes in range(1, 13):
        dies_mes = calendar.monthrange(any_sel, mes)
        start_date = datetime.date(any_sel, mes, 1)
        end_date = datetime.date(any_sel, mes, dies_mes[1])
        num_proves = proves.values('id').filter(data_fi_prestacio__range=(start_date, end_date)).count()
        row[1].append(num_proves)

    taula_series.append(row)



    #pacientes ingressats hospitalitzacio    
    row = ("<b>Prestacions pacients ingressats</b>", [])
    for mes in range(1, 13):
        dies_mes = calendar.monthrange(any_sel, mes)
        start_date = datetime.date(any_sel, mes, 1)
        end_date = datetime.date(any_sel, mes, dies_mes[1])
        # TODO: Caldria que el contains fos una expresió regular
        num_proves = proves.values('id','ut_solicitant_codi').filter(
                                   #aixo no es certservei_solicitant__tipus_servei__es_hospital=True,   
                                   #tipus_episodi='H',
                                   #aixo funciona pero lent 
                                   ut_solicitant_codi__contains='UH',
                                   #servei_solicitant__es_hospitalitzat= True, i aixo no va perque hi ha molts serveis sense 
                                   data_fi_prestacio__range=(start_date, end_date)).count()
        row[1].append(num_proves)
        
    taula_series.append(row)
    


    # Grafics
    grafics = [
               {'tipus': "linies", 
                'tamany': 100,                
                'titol': "Prestacions realitzades",
                'descripcio': "Número de prestacions realitzades",
                'categories': [ 'Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre',
                                'Octubre', 'Novembre', 'Desembre'],
                'series': taula_series},              
                ]
    
    # Definició
    definicio = """Aquesta és l'evolució de l'activitat realitzada (no facturada) durant l'any. Aquesta activitat
    es calcula sumant totes les prestacions realitzades. </br></br>
    Les prestacions a pacients ingressats, són aquelles que provenen de serveis peticionaris hospitalaris i en les què
    el pacient estava ingressat en la data de l'exploració. </br></br>
    Les prestacions totals inclouen les dels pacients ingressats.
     """
    apps.meta.auditoria.request_handler(request,7,'Tipus consulta: proves_realitzades ; ' + str(request.session['enabled_filters']) + ' ')
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url,
                               'definicio':definicio,
                               'grafics': grafics},
                              context_instance=RequestContext(request))                          

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def temps_entre_sollicitud_i_exploracio(request):
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'iso' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2011, 'unitat': 0, 'modalitat': 0}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    # Define master query
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    proves = ProvaOhdim.objects.using('portal_dades'
                            ).filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                     agenda__agenda_idi__es_ics=False,
                                     data_fi_prestacio__isnull=False 
                            ).filter( Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')) 
    if request.session['enabled_filters']['unitat'] == 0:
        proves = proves.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))
    else:
        proves = proves.filter(agenda__agenda_idi__unitat_idi_id = request.session['enabled_filters']['unitat'])

    if request.session['enabled_filters']['modalitat'] != 0:
        proves = proves.filter(agenda__agenda_idi__idtipoexploracion=request.session['enabled_filters']['modalitat'])
    
    taula_series = []
    
    #PROVES INGRESSATS
    row = ("<b>Prestacions pacients ingressats</b>", [])
    any_sel = int(request.session['enabled_filters']['any'])
    for mes in range(1, 13):
        dies_mes = calendar.monthrange(any_sel, mes)
        start_date = datetime.date(any_sel, mes, 1)
        end_date = datetime.date(any_sel, mes, dies_mes[1])
        
        # TODO: Caldria que el contains fos una expresió regular
        data_solicitut = proves.filter(#tipus_episodi='H',
                                       #servei_solicitant__es_hospital=True    ,
                                       ut_solicitant_codi__contains='UH',
                                       
                                       data_fi_prestacio__range=(start_date, end_date)                                    
                                ).extra( select={'avg': " cast(  avg( " +
                                                  "TIMESTAMPDIFF( hour,ADDTIME(data_sollicitud_prestacio ,if( `hora_sollicitud_prestacio` is Null,'0' ,  hora_sollicitud_prestacio ))," + 
                                                                     " ADDTIME(data_fi_prestacio ,if( `hora_fi_prestacio` is Null,'0' ,  hora_fi_prestacio )) ) ) as UNSIGNED)"}
                                ).values('avg')
                                
        if data_solicitut[0]['avg'] != None:
            dies = str(data_solicitut[0]['avg']).replace(',', '.')
        else:
            dies = 0
        row[1].append(dies)
        
    taula_series.append(row)
    #PROVES TOTS
    row = ("<b>Prestacions totals</b>", [])
    for mes in range(1, 13):
        dies_mes = calendar.monthrange(any_sel, mes)
        start_date = datetime.date(any_sel, mes, 1)
        end_date = datetime.date(any_sel, mes, dies_mes[1])
        
        data_solicitut = proves.filter(data_fi_prestacio__range=(start_date, end_date)                                    
                                ).extra( select={'avg': " cast(  avg( " +
                                                                "TIMESTAMPDIFF( hour,ADDTIME(data_sollicitud_prestacio ,if( `hora_sollicitud_prestacio` is Null,'0' ,  hora_sollicitud_prestacio ))," + 
                                                                                   " ADDTIME(data_fi_prestacio ,if( `hora_fi_prestacio` is Null,'0' ,  hora_fi_prestacio )) ) )  as UNSIGNED )   "}

                                                                        ).values('avg')
        if data_solicitut[0]['avg'] != None:
            dies = str(data_solicitut[0]['avg']).replace(',', '.')
        else:
            dies = 0
        row[1].append(dies)
        
    taula_series.append(row)

    # Grafics
    grafics = [
               {'tipus': "linies", 
                'tamany': 100,                
                'titol': "Temps promig en hores",
                'descripcio': "Temps promig en hores entre data de solicitud i data d'exploració de prestacions fetes",
                'categories': [ 'Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre',
                                'Octubre', 'Novembre', 'Desembre'],
                'series': taula_series},  
                ]
  
    # Definició
    definicio = """Evolució del temps promig en hores entre la data de sol·licitud, i la data de realització de l'exploració
    de tota l'activitat realitzada. En aquest temps promig no es fa res amb els caps de setmana, és a dir, el temps 
    d'una prova sol·licitada en un divendres i realitzada en un dilluns, és de 3 dies. </br></br>
    Les prestacions a pacients ingressats, són aquelles que provenen de serveis peticionaris hospitalaris i en les què
    el pacient estava ingressat en la data de l'exploració. </br></br>
    Les prestacions totals inclouen les dels pacients ingressats.
     """
    apps.meta.auditoria.request_handler(request,7,'Tipus consulta:  temps_entre_sollicitud_i_exploracio ; ' + str(request.session['enabled_filters']) + ' ')
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url,
                               'definicio':definicio,
                               'grafics': grafics},
                              context_instance=RequestContext(request))   
    
    
    
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def temps_entre_exploracio_i_informe(request):
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'iso' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2011, 'unitat': 0, 'modalitat': 0}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    # Define master query
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    proves = ProvaOhdim.objects.using('portal_dades'
                            ).filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                     agenda__agenda_idi__es_ics=False,
                                     data_fi_prestacio__isnull=False,
                                     data_document_informat__isnull=False
                            ).filter( Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE'))  
    if request.session['enabled_filters']['unitat'] == 0:
        proves = proves.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))
    else:
        proves = proves.filter(agenda__agenda_idi__unitat_idi_id = request.session['enabled_filters']['unitat'])

    if request.session['enabled_filters']['modalitat'] != 0:
        proves = proves.filter(agenda__agenda_idi__idtipoexploracion=request.session['enabled_filters']['modalitat'])
             
    taula_series = []
    any_sel = int(request.session['enabled_filters']['any'])
    row = ("<b>Prestacions pacients ingressats</b>", [])
    for mes in range(1, 13):
        dies_mes = calendar.monthrange(any_sel, mes)
        start_date = datetime.date(any_sel, mes, 1)
        end_date = datetime.date(any_sel, mes, dies_mes[1])
        
        data_solicitut = proves.filter(data_fi_prestacio__range=(start_date, end_date),
                                       #tipus_episodi='H',
                                       ut_solicitant_codi__contains='UH', 
                                       ).extra( select={'avg': " cast(  avg( " +
                                                                "TIMESTAMPDIFF( hour, ADDTIME(data_fi_prestacio ,if( `hora_fi_prestacio` is Null,'0' ,  hora_fi_prestacio ))," + 
                                                                                   "  ADDTIME(data_document_informat ,if( `hora_document_informat` is Null,'0' ,  hora_document_informat ))                ) )  as UNSIGNED )   "}
                                ).values('avg')
                                
        if data_solicitut[0]['avg'] != None:
            dies = str(data_solicitut[0]['avg']).replace(',', '.')
        else:
            dies = 0
        row[1].append(dies)
        
    taula_series.append(row)

    row = ("<b>Prestacions totals</b>", [])
    for mes in range(1, 13):
        dies_mes = calendar.monthrange(any_sel, mes)
        start_date = datetime.date(any_sel, mes, 1)
        end_date = datetime.date(any_sel, mes, dies_mes[1])
        
        data_solicitut = proves.filter(data_fi_prestacio__range=(start_date, end_date)                                                                            
                                ).extra( select={'avg': " cast(  avg( " +
                                                                "TIMESTAMPDIFF( hour, ADDTIME(data_fi_prestacio ,if( `hora_fi_prestacio` is Null,'0' ,  hora_fi_prestacio ))," + 
                                                                                   "  ADDTIME(data_document_informat ,if( `hora_document_informat` is Null,'0' ,  hora_document_informat ))                ) )  as UNSIGNED )   "}
                                ).values('avg')

        if data_solicitut[0]['avg'] != None:
            dies = str(data_solicitut[0]['avg']).replace(',', '.')
        else:
            dies = 0
        row[1].append(dies)
        
    taula_series.append(row)

    # Grafics
    grafics = [
               {'tipus': "linies", 
                'tamany': 100,                
                'titol': "Temps promig en hores",
                'descripcio': "Temps promig en hores entre data de l'exploració i data d'informe de les prestacions",
                'categories': [ 'Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre',
                                'Octubre', 'Novembre', 'Desembre'],
                'series': taula_series},  
                              
                ]

    # Definició
    definicio = """Evolució del temps promig en hores entre la realització de l'exploració i l'alliberament de l'informe de tota
    l'activitat realitzada. En aquest temps promig no s'han tingut en compte els caps de setmana, és a dir, el temps
    d'una prova realitzada en un divendres i alliberada en un dilluns, és de 3 dies. </br></br>
    Les prestacions a pacients ingressats, són aquelles que provenen de serveis peticionaris hospitalaris i en les què
    el pacient estava ingressat en la data de l'exploració. </br></br>
    Les prestacions totals inclouen les dels pacients ingressats.
     """  
    apps.meta.auditoria.request_handler(request,7,'Tipus consulta:  temps_entre_exploracio_i_informe ; ' + str(request.session['enabled_filters']) + ' ')    
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url,
                               'definicio':definicio,
                               'grafics': grafics},
                              context_instance=RequestContext(request))   


@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def temps_entre_sollicitud_i_informe(request):
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'iso' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2011, 'unitat': 0, 'modalitat': 0}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    # Define master query
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    proves = ProvaOhdim.objects.using('portal_dades'
                            ).filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                     
                                     agenda__agenda_idi__es_ics=False,
                                     data_fi_prestacio__isnull=False,
                                     data_document_informat__isnull=False
                                     ).filter( Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE'))  
    if request.session['enabled_filters']['unitat'] == 0:
        proves = proves.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))
    else:
        proves = proves.filter(agenda__agenda_idi__unitat_idi_id = request.session['enabled_filters']['unitat'])

    if request.session['enabled_filters']['modalitat'] != 0:
        proves = proves.filter(agenda__agenda_idi__idtipoexploracion=request.session['enabled_filters']['modalitat'])
        
    proves.filter(data_document_informat__isnull=False)
             
    taula_series = []
    any_sel = int(request.session['enabled_filters']['any'])
    row = ("<b>Prestacions pacients ingressats</b>", [])
    for mes in range(1, 13):
        dies_mes = calendar.monthrange(any_sel, mes)
        start_date = datetime.date(any_sel, mes, 1)
        end_date = datetime.date(any_sel, mes, dies_mes[1])
        
        data_solicitut = proves.filter(data_fi_prestacio__range=(start_date, end_date),
                                       #tipus_episodi='H',
                                       ut_solicitant_codi__contains='UH', 
                                ).extra( select={'avg': " cast(  avg( " +
                                                                "TIMESTAMPDIFF( hour, ADDTIME(data_sollicitud_prestacio ,if( `hora_sollicitud_prestacio` is Null,'0' ,  hora_sollicitud_prestacio ))," + 
                                                                                   "  ADDTIME(data_document_informat ,if( `hora_document_informat` is Null,'0' ,  hora_document_informat ))                ) )  as UNSIGNED )   "}
                                ).values('avg')

        if data_solicitut[0]['avg'] != None:
            dies = str(data_solicitut[0]['avg']).replace(',', '.')
        else:
            dies = 0
        row[1].append(dies)
        
    taula_series.append(row)

    row = ("<b>Prestacions totals</b>", [])
    for mes in range(1, 13):
        dies_mes = calendar.monthrange(any_sel, mes)
        start_date = datetime.date(any_sel, mes, 1)
        end_date = datetime.date(any_sel, mes, dies_mes[1])
        
        data_solicitut = proves.filter(data_fi_prestacio__range=(start_date, end_date)                                                                            
                                ).extra( select={'avg': " cast(  avg( " +
                                                                "TIMESTAMPDIFF( hour, ADDTIME(data_sollicitud_prestacio ,if( `hora_sollicitud_prestacio` is Null,'0' ,  hora_sollicitud_prestacio ))," + 
                                                                                   "  ADDTIME(data_document_informat ,if( `hora_document_informat` is Null,'0' ,  hora_document_informat ))                ) )  as UNSIGNED )   "}
#SI VOLEM FILTRAR ELS QUE ESTAN PER SOBRE LES X hores 
#                               ).extra( where={" cast((TIMESTAMPDIFF( hour, ADDTIME(data_sollicitud_prestacio ,if( `hora_sollicitud_prestacio` is Null,'0' ,  hora_sollicitud_prestacio )),  ADDTIME(data_document_informat ,if( `hora_document_informat` is Null,'0' ,  hora_document_informat )) ))  as UNSIGNED)  > 74" }                                        
                                ).values('avg')
                                
        if data_solicitut[0]['avg'] != None:
            dies = str(data_solicitut[0]['avg']).replace(',', '.')
        else:
            dies = 0
        row[1].append(dies)
        
    taula_series.append(row)

    # Grafics
    grafics = [
               {'tipus': "linies", 
                'tamany': 100,                
                'titol': "Temps promig en hores",
                'descripcio': "Temps promig en hores entre data de l'exploració i data d'informe de les prestacions",
                'categories': [ 'Gener', 'Febrer', 'Març', 'Abril', 'Maig', 'Juny', 'Juliol', 'Agost', 'Setembre',
                                'Octubre', 'Novembre', 'Desembre'],
                'series': taula_series},  
                              

                ]

    # Definició
    definicio = """Evolució del temps promig en hores entre la realització de l'exploració i l'alliberament de l'informe de tota
    l'activitat realitzada. En aquest temps promig no s'han tingut en compte els caps de setmana, és a dir, el temps
    d'una prova realitzada en un divendres i alliberada en un dilluns, és de 3 dies. </br></br>
    Les prestacions a pacients ingressats, són aquelles que provenen de serveis peticionaris hospitalaris i en les què
    el pacient estava ingressat en la data de l'exploració. </br></br>
    Les prestacions totals inclouen les dels pacients ingressats.
     """  
    
    
    apps.meta.auditoria.request_handler(request,7,'Tipus consulta:  temps_entre_sollicitud_i_informe ; ' + str( request.session['enabled_filters'] )+ ' ')
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url,
                               'definicio':definicio,
                               'grafics': grafics,
                               'detall':True,
 
                               },
                              context_instance=RequestContext(request))   





@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def proves_detall(request):
    # Cal definir els filtres que volem per defecte
 
    request.session['current_filters'] = 'iso_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes':0 }
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    anys = request.session['enabled_filters']['any']
    mes = request.session['enabled_filters']['mes']
   
  
    if mes != 0:
        dies_mes = calendar.monthrange(anys, mes)
        start_date = datetime.date(anys, mes, 1)
        end_date = datetime.date(anys, mes, dies_mes[1])
    else:
        start_date = datetime.date(anys, 1, 1)
        end_date = datetime.date(anys, 12, 31) 
  

    # Define master query
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    proves = ProvaOhdim.objects.using('portal_dades'
                            ).filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                     agenda__agenda_idi__es_ics=False,
                                     data_fi_prestacio__isnull=False,
                                     data_document_informat__isnull=False
                                     ).filter( Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE'))  
    if request.session['enabled_filters']['unitat'] == 0:
        proves = proves.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))
    else:
        proves = proves.filter(agenda__agenda_idi__unitat_idi_id = request.session['enabled_filters']['unitat'])

    if request.session['enabled_filters']['modalitat'] != 0:
        proves = proves.filter(agenda__agenda_idi__idtipoexploracion=request.session['enabled_filters']['modalitat'])
        
    taula_series = []
    
    #TODO: posar diferencia entre hores en extra
    proves_queryset_iterator = proves.filter(data_fi_prestacio__range=(start_date, end_date)
                                ).extra( select={'dif_hores': " cast(  ( " +
                                                                "TIMESTAMPDIFF( hour, ADDTIME(data_sollicitud_prestacio ,if( `hora_sollicitud_prestacio` is Null,'0' ,  hora_sollicitud_prestacio ))," + 
                                                                                   "  ADDTIME(data_document_informat ,if( `hora_document_informat` is Null,'0' ,  hora_document_informat ))                ) )  as UNSIGNED )   "}
                                # SI VOLEM FILTRAR ELS QUE ESTAN PER SOBRE LES X hores
                                #).extra( where={" cast((TIMESTAMPDIFF( hour, ADDTIME(data_sollicitud_prestacio ,if( `hora_sollicitud_prestacio` is Null,'0' ,  hora_sollicitud_prestacio )),  ADDTIME(data_document_informat ,if( `hora_document_informat` is Null,'0' ,  hora_document_informat )) ))  as UNSIGNED)  > 74" }
                                ).values('id',
                                         'historia_clinica_id',
                                         'tipus_modalitat', 
                                         'agenda__agenda_idi__obs',
                                         'prestacio_id',
                                         'prestacio__descripcio',
                                         'metge_informa','metge_revisa',
                                         'metge_allibera',
                                         'metge_informa__cognoms_nom','metge_revisa__cognoms_nom','metge_allibera__cognoms_nom', 
                                         'data_cita',
                                         'hora_cita',
                                         'data_fi_prestacio',
                                         'hora_fi_prestacio',
                                         'data_sollicitud_prestacio',
                                         'hora_sollicitud_prestacio',
                                         'data_document_informat',
                                         'hora_document_informat',
                                         'data_document_revisat',
                                         'hora_document_revisat',
                                         'ut_solicitant_codi',
                                         'tipus_episodi',
                                         'dif_hores' # es el extra!!!,
                                         
                                         
                                       ).order_by('tipus_modalitat', 'agenda', 'historia_clinica_id', 'data_cita', 'hora_cita').iterator()

    import copy
    init_object = [0,0,0,0,0,0,0,0,0,0,0,0,0]
    icona_oky= '<IMG SRC="/static/admin/img/admin/icon_success.gif" BORDER=0 ALT="Fet">'

    #NOM I COGNOMS i DETALL
    taula_proves = {}
    #rows_duplicats = copy.copy(proves_queryset_iterator)   No es pot ducplicar un iterador¿?
    taula_detall = []
    
    for prova in proves_queryset_iterator:
        
        if prova['metge_allibera'] == None: 
            metge_allibera = 'Sense dades' 
        else: 
            metge_allibera =  prova['metge_allibera__cognoms_nom']
            
        hospitalitzat = ' '
        if 'UH' in (prova['ut_solicitant_codi']) :
            hospitalitzat = icona_oky

        if prova['data_fi_prestacio'] == None  or  prova['hora_fi_prestacio'] == None: 
            data_fi_prestacio= '-'
        else:  
            data_fi_prestacio =  datetime.datetime(prova['data_fi_prestacio'].year, prova['data_fi_prestacio'].month, prova['data_fi_prestacio'].day, prova['hora_fi_prestacio'].hour, prova['hora_fi_prestacio'].minute, prova['hora_fi_prestacio'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_sollicitud_prestacio'] == None  or  prova['hora_sollicitud_prestacio'] == None:
            data_sollicitud_prestacio = '-'
        else:
            data_sollicitud_prestacio = datetime.datetime(prova['data_sollicitud_prestacio' ].year, prova['data_sollicitud_prestacio'].month, prova['data_sollicitud_prestacio'].day, prova['hora_sollicitud_prestacio'].hour, prova['hora_sollicitud_prestacio'].minute, prova['hora_sollicitud_prestacio'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_document_revisat'] == None  or  prova['hora_document_revisat'] == None:
            data_i_hora_doc_rev = '-'      
        else:
            data_i_hora_doc_rev = datetime.datetime(prova['data_document_revisat'].year, prova['data_document_revisat'].month, prova['data_document_revisat'].day, prova['hora_document_revisat'].hour, prova['hora_document_revisat'].minute, prova['hora_document_revisat'].second).strftime('%Y/%m/%d %H:%M')
        
        if hospitalitzat == ' ':#ambulatori
            if prova['dif_hores'] ==None:
                dif_hores = ''
            else:
                dif_hores = str(int(prova['dif_hores'])/24) + ' dies'
        else:  
            dif_hores = str(prova['dif_hores']) + ' h'
            
        taula_detall.append((prova['id'],( prova['historia_clinica_id'],
                                           prova['agenda__agenda_idi__obs'],
                                           prova['prestacio__descripcio'] ,
                                           metge_allibera,
                                           data_sollicitud_prestacio,
                                           data_fi_prestacio,
                                           data_i_hora_doc_rev,
                                           hospitalitzat,
                                           prova['tipus_episodi'],
                                           dif_hores
                                           ) 
                             )
                            )
#TODO: revisar que totes les funcions facin servir document alliberat!!!! i no pas informat!!!
#TODO: revisar totes les funcions de data , i si no hi ha la hora possar la data almenys  aradilegs també!!!

   # grafics
    new_table = []
    for row in taula_detall:
        new_table.append({'pk':row[0], 
                          'hc':row[1][0],
                          'agenda':row[1][1], 
                          'exploracio':row[1][2],
                          'informat':row[1][3], 
                          'data_sollicitud_prestacio':row[1][4], 
                          'data_exploracio':row[1][5], 
                          'data_informe':row[1][6],
                          'hospitalitzat':row[1][7],
                          'tipus_episodi':row[1][8],
                          'dif_hores':row[1][9]
                          })
        
    nregistres= str(len(new_table))


    table = DetallProvesRealitzadesDetall(new_table, order_by=request.GET.get('sort'))
    #table.paginate(page=request.GET.get('page', 1))
    
    
    taula_detall = table
    nregistres= str(len(new_table))
    
    grafics = [
              
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Detall presatacions realitzades: " + nregistres ,
                'series': taula_detall},
                ]

    # Definició
    definicio = """Detall de tota
    l'activitat realitzada. apareix el temps entre que es demana la prestació i l'informe es alliberat. </br></br>
    Les prestacions a pacients ingressats, són aquelles que provenen de serveis peticionaris hospitalaris i en les què
    el pacient estava ingressat en la data de l'exploració. </br></br>
    Les prestacions totals inclouen les dels pacients ingressats.
     """  
    apps.meta.auditoria.request_handler(request,7,'Tipus consulta:  detall ; ' + str(request.session['enabled_filters']) + ' ') 
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':indicadors,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url,
                               'definicio':definicio,
                               'grafics': grafics,
                               'detall':True },
                              context_instance=RequestContext(request))   