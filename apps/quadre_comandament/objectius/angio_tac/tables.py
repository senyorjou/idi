# coding: utf8
#from apps import django_tables2 as tables
import django_tables2 as tables
from django.utils.safestring import mark_safe
from apps.django_tables2.utils import A  # alias for Accessor
        
        


class HtmlColumn(tables.Column):
    def render(self, value):
        return mark_safe(value)

 
class RadiolegsAngioTable(tables.Table):
    
    
    radioleg = HtmlColumn(verbose_name='Radiòlegs')
    p_altres = tables.Column(verbose_name='P. Altres')
    p_ictus = tables.Column(verbose_name='P. ICTUS')
    p_hsa = tables.Column(verbose_name='P. H.S.A.')
    total = tables.Column(verbose_name='Consum total')
    detall = HtmlColumn(verbose_name='Detall') 
    
    class Meta:
        attrs = {'class': 'paleblue'}
            
 
 
 
class RadiolegsAngioTableDetall(tables.Table):
    
    
    data_prestacio = HtmlColumn(verbose_name='Data intervenció')
    nhistoria = tables.Column(verbose_name='NHC Pacient')
    pacient_nom = tables.Column(verbose_name='Pacient')
    programa = tables.Column(verbose_name='Programa.')
    obs = tables.Column(verbose_name='Observacions')
    total = tables.Column(verbose_name='Total consum')
    detall = HtmlColumn(verbose_name='Detall') 
    
    class Meta:
        attrs = {'class': 'paleblue'}
        

 
class RadiolegsAngioTableIntervencioDetall(tables.Table):
    
    movimient_id = HtmlColumn(verbose_name='Id Moviment')
    codi_barres = tables.Column(verbose_name='Codi barres')
    producte = tables.Column(verbose_name='Producte')
    cost = tables.Column(verbose_name='Cost')
    
    class Meta:
        attrs = {'class': 'paleblue'}
         
        
 
  