from django.conf.urls.defaults import url, patterns

urlpatterns = patterns('apps.quadre_comandament.objectius.angio_tac.views',
    url(r'^$', 'index'), #    
    url(r'^programes-angio/$', 'programesAngiograf'),
    url(r'^programes-angio-detall/(?P<metge_id>\d+)', 'programesAngiografDetall'),
    url(r'^programes-angio-detall/', 'programesAngiograf'),
    url(r'^programes-angio-intervencio-detall/(?P<intervencio_id>\d+)', 'programesAngiografIntervencioDetall'),
    url(r'^programes-angio-intervencio-detall/', 'programesAngiograf'),


)
