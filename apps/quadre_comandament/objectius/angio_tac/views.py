# coding: utf8

from __future__ import division# per fer divions i que retorniel reste /
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from apps.quadre_comandament.common import initFilters
import datetime,calendar,time
from apps.meta.models import AuthUser,AuthUserProfile
from apps.meta import auditoria
from django.db import connections


from apps.quadre_comandament.objectius.angio_tac.tables import RadiolegsAngioTable, RadiolegsAngioTableDetall,RadiolegsAngioTableIntervencioDetall


indicadors = []
objectiu = "Gestió del angio-tac del Hosp. Vall d'Hebrón."
objectiu_url = "angio-tac"

def get_indicadors(request):
    # Recordem que no es pot canviar una variable global ( per molts motius , multiples procesos ... etc)
    indicadors_local =  indicadors
    #mirem si te permisos per validar , si en te mostrem el boto i el formulari per poder fer-ho , 15 i 17 informatics, 26 cap unitat, 27 director unitat
    if AuthUserProfile.objects.get( user = AuthUser.objects.get(username =str(request.user)).id).categoria_profesional.id in [15,17,26,27,31]:
        #Programes            
        if AuthUserProfile.objects.get( user = AuthUser.objects.get(username =str(request.user)).id).unitat_idi.id in[ 17,6 ]:#unitat 17 es SSCCC i el 6 es angio
            if 'Programes Angio' not in indicadors_local:
                indicadors_local.append('Programes Angio')
    else:
        if 'Consum Angiograf' in indicadors_local:
            indicadors_local.remove('Consum Angiograf')
        
    return indicadors_local

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def index(request):
    request.session['enabled_filters'] = {}
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                          

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def radiolegs(request):
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                              'objectiu':objectiu,
                              'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request)) 





  
##############################################################
## control de consum del Angiograf 
##############################################################
#
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def programesAngiograf(request):
    
    request.session['current_filters'] = 'angio_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any':  int(time.strftime("%Y", time.gmtime())), 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    anys = request.session['enabled_filters']['any']
    mes = request.session['enabled_filters']['mes']
    titol = ''
    if anys == 0 :
        anys = 1991
        start_date = str(datetime.date(anys, 1, 1)).replace("-","")
        end_date = str(datetime.date(int(time.strftime("%Y", time.gmtime())), 12, 31)).replace("-","")
        titol = ' des de gener  del 2010 fins a dia d\'avui'
    else:
        
        if mes != 0:
            dies_mes = calendar.monthrange(anys, mes)
            start_date = str(datetime.date(anys, mes, 1)).replace("-","")
            end_date = str(datetime.date(anys, mes, dies_mes[1])).replace("-","")
        else:
            start_date = str(datetime.date(anys, 1, 1)).replace("-","")
            end_date = str(datetime.date(anys, 12, 31)).replace("-","") 

#    # Definim querys per defecte
    #user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))

    cursor = connections['idistock_vh'].cursor()
    query = """

    SELECT 
        medico_id,
        medico_nombre,
        sum(p_altres ) as  p_altres,
        sum (p_ictus) as  p_ictus,
        sum (p_hsa ) as p_hsa,
        sum(total) as total 
    FROM (

        SELECT
            t.medico_id,
            medico_nombre,
            sum(case IdPrograma  when  '1' then 1 else 0 end ) as p_altres ,
            sum(case IdPrograma  when  '2' then 1 else 0 end ) as p_ictus ,
            sum(case IdPrograma  when  '3' then 1 else 0 end ) as p_hsa ,
            sum(Total) as total
        
        FROM (

            SELECT TOP 100 PERCENT
    
                dbo.Persona.IdPersona as medico_id,
                ISNULL(dbo.Persona.Apellido1Persona, '') + ' ' + ISNULL(dbo.Persona.Apellido2Persona, '') + ', ' + ISNULL(dbo.Persona.NombrePersona, '') AS medico_nombre ,
                CASE dbo.Intervencion.IdPrograma 
                    WHEN 1 then 'Altres'
                    WHEN 2 THEN 'ICTUS'
                    WHEN 3 THEN 'H.S.A'
                    ELSE 'Indefinit'
                END as programa, 
                dbo.Intervencion.IdPrograma as IdPrograma ,
                dbo.Intervencion.IdServicio,
                dbo.Intervencion.IdServicioOrigen,
                dbo.Servicio.amdRealizacion,
                dbo.Servicio.PHistoria, 
                dbo.Servicio.PApellido1,  
                dbo.Servicio.PApellido2, 
                dbo.Servicio.PNombre, 
                SUM(dbo.PedidoDetalle.Tarifa) AS Total 
    
            FROM  dbo.Intervencion 
                INNER JOIN dbo.Servicio ON dbo.Intervencion.IdServicio = dbo.Servicio.IdServicio 
                INNER JOIN dbo.Movimiento ON dbo.Intervencion.IdIntervencion = dbo.Movimiento.IdIntervencion 
                INNER JOIN dbo.MovimientoDetalle ON dbo.Movimiento.IdMovimiento = dbo.MovimientoDetalle.IdMovimiento 
                INNER JOIN dbo.Lote ON dbo.MovimientoDetalle.IdLote = dbo.Lote.IdLote 
                INNER JOIN dbo.PedidoDetalle ON dbo.Lote.IdPedidoDetalle = dbo.PedidoDetalle.IdPedidoDetalle 
                LEFT JOIN  dbo.Persona  ON dbo.Persona.IdPersona = Intervencion.IdUsrMedido 
        
            WHERE 
                --(dbo.Intervencion.IdPrograma) =  2 AND 
                (dbo.Servicio.amdRealizacion >= %s )
                AND (dbo.Servicio.amdRealizacion <= %s ) 
        
            GROUP BY  dbo.Persona.IdPersona ,dbo.Persona.NombrePersona,dbo.Persona.Apellido1Persona,dbo.Persona.Apellido2Persona, dbo.Intervencion.IdPrograma, dbo.Intervencion.IdServicio,dbo.Intervencion.IdServicioOrigen, dbo.Servicio.amdRealizacion, dbo.Servicio.PHistoria, dbo.Servicio.PApellido1, dbo.Servicio.PApellido2,  dbo.Servicio.PNombre 
            --ORDER BY  dbo.Persona.NombrePersona, dbo.Intervencion.IdPrograma ASC, dbo.Servicio.amdRealizacion DESC

        ) as t GROUP BY medico_id,medico_nombre,IdPrograma

    ) as r  GROUP BY medico_id,medico_nombre
    
    ORDER BY medico_nombre
    
    """
    
    cursor.execute(query, [start_date,end_date])
    queryset = cursor.fetchall()
     

    import copy
    init_object = [0,0,0,0,0,0]
    taula_series2_tmp = {}

    for prova in queryset:
        if  prova[0] == None :
            taula_series2_tmp[0] = copy.copy(init_object)
            taula_series2_tmp[0][0] = "<span style='color: red;'>Programa sense radiòleg associat</span>"
            taula_series2_tmp[0][1] = prova[1]
            taula_series2_tmp[0][2] = prova[2]
            taula_series2_tmp[0][3] = prova[3]
            taula_series2_tmp[0][4] = prova[4]
            taula_series2_tmp[0][5] = "<a href='../programes-angio-detall/0' class='addlink'></a>"

        else:
            taula_series2_tmp[prova[0]] = copy.copy(init_object)
            taula_series2_tmp[prova[0]][0] = prova[1]#'medico_nombre']
            taula_series2_tmp[prova[0]][1] = prova[2]#'p_altres']
            taula_series2_tmp[prova[0]][2] = prova[3]#'p_ictus']
            taula_series2_tmp[prova[0]][3] = prova[4]#'p_hsa']
            taula_series2_tmp[prova[0]][4] = prova[5]#'total']
            taula_series2_tmp[prova[0]][5]  = "<a href='../programes-angio-detall/%s' class='addlink'></a>" % prova[0]



    # taula_series2 = sorted(taula_series2_tmp, key=lambda row: row[1][0])
    new_table = []
    for clau,valor in taula_series2_tmp.iteritems():
        new_table.append({'radioleg':valor[0].upper(), 
                          'p_altres':valor[1],
                           'p_ictus':valor[2],
                            'p_hsa':valor[3],
                             'total':valor[4],
                             'detall':valor[5]})
        
        
    table = RadiolegsAngioTable(new_table, order_by=request.GET.get('sort'))
    #table.paginate(page=request.GET.get('page', 1))
    taula_series = table

    # if request.GET.get('export','none') == "excel":   
    # grafics
    grafics = [
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Programes angiògraf" + str(titol) ,
                'categories': [ 'radioleg', 'p_altres', 'p_ictus','p_hsa','total','detall'],
                'series': taula_series},              
                ]
    
        # Definició
    definicio = """
        Recompte de l'activitat intervencionista en l'angiògraf.</br></br>
        Les dades es treuen de la base de dades IDISTOCK. </br></br>
        El consum total es la suma del cost de tot el material emprat pel radiòleg en les intervencions.
        """
        
    auditoria.request_handler(request,8,'Tipus consulta: programesAngiograf; ' + str(request.session['enabled_filters']) + ' ' )
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                      

    
    
##############################################################
## control de consum del Angiograf  detall
##############################################################
#
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def programesAngiografDetall(request,metge_id):
    
    
    request.session['current_filters'] = 'angio_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any':  int(time.strftime("%Y", time.gmtime())), 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    anys = request.session['enabled_filters']['any']
    mes = request.session['enabled_filters']['mes']

    if mes != 0:
        dies_mes = calendar.monthrange(anys, mes)
        start_date = str(datetime.date(anys, mes, 1)).replace("-","")
        end_date = str(datetime.date(anys, mes, dies_mes[1])).replace("-","")
    else:
        start_date = str(datetime.date(anys, 1, 1)).replace("-","")
        end_date = str(datetime.date(anys, 12, 31)).replace("-","") 




    cursor = connections['idistock_vh'].cursor()
    query = """
  

    SELECT 
    
        dbo.Intervencion.IdIntervencion as intervencion_id,
        dbo.Servicio.amdRealizacion as data_prestacio, 
        dbo.Servicio.PHistoria as nhistoria, 
        ISNULL(dbo.Servicio.PApellido1, '') + ' ' + ISNULL(dbo.Servicio.PApellido2, '') + ', ' + ISNULL(dbo.Servicio.PNombre, '') AS pacient_nom ,
        CASE dbo.Intervencion.IdPrograma 
            WHEN 1 then 'Altres'
            WHEN 2 THEN 'ICTUS'
            WHEN 3 THEN 'H.S.A'
            ELSE 'Indefinit'
        END as programa,
        Intervencion.obs as obs,
        sum(dbo.PedidoDetalle.Tarifa) as total,
        ISNULL(dbo.Persona.Apellido1Persona, '') + ' ' + ISNULL(dbo.Persona.Apellido2Persona, '') + ', ' + ISNULL(dbo.Persona.NombrePersona, '') AS medico_nombre  
 
    FROM  dbo.Intervencion 
        INNER JOIN dbo.Servicio ON dbo.Intervencion.IdServicio = dbo.Servicio.IdServicio 
        INNER JOIN dbo.Movimiento ON dbo.Intervencion.IdIntervencion = dbo.Movimiento.IdIntervencion 
        INNER JOIN dbo.MovimientoDetalle ON dbo.Movimiento.IdMovimiento = dbo.MovimientoDetalle.IdMovimiento 
        INNER JOIN dbo.Lote ON dbo.MovimientoDetalle.IdLote = dbo.Lote.IdLote 
        INNER JOIN dbo.Producto ON dbo.Lote.IdProducto = dbo.Producto.IdProducto
        INNER JOIN dbo.PedidoDetalle ON dbo.Lote.IdPedidoDetalle = dbo.PedidoDetalle.IdPedidoDetalle 
        LEFT JOIN  dbo.Persona  ON dbo.Persona.IdPersona = Intervencion.IdUsrMedido 

    WHERE 
        --(dbo.Intervencion.IdPrograma) =  2 AND 
        (dbo.Servicio.amdRealizacion >= %s) 
        AND (dbo.Servicio.amdRealizacion <= %s) 
        AND dbo.Persona.IdPersona = %s

    GROUP BY  dbo.Intervencion.IdPrograma, dbo.Intervencion.IdIntervencion, dbo.Intervencion.Obs, dbo.Servicio.amdRealizacion, dbo.Servicio.PHistoria, dbo.Servicio.PApellido1, dbo.Servicio.PApellido2, dbo.Servicio.PNombre , dbo.Persona.Apellido1Persona, dbo.Persona.Apellido2Persona, dbo.Persona.NombrePersona
    ORDER BY    dbo.Servicio.amdRealizacion ASC
    
    """
    
    cursor.execute(query, [start_date,end_date,metge_id])
    queryset = cursor.fetchall()
     

    import copy
    init_object = [0,0,0,0,0,0,0]
    taula_series2_tmp = {}
    nom_metge = ''
    
    for prova in queryset:
        if nom_metge == '':
            nom_metge = prova[7]
            
        if  prova[0] == None :
            taula_series2_tmp[0] = copy.copy(init_object)
            taula_series2_tmp[0][0] = "<span style='color: red;'>Prestació erronea</span>"
            taula_series2_tmp[0][1] = prova[1]
            taula_series2_tmp[0][2] = prova[2]
            taula_series2_tmp[0][3] = prova[3]
            taula_series2_tmp[0][4] = prova[4]
            taula_series2_tmp[0][5] = prova[5]
            taula_series2_tmp[0][6] = "<a href='../programes-angio-intervencio-detall/0' class='addlink'></a>"

        else:
            taula_series2_tmp[prova[0]] = copy.copy(init_object)
            taula_series2_tmp[prova[0]][0] = prova[1]#data_prestacio
            taula_series2_tmp[prova[0]][1] = prova[2]#nhistoria
            taula_series2_tmp[prova[0]][2] = prova[3]#pacient_nom
            taula_series2_tmp[prova[0]][3] = prova[4]#programa
            taula_series2_tmp[prova[0]][4] = prova[5]#obs
            taula_series2_tmp[prova[0]][5] = prova[6]#total
            taula_series2_tmp[prova[0]][6]  = "<a href='../programes-angio-intervencio-detall/%s' class='addlink'></a>" % prova[0]#intervencion_id



    # taula_series2 = sorted(taula_series2_tmp, key=lambda row: row[1][0])
    new_table = []
    for clau,valor in taula_series2_tmp.iteritems():
        if valor[4] == None: valor[4] = ''
        if valor[5] == None: valor[5] = '?'
        new_table.append({'data_prestacio':str(valor[0])[6:8] + '/'+ str(valor[0])[4:6] + '/' + str(valor[0])[0:4], 
                          'nhistoria':valor[1],
                           'pacient_nom':valor[2],
                            'programa':valor[3],
                             'obs':valor[4],
                             'total':valor[5],
                             'detall':valor[6]})
        
        
    table = RadiolegsAngioTableDetall(new_table, order_by=request.GET.get('sort'))
    #table.paginate(page=request.GET.get('page', 1))
    taula_series = table

    # if request.GET.get('export','none') == "excel":   
    # grafics
    grafics = [
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': u"Programes angiògraf realitzats per " + unicode(nom_metge) ,
                'categories': [ 'data_prestacio', 'nhistoria', 'pacient_nom','programa','obs','total','detall'],
                'series': taula_series},              
                ]
    
        # Definició
    definicio = """
        Detall dels programes realitzats.</br></br>
        Les dades es treuen de la base de dades IDISTOCK. </br></br>
        EL total consum es la suma de costos de cada radiòleg basats en el material emprat en les intervencions.
        """
        
        
        
    auditoria.request_handler(request,8,'Tipus consulta: acitivtatOrdinaria; ' + str(request.session['enabled_filters']) + ' ' )
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                      




  
##############################################################
## control de consum del Angiograf  detall
##############################################################
#
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def programesAngiografIntervencioDetall(request,intervencio_id):
    
    cursor = connections['idistock_vh'].cursor()
    query = """
    
    /****** Script for SelectTopNRows command from SSMS  ******/
    
    SELECT 
        dbo.MovimientoDetalle.IdMovimientoDetalle,
        dbo.Lote.Barras as codi_barres,
        dbo.Producto.Producto as producte,
        dbo.PedidoDetalle.Tarifa as cost
 
    FROM  dbo.Movimiento 
        INNER JOIN dbo.MovimientoDetalle ON dbo.Movimiento.IdMovimiento = dbo.MovimientoDetalle.IdMovimiento 
        INNER JOIN dbo.Lote ON dbo.MovimientoDetalle.IdLote = dbo.Lote.IdLote 
        INNER JOIN dbo.Producto ON dbo.Lote.IdProducto = dbo.Producto.IdProducto
        LEFT OUTER JOIN dbo.PedidoDetalle ON dbo.Lote.IdPedidoDetalle = dbo.PedidoDetalle.IdPedidoDetalle 
    WHERE 
        dbo.Movimiento.IdIntervencion  = %s
    
    """
    
    cursor.execute(query, [intervencio_id])
    queryset = cursor.fetchall()

    import copy
    init_object = [0,0,0,0]
    taula_series2_tmp = {}

    for prova in queryset:
        if  prova[0] == None :
            taula_series2_tmp[0] = copy.copy(init_object)
            taula_series2_tmp[0][0] = "<span style='color: red;'>Intervenció error</span>"
            taula_series2_tmp[0][1] = prova[1]
            taula_series2_tmp[0][2] = prova[2]
            taula_series2_tmp[0][3] = prova[3]
        else:
            taula_series2_tmp[prova[0]] = copy.copy(init_object)
            taula_series2_tmp[prova[0]][0] = prova[0]#moviment_id
            taula_series2_tmp[prova[0]][1] = prova[1]#codi_barres
            taula_series2_tmp[prova[0]][2] = prova[2]#producte
            taula_series2_tmp[prova[0]][3] = prova[3]#cost

    # taula_series2 = sorted(taula_series2_tmp, key=lambda row: row[1][0])
    new_table = []
    for clau,valor in taula_series2_tmp.iteritems():
        if valor[3] == None:
            valor[3] = '?'
        new_table.append({
                          'movimient_id':valor[0],
                          'codi_barres':valor[1], 
                          'producte':valor[2],
                          'cost': valor[3]   
                            })
        
    table = RadiolegsAngioTableIntervencioDetall(new_table, order_by=request.GET.get('sort'),exclude=('movimient_id'))

    taula_series = table
    # grafics
    grafics = [
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Detall consum en la intervenció realitzada",
                'categories': [ 'movimient_id','codi_barres', 'producte', 'cost'],
                'series': taula_series},              
                ]
    
        # Definició
    definicio = """
        Detall de productes fets servits en la intervenció sel·leccionada.</br></br>
        Les dades es treuen de la base de dades IDISTOCK. </br></br>
        """
        
        
        
    auditoria.request_handler(request,8,'Tipus consulta: acitivtatOrdinaria; ' + str(request.session['enabled_filters']) + ' ' )
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                      


