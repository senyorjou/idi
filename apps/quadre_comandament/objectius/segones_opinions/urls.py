from django.conf.urls.defaults import url, patterns

urlpatterns = patterns('apps.quadre_comandament.objectius.segones_opinions.views',
    url(r'^$', 'index'), #
    url(r'^vista-segones-opinions/$', 'index'),    
    url(r'^segones-opinions/$', 'segonesOpinions'),
    url(r'^segones-opinions-detall/(?P<metge_id>\d+)', 'segonesOpinionsDetall'),
    url(r'^segones-opinions-detall/', 'segonesOpinions'),
    
    #url(r'^segones-opinions-prova/', 'segonesOpinionsProva'),

      
)
