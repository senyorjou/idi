# coding: utf8

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response,redirect
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from apps.quadre_comandament.common import initFilters
from apps.quadre_comandament.models import  ProvaOhdim, Metge,ProvaOhdimCodificacioGenerica,ProvaOhdimCodificacioGenerica_Form
from apps.quadre_comandament.objectius.radiolegs.tables import RadiolegsTable, RadiolegsOrdinariaTable,DetallRadiolegsTable,RadiolegsExtraordinariaTable,RadiolegsExtraordinariaValidarTable,DetallRadiolegsTableSeram
import datetime,calendar,time
from django.db.models import Q,Count,Sum
from apps.meta.models import AuthUser,AuthUserProfile
from apps.meta import auditoria
from django.db.models import F# https://docs.djangoproject.com/en/dev/topics/db/queries/#query-expressions




indicadors = ['Segones Opinions']#,'segones opinions prova']
objectiu = "Gestió de l'activitat realitzada en altres centres no I.D.I."
objectiu_url = "vista-segones-opinions"

def get_indicadors(request):
    # Recordem que no es pot canviar una variable global ( per molts motius , multiples procesos ... etc)
    indicadors_local =  indicadors
    return indicadors_local

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def index(request):
    request.session['enabled_filters'] = {}
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                          

    
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def gestioSegonesOpinions(request):
    request.session['enabled_filters'] = {}
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                          
    
    
    
    
 ##################33333PRIOVES MAPES    
    
@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def segonesOpinionsProva(request):
    request.session['enabled_filters'] = {}
    return render_to_response('quadre_comandament/gestio_unitats/prova.html',
                              context_instance=RequestContext(request))
    
#############################################################
# Segones opnions 
#############################################################

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def segonesOpinions(request):
   
    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'radiolegs_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    anys = request.session['enabled_filters']['any']

    
    mes = request.session['enabled_filters']['mes']
    unitatsel = request.session['enabled_filters']['unitat']
    modalitatsel = request.session['enabled_filters']['modalitat']
    
    
    if mes != 0:
        dies_mes = calendar.monthrange(anys, mes)
        start_date = datetime.date(anys, mes, 1)
        end_date = datetime.date(anys, mes, dies_mes[1])
    else:
        
        if int(anys) == 0:
            start_date = datetime.date(2000, 1, 1)
            end_date = datetime.date(datetime.datetime.now().year, 12, 31)
        else:
            start_date = datetime.date(anys, 1, 1)
            end_date = datetime.date(anys, 12, 31)
        
     
        
    # Definim querys per defecte
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    
                                                                        # no volem agenda ICS pero !!! si volem els metges que no tenen ralcio ( null )
    master_queryset = ProvaOhdim.objects.using('portal_dades').exclude( 
                                                                        Q(agenda__agenda_idi__es_ics=True ) & (
                                                                        Q(metge_informa__es_idi=False,  metge_informa__es_idi__isnull=False ) |
                                                                        Q(metge_revisa__es_idi=False,   metge_revisa__es_idi__isnull=False  ) |  
                                                                        Q(metge_allibera__es_idi=False, metge_allibera__es_idi__isnull=False)
                                                                        )
                                                                         # NO volem els metges que son del ICS i que la agenda es del ICS!!!  ( LA Q es totalment necessaria sino peta ( fa la join i li canvia el nom a la taula i despres alferla sevir peta))
                                                                       ).exclude (Q(agenda__agenda_idi__es_ics=True , metge_informa__es_idi__isnull = True, metge_revisa__es_idi__isnull = True, metge_allibera__es_idi__isnull =True))
                                                                                  
    #Ens quedem els que o bé no tenen realcio amb prova_ohdim_codificacio_generica o bé tenen un null possat perque al validar-lo es va treure el valor
    #master_queryset = master_queryset.exclude( provaohdimcodificaciogenerica__codificacio_generica_final__in=[7,8])

    #volem les prestacions entre el rang de dades, i que estiguin concloses 
    master_queryset =   master_queryset.filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))

    if unitatsel != 0 and modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
    elif unitatsel!= 0:
        master_queryset = master_queryset.filter( agenda__agenda_idi__unitat_idi_id=unitatsel) 
            
    elif modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                         agenda__agenda_idi__idtipoexploracion=modalitatsel)
    else:   
        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                          agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))


    # total de prestacions realitzades
    query_set_total =   master_queryset.annotate(informes_creats=Count('id'))
    total = query_set_total.count()
        
    
    
    
     
    #perestacions realitzades en altres centres , segones opinions
    master_queryset =   master_queryset.filter(prestacio=18)
   

    import copy
    init_object = [0,0,0,0,0,0,0,0]
    
    taula_series2_tmp = {}

    queryset = master_queryset.values('metge_informa' ,'metge_informa__cognoms_nom' ).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')

    #NOM I COGNOMS i DETALL
    for prova in queryset:
        if  prova['metge_informa__cognoms_nom'] == None :
            try:
                if taula_series2_tmp[0] == None:
                    taula_series2_tmp[prova['metge_informa']][0]  = "<span style='color: red;'>Informes d'altres metges</span>"
                    taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../segones-opinions-detall/0' class='addlink'></a>"
            except:
                taula_series2_tmp[0] = copy.copy(init_object)
                taula_series2_tmp[0][0]  = "<span style='color: red;'>Informes d'altres metges</span>"
                taula_series2_tmp[0][7]  = "<a href='../activitat-ordinaria-detall/0' class='addlink'></a>"

        else:
            taula_series2_tmp[prova['metge_informa']] = copy.copy(init_object)
            taula_series2_tmp[prova['metge_informa']][0]  = prova['metge_informa__cognoms_nom']
            taula_series2_tmp[prova['metge_informa']][7]  = "<a href='../segones-opinions-detall/%s' class='addlink'></a>" % prova['metge_informa']

    # INFORMES CREATS
    queryset = master_queryset.values('metge_informa','metge_informa__cognoms_nom',).annotate(informes_creats=Count('id')).order_by('metge_informa__cognoms_nom')
    for prova in queryset:
        if  prova['metge_informa__cognoms_nom'] == None :
            taula_series2_tmp[0][1] = prova['informes_creats'] # s'ha de su
        else:
            taula_series2_tmp[prova['metge_informa']][1] = prova['informes_creats']
    # INFORMES REVISATS
    queryset = master_queryset.values('metge_revisa','metge_revisa__cognoms_nom').annotate(informes_revisats=Count('id')).order_by('metge_revisa__cognoms_nom')
    for prova in queryset:
        try:      
            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
        except KeyError:# no ha informat cap informe pero si revisat  o alliberat ?? els afegim
            taula_series2_tmp[prova['metge_revisa']] = copy.copy(init_object) # PETA que no hi ha el metge que informa === a el afegim aqui
            taula_series2_tmp[prova['metge_revisa']][0]  = prova['metge_revisa__cognoms_nom'] 
            taula_series2_tmp[prova['metge_revisa']][1] = 0
            taula_series2_tmp[prova['metge_revisa']][2] = prova['informes_revisats']
            taula_series2_tmp[prova['metge_revisa']][7]  = "<a href='../segones-opinions-detall/%s' class='addlink'></a>" % prova['metge_revisa']
    # INFORMES ALLIBERATS
    queryset = master_queryset.values('metge_allibera','metge_allibera__cognoms_nom').annotate(informes_alliberats=Count('id')).order_by('metge_allibera__cognoms_nom')
    
   
    for prova in queryset:
        try:
            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
        except KeyError:
            taula_series2_tmp[prova['metge_allibera']] = copy.copy(init_object)
            taula_series2_tmp[prova['metge_allibera']][1] = 0
            taula_series2_tmp[prova['metge_allibera']][2] = 0
            taula_series2_tmp[prova['metge_allibera']][3] = prova['informes_alliberats']
            taula_series2_tmp[prova['metge_allibera']][7]  = "<a href='../segones-opinions-detall/%s' class='addlink'></a>" % prova['metge_allibera']


         
    # INFORMES ACI
    queryset = master_queryset.filter(provaohdimcodificaciogenerica__codificacio_generica_final=7).values('metge_allibera').annotate(informes_alliberats=Count('id'))
    for prova in queryset:       
        taula_series2_tmp[prova['metge_allibera']][4] = prova['informes_alliberats']
    # INFORMES ACH
    queryset = master_queryset.filter(provaohdimcodificaciogenerica__codificacio_generica_final=8).values('metge_allibera').annotate(informes_alliberats=Count('id'))
    for prova in queryset:           
        taula_series2_tmp[prova['metge_allibera']][5] = prova['informes_alliberats']
        
    # SERAM 
    #TODO: alguna cosa hem de fer per comprovar aquelles proves que no tenen codi seram , o assegurar-nos que totes les prestacions en tenen...
    #nomes aquells que ha informat i ha alliberat el mateix radioleg
    queryset = master_queryset.values('metge_allibera').filter( metge_allibera = F('metge_informa')).annotate(temps_seram=Sum('prestacio__codi_seram__temps_metge')).order_by('metge_allibera__cognoms_nom')
    for prova in queryset:
        if prova['temps_seram'] == None:
            taula_series2_tmp[prova['metge_allibera']][6] = 0
        else:
            taula_series2_tmp[prova['metge_allibera']][6] = prova['temps_seram']

    
#    #hara sumem el 25% dels revisats 
#    queryset = master_queryset.values('metge_allibera').exclude( metge_allibera = F('metge_informa')).annotate( temps_seram=Sum('prestacio__codi_seram__temps_metge') ).order_by('metge_allibera__cognoms_nom')
#    for prova in queryset:
#        if not (prova['temps_seram'] == None):
#            taula_series2_tmp[prova['metge_allibera']][6] = taula_series2_tmp[prova['metge_allibera']][6] + (25*int(prova['temps_seram'])/100)

    # Taula html - sort & filters
    taula_series2 = [] #creem una tupla de metge i les dades
    
    
    inf_alres_m = [0,0,0,0,0,0,0,0]
    dades_metge_tmp=[0,0,0,0,0,0,0,0]
    #Sumem la moralla , els que no estan a la taula metge i els metge que son null en el provaohdim
    for dades_metge in taula_series2_tmp.items():
        if dades_metge[1][0] == None or dades_metge[1][0] == 0:
            if dades_metge[1][1] == None: dades_metge_tmp[0]= 0
            else:dades_metge_tmp[0] = dades_metge[1][1] 
            if dades_metge[1][2] == None: dades_metge_tmp[1]=0
            else:dades_metge_tmp[1] = dades_metge[1][2]
            if dades_metge[1][3] == None:  dades_metge_tmp[2]=0
            else:dades_metge_tmp[2] = dades_metge[1][3]
            if dades_metge[1][4] == None: dades_metge_tmp[3]=0
            else:dades_metge_tmp[3] = dades_metge[1][4]
            if dades_metge[1][5] == None:  dades_metge_tmp[4]=0
            else:dades_metge_tmp[4] = dades_metge[1][5]
            if dades_metge[1][6] == None: dades_metge_tmp[5]=0
            else:dades_metge_tmp[5] = dades_metge[1][6]
                
            inf_alres_m = [ inf_alres_m[0],
                            inf_alres_m[1] + int(dades_metge_tmp[0]) ,
                            inf_alres_m[2] + int(dades_metge_tmp[1]),
                            inf_alres_m[3] + int(dades_metge_tmp[2]),
                            inf_alres_m[4] + int(dades_metge_tmp[3]),
                            inf_alres_m[5] + int(dades_metge_tmp[4]),
                            inf_alres_m[6] + int(dades_metge_tmp[5]),
                            inf_alres_m[7] ]
        else:
            if dades_metge[0]== 0:
                inf_alres_m =  dades_metge[1]
            else:
                taula_series2.append((dades_metge[1][0], dades_metge[1]))
                
    if inf_alres_m[0] != 0:#afegim la suma dels que notenen nom i errors
        taula_series2.append((inf_alres_m[0], inf_alres_m))
        
        
    taula_series2 = sorted(taula_series2, key=lambda row: row[1][0])
    new_table = []
    total_informes_segones_op = 0
    for serie in taula_series2:
        new_table.append({'radioleg':serie[0], 'informes_creats':serie[1][1], 'informes_revisats':serie[1][2], 'informes_alliberats':serie[1][3], 'aci':serie[1][4],'ach':serie[1][5] ,'temps_seram':serie[1][6],'detall':serie[1][7]})
        total_informes_segones_op  += int(serie[1][3])
    
   
    table = RadiolegsOrdinariaTable(new_table, order_by=request.GET.get('sort'), exclude=('informes_revisats','informes_creats','temps_seram' ) )
    #table.paginate(page=request.GET.get('page', 1))
    taula_series = table


    #FORMATGE 
    formatge_series = []
    
    
    
    total-=total_informes_segones_op
    
    resultat = (("Segones opinions: " + str(total_informes_segones_op ), total_informes_segones_op),("Proves pròpies: " + str(total) ,total))
    for row in resultat:
        formatge_series_item = (row[0], row[1])
        formatge_series.append(formatge_series_item)


    grafics = [
               {'tipus': "taula2", 
                'tamany': 100,  
                'titol': "Informes d'estudis realitzats en altres centres. ( segones opinions )",
                'categories': [ 'Creats', 'Revisats', 'Alliberats','aci','ach','temps_seram', 'Detall'],
                'series': taula_series}, 
               
               {'tipus': "formatge", 
                'tamany': 100,
                'titol': "% de segones opinons",
                'descripcio': "% de segones opinions sobre el total de proves realitzades.",                
                'series': formatge_series }
               
                            
                ]
    
        # Definició
    definicio = """
       Suma de totes les prestacions alliberades pels radiòlegs que han estat desglossades al SAP en la prestació "Segones opinions"</br></br>
        """
    auditoria.request_handler(request,8,'Tipus consulta: acitivtatOrdinaria; ' + str(request.session['enabled_filters']) + ' ' )
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'definicio':definicio,
                               'objectiu_url':objectiu_url},
                              context_instance=RequestContext(request))                      





@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def segonesOpinionsDetall(request, metge_id):

    # Cal definir els filtres que volem per defecte
    request.session['current_filters'] = 'radiolegs_filters' 
    if not request.session['current_filters'] in request.session:
        enabled_filters = {'any': 2012, 'unitat': "0", 'modalitat': "0", 'mes': int(time.strftime("%m", time.gmtime()))}
        initFilters(request, enabled_filters, request.session['current_filters'])
    request.session['enabled_filters'] = request.session[request.session['current_filters']]

    #request.session['enabled_filters'] = {}
    anys = request.session['radiolegs_filters']['any']
    mes = request.session['radiolegs_filters']['mes']
    unitatsel = request.session['radiolegs_filters']['unitat']
    modalitatsel = request.session['radiolegs_filters']['modalitat']

    if mes != 0:
        dies_mes = calendar.monthrange(anys, mes)
        start_date = datetime.date(anys, mes, 1)
        end_date = datetime.date(anys, mes, dies_mes[1])
    else:
        start_date = datetime.date(anys, 1, 1)
        end_date = datetime.date(anys, 12, 31) 
  
    #TODO: AIXO S?HA DE TREURE PERQUE POTSER HI HAN DIGANOSTICAT INFORMES DALTRES CENTRES i els volem veure??? SIiiii
    user_unitats_permisos = set(unitat['unitatat_facturacio_id'] for unitat in request.user.get_profile().unitats_idi_permissos.all().values('unitatat_facturacio_id'))
    
    if metge_id == '0':
        radioleg_nom_cognoms = "Informes d'altres metges"
        master_queryset = ProvaOhdim.objects.using('portal_dades').filter( agenda__agenda_idi__es_ics=False
                                                                 ).filter( Q(metge_informa__isnull=True) |
                                                                           Q(metge_revisa__isnull=True) |
                                                                           Q(metge_allibera__isnull=True)
                                                                  ).filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))
    else:
        radioleg_nom_cognoms = Metge.objects.using('portal_dades').get(pk=long(metge_id)).cognoms_nom
        master_queryset = ProvaOhdim.objects.using('portal_dades').filter( Q(metge_informa=metge_id) |
                                                                           Q(metge_revisa=metge_id) |
                                                                           Q(metge_allibera=metge_id)
                                                                 ).filter(Q(data_document_revisat__range=(start_date, end_date) ) & (Q(status_prestacio_codi='CO') |  Q(status_prestacio_codi='RE')))

    if unitatsel != 0 and modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel, 
                                                 agenda__agenda_idi__idtipoexploracion=modalitatsel)
    elif unitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id=unitatsel)  
            
    elif modalitatsel != 0:
        master_queryset = master_queryset.filter(agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos),
                                                 agenda__agenda_idi__idtipoexploracion=modalitatsel)
    else:   
        master_queryset = master_queryset.filter(agenda__agenda_idi__idagenda__isnull=False,
                                                 agenda__agenda_idi__unitat_idi_id__in=(user_unitats_permisos))

    # NO HI HA UANLTRE FORAM; ADE FER:_HO , amb lexlude es fa un lio i no fa bé la consulta mysql
    # treiem els que tenen valor en ACI ACH == ordinaria
    master_queryset = master_queryset.filter( ~Q(provaohdimcodificaciogenerica__codificacio_generica_final=7) & ~Q(provaohdimcodificaciogenerica__codificacio_generica_final=8))
    
#perestacions realitzades en altres centres , segones opinions
    master_queryset =   master_queryset.filter(prestacio=18)
                              
    proves_queryset_iterator = master_queryset.values( 'id', 
                                       'historia_clinica_id',
                                       'tipus_modalitat', 
                                       'agenda__agenda_idi__obs',
                                       'prestacio_id',
                                       'prestacio__descripcio',
                                       'metge_informa','metge_revisa','metge_allibera',
                                       'metge_informa__cognoms_nom','metge_revisa__cognoms_nom','metge_allibera__cognoms_nom', 
                                       'codificacio_generica__codi',
                                       'data_cita',
                                       'hora_cita',
                                       'data_document_informat',
                                       'hora_document_informat',
                                       'data_document_revisat',
                                       'hora_document_revisat'
                                       ).order_by('tipus_modalitat', 'agenda', 'historia_clinica_id', 'data_cita', 'hora_cita').iterator()

    import copy
    init_object = [0,0,0,0,0,0,0,0,0,0,0,0,0]
    icona_oky= '<IMG SRC="/static/admin/img/admin/icon_success.gif" BORDER=0 ALT="Fet">'

    #NOM I COGNOMS i DETALL
    taula_proves = {}
    #rows_duplicats = copy.copy(proves_queryset_iterator)   No es pot ducplicar un iterador¿?
    taula_detall = []
    #radioleg_nom_cognoms = 'Desconegut'
    
    #i = len(proves_queryset) 263 i nomes fa el bucle de 100 !!!!!!!!!!!!!!!!!1
    # els querysets tenen un problema de cache i es queden nomes amb 100 registres -> !!!!!!!
    # http://jeffelmore.org/2010/09/25/smarter-caching-of-django-querysets/#comment-100
    # es mes eficient possar-ho en una named-tuple 
    #    taula_detall_item = namedtuple('Detall', 'id historia_clinica_id tipus_modalitat agenda__agenda_idi__obs prestacio_id prestacio__descripcio metge_informa metge_revisa metge_allibera metge_informa__cognoms_nom metge_revisa__cognoms_nom metge_allibera__cognoms_nom codificacio_generica__codi data_cita hora_cita data_document_informat hora_document_informat data_document_revisat hora_document_revisat')

    for prova in proves_queryset_iterator:
        
        taula_proves[prova['id']] = copy.copy(init_object)

        if prova['metge_informa'] == long(metge_id):
            metge_informa = icona_oky 
        else: 
            metge_informa =  prova['metge_informa__cognoms_nom']

        if prova['metge_revisa'] == long(metge_id):
            metge_revisa = icona_oky 
        else: 
            metge_revisa =  prova['metge_revisa__cognoms_nom']
        if prova['metge_allibera'] == long(metge_id):
            metge_allibera = icona_oky 
        else: 
            metge_allibera =  prova['metge_allibera__cognoms_nom']
            
        taula_proves[prova['id']][1]  = prova
        #if (radioleg_nom_cognoms == '' and prova['metge_informa__cognoms_nom'] !=None ):
        #    radioleg_nom_cognoms = prova['metge_informa__cognoms_nom']

        if prova['data_cita'] == None  or  prova['hora_cita'] == None: 
            data_i_hora_cita= '-'
        else:  
            data_i_hora_cita =  datetime.datetime(prova['data_cita'].year, prova['data_cita'].month, prova['data_cita'].day, prova['hora_cita'].hour, prova['hora_cita'].minute, prova['hora_cita'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_document_informat'] == None  or  prova['hora_document_informat'] == None:
            data_i_hora_doc_inf = '-'
        else:
            data_i_hora_doc_inf = datetime.datetime(prova['data_document_informat' ].year, prova['data_document_informat'].month, prova['data_document_informat'].day, prova['hora_document_informat'].hour, prova['hora_document_informat'].minute, prova['hora_document_informat'].second).strftime('%Y/%m/%d %H:%M')
        if prova['data_document_revisat'] == None  or  prova['hora_document_revisat'] == None:
            data_i_hora_doc_rev = '-'      
        else:
            data_i_hora_doc_rev = datetime.datetime(prova['data_document_revisat'].year, prova['data_document_revisat'].month, prova['data_document_revisat'].day, prova['hora_document_revisat'].hour, prova['hora_document_revisat'].minute, prova['hora_document_revisat'].second).strftime('%Y/%m/%d %H:%M')

        # Ara buscarem totes aquelles proves que tenen codificacio generica per posar la codificacio inicial i la final i si esta validat o no i per qui
        codgenerica_queryset = ProvaOhdimCodificacioGenerica.objects.using('portal_dades').filter(prova_ohdim__exact= prova['id'])#.values('codificacio_generica_inicial','codificacio_generica_final')
        # en prinicpi nomes pot ser un pero... retorna un queryset
        codificacio_inicial= ''
        codificacio_final= ''
        validat = ''
        #pasem a la vista de modificar el pk de provaohdim per si no hi ha codificaciogenirica ( descuid del radioleg)
        modificar = "<a href='../activitat-total-detall-modificar/%s&%s' class='changelink'></a>" % (prova['id'],'0')
     
        for codgen in codgenerica_queryset:
            #ls pk de prevaohdim_codificaio_geenrica   aixi el paser en un link
            modificar="<a href='../activitat-total-detall-modificar/%s&%s' class='changelink'></a>" % ( prova['id'],codgen.id)
    
            try:
                codificacio_inicial = str(codgen.codificacio_generica_inicial)
                if codificacio_inicial=='None' :codificacio_inicial =''
            except:
                codificacio_inicial = '' 
            try:
                codificacio_final = str(codgen.codificacio_generica_final)
                if codificacio_final=='None' :codificacio_final =''
            except:
                codificacio_final = ''
            if codgen.validat:
                validat =  icona_oky                
                modificar = ''
            
            
        duplicat_trobat=None
        #Les proves consecuteives  les marqeum amb marrro!!! 
#        for row_duplicats in rows_duplicats:
#            if (prova['historia_clinica_id'] == row_duplicats['historia_clinica_id'] ) and ( prova['data_cita'] == row_duplicats['data_cita']) and (prova['prestacio_id'] != row_duplicats['prestacio_id']):
#                duplicat_trobat='style=color:brown;'
                
        taula_detall.append((prova['id'],( prova['historia_clinica_id'],
                                           prova['agenda__agenda_idi__obs'],
                                           prova['prestacio__descripcio'] ,
                                           metge_informa , 
                                           metge_revisa, 
                                           metge_allibera,
                                           codificacio_inicial, 
                                           codificacio_final,
                                           data_i_hora_cita,
                                           data_i_hora_doc_inf,
                                           data_i_hora_doc_rev,
                                           validat,
                                           modificar
                                           ), duplicat_trobat 
                             )
                            )
#    # if request.GET.get('export','none') == "excel":   

    # grafics
    new_table = []
    for row in taula_detall:
        new_table.append({'pk':row[0], 
                          'hc':row[1][0],
                          'agenda':row[1][1], 
                          'exploracio':row[1][2],
                          'informat':row[1][3], 
                          'revisat':row[1][4], 
                          'alliberat':row[1][5], 
                          'codificacio_inicial':row[1][6],
                          'codificacio_final':row[1][7],
                          'data_exploracio':row[1][8],
                          'data_informe':row[1][9],
                          'data_revisio':row[1][10],
                          'validat':row[1][11],
                          'codgen_id':row[1][12]                           
                          })
    table = DetallRadiolegsTable(new_table, order_by=request.GET.get('sort'),  exclude=('informat','revisat','alliberat','codificacio_final','validat','codgen_id','data_revisio','data_exploracio' ))
    #table.paginate(page=request.GET.get('page', 1))
    
    
    taula_detall = table
    nregistres= str(len(new_table))
    
    grafics = [
              
               {'tipus': "taula2", 
                'tamany': 100,                
                'titol': "Detall d'altres opinions realitzades per: " + radioleg_nom_cognoms  + ".    Informes: " + nregistres,
                'categories': [ 'Història clínica','Exploració', 'Agenda','Informat', 'Revisat', 'Alliberat', 'Codificació', 'Data exploració', 'Data informe' ,'Data revisió' ,'validat'],
                'series': taula_detall},
                ]
    
    definicio = """
        Suma de tota l'activitat realitzada en altres centres ( segones opinions ).</br></br> 
        """

    auditoria.request_handler(request,8,'Tipus consulta: activitatOrdinariaDetall ; ' + str(request.session['enabled_filters']) + ' ,metge_id:' + str(metge_id) )
    return render_to_response('quadre_comandament/indicador.html',
                              {'indicadors':get_indicadors(request),
                               'grafics':grafics,
                               'objectiu':objectiu,
                               'objectiu_url':objectiu_url,
                               'definicio':definicio},
                              context_instance=RequestContext(request)) 
    
    
    