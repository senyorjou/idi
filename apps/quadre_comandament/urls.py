# coding: utf8

from django.conf.urls.defaults import url, patterns, include


urlpatterns = patterns('apps.quadre_comandament.views',
    url(r'^$', 'index'), #
    url(r'^index$', 'index'), #
    url(r'^set-filters$', 'setFilters'), 
    url(r'^activitat/', include('apps.quadre_comandament.objectius.activitat.urls')), #
    url(r'^radiolegs/', include('apps.quadre_comandament.objectius.radiolegs.urls')), #
    url(r'^tecnics_infermers/', include('apps.quadre_comandament.objectius.tecnics_infermers.urls')), #
    url(r'^iso/', include('apps.quadre_comandament.objectius.iso.urls')), #
    url(r'^vista-segones-opinions/', include('apps.quadre_comandament.objectius.segones_opinions.urls')), #
    url(r'^angio-tac/', include('apps.quadre_comandament.objectius.angio_tac.urls')), #
    )
