# coding: utf8

# Create your views here.
import calendar
import locale
from datetime import date,datetime,time
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from apps.meta.models import MetaMes,MetaTipoexploracion
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponseRedirect

from models import ProvaOhdim
from django.db.models import Max
from apps.meta.models import AuthUser,AuthUserProfile


@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def index(request):

    darrera_actulitzacio_mdos =  ProvaOhdim.objects.all().exclude(status_prestacio_codi='AN').aggregate(Max('data_fi_prestacio'))['data_fi_prestacio__max']
    try:
        darrera_actulitzacio_facturacio = MetaMes.objects.get(id = datetime.now().month-1 ).mes
    except:
        darrera_actulitzacio_facturacio = MetaMes.objects.get(id = datetime.now().month).mes
    #darrera_actulitzacio_facturacio  = calendar.month_name[ datetime.now().month-1].decode(myencoding)
    
    ###
    ##Indexos objectius dinamics
    ###
    index_dinamic = {}
    #Programes Angiograf VH -< conexio amb seqlserver GARBI            
    if AuthUserProfile.objects.get( user = AuthUser.objects.get(username =str(request.user)).id).unitat_idi.id in[ 17,6 ]:#unitat 17 es SSCCC i el 6 es angio
        index_dinamic = {'Angio tac' :"Gestiò del consum del angiògraf de VH. "}
    
       
    
    
    return render_to_response('quadre_comandament/index.html', 
                               {'darrera_actulitzacio_mdos':darrera_actulitzacio_mdos,
                                'darrera_actulitzacio_facturacio':darrera_actulitzacio_facturacio,
                                'index_dinamic': index_dinamic,
                                },                         
                              context_instance=RequestContext(request))

@login_required
#@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
def setFilters(request):
    if 'filters' in request.session:
        for filtre in request.session['filters']:
            if filtre['nom'].lower() in request.POST:
                request.session[request.session['current_filters']][filtre['nom'].lower()] = int(request.POST[filtre['nom'].lower()])
    #else:
        
    request.session.modified = True
    return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))

@login_required
@permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
#@user_passes_test(lambda u: u.has_perm('meta.app_permis'))
def rendiments_unitat(request):
    return render_to_response('quadre_comandament/cercarendimentsunitat.html' ,
                               {'messos': MetaMes.objects.all().order_by('id') ,
                                 'tipusexploracio': MetaTipoexploracion.objects.all().order_by('id') },
                                context_instance=RequestContext(request))


