# coding: utf-8

from django.db import models
from apps.meta.models import AuthUser,User  
from django.db.models.signals import post_save
from apps.meta.composite_key_model import CompositeKeyModel
from django.forms import ModelForm,Widget
from django.forms.models import BaseForm

#from django.forms import inlineformset_factory




##ATENCIO per a fer les JOIN no funciona!!!! hi ha un tiket al django ->> https://code.djangoproject.com/ticket/373
## no crea automaticament les foreign keys --> Your model must have a dummy primary key (id)

class Agenda(CompositeKeyModel):
    connection_name = 'portal_dades'# Static Var
    id = models.BigIntegerField(primary_key=True)
    hospital_id = models.ForeignKey('hospital',null=False, blank=False)
    idsap = models.BigIntegerField(null=False, blank=False)
    metge = models.ForeignKey('Metge', null=True, blank=True)
    codi = models.CharField( max_length=24)
    descripcio = models.CharField(max_length=180)
    agenda_idi = models.ForeignKey('AgendaIdi', null=True, blank=True,db_column='id') # ens inventem la columna , es epr dir als models que la referencia
    class Meta:
        db_table = u'agenda'
        ordering = ['codi'] 
        unique_together = (("hospital_id", "id"),)
    def __unicode__(self):
        return self.codi     
        
       
class AgendaIdi(models.Model):
    connection_name = 'portal_dades'# Static Var
    idagenda = models.IntegerField(db_column='IdAgenda') # Field name made lowercase.
    codagenda = models.BigIntegerField(db_column='CodAgenda') # Field name made lowercase.
    agenda = models.CharField( max_length=135, db_column='Agenda', blank=True) # Field name made lowercase.
    idunidadfac = models.IntegerField(db_column='IdUnidadFac') # Field name made lowercase.
    idtipoexploracion = models.IntegerField(db_column='IdTipoExploracion') # Field name made lowercase.
    obs = models.CharField(max_length=135, db_column='Obs', blank=True) # Field name made lowercase.
    unitattractament = models.CharField(max_length=96, db_column='UnitatTractament', blank=True) # Field name made lowercase.
    db = models.CharField(max_length=135, db_column='DB', blank=True) # Field name made lowercase.
    idcentre = models.IntegerField(db_column='IdCentre') # Field name made lowercase.
    hospital = models.ForeignKey('Hospital')
    unitat_idi_id = models.IntegerField(null=True, blank=True)
    es_ics = models.IntegerField(null=True, db_column='es_ICS', blank=True) # Field name made lowercase.
    id = models.IntegerField(primary_key=True, null=False, db_column='id') # Field name made lowercase.
    
    class Meta:
        db_table = u'agenda_idi'
        ordering = ['agenda'] 
    def __unicode__(self):
        return self.agenda   

class AgendaProgramacio(models.Model):
    connection_name = 'portal'# Static Var
    idprogramacionagenda = models.IntegerField(primary_key=True, db_column='IdProgramacionAgenda') # Field name made lowercase.
    idagenda = models.IntegerField(db_column='IdAgenda') # Field name made lowercase.
    diasemana = models.IntegerField(db_column='DiaSemana') # Field name made lowercase.
    horainicial = models.IntegerField(db_column='HoraInicial') # Field name made lowercase.
    horafinal = models.IntegerField(db_column='HoraFinal') # Field name made lowercase.
    frecuencia = models.IntegerField(db_column='Frecuencia') # Field name made lowercase.
    clase = models.CharField(max_length=135, db_column='Clase', blank=True) # Field name made lowercase.
    especialidad = models.CharField(max_length=135, db_column='Especialidad', blank=True) # Field name made lowercase.
    descripcion = models.CharField(max_length=135, db_column='Descripcion') # Field name made lowercase.
    class Meta:
        db_table = u'meta_agenda_programacio'
    def __unicode__(self):
        return self.descripcion      
        

class Hospital(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True)
    codi = models.CharField(max_length=6)
    descripcio = models.CharField(max_length=135)
    territori = models.CharField(max_length=135)
    idi_id = models.IntegerField()
    class Meta:
        db_table = u'hospital'  
        ordering = ['descripcio'] 
    def __unicode__(self):
        return self.descripcio     

class ImportacioFitxer(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True)
    nom_taula = models.CharField(max_length=135)
    descripcio = models.CharField(max_length=180)
    nom_mdo = models.CharField(max_length=135)
    es_mestre = models.IntegerField()
    ordre_importacio = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'importacio_fitxer'


class ImportacioCamp(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True)
    taula = models.ForeignKey(ImportacioFitxer)
    column = models.CharField(max_length=135, blank=True)
    fieldname = models.CharField(max_length=135, blank=True)
    key = models.CharField(max_length=135, blank=True)
    type = models.CharField(max_length=135, blank=True)
    length = models.CharField(max_length=135, blank=True)
    outputlen = models.CharField(max_length=135, blank=True)
    decimals = models.CharField(max_length=135, blank=True)
    fieldname_idi = models.CharField(max_length=135, blank=True)
    class Meta:
        db_table = u'importacio_camp'
    def __unicode__(self):
        return self.fieldname


class Mes(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True, db_column='IdMes') # Field name made lowercase.
    mes = models.CharField(max_length=135, db_column='Mes', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'mes'
    def __unicode__(self):
        return self.mes
    
    
class UsuariSAP(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True)
    hospital_codi = models.CharField(max_length=2)
    dni = models.CharField(max_length=48, blank=True)
    cognoms_nom = models.CharField(max_length=180, blank=True)
    n_collegiat = models.BigIntegerField(null=True, blank=True)
    servei_codi = models.CharField(max_length=6)
    es_idi = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'usuari_sap'
        #ordering = ['cognoms_nom']
    def __unicode__(self):
        return self.cognoms_nom 
    
    
    
    
class Metge(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True)
    cognoms_nom = models.CharField(max_length=180, blank=True)
    n_collegiat = models.BigIntegerField(null=True, blank=True)
    es_met = models.IntegerField(null=True, blank=True)
    es_cap = models.IntegerField(null=True, blank=True)
    es_inf = models.IntegerField(null=True, blank=True)
    es_res = models.IntegerField(null=True, blank=True)
    dni = models.CharField(max_length=48, blank=True)
    es_idi = models.IntegerField(null=True, blank=True)
    
    class Meta:
        db_table = u'metge'
        ordering = ['cognoms_nom']
    def __unicode__(self):
        return self.cognoms_nom 
  
class MetgeServei(models.Model):
    connection_name = 'portal_dades'# Static Var
    hospital = models.ForeignKey('Hospital', primary_key=True)
    metge = models.ForeignKey('Metge', primary_key=True)
    metge_cognoms_nom = models.CharField(max_length=180, blank=True)
    servei_id = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'metge_servei'  

#class Seram(models.Model):
#    codi_sap = models.CharField(primary_key=True, max_length=45)
#    idsubgrup = models.IntegerField(db_column='IdSubGrup') # Field name made lowercase.
#    descripcio = models.CharField(max_length=300)
#    codi_grup = models.IntegerField()
#    descripcio_grup = models.CharField(max_length=300)
#    temps_sala = models.IntegerField()
#    temps_metge = models.IntegerField()
#    urv_a = models.DecimalField(max_digits=9, decimal_places=2)
#    ura_a = models.DecimalField(max_digits=9, decimal_places=2)
#    urv_p = models.DecimalField(max_digits=9, decimal_places=2)
#    ura_p = models.DecimalField(max_digits=9, decimal_places=2)
#    class Meta:
#        db_table = u'seram'
#    def __unicode__(self):
#        return self.descripcio

class CodiSeram(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True) # Field name made lowercase.
    descripcio = models.CharField(max_length=100)
    grup_id = models.IntegerField()
    temps_sala = models.IntegerField()
    temps_metge = models.IntegerField()
    urv_a = models.DecimalField(max_digits=9, decimal_places=2)
    ura_a = models.DecimalField(max_digits=9, decimal_places=2)
    urv_p = models.DecimalField(max_digits=9, decimal_places=2)
    ura_p = models.DecimalField(max_digits=9, decimal_places=2)
    class Meta:
        db_table = u'codi_seram'
    def __unicode__(self):
        return self.descripcio

class ZonaAnatomica(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True)
    codi = models.CharField(max_length=30)
    descripcio = models.CharField(max_length=240, blank=True)
    class Meta:
        db_table = u'zona_anatomica'
        ordering = ['descripcio'] 
    def __unicode__(self):
        return self.descripcio     

class Prestacio(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True)
    codi = models.CharField(max_length=30)
    tipus_prestacio = models.ForeignKey('TipusPrestacio', null=True, blank=True)
    es_grup_monitoritzacio = models.IntegerField(null=True, blank=True)
    descripcio = models.CharField(max_length=240, blank=True)
    codi_seram = models.ForeignKey('CodiSeram', null=True, blank=True)
    tipo_exploracion_idi_id = models.IntegerField(null=True, blank=True)
    zona_anatomica_idi = models.ForeignKey('ZonaAnatomica', null=True, blank=True)
    prestacio_idi = models.ForeignKey('PrestacioIdi', null=True, blank=True)
    class Meta:
        db_table = u'prestacio'
        ordering = ['codi'] 
    def __unicode__(self):
        return self.codi + " - " + self.descripcio     

class PrestacioIdi(models.Model):
    connection_name = 'portal_dades'# Static Var
    idexploracion = models.IntegerField(primary_key=True, db_column='IdExploracion') # Field name made lowercase.
    codexploracion = models.CharField(max_length=30, db_column='CodExploracion') # Field name made lowercase.
    exploracion = models.CharField(max_length=240, db_column='Exploracion', blank=True) # Field name made lowercase.
    idtipoexploracion = models.IntegerField(db_column='IdTipoExploracion') # Field name made lowercase.
    idcodiseram = models.ForeignKey( 'CodiSeram', null=True, db_column='IdCodiSeram', blank=True) # Field name made lowercase.
    idzonaanatomica = models.IntegerField(db_column='IdZonaAnatomica') # Field name made lowercase.
    class Meta:
        db_table = u'exploracion'
    def __unicode__(self):
        return self.codexploracion         


class CodificacioGenerica(models.Model):
    connection_name = 'portal_dades'# Static Var
    id =  models.IntegerField(primary_key=True)
    codi = models.CharField(max_length=3, blank=True)
    descripcio = models.CharField(max_length=254, blank=True)
    class Meta:
        db_table = u'codificacio_generica'
    def __unicode__(self):
        return self.codi
    
class TipusServei(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True)
    codi = models.CharField(max_length=6)
    descripcio = models.CharField(max_length=135)
    es_hospital = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'tipus_servei'
    def __unicode__(self):
        return self.descripcio
        
class Servei(models.Model):
    connection_name = 'portal_dades'# Static Var
    hospital =  models.ForeignKey('Hospital', null=True, blank=True)
    id = models.IntegerField(primary_key=True)
    codi = models.CharField(max_length=24)
    inici = models.CharField(max_length=24, blank=True)
    fi = models.CharField(max_length=24)
    tipus_servei = models.ForeignKey('TipusServei' , null=True) # aqui necesitem un left join 
    uci = models.IntegerField(null=True, blank=True)
    cae = models.IntegerField(null=True, blank=True)
    spec = models.CharField(max_length=15, blank=True)
    unpro = models.CharField(max_length=30, blank=True)
    descripcio = models.CharField(max_length=120, blank=True)
    hospital_codi = models.CharField(max_length=6)
    es_hospitalitzat = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'servei'
    def __unicode__(self):
        return self.descripcio

class TipusModalitat(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True)
    codi = models.CharField(max_length=135)
    descripcio = models.CharField(max_length=135)
    idi_tipo_exploracion_id = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'tipus_modalitat'
    def __unicode__(self):
        return self.descripcio
    

class ProvaOhdim(models.Model):
    # Static Vars
    connection_name = 'portal_dades'
    #
    id = models.IntegerField(primary_key=True)
    hospital = models.ForeignKey('Hospital', null=True, blank=True)
    episodi_codi = models.CharField(max_length=30, blank=True)
    idsap = models.IntegerField(null=False)
    prestacio = models.ForeignKey('Prestacio', null=True, blank=True)
    status_prestacio_codi = models.CharField(max_length=9, blank=True)
    motiu_anullacio_codi = models.CharField(max_length=9, blank=True)
    historia_clinica_id = models.IntegerField(null=True, blank=True)
    tipus_episodi = models.CharField(max_length=6, blank=True)
    tipus_moviment = models.CharField(max_length=3, blank=True)
    clase_moviment = models.CharField(max_length=6, blank=True)
    ut_solicitant_codi = models.CharField(max_length=24, blank=True)
    #ut_solicitant_codi = models.ForeignKey('Servei', to_field='codi' , null=True, blank=True)
    servei_solicitant = models.ForeignKey('Servei', null=True, blank=True)
    up_solicitant_id = models.IntegerField(null=True, blank=True)
    ut_gestora_codi = models.CharField(max_length=24, blank=True)
    servei_gestor_codi = models.CharField(max_length=24, blank=True)
    up_gestora_id = models.IntegerField(null=True, blank=True)
    es_cae_ut_gestora = models.IntegerField(null=True, blank=True)
    ut_agenda_codi = models.CharField(max_length=24, blank=True)
    agenda = models.ForeignKey('Agenda', to_field='id' ,db_column='agenda_id', null=True, blank=True)
    agenda_codi = models.ForeignKey('Agenda',to_field='codi', db_column='agenda_codi',null=True, blank=True)
    es_maquina_portatil = models.IntegerField(null=True, blank=True)
    tipus_modalitat = models.ForeignKey('TipusModalitat', null=True, blank=True)
    centre_derivacio = models.CharField(max_length=30, blank=True)
    es_urgent_programada = models.IntegerField(null=True, blank=True)
    es_diagnostic_seguiment = models.IntegerField(null=True, blank=True)
    tecnic_realitza = models.ForeignKey('UsuariSAP',null=True, blank=True)
    metge_informa = models.ForeignKey('Metge', null=True, blank=True)
    metge_revisa = models.ForeignKey('Metge', null=True, blank=True)
    metge_allibera = models.ForeignKey('Metge', null=True, blank=True)
    interlocutor_ordenant = models.IntegerField(null=True, blank=True)
    codificacio_generica = models.ForeignKey('CodificacioGenerica' , null=True, blank=True) # models.CharField(max_length=3, blank=True)
#    codificacio_generica = models.ForeignKey(CodificacioGenerica , null=True, blank=True ) # models.CharField(max_length=3, blank=True)
    concepte_facturable = models.CharField(max_length=54, blank=True)
    data_sollicitud_prestacio = models.DateField(null=True , blank=True)
    hora_sollicitud_prestacio = models.TimeField(null=True ,blank=True )
    data_cita = models.DateField(null=True, blank=True )
    hora_cita = models.TimeField(null=True, blank=True)
    data_adminisio_prestacio = models.DateField(null=True , blank=True) # nota s'ha de opssar blank=True per a que si es deixa en buit agafi null sino peta io diu que has de possar una data
    hora_admisio_prestacio = models.TimeField(null=True ,blank=True)
    data_inici_prestacio = models.DateField(null=True ,blank=True)
    hora_inici_prestacio = models.TimeField(null=True, blank=True)
    data_fi_prestacio = models.DateField(null=True ,blank=True)
    hora_fi_prestacio = models.TimeField(null=True ,blank=True)
    data_document_informat = models.DateField(null=True ,blank=True)
    hora_document_informat = models.TimeField(null=True ,blank=True )
    data_document_revisat = models.DateField(null=True ,blank=True)
    hora_document_revisat = models.TimeField(null=True ,blank=True)
    data_neixament = models.DateField(null=True ,blank=True)
    pais_neixament_codi = models.CharField(max_length=6, blank=True)
    sexe_id = models.IntegerField(null=True, blank=True)
    pais_residencia_codi = models.CharField(max_length=6, blank=True)
    regim_financier_episodi = models.CharField(max_length=6, blank=True)
    provincia = models.IntegerField(null=True, blank=True)
    comarca = models.IntegerField(null=True, blank=True)
    municipi = models.IntegerField(null=True, blank=True)
    localitat = models.IntegerField(null=True, blank=True)
    area_basica_salut = models.CharField(max_length=12, blank=True)
    codi_postal = models.CharField(max_length=15, blank=True)
    numero_projeccions = models.DecimalField(null=True, max_digits=16, decimal_places=3, blank=True)
    quantitat = models.DecimalField(null=True, max_digits=16, decimal_places=3, blank=True)
    numero_projeccions_erronies = models.DecimalField(null=True, max_digits=16, decimal_places=3, blank=True)
    numero_plaques = models.DecimalField(null=True, max_digits=16, decimal_places=3, blank=True)
    versio = models.DateTimeField()
    
    
    class Meta:
        db_table = u'prova_ohdim'
        unique_together = (('id', 'hospital'),)
    def __unicode__(self):
        return    str(self.hospital) + ' - '  + str(self.idsap) +  ' - ' + str(self.data_cita) + ' -> '+ str(self.prestacio) 
         

from datetime import datetime
from apps.meta.models import AuthUser

class ProvaOhdimCodificacioGenerica(models.Model):
    connection_name = 'portal_dades'# Static Var 
    id = models.IntegerField(primary_key=True, null=True, blank=True)
    prova_ohdim = models.OneToOneField('ProvaOhdim')# es  millor que foreign key   https://docs.djangoproject.com/en/dev/ref/models/fields/#onetoonefield
    codificacio_generica_inicial = models.ForeignKey(CodificacioGenerica, blank=True,null=True)# blank=True per quan creem un valor
    codificacio_generica_final = models.ForeignKey(CodificacioGenerica, null=True, blank=True)
    data = models.DateTimeField(default=datetime.now)
    modificat = models.BooleanField(null=True, blank=True)
    modificat_per = models.ForeignKey(User, db_column='modificat_per_id',null=True, blank=True)#models.OneToOneField(AuthUser, to_field="username", verbose_name=('Usuari'), blank=True, null=True)#  
    modificat_data = models.DateTimeField(null=True, blank=True , verbose_name=('Data modificació'))
    validat = models.BooleanField(null=True, blank=True)
    validat_per = models.ForeignKey(User, db_column='validat_per_id',null=True, blank=True)#models.OneToOneField(AuthUser, to_field="username", verbose_name=('Usuari'), blank=True, null=True)# models.IntegerField( null=True, blank=True)
    validat_data = models.DateTimeField(null=True, blank=True, verbose_name=('Data validació'))
    revalidat = models.BooleanField(null=True, blank=True)
    revalidat_per = models.ForeignKey( User, db_column='revalidat_per_id',null=True, blank=True)#models.OneToOneField(AuthUser, to_field="username", verbose_name=('Usuari'), blank=True, null=True)# models.IntegerField( null=True, blank=True)
    revalidat_data = models.DateTimeField(null=True, blank=True , verbose_name=('Data confirmació'))
    codificacio_generica_validada = models.ForeignKey(CodificacioGenerica, null=True, blank=True)
    
    class Meta:
        db_table = u'prova_ohdim_codificacio_generica'
    def __unicode__(self):
        return  str(self.id) +  ': ' + str(self.prova_ohdim_id)
#
## ... Aquesta nomes serveix quan creem desde la interficie esclar si fem la importacio del MDO el portal no s'entera
#def create_ProvaOhdimCodificacioGenerica(sender, instance, created, **kwargs):
#    if created:
#        ProvaOhdimCodificacioGenerica.objects.create(user=instance)
#
#post_save.connect(create_ProvaOhdimCodificacioGenerica, sender=AuthUser, dispatch_uid="create_ProvaOhdimCodificacioGenerica")
#    ## en les vistes -->  p = User.objects.get(username="xxxx").get_profile()  # notice the ()
#    ##                    print p.attribute1

class TipusEpisodi(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True)
    codi = models.CharField(max_length=6)
    descripcio = models.CharField(max_length=135)
    class Meta:
        db_table = u'tipus_episodi'

class TipusPrestacio(models.Model):
    connection_name = 'portal_dades'# Static Var
    id = models.IntegerField(primary_key=True)
    codi = models.CharField(max_length=135)
    descripcio = models.CharField(max_length=135)
    class Meta:
        db_table = u'tipus_prestacio'



class UnitatIdi(models.Model):
    connection_name = 'portal_dades'# Static Var
    idunitat = models.IntegerField(db_column='IdUnitat') # Field name made lowercase.
    unitat = models.CharField(max_length=600, db_column='Unitat', blank=True) # Field name made lowercase.
    idcentre = models.IntegerField(db_column='IdCentre') # Field name made lowercase.
    class Meta:
        db_table = u'unitat_idi'

class UnitatProductiva(models.Model):
    connection_name = 'portal_dades'# Static Var
    codup = models.CharField(max_length=15, db_column='CodUP') # Field name made lowercase.
    desup = models.CharField(max_length=300, db_column='DesUP', blank=True) # Field name made lowercase.
    epcod = models.CharField(max_length=12, blank=True)
    identidad = models.IntegerField(null=True, db_column='IdEntidad', blank=True) # Field name made lowercase.
    idup = models.IntegerField(db_column='IdUp') # Field name made lowercase.
    coscs = models.CharField(max_length=135, blank=True)
    ambit = models.CharField(max_length=135, blank=True)
    titular = models.CharField(max_length=135, blank=True)
    tipus = models.CharField(max_length=135, blank=True)
    coddap = models.CharField(max_length=15, db_column='CodDap', blank=True) # Field name made lowercase.
    id = models.IntegerField()
    id_borram = models.IntegerField(db_column='ID_BORRAM') # Field name made lowercase.
    class Meta:
        db_table = u'unitat_productiva'        
        
        
        
        
        
        

#''''''''''''''
# Formalaris de models  https://docs.djangoproject.com/en/1.3/topics/forms/modelforms/
#''''''''''''''
####################################################################

#class ReadOnlyWidget(Widget):
#
#    def __init__(self, original_value, display_value=None):
#        self.original_value = original_value
#        if display_value:
#            self.display_value = display_value
#        super(ReadOnlyWidget, self).__init__()
#
#    def _has_changed(self, initial, data):
#        return False
#
#    def render(self, name, value, attrs=None):
#        if  hasattr(self, 'display_value'):
#            if self.display_value is not None:
#                return unicode(self.display_value)
#        return unicode(self.original_value)
#
#    def value_from_datadict(self, data, files, name):
#        return self.original_value



def _get_cleaner(form, field):
    # quan fem un modelDrom.isvalid() -> entre altres crida la funcio  _clean_fields(self) de la clase .form.forms on crida "clean" de cada camp
    # es la funcio aquesta i l'omple despres amb el valor que havia retornat de la qurey
    def clean_field():
        if form.instance and form.instance.pk:
            return getattr(form.instance, field, None)
        else:
            return form.cleaned_data[field]
    return clean_field

class ROFormMixin(BaseForm):
    def __init__(self, *args, **kwargs):
        super(ROFormMixin, self).__init__(*args, **kwargs)
        #print self.fields['usuari_modificador_id']
                
        if hasattr(self, "read_only"):
            if self.instance: #and self.instance.pk: # aixo nomes en cas que colguem que la primera vegada que editem els readonly es pugui. en aquest cas no volem 
                for field in self.read_only:
                    #self.fields[field].widget = ReadOnlyWidget(field) NO CHUTA 
                    self.fields[field].widget.attrs['readonly'] = True
                    ###proves
                    # self.fields[field].required = False
                #self.fields[field].widget.attrs['disabled'] = 'disabled'  #NO CHUTA 
                    ###Fi proves
                    setattr(self, "clean_" + field, _get_cleaner(self, field))#afegim la funcio de netejar el camp readonly-> posa None i despres carrega el valor en questió
                    

class ProvaOhdimCodificacioGenerica_FormBase(ModelForm):
        class Meta:
            model = ProvaOhdimCodificacioGenerica
            fields = ['id' ,  'codificacio_generica_final']
            #fields = ['id' , 'codificacio_generica_inicial', 'codificacio_generica_final', 'data','modificat','modificat_per','modificat_data','validat','validat_per','validat_data','revalidat','revalidat_per','revalidat_data' ]
            #exlude =['prova_ohdim_id'] OJO aixo no va perque com fas exlude no sap el camps que hi ha i per alguna rao fa un select de tot!!! prova_ohdim !!!!



class ProvaOhdimCodificacioGenerica_Form(ProvaOhdimCodificacioGenerica_FormBase ,ROFormMixin):
        read_only = ('id', )#,  'data','modificat','modificat_per','modificat_data','validat','validat_per','validat_data','revalidat','revalidat_per','revalidat_data' )
        def __init__(self, *args, **kwargs):
            #hereda de provaohdim i de ROmixin on fara que els camps defints ro siguin read only
            super(ProvaOhdimCodificacioGenerica_Form, self).__init__(*args, **kwargs) 
