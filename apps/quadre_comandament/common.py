# coding: utf8
from django.db import connections
#from django.template import RequestContext


def initFilters(request, enabled_filters, current_filters):
    request.session[current_filters] = enabled_filters
    if request.user.authuserprofile.unitat_idi_id != 17:
        try:
            del request.session[current_filters]['centre']
            del request.session[current_filters]['unitat']
        except:
            pass
        
    if 'filters' not in request.session:
        
        cursor = connections['default'].cursor()
        # TODO: nomes shan de mostrar les unitats del centre sellecionat!!!
        # Centres
        #query = "select * from portal.meta_centre_idi;"
        query = "SELECT idi_id, descripcio FROM `portal_dades`.`hospital` order by idi_id;"
        cursor.execute(query, [])
        centres = cursor.fetchall()
 
        #permisos del usuari
        permisosStr = ()
        permisos = request.user.authuserprofile.unitats_idi_permissos.all()
        for permis in permisos:
            permisosStr = permisosStr  + (int(permis.id),) 
        if len(permisosStr) == 0: # si no te permisos assignats, com a minim li donem els que te a la unitat on esta donat d'alta?
            permisosStr = '('+ str(request.user.authuserprofile.unitat_idi_id ) +')'
        if len(permisosStr) == 1:#hem d'esborral la ","    
            permisosStr = str(permisosStr).replace(',','')
        # Unitats
        if(request.user.authuserprofile.unitat_idi_id == 17): 
            query = "SELECT id, descripcio FROM portal.meta_unitat_idi where actiu=1;"
        else:
            #query = 'SELECT id, descripcio FROM portal.meta_unitat_idi ci WHERE actiu=1 AND ci.id = ' + str(request.user.authuserprofile.unitat_idi_id) + '; '
            query = 'SELECT id, descripcio FROM portal.meta_unitat_idi ci WHERE actiu=1 AND ci.id in ' + str(permisosStr) + '; '
        cursor.execute(query, [])
        unitats = cursor.fetchall()
        
        # Entitats
        query = "SELECT id as IdEntidad, descripcio as Entidad FROM `portal`.`meta_entitat` order by descripcio;"
        cursor.execute(query, [])
        entitats = cursor.fetchall()      
        
        # Modalitats
        if(request.user.authuserprofile.unitat_idi_id == 17): 
            query = 'SELECT * FROM portal.meta_tipoexploracion;'
        else:
            query =  ' SELECT DISTINCTROW agenda_idi.IdTipoExploracion,TipoExploracion FROM  portal_dades.unitat_idi'
            query += '   INNER JOIN portal_dades.agenda_idi on portal_dades.unitat_idi.IdUnitat = portal_dades.agenda_idi.unitat_idi_id'
            query += '   INNER JOIN portal.meta_tipoexploracion ON  portal.meta_tipoexploracion.IdTipoExploracion = agenda_idi.IdTipoExploracion'
            query += ' WHERE unitat_idi_id in ' + str(permisosStr) + '; '
        cursor.execute(query, [])
        modalitats = cursor.fetchall()   
        
        # Mesos
        query = "select * from portal_dades.mes;"
        cursor.execute(query, [])
        mesos = cursor.fetchall() 
        
        # Anys
        cursor = None
        cursor = connections['portal_dades'].cursor()
        query = "SELECT year(min(p.`data_cita`)) as any_min, year(max(p.`data_cita`)) as any_max FROM prova_ohdim p;"
        cursor.execute(query, [])
        anys_periode = cursor.fetchall()[0]
            
        anys_tmp = range( 2010 ,anys_periode[1]+1) # per ara nomes tenim a partir del 2010
        anys = []
        for i in anys_tmp:
            anys.append((int(i),str(i)))
            
        cursor = None
        
        filtres = [
                   {'nom': "any", 'valors': anys},
                   {'nom': "centre", 'valors': centres},
                   {'nom': "unitat", 'valors': unitats},
                   {'nom': "entitat", 'valors': entitats},
                   {'nom': "modalitat", 'valors': modalitats},
                   {'nom': "mes", 'valors': mesos},
                                            
        ]
        
        request.session['filters'] = filtres
