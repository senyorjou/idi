# coding: utf-8
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
import apps.meta.auditoria
from django.shortcuts import redirect


@login_required
#tohom ha de tenir permisos
#@permission_required('meta.guardies', login_url='/accounts/permisdenegat/')
def index(request):
    apps.meta.auditoria.request_handler(request,12,'' )
    
    return redirect('http://calaix.idi-cat.org')

