from apps.inventari.models import Sala, Aparell, TargetaGrafica, Pantalla, Empresa, Marca, Model, Memoria, Cpu, Tipus, AparellPantalles, Nota, AparellGrafiques
from django.contrib import admin


class SalaAdmin(admin.ModelAdmin):
    model = Sala
    list_display = ['unitat','nom']
    list_filter = [ 'unitat']

class TargetaGraficaAdmin(admin.ModelAdmin):
    list_display = ['nom']

class PantallaAdmin(admin.ModelAdmin):
    list_display = ['nom']

class EmpresaAdmin(admin.ModelAdmin):
    list_display = ['nom']

class MarcaAdmin(admin.ModelAdmin):
    list_display = ['nom']

class ModelAdmin(admin.ModelAdmin):
    list_display = ['nom']
    
class MemoriaAdmin(admin.ModelAdmin):
    list_display = ['nom']
    
class CpuAdmin(admin.ModelAdmin):
    list_display = ['nom']

class TipusAdmin(admin.ModelAdmin):
    list_display = ['nom']

class AparellGrafiquesInline(admin.StackedInline):
    model = AparellGrafiques
    max_num = 2
    can_delete = False

class AparellPantallesInline(admin.StackedInline):
    model = AparellPantalles
    max_num = 3
    can_delete = False

class NotesInline(admin.StackedInline):
    model = Nota
    extra = 1
    can_delete = False
        
class AparellAdmin(admin.ModelAdmin):
    model = Aparell
    list_display = ['etiqueta', 'etiquetahospital', 'model', 'unitat', 'sala', 'ip']
    list_filter = [ 'unitat']
    search_fields = ['descripcio', 'etiqueta', 'etiquetahospital', 'ip']
    inlines = [AparellPantallesInline, AparellGrafiquesInline, NotesInline]


# Inventaris
admin.site.register(Aparell, AparellAdmin)
admin.site.register(Sala, SalaAdmin)
admin.site.register(TargetaGrafica, TargetaGraficaAdmin)
admin.site.register(Pantalla, PantallaAdmin)
admin.site.register(Empresa, EmpresaAdmin)
admin.site.register(Marca, MarcaAdmin)
admin.site.register(Model, ModelAdmin)
admin.site.register(Memoria, MemoriaAdmin)
admin.site.register(Cpu, CpuAdmin)
admin.site.register(Tipus, TipusAdmin)
admin.site.register(AparellPantalles)
admin.site.register(Nota)
