# encoding: utf8

from django.db import models
from apps.meta.models import MetaUnitatIdi
from apps.smart_selects.db_fields import ChainedForeignKey 
    
#class Idea(models.Model):
#    id = models.IntegerField(primary_key=True)
#    data = models.DateTimeField()
#    user = models.ForeignKey(AuthUser, null=True, blank=True)
#    concepte = models.CharField(max_length=768, blank=True)
#    idea = models.TextField(blank=True)
#    resposta = models.TextField(blank=True)
#    class Meta:
#        db_table = u'intranet_idees'    
#    def __unicode__(self):
#        return unicode(self.concepte)
#              'dgomez@idi.catsalut.cat', [self.user.email], fail_silently=False)           

TIPUS_APARELLS = {"PC":"PC",
                  "Impresora": "Impresora",
                  "Multifunció": "Multifunci�",
                  "Modalitat":"Modalitat",
                  "Reveladora":"Reveladora",
                  "Workstation":"Workstation",
                  "Scaner":"Scaner",
                  "Servidor":"Servidor"                
                  }

# imit_choices_to=TIPUS_APARELLS, 
class Sala(models.Model):
    # Static Vars
    connection_name = 'inventari'
    
    id = models.AutoField(primary_key=True)
    unitat = models.ForeignKey(MetaUnitatIdi, null=True, blank=True)
    nom = models.CharField(max_length=45, null=True, blank=True)
    
    class Meta:
        db_table = u'sala'
        ordering = ['nom']
    def __unicode__(self):
        return self.nom   

class TargetaGrafica(models.Model):
    # Static Vars
    connection_name = 'inventari'
    
    id = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=64, null=True, blank=True)
    
    class Meta:
        db_table = u'targetagrafica'
        ordering = ['nom']
    def __unicode__(self):
        return self.nom
            
class AparellGrafiques(models.Model):
    # Static Vars
    connection_name = 'inventari'
    
    id = models.AutoField(primary_key=True)
    aparell = models.ForeignKey('Aparell', null=True, blank=True)
    targetagrafica = models.ForeignKey(TargetaGrafica, null=True, blank=True)
    class Meta:
        db_table = u'aparell_grafiques'

class AparellPantalles(models.Model):
    # Static Vars
    connection_name = 'inventari'
    
    id = models.AutoField(primary_key=True)
    aparell = models.ForeignKey('Aparell', null=True, blank=True)
    pantalla = models.ForeignKey('Pantalla', null=True, blank=True)
    class Meta:
        db_table = u'aparell_pantalles'

class Pantalla(models.Model):
    # Static Vars
    connection_name = 'inventari'
    
    id = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=64, null=True, blank=True)
    
    class Meta:
        db_table = u'pantalla'
        ordering = ['nom']
    def __unicode__(self):
        return self.nom        

class Cpu(models.Model):
    # Static Vars
    connection_name = 'inventari'
    
    id = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=64, null=True, blank=True)
    
    class Meta:
        db_table = u'cpu'
        ordering = ['nom']
    def __unicode__(self):
        return self.nom   

class Memoria(models.Model):
    # Static Vars
    connection_name = 'inventari'
    
    id = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=64, null=True, blank=True)
    
    class Meta:
        db_table = u'memoria'
        ordering = ['nom']
    def __unicode__(self):
        return self.nom   

class Model(models.Model):
    # Static Vars
    connection_name = 'inventari'
    
    id = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=64, null=True, blank=True)
    
    class Meta:
        db_table = u'model'
        ordering = ['nom']
    def __unicode__(self):
        return self.nom   

class Tipus(models.Model):
    # Static Vars
    connection_name = 'inventari'
    
    id = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=64, null=True, blank=True)
    
    class Meta:
        db_table = u'tipus'
        ordering = ['nom']
    def __unicode__(self):
        return self.nom      

class Marca(models.Model):
    # Static Vars
    connection_name = 'inventari'
    
    id = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=64, null=True, blank=True)
    
    class Meta:
        db_table = u'marca'
        ordering = ['nom']
    def __unicode__(self):
        return self.nom        

class Empresa(models.Model):
    # Static Vars
    connection_name = 'inventari'
    
    id = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=64, null=True, blank=True)
    
    class Meta:
        db_table = u'empresa'
        ordering = ['nom']
    def __unicode__(self):
        return self.nom
            
class Nota(models.Model):
    # Static Vars
    connection_name = 'inventari'
    
    id = models.AutoField(primary_key=True)
    aparell = models.ForeignKey('Aparell', null=True, blank=True)
    nom = models.CharField(max_length=64, null=True, blank=True)
    descripcio = models.TextField(null=True, blank=True)
    
    
    class Meta:
        db_table = u'nota'
        ordering = ['nom']
    def __unicode__(self):
        return self.nom        
                
class Aparell(models.Model):
    # Static Vars
    connection_name = 'inventari'
        
    id = models.AutoField(primary_key=True)
    unitat = models.ForeignKey(MetaUnitatIdi, null=False, blank=False)
    #sala = models.ForeignKey(Sala, null=False, blank=False)
    sala = ChainedForeignKey(Sala, chained_field="unitat", chained_model_field="unitat", show_all=False, auto_choose=True)
    tipus = models.ForeignKey(Tipus, null=False, blank=False)
    etiqueta = models.CharField(default='Pendent', max_length=45, null=False, blank=False, verbose_name="Etiqueta inventari IDI")
    etiquetahospital = models.CharField(max_length=45, null=True, blank=True, verbose_name="Etiqueta hospital")
    descripcio = models.CharField(max_length=45, null=True, blank=True)
    cpu = models.ForeignKey(Cpu, null=True, blank=True)
    memoria = models.ForeignKey(Memoria, null=True, blank=True)
    model = models.ForeignKey(Model, null=True, blank=True)
    marca = models.ForeignKey(Marca, null=True, blank=True)
    empresa = models.ForeignKey(Empresa, null=True, blank=True)
    numserie = models.CharField(max_length=64, null=True, blank=True)
    macaddr = models.CharField(max_length=64, null=True, blank=True)
    grafiques = models.ManyToManyField(TargetaGrafica, null=True, blank=True, through=AparellGrafiques)
    pantalles = models.ManyToManyField(Pantalla, null=True, blank=True, through=AparellPantalles)
    ip = models.CharField(max_length=256, null=True, blank=True)
    datacompra = models.DateField(null=True, blank=True, verbose_name="Data de compra")
    datafigarantia = models.DateField(null=True, blank=True, verbose_name="Data fi de garantia")
    #ip_ocupada = models.BooleanField(default=False, verbose_name="IP Lliure")

    class Meta:
        db_table = u'aparell'    
        #ordering = ['etiqueta']
    def __unicode__(self):
        return self.etiqueta
        
      