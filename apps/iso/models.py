# coding: utf-8
from django.db import models
from apps.meta.models import MetaUnitatIdi,AuthUser
from django.forms.models import BaseForm
from django.forms import ModelForm
from django.utils.encoding import smart_unicode
import datetime
from django.core.mail import send_mail
from django.conf import settings


PRIORITAT_VALUES = ( (1, 'Baixa'), (2, 'Normal'), (3, 'Alta'))
# TODO: Canviar estats
ESTAT_VALUES = ( (1, 'Pendent de solució'), (2, 'Solució proposada'), (3, 'Solució validada'), (4, 'Tancada'))       
ESTAT_AC_VALUES = ( (1, 'Pendent de solució'), (2, 'Solució proposada'), (3, 'Solució validada'), (4, 'Tancada'))
TIPUS_AC_VALUES = ((1, 'Correctiva'), (2, 'Preventiva'))

class Comentari(models.Model):
    id = models.AutoField(primary_key=True)
    descripcio = models.TextField(max_length=1500, null=True,  blank=True)
    user = models.ForeignKey(AuthUser, null=True,  blank=True)    
    data = models.DateTimeField(default=datetime.datetime.now)
    ticket = models.ForeignKey('CauTicket', null=True,  blank=True)
    class Meta:
        db_table = u'cau_ticket_comentari'
        ordering = ['-id']
    def __unicode__(self):
        return "Comentari #" + str(self.id)
    def notifica_responsable(self):
        self.ticket.notifica_responsable()
    def notifica_usuari(self):
        self.ticket.notifica_usuari()   
             
class AccioCorrectiva(models.Model):
    id = models.AutoField(primary_key=True)
    resum = models.CharField(max_length=135)
    descripcio = models.TextField(max_length=1500, null=True,  blank=True)
    tipus = models.IntegerField(choices=TIPUS_AC_VALUES, null=False, blank=False)
    unitat_idi = models.ForeignKey(MetaUnitatIdi,blank=True,default=17)# 17 es SSCC
    informat_per = models.ForeignKey(AuthUser, related_name="%(app_label)s_%(class)s_informat_per_related") # Field name made lowercase.
    analisi_causes = models.TextField(max_length=1500, null=True,  blank=True)
#    prioritat = models.IntegerField(choices=PRIORITAT_VALUES, default=1, verbose_name="Prioritat demanada")
    estat = models.IntegerField(choices=ESTAT_AC_VALUES, default=1)
    data_obertura = models.DateTimeField(default=datetime.datetime.now, verbose_name="Data obertura")
    solucio = models.TextField(max_length=1500, null=True,  blank=True)
    data_validacio_solucio = models.DateTimeField(null=True,  blank=True, verbose_name="Data obertura")
    solucio_validada_per = models.ForeignKey(AuthUser, null=True, blank=True,  related_name="%(app_label)s_%(class)s_assignat_a_related", limit_choices_to = {'is_superuser': True}) # Field name made lowercase.
    #modificat_per = models.ForeignKey(AuthUser, null=True,  blank=True,related_name="%(app_label)s_%(class)s_modificat_per_related", limit_choices_to = {'is_staff': True}) # Field name made lowercase.
    data_prevista_solucio = models.DateTimeField(null=True,  blank=True)
    validat_per = models.ForeignKey(AuthUser, null=True,  blank=True,related_name="%(app_label)s_%(class)s_tancat_per_related", limit_choices_to = {'is_staff': True}) # Field name made lowercase.
    tancat_per = models.ForeignKey(AuthUser, null=True,  blank=True,related_name="%(app_label)s_%(class)s_tancat_per_related", limit_choices_to = {'is_staff': True}) # Field name made lowercase.
    data_tancament = models.DateTimeField(null=True,  blank=True)
    
    class Meta:
        db_table = u'iso_acciocorrectiva'
    def __unicode__(self):
        return   u'Tiket:' + str( self.id )   
     
class NoConformitat(models.Model):
    id = models.AutoField(primary_key=True)
    resum = models.CharField(max_length=135)
    descripcio = models.TextField(max_length=1500, null=True,  blank=True)
    
#    categoria = models.ForeignKey(CauCategoria, blank=True)# 17 es SSCC
    unitat_idi = models.ForeignKey(MetaUnitatIdi,blank=True,default=17)# 17 es SSCC
    informat_per = models.ForeignKey(AuthUser, related_name="%(app_label)s_%(class)s_informat_per_related") # Field name made lowercase.
#    prioritat = models.IntegerField(choices=PRIORITAT_VALUES, default=1, verbose_name="Prioritat demanada")
    estat = models.IntegerField(choices=ESTAT_VALUES, default=1)
    data_obertura = models.DateTimeField(default=datetime.datetime.now, verbose_name="Data obertura")
    solucio = models.TextField(max_length=1500, null=True,  blank=True)
    data_validacio_solucio = models.DateTimeField(null=True,  blank=True, verbose_name="Data obertura")
    solucio_validada_per = models.ForeignKey(AuthUser, null=True, blank=True,  related_name="%(app_label)s_%(class)s_assignat_a_related", limit_choices_to = {'is_superuser': True}) # Field name made lowercase.
    #modificat_per = models.ForeignKey(AuthUser, null=True,  blank=True,related_name="%(app_label)s_%(class)s_modificat_per_related", limit_choices_to = {'is_staff': True}) # Field name made lowercase.
    data_prevista_solucio = models.DateTimeField(null=True,  blank=True)
    tancat_per = models.ForeignKey(AuthUser, null=True,  blank=True,related_name="%(app_label)s_%(class)s_tancat_per_related", limit_choices_to = {'is_staff': True}) # Field name made lowercase.
    validat_per = models.ForeignKey(AuthUser, null=True,  blank=True,related_name="%(app_label)s_%(class)s_tancat_per_related", limit_choices_to = {'is_staff': True}) # Field name made lowercase.
    data_tancament = models.DateTimeField(null=True,  blank=True)
    accio_correctiva = models.ForeignKey(AccioCorrectiva, null=True,  blank=True)

    class Meta:
        db_table = u'iso_noconformitat'
        #abstract = True
    def notifica_responsable(self):
        if self.informat_per.email != None:
            send_mail("[IDI] Tiquet #" + str(self.id) + " - " + self.get_estat_display() + " - " + str(self.unitat_idi), 
                      "El tiquet #" + str(self.id) + " ha estat actualitzat. \n\n" +
                      "Resum: \n\n" + self.resum + "\n\n" +
                      "Comentaris: \n\n" + "".join(["["+str(x.data)+"]\n" + x.descripcio + "\n\n" for x in self.cauticketcomentari_set.all()]) + 
                      "Pots visualitzar-ne els detalls a l'apartat d'eines de la intranet.\n\n" + "\n" +
                      "http://web.idi-cat.org/cau/incidencia/"+ str(self.id) + "/\n\n", 
                      settings.DEFAULT_FROM_EMAIL, [self.assignat_a.email], fail_silently=False)
    def notifica_usuari(self):
        if self.assignat_a != None:
            send_mail("[IDI] Tiquet #" + str(self.id) + " - " + self.get_estat_display(), 
                      "El tiquet #" + str(self.id) + " ha estat actualitzat. \n\n" +
                      "Resum: \n\n" + self.resum + "\n\n" +
                      "Comentaris: \n\n" + "".join(["["+str(x.data)+"]\n" + x.descripcio + "\n\n" for x in self.cauticketcomentari_set.all()]) + "\n" +                     
                      "Pots visualitzar-ne els detalls a l'apartat d'eines de la intranet.\n\n" +
                      "http://web.idi-cat.org/cau/incidencia/"+ str(self.id) + "/\n\n",
                      settings.DEFAULT_FROM_EMAIL, [self.informat_per.email], fail_silently=False)
    def __unicode__(self):
        return   u'Tiket:' + str( self.id ) 
