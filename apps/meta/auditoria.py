'''
Created on 26/07/2011
@author: amateu
'''
from apps.meta.models import Audit,TipusAudit
import datetime
       

#user_logged_in = Signal(providing_args=['request', 'user'])
def login_handler( sender, **kwargs):
    #print "user login! " + str( kwargs['user'])
    a = Audit(idauth_user= kwargs['user'],dataevent=datetime.datetime.now(),idtipusaudit=TipusAudit(idtipusaudit=1))#1 es login
    a.save()

   
#user_logged_out = Signal(providing_args=['request', 'user'])
def logout_handler( sender, **kwargs):
    #print "user logout! " + str(kwargs['user'])
    a = Audit(idauth_user= kwargs['user'],dataevent=datetime.datetime.now(),idtipusaudit=TipusAudit(idtipusaudit=2))#1 es login
    a.save()

    
def request_handler( request,tipusauditoria,dades = ''):
    '''
    per tal dafegir  un registre a la BD dalgun event nomes cal cridar:
    apps.audit.auditoria.request_handler(request,tipusDeAuditoria,rutaInf)  
    on 
    request si estem en la views es el objecte amb la info del client i el que pregunta,
    tipusDeAuditoria: es el itpus ( podem afegir mes mitjancant el http:/rutaserver/apps/admin) i podem habilitar i desavilitar els events que volem enregistrear ('actiu')
    dades: les dades relevents al event , en la descripcio del tipusAudit posem entre parenteisis les dades que son, exemple (id de pacient)
    '''
    
    
    if TipusAudit.objects.get(idtipusaudit=tipusauditoria).actiu:
        a = Audit(idauth_user= request.user,dataevent=datetime.datetime.now(),idtipusaudit=TipusAudit(idtipusaudit=tipusauditoria),dades = dades)#1 es login
        a.save()
   
