# coding: utf8

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Permission
import urlparse

from django.conf import settings
from django.http import HttpResponseRedirect
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect

# Avoid shadowing the login() and logout() views below.
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.sites.models import get_current_site
from django.contrib.auth.views import password_reset

from django.views.generic.simple import redirect_to
from apps.meta.models import AuthUser,AuthUserProfile
from django.shortcuts import render_to_response, redirect


@login_required
def main_index_view(request):
    '''
    Nomes mostrarem aquelles aplicacions a les que el user tingui permisos
    hem de recuperar tots els permisos i mirar quins perms estan i daquest dins de eprmisos 
    recueprar el nom ( que sera la ruta en inde.html /{{urlApp}}/ ) i la descripcio {{desName}}
    per a que ho vegui el ususari
    '''
    from apps.meta.models import Meta
    apps = Meta().permissions

    permsUsuari = request.user.get_all_permissions()
    
    #TOTES LES EINES QUE VOLGUEM QUE TINGUIN TOTS AQUI!!!
    permsUsuari.add('meta.cau')
    permsUsuari.add('meta.calaix')
    
    # em si te permisos  15 i 17 informatics, 26 cap unitat, 27 director unitat 
    # 9,8,6, 30 son facultatius adjunts, els resis cobren minuta i no estan donats d'alta al sistema per tant no podran veure's aqui
    #Facultatius 
    if AuthUserProfile.objects.get( user = AuthUser.objects.get(username =str(request.user)).id).categoria_profesional.id in [4,5,6,15,17, 26,27,31,30]:#
        permsUsuari.add('meta.seguiment_personal')
    else:
        try:
            permsUsuari.remove('meta.seguiment_personal')
        except KeyError:
            pass
    lpemisos =[]
    
    if 'intranet' in request.session and request.session['intranet']:
        p = Permission.objects.get(codename='intranet')
        lpemisos.append(p)
        #return HttpResponseRedirect("/intranet/calendari_personal/")
        #return HttpResponseRedirect("/intranet/")
        return redirect('calendari_personal_intranet')
    else:
        for perm in permsUsuari:
            codename = perm.split('.')[1]
            if any(unicode(codename) in unicode(i) for i in apps):
                if not codename == 'intranet':
                    p = Permission.objects.get(codename=codename)
                    lpemisos.append(p)  
        
       
    return render_to_response('meta/index.html', {'permisos_usuari': lpemisos},
                              context_instance=RequestContext(request)) 

    
@login_required 
def permisdenegat(request):
    '''
    Es mostra la llista de permisos que te el usuari
    '''
    permsUsuari = request.user.get_all_permissions()
    lpemisos =[]  
    for perm in permsUsuari:
        p = Permission.objects.get(codename=perm.split('.')[1])
        lpemisos.append(p)  
    
    return render_to_response('meta/permisdenegat.html', {'permisos_usuari': lpemisos},
                              context_instance=RequestContext(request))
  
  

def permisdenegat_intranet(request):
    '''
    Es mostra un  missatge notificant al ususari que ha de sortir i tornar a entrar ala intranet
    ja que esta en apliacions o a l inreves
    '''
    return render_to_response('meta/permisdenegat_intranet.html' ,
                              context_instance=RequestContext(request))
  
           
#@login_required
#class AboutView(TemplateView):
#    '''
#    Es una forma rapida de retonar una pagina estatica ( ES una classe heretada i no una funcio!!!)
#    la podem passar atraves de la urls.py i menys codi encara = millor= mes dijango!!!
#    '''
#    template_name = "meta/sobre.html"
#

 
def password_reset_idi(request):
    """
    django.contrib.auth.views.password_reset view (forgotten password)
    """
    if not request.user.is_authenticated():
        return password_reset(request,
                          template_name='registration/password_reset_form.html',
                          email_template_name= 'registration/password_reset_email.html',
                          post_reset_redirect='/registration/reset/done')
    else:
        return HttpResponseRedirect("/meta/index.html")
#    



@csrf_protect
@never_cache
def login(request, template_name='registration/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.REQUEST.get(redirect_field_name, '')

    if request.method == "POST":
        form = authentication_form(data=request.POST)
        if form.is_valid():
            netloc = urlparse.urlparse(redirect_to)[1]

            # Use default setting if redirect_to is empty
            if not redirect_to:
                redirect_to = settings.LOGIN_REDIRECT_URL

            # Security check -- don't allow redirection to a different
            # host.
            elif netloc and netloc != request.get_host():
                redirect_to = settings.LOGIN_REDIRECT_URL

            # Okay, security checks complete. Log the user in.
            auth_login(request, form.get_user())

            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()
            
            #amp modificacio, aqui afgim a la sessio la sellcio del usuari
            intranet = request.POST.get('intranet')
            if (intranet == 'True'):
                request.session['intranet'] = True
                redirect_to = "/intranet/calendari_personal/"                
                #redirect_to = "/intranet/calendari_personal/"
            else:
                request.session['intranet'] = False

          
            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    request.session.set_test_cookie()

    current_site = get_current_site(request)

    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    context.update(extra_context or {})
    return render_to_response(template_name, context,
                              context_instance=RequestContext(request, current_app=current_app))


