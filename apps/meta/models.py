# coding: utf-8
#
from django.db import models
from django.contrib.auth.models import User
from django.forms.models import BaseForm
import datetime
from apps.idirh.models import CalendariLaboral, JornadaAnual

# Models de PLANILLES que caldria importar,
# (es referancien directament perevitar imports circulars)
# https://docs.djangoproject.com/en/dev/ref/models/fields/#foreignkey
# from apps.planilles.models import CalendariLaboral

#from django.db.models.signals import post_save #profiles

'''
CONTROL DE AUDITORIES , LOGINS I EL QUE VLGUEM AUDITAR
'''
import django.db.models.options as options
options.DEFAULT_NAMES = options.DEFAULT_NAMES + ('using',)

#class MultiDBModel(models.Model):
#    # A handy constant for the name of the alternate database.
##    class Meta:
##        using = 'other'
#    def save_model(self, request, obj, form, change):
#        # Tell Django to save objects to the 'other' database.
#        obj.save(using=self.Meta.using)
#    def delete_model(self, request, obj):
#        # Tell Django to delete objects from the 'other' database
#        obj.delete(using=self.Meta.using)
#    def queryset(self, request):
#        # Tell Django to look for objects on the 'other' database.
#        return super(MultiDBModel, self).queryset(request).using(self.Meta.using)
#    
#    def list_filter2(self,request):
#        # Tell Django to look for objects on the 'other' database.
#        return super(MultiDBModel, self).list_filter(request).using(self.Meta.using)
#    
#    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
#        # Tell Django to populate ForeignKey widgets using a query
#        # on the 'other' database.
#        return super(MultiDBModel, self).formfield_for_foreignkey(db_field, request=request, using=self.Meta.using, **kwargs)
#    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
#        # Tell Django to populate ManyToMany widgets using a query
#        # on the 'other' database.
#        return super(MultiDBModel, self).formfield_for_manytomany(db_field, request=request, using=self.Meta.using, **kwargs)



class TipusAudit(models.Model):
    '''
    control de auditoria , enregistra els accesos a la BD i les consultes
    '''
    idtipusaudit = models.AutoField(db_column='id_tipus_audit',primary_key=True)
    destipusaudit = models.CharField(max_length=255, db_column='des_tipus_audit', blank=True) # Field name made lowercase.
    actiu = models.BooleanField(db_column='actiu') 
    
    class Meta:
        db_table = u'meta_tipus_audit'
    def __unicode__(self):
        return self.destipusaudit
           
class Audit(models.Model):
    '''
    control de auditoria , enregistra els accesos a la BD i les consultes
    '''
    idaudit = models.AutoField(db_column='id_audit',primary_key=True) # Field name made lowercase.
    idauth_user = models.ForeignKey(User, null=True, db_column='id_auth_user', blank=True) # Field name made lowercase. 
    dataevent = models.DateTimeField( db_column='data_event', blank=True) # Field name made lowercase.
    idtipusaudit = models.ForeignKey(TipusAudit, null=True, db_column='id_tipus_audit', blank=True) # Field name made lowercase.
    dades = models.CharField(max_length=500, db_column='dades', blank=True) # Field name made lowercase.
    
    class Meta:
        db_table = u'meta_audit'
    def __unicode__(self):
        return str( self.idauth_user) +  TipusAudit.objects.get(idtipusaudit=self.idtipusaudit.idtipusaudit).destipusaudit +  self.dades 
    
           

class MetaCentreIdi(models.Model):
    id = models.AutoField(primary_key=True)
    descripcio = models.CharField(max_length=300)
    class Meta:
        db_table = u'meta_centre_idi'
        ordering = ['descripcio']
    def __unicode__(self):
        return self.descripcio    
        
    
class MetaUnitatIdi(models.Model):
    id = models.AutoField(primary_key=True)
    descripcio = models.CharField(max_length=600, blank=True)
    centre_idi = models.ForeignKey(MetaCentreIdi)
    unitatat_facturacio_id = models.IntegerField()
    actiu = models.IntegerField()
    calendari_laboral = models.ForeignKey(CalendariLaboral, null=True)
    class Meta:
        db_table = u'meta_unitat_idi'
        ordering = ['descripcio']
        using = "portal"
    def __unicode__(self):
        return self.descripcio
    def save_model(self, request, obj, form, change):
        # Tell Django to save objects to the 'other' database.
        obj.save(using=self.Meta.using)
    def delete_model(self, request, obj):
        # Tell Django to delete objects from the 'other' database
        obj.delete(using=self.Meta.using)
    def queryset(self, request):
        # Tell Django to look for objects on the 'other' database.
        return super(MetaUnitatIdi, self).queryset(request).using(self.Meta.using)
    def list_filter2(self,request):
        # Tell Django to look for objects on the 'other' database.
        return super(MetaUnitatIdi, self).list_filter(request).using(self.Meta.using)
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        # Tell Django to populate ForeignKey widgets using a query
        # on the 'other' database.
        return super(MetaUnitatIdi, self).formfield_for_foreignkey(db_field, request=request, using=self.Meta.using, **kwargs)
    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        # Tell Django to populate ManyToMany widgets using a query
        # on the 'other' database.
        return super(MetaUnitatIdi, self).formfield_for_manytomany(db_field, request=request, using=self.Meta.using, **kwargs)
    
    
class MetaGrupCategoriaProfessional(models.Model):
    nom = models.CharField(max_length=150)
    created_at = models.DateTimeField(auto_now_add=True) 
    updated_at = models.DateTimeField(auto_now=True) 

    class Meta:
        db_table = u'meta_grup_categoria_professional'

    def __unicode__(self):
        return self.nom
    
class MetaUnitatIdiKarat(models.Model):
    id = models.IntegerField(primary_key=True)
    descripcio = models.CharField(max_length=600)
    meta_unitat_idi = models.ForeignKey(MetaUnitatIdi)
    
    class Meta:
        db_table = u'meta_unitat_idi_karat' 
    def __unicode__(self):
        return self.descripcio
    

class MetaCategoriaProfesionalKarat(models.Model):
    id = models.AutoField(primary_key=True)
    descripcio = models.CharField(max_length=45)
    abreviatura = models.CharField(max_length=8)
    grup = models.ForeignKey(MetaGrupCategoriaProfessional, null=True, default=None, on_delete=models.SET_NULL)

    class Meta:
        db_table = u'meta_categoria_profesional_karat'
        ordering = ['descripcio']

    def __unicode__(self):
        return self.descripcio

    
class MetaCategoriaProfesional(models.Model):
    id = models.AutoField(primary_key=True)
    descripcio = models.CharField(max_length=405)
    orden = models.IntegerField()
    #grup = models.ForeignKey(MetaGrupCategoriaProfessional, null=True, blank=True)
    categoria_profesional_karat = models.ForeignKey(MetaCategoriaProfesionalKarat,null=True)
    responsabilitat_karat  = models.BooleanField(default= False , null=False, blank=False,)

    class Meta:
        db_table = u'meta_categoria_profesional'
    def __unicode__(self):
        return self.descripcio
    

class AuthUser(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.CharField(unique=True, max_length=90)
    first_name = models.CharField(max_length=90)
    last_name = models.CharField(max_length=90)
    email = models.CharField(max_length=225)
    password = models.CharField(max_length=384)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    is_superuser = models.IntegerField()
    last_login = models.DateTimeField()
    date_joined = models.DateTimeField()
    

    class Meta:
        db_table = u'auth_user'
    
    def __unicode__(self):
        return self.first_name + " " + self.last_name.replace(",", "")

# NOTE: Aquest signal ja no cal perquè ara està el profile inline.
#es el signal per crear el profile just despres de crear l'usuari
# definition of UserProfile from above
# ...
#def create_user_profile(sender, instance, created, **kwargs):
#    if created:
#        AuthUserProfile.objects.create(user=instance)
        #aup= AuthUserProfile.objects.get(user=instance)
        #up.personanombrecompleto = up.nombreusuario + "; " + up.apellido1 + " " + up.apellido2

#post_save.connect(create_user_profile, sender=User, dispatch_uid="create_user_profile")

## en les vistes -->  p = User.objects.get(username="xxxx").get_profile()  # notice the ()
##                    print p.attribute1


class AuthUserProfile(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.OneToOneField(User, unique=True)
    categoria_profesional = models.ForeignKey(MetaCategoriaProfesional, blank=False, default=0)
    nombre_usuario = models.CharField(max_length=405)
    dpo = models.NullBooleanField(null=True, blank=True, default=False)
    bloqueado = models.NullBooleanField(null=True, blank=True)
    password_usuario = models.CharField(max_length=288, blank=True)
    unitat_idi = models.ForeignKey(MetaUnitatIdi, blank=True, default=17)  # 17 es SSCC
    #Aquest es el el 'set' de unitats que pot tenir acces un usuari
    unitats_idi_permissos = models.ManyToManyField(MetaUnitatIdi, related_name="auth_user_profile_meta_unitats_idi", verbose_name=('Unitats IDI a les que te permisos'), blank=True, null=True)
    #En cas de no ser IDI no tindra acces a la intranet.
    es_idi = models.BooleanField(null=False, blank=False, default=True, verbose_name='Es personal IDI. En cas de ser desmarcat no tindrà accés al intranet però si a les eines que tingui permisos assignats.')
    es_ics = models.BooleanField(null=False, blank=False, default=False)
    es_gestio_idi = models.BooleanField(null=False, blank=False, default=False)
    categoria_profesional_karat = models.ForeignKey(MetaCategoriaProfesionalKarat, blank=True, default=950)
    data = models.DateTimeField()
    jornada_anual = models.ForeignKey(JornadaAnual, null=False, default=1)
    es_actiu = models.BooleanField(null=False, default=True, verbose_name="Te contracte laboral en alta. En cas de ser desmarcat (baixa) no tindrà accés a les eines  ni aparexerà en els Calendaris però si a l'intranet per veure les seves nomines etc")
    # M2M de categories que es poden manipular
    categories_de_control = models.ManyToManyField(MetaGrupCategoriaProfessional, null=True, blank=True,
                                    verbose_name='Grups de Categories Karat sobre les que es tenen permisos')

    class Meta:
        db_table = u'auth_user_profile'

    def __unicode__(self):
        return AuthUser.objects.get(id=self.user.id).first_name + " " + AuthUser.objects.get(id=self.user.id).last_name

    # Per guardar l'històric de les persones    
    def save(self, force_insert=False, force_update=False ,*args, **kwargs):
        crearregistre = False #regsitre en l'historic -> en meta auth user categoria ( que tambe guardem els canvis de unitat, dpos , etc )
        try:
            auObj = AuthUserProfile.objects.get(id=self.id)
            if (self.categoria_profesional != auObj.categoria_profesional) or ( self.unitat_idi != auObj.unitat_idi ) or ( self.dpo != auObj.dpo ) or (   (self.categoria_profesional_karat_id !=  auObj.categoria_profesional_karat_id)   and auObj.categoria_profesional_karat_id != None  ):
                crearregistre = True
        except AuthUserProfile.DoesNotExist:
            crearregistre = True

        super(AuthUserProfile, self).save(force_insert, force_update)
        
        if crearregistre:# crea un registre a historia
            obj = MetaAuthUserCategoria(
                                        auth_user_profile =self,
                                        meta_categoria_profesional=self.categoria_profesional,
                                        categoria_profesional_karat=self.categoria_profesional_karat,
                                        unitat_idi=self.unitat_idi,
                                        dpo=self.dpo,
                                        anyo=datetime.datetime.now().year,
                                        jornada_anual = self.jornada_anual,
                                        es_actiu = self.es_actiu 
                                        )
            obj.save()
        

    def get_nom_normalitzat(self):
        """
        Converteix NOM NOM --> Nom Nom
        """
        noms = self.user.first_name.split(' ')

        return " ".join([x.capitalize() for x in noms])

    def get_cognom_normalitzat(self):
        """"
        Converteix COGNOM , COGNOM --> Cognom Cognom
        """
        noms = self.user.last_name.split(',')

        return " ".join([x.strip().capitalize() for x in noms])


    def permis_planilles(self):
        """
        Retorna el permis necessari per la aplicació.
        4 de definits, ROOT, RRHH, CAPU i USER
        Per check a views:
            request.user.get_profile().permis_planilles()
        a templates:
            request.user.get_profile.permis.planilles
        """
        perm_RRHH = u'Planilles RRHH'
        perm_CAPU = u'Planilles CAPU'

        if self.user.is_superuser:
            return 'ROOT'

        groups = [group.name for group in self.user.groups.all()]
        if perm_RRHH in groups:
            return 'RRHH'

        if perm_CAPU in groups:
            return 'CAPU'

        return 'USER'

    def permisos(self, bloc):
        """
        Retorna 0, 1, 2 depenent si pot llegir, editar o esborrar
        """
        categoria = self.categoria_profesional.id
        unitat = self.unitat_idi.id
        # per defecte nomes es por llegir
        ret = 0
        print categoria, unitat

        # Superusers, de RRHH
        if categoria in (1, 26):
            return 2

        if bloc == 'calendari':
            ret = 2

        return ret

    def empresa(self):
        """
        Retorna el nom de la empresa a la que pertany
        """
        if self.es_idi:
            return 'IDI'
        elif self.es_ics:
            return 'ICS'
        else:
            return None

    def get_quita(self):
        """
        Retorna la data d'inici del periode a calcular.
        None, False o similar ja va be com a retorn
        """
        if True:
            return datetime.date(2013, 1, 1)

        return None


#relaciopnem usuari i la categoria  i unitat per enregistrear la categoria a mida que va canviant en el temops --> DPOS etc 
class MetaAuthUserCategoria(models.Model):

    id = models.AutoField(primary_key=True)
    auth_user_profile = models.ForeignKey(AuthUserProfile, db_column='auth_user_id',null= False) # Field name made lowercase.
    meta_categoria_profesional = models.ForeignKey(MetaCategoriaProfesional)
    categoria_profesional_karat = models.ForeignKey(MetaCategoriaProfesionalKarat, null=True)
    anyo = models.IntegerField()
    unitat_idi = models.ForeignKey(MetaUnitatIdi,blank=True,default=17)# 17 es SSCC
    dpo = models.NullBooleanField(null=True, blank=True, default=False)
    data = models.DateTimeField(default=datetime.datetime.now())
    jornada_anual = models.ForeignKey(JornadaAnual, null=True )
    es_actiu = models.BooleanField(default= True)
    
    
    class Meta:
        db_table = u'meta_auth_user_categoria'



#per crear profiles quan no existeixen al ger el get
User.profile = property(lambda u: AuthUserProfile.objects.get_or_create(user=u)[0])
AuthUser.profile = property(lambda u: AuthUserProfile.objects.get_or_create(user=u)[0])
try:
    User.unitat_idi = property(lambda u: AuthUserProfile.objects.get(user=u).unitat_idi)
except:
    User.unitat_idi = property()


'''
        
CONTROL DE PERMISOS A LES DIFERENTS APLICACIONS DEL PORTAL
'''
    
    
class AppPermis(models.Model):
    '''
    Control de acces a les diferents aplicacions.
     
    Afegeix aqui les noves apliacions. s'ha de afegir una nova tupla amb la nova aplicaio
     
        El primer camp 'codename' ha de ser igual a la URL aixi apareixera  automaticament en
        el index de apliacions del usuari ( si te permisos).
        El segon camp es la descripcio (name) que els apreixera al ususaris.
    
    apreixera en index.html i en permisdenegat.html. Pasos a sequir:
        
     0: afegim aqui (en la clase Meta a permission) la url i la descripcio de laplicaio   
     1: manger.py syncDB (aixo afegira el nou permis creat aqui a la taula auth_permision) ( en settings_develop.py jhem de afegir la apliacio exemple 'guardies')
     2: creem un grup nou desde la plana de admin i li afegim nomes el nou permis que estara en meta | app permis | permis_nou
     3: als usuaris que volem que tonguin acces a la nova apliacio li afegim el nou grup acavat de crear. Llestos!!!
     
    Recordar que en totes les vistes de la nova apliacacio Viwes.py s'ha de afegir al devant de la 
    classe el seguent
    
        @login_required
        @permission_required('meta.quadre_comandament', login_url='/accounts/permisdenegat/')
        class mivista(models.model): ...
    
    on la primera @ obliga a que estiguem logeggats i la segona li demana que tingui permisos 
    en aquest exemple a quadre comandemanent 
      
    '''
    # aquesta taula nomes te l id no hem de fer res en ella nomes es per poder generar els permisos personalitzats
    #idapppermis = models.AutoField(db_column='id_app_permisos',primary_key=True)
    #desapppermis = models.CharField(max_length=255, db_column='des_app_permis', blank=True) # Field name made lowercase.
       
class Meta:
    db_table = u'meta_app_permis'
    permissions = [
            #url, descripcio per a el usuari
            ('admin', 'Pagina d\'administracio'),
            ('historic_rm_vh', 'Historic RM VH antiga Blava'),
            ('quadre_comandament', 'Quadre de comandament'),
           # ('indicadors', 'Indicadors ISO per a supervisio de les unitats'),
            ('guardies', 'Gestió de les guardies del dep. dinformàtica'),
            ('rrhh','Aplicacions del dep. de RRHH'),
            ('intranet','Intranet de l\'IDI'),
            ('historic_tctr_vh', 'Historic TC Trauma VH antiga Blava'),
            ('calaix', 'Calaix per a traspassar fitxers de gran mida'),
            ('cau', 'Centre Atencio Usuari'),
            ('seguiment_personal','Seguiment Personal'),
            ('inventari', "Gestió de l'inventari"),
            ('revalidar','Revalidació de la activitat extraordinaria validada.'),
            ('idirh','Gestió de recusos humans. ')
            
            ]
    
          
    
'''
TAULES MESTRES, ARA SON VISTES PERO PODEN SER LES TAULES I VISTES EN FACNETDB, ETC
'''

#class MetaSession(models.Model):
#    id = models.IntegerField(primary_key=True, db_column='id') # Field name made lowercase.
#    descripcio = models.CharField(max_length=254, db_column='descripcio') # Field name made lowercase.
#    class Meta:
#        db_table = u'meta_session'


class MetaMes(models.Model):
    id = models.AutoField(primary_key=True, db_column='IdMes') # Field name made lowercase.
    mes = models.CharField(max_length=135, db_column='Mes', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'meta_mes'
        
class MetaTipoexploracion(models.Model):
    id = models.AutoField(primary_key=True, db_column='IdTipoExploracion') # Field name made lowercase.
    tipoexploracion = models.CharField(max_length=135, db_column='TipoExploracion', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'meta_tipoexploracion'

class MetaCentreidi(models.Model):
    id =  models.AutoField(primary_key=True) # Field name made lowercase.
    descripcio = models.CharField(max_length=135) # Field name made lowercase.
    class Meta:
        db_table = u'meta_centre_idi'
    def __unicode__(self):
        return unicode(self.descripcio)       
        
class MetaUnidadfac(models.Model):
    id = models.AutoField(primary_key=True, db_column='IdUnidadFac') # Field name made lowercase.
    unidadfac = models.CharField(max_length=135, db_column='UnidadFac', blank=True) # Field name made lowercase.
    ufnombrefscal = models.CharField(max_length=300, db_column='UFNombreFscal', blank=True) # Field name made lowercase.
    ns = models.CharField(max_length=135, db_column='NS', blank=True) # Field name made lowercase.
    ufcif = models.CharField(max_length=36, db_column='UFCIF', blank=True) # Field name made lowercase.
    ufdireccionfiscal = models.CharField(max_length=300, db_column='UFDireccionFiscal', blank=True) # Field name made lowercase.
    ufcpfiscal = models.CharField(max_length=30, db_column='UFCPFiscal', blank=True) # Field name made lowercase.
    ufmunicipiofiscal = models.CharField(max_length=135, db_column='UFMunicipioFiscal', blank=True) # Field name made lowercase.
    idcentre = models.ForeignKey(MetaCentreidi, null=True, db_column='IdCentre', blank=True) # Field name made lowercase.
    codup = models.CharField(max_length=15, db_column='CodUP', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'meta_unidadfac'
    def __unicode__(self):
        return self.unidadfac



class MetaAgenda(models.Model):
    
    id = models.AutoField(primary_key=True, db_column='IdAgenda') # Field name made lowercase.
    codagenda = models.IntegerField(db_column='CodAgenda') # Field name made lowercase.
    agenda = models.CharField(max_length=135, db_column='Agenda', blank=True) # Field name made lowercase.
    idunidadfac = models.ForeignKey(MetaUnidadfac, null=True, db_column='IdUnidadFac', blank=True) # Field name made lowercase.
    idtipoexploracion =  models.ForeignKey(MetaTipoexploracion, null=True, db_column='IdTipoExploracion', blank=True) # Field name made lowercase.
    obs = models.CharField(max_length=135, db_column='Obs', blank=True) # Field name made lowercase.
    unitattractament = models.CharField(max_length=96, db_column='UnitatTractament', blank=True) # Field name made lowercase.
    db = models.CharField(max_length=135, db_column='DB', blank=True) # Field name made lowercase.
    idcentre = models.ForeignKey(MetaCentreidi, null=True, db_column='IdCentre', blank=True) # Field name made lowercase.
    #es_ics = models.IntegerField(null=True, db_column='es_ICS', blank=True) # Field name made lowercase.
    
    class Meta:
        db_table = u'meta_agenda'

        
class MetaOpcions(models.Model):
    id = models.AutoField(primary_key=True)
    key = models.CharField(max_length=300)
    value = models.CharField(max_length=300)
    app_key = models.CharField(max_length=300)
    class Meta:
        db_table = u'meta_opcions'        
    def __unicode__(self):
        return self.key + " " + self.value
    
class MetaDiaFestiu(models.Model):
    id = models.AutoField(primary_key=True)
    centre = models.ForeignKey(MetaCentreidi, null=True, blank=True)
    desc = models.CharField(max_length=600, blank=True)
    data = models.DateField(null=True, blank=True)
    class Meta:
        db_table = u'meta_diafestiu'
    def __unicode__(self):
        return unicode(self.data)        
    
    
    
def _get_cleaner(form, field):
    # quan fem un modelDrom.isvalid() -> entre altres crida la funcio  _clean_fields(self) de la clase .form.forms on crida "clean" de cada camp
    # es la funcio aquesta i l'omple despres amb el valor que havia retornat de la qurey
    def clean_field():
        if form.instance and form.instance.pk:
            return getattr(form.instance, field, None)
        else:
            return form.cleaned_data[field]
    return clean_field

class ROFormMixin(BaseForm):
    def __init__(self, *args, **kwargs):
        super(ROFormMixin, self).__init__(*args, **kwargs)
        #print self.fields['usuari_modificador_id']
                
        if hasattr(self, "read_only"):
            if self.instance: #and self.instance.pk: # aixo nomes en cas que colguem que la primera vegada que editem els readonly es pugui. en aquest cas no volem 
                for field in self.read_only:
                    #self.fields[field].widget = ReadOnlyWidget(field) NO CHUTA 
                    self.fields[field].widget.attrs['readonly'] = True
                    ###proves
                    # self.fields[field].required = False
                #self.fields[field].widget.attrs['disabled'] = 'disabled'  #NO CHUTA 
                    ###Fi proves
                    setattr(self, "clean_" + field, _get_cleaner(self, field))#afegim la funcio de netejar el camp readonly-> posa None i despres carrega el valor en questió
