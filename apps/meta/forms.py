# coding: utf-8
from django import forms
from django import forms
from django.contrib import auth
from django.utils.translation import ugettext_lazy as _ 

from django.contrib.admin.widgets import AdminSplitDateTime 
from django.forms import SplitDateTimeWidget
from datetime import datetime


MES_CHOICES = (
    ('1', 'Gener'),
    ('2', 'Febrer'),
    ('3', 'Març'),
    ('4', 'Abril'),
    ('5', 'Maig'),
    ('6', 'Juny'),
    ('7', 'Juliol'),
    ('8', 'Agost'),
    ('9', 'Setembre'),
    ('10', 'Octubre'),
    ('11', 'Novembre'),
    ('12', 'Desembre'),
)


ANY_CHOICES = []      
for i in range(2011, datetime.now().year + 1 ):
    ANY_CHOICES.append((str(i), str(i)))
  

from django.contrib.admin import widgets 

class ComplementairesForm(forms.Form):
  #  mes = forms.IntegerField(required=True, widget=forms.Select(choices=MES_CHOICES))    
  #  any = forms.IntegerField(required=True, widget=forms.Select(choices=ANY_CHOICES))
    informes_alliberats_des_de_dia = forms.DateField(required=True,widget=widgets.AdminDateWidget)
    informes_alliberats_fins_a_dia = forms.DateField(required=True,widget=widgets.AdminDateWidget) 
    informes_validats_a_partir_de = forms.DateTimeField(required=True,widget=widgets.AdminSplitDateTime)
    
    
class AnyMesForm(forms.Form):
    mes = forms.IntegerField(required=True, widget=forms.Select(choices=MES_CHOICES))    
    any = forms.IntegerField(required=True, widget=forms.Select(choices=ANY_CHOICES))
    
    
    

class ValidatingPasswordChangeForm(auth.forms.PasswordChangeForm):
    MIN_LENGTH = 8

    def clean_new_password1(self):
        password1 = self.cleaned_data.get('new_password1')

        # At least MIN_LENGTH long
        if len(password1) < self.MIN_LENGTH:
            raise forms.ValidationError("El nou password ha de ser de almenys %d caràcters." % self.MIN_LENGTH)

        # At least one letter and one non-letter
        first_isalpha = password1[0].isalpha()
        if all(c.isalpha() == first_isalpha for c in password1):
            raise forms.ValidationError("El nou password ha de tenir almenys una lletra i un número o símbol.")

        return password1
    
class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set his/her password without
    entering the old password
    """
    new_password1 = forms.CharField(label=_("New password"), widget=forms.PasswordInput)
    new_password2 = forms.CharField(label=_("New password confirmation"), widget=forms.PasswordInput)

    MIN_LENGTH = 8
    
    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(SetPasswordForm, self).__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(_("The two password fields didn't match."))
        
        # At least MIN_LENGTH long
        if len(password1) < self.MIN_LENGTH:
            raise forms.ValidationError("El nou password ha de ser de almenys %d caràcters." % self.MIN_LENGTH)

        # At least one letter and one non-letter
        first_isalpha = password1[0].isalpha()
        if all(c.isalpha() == first_isalpha for c in password1):
            raise forms.ValidationError("El nou password ha de tenir almenys una lletra i un número o símbol.")
        return password2
    
    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password1'])
        if commit:
            self.user.save()
        return self.user

#from django.forms import ModelForm
#from apps.quadre_comandament.models import ProvaOhdimCodificacioGenerica
# 
#class ProvaOhdimCodificacioGenericaForm(ModelForm):
#    class Meta:
#        model = ProvaOhdimCodificacioGenerica
#        fields = ('id', 'prova_ohdim')
#
#from django.forms.models import BaseModelFormSet
#class BaseProvaOhdimCodificacioGenericaFormSet(BaseModelFormSet):
#    def __init__(self, *args, **kwargs):
#        super(BaseProvaOhdimCodificacioGenericaFormSet, self).__init__(*args, **kwargs)
#        self.queryset = ProvaOhdimCodificacioGenerica.objects.filter(name__startswith='O')
#        
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
 
class UserCreateForm(UserCreationForm):
    email = forms.EmailField(required=True)
 
    class Meta:
        model = User
        fields = ("username", "email", "password1", "password2")
 
    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user