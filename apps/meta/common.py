# coding: utf8

import os
import posixpath
import urllib
from django.db import connections
import datetime
from apps.meta.models import AuthUserProfile, MetaUnitatIdiKarat,MetaCategoriaProfesionalKarat, User
from django.core.mail import send_mail
from django.conf import settings
from apps.idirh.models import JornadaAnual
 
 
 
def sanitize_path(path):
    path = posixpath.normpath(urllib.unquote(path))
    newpath = ''
    for part in path.split('/'):
        if not part:
            # strip empty path components
            continue
    
        drive, part = os.path.splitdrive(part)
        head, part = os.path.split(part)
        if part in (os.curdir, os.pardir):
            # strip '.' amd '..' in path
            continue
    
        newpath = os.path.join(newpath, part).replace('\\', '/')
    return newpath

def sincronitza_categories_karat_portal( ):
        #https://docs.djangoproject.com/en/dev/topics/db/sql/#performing-raw-queries , no fem servir raw perque el model esta en karat 
        #cursor = connections['karat'].cursor()
        query = """

            SELECT DISTINCT 

                categorias.xcategoria_id as id,
                categorias.xdescripcion as descripcio,
                categorias.xabreviada as  abreviatura 

            FROM   [CACHE]..[_SYSTEM].[no_tra_laborales]  as no_tra_laborales
            INNER JOIN [CACHE]..[_SYSTEM].[no_categorias] as categorias  ON  no_tra_laborales.xcategoria_id = categorias.xcategoria_id
            WHERE 
             ( xfecha_baja is  NULL )
             -- A2006 es el nom de l'empresa en el nou conveni 
             AND (no_tra_laborales.xpersona_id = 'A2006' )
            """
        #https://docs.djangoproject.com/en/dev/topics/db/sql/#performing-raw-queries No fem servir un cursor fem servir Raw perque el model be a ser el mateix 
        # en el portal que en Cache i aixi podem fer servir els alias ( els noms dels camps en comptes de objectre[3] per referise al camp 3 )    
        categories_db_karat = MetaCategoriaProfesionalKarat.objects.raw(query).using('karat')
        creats = 0
        n = 0
        for categoria in categories_db_karat:
            n += 1
            #https://docs.djangoproject.com/en/dev/ref/models/querysets/  -> get_or_create     -> sino hi es en crea un de nou
            cat, created = MetaCategoriaProfesionalKarat.objects.get_or_create(id = categoria.id)
            cat.descripcio = categoria.descripcio
            cat.abreviatura = categoria.abreviatura
            cat.save()
            if created:
                creats = creats + 1   

        return n, creats
    
#Son iguals encara que la descripcio es una mica diferent
def sincronitza_unitats_karat( ):
    
        #https://docs.djangoproject.com/en/dev/topics/db/sql/#performing-raw-queries , no fem servir raw perque el model esta en karat 
        #cursor = connections['karat'].cursor()
        query = """

        SELECT  *
        FROM   [CACHE]..[_SYSTEM].[no_CT_generales] 
        WHERE xpersona_id = 'A2006' order by xctrabajo_id asc
        """
        print query    
    
def sincronitza_karat_portal():
    #Sincronitzem els catetgories
    sincronitza_categories_karat_portal()
    
    cursor = connections['karat'].cursor()
    llistatKarat = []
    
    query = """
        SELECT   
            xc_personas.xpersona_id ,  
            xc_personas.xnif ,
            xc_personas.xnombre ,
            xc_personas.xapellido1 ,
            xc_personas.xapellido2
            --no_categorias.xabreviada,
            --no_categorias.xdescripcion,
            --no_tra_laborales.xfecha_categoria
            --,no_tra_laborales.xfecha_camb_cat,
            --no_tra_laborales.xjornada_reducida,
            --no_tra_laborales.xdias_horas_tp
            --,xfecha_baja
            ,mail.xmail_xc
            ,no_tra_laborales.xcategoria_id
            ,no_tra_laborales.xctrabajo_id as centro_trabajo
         FROM [CACHE]..[_SYSTEM].xc_personas as  xc_personas
         INNER JOIN [CACHE]..[_SYSTEM].[no_tra_laborales] as  no_tra_laborales ON xc_personas.xpersona_id = no_tra_laborales.xidentificador
         INNER JOIN [CACHE]..[_SYSTEM].[no_categorias] as  no_categorias ON no_categorias.xcategoria_id = no_tra_laborales.xcategoria_id
         LEFT JOIN  [CACHE]..[_SYSTEM].xc_personas_mail as mail ON xc_personas.xpersona_id = mail.xpersona_id
         WHERE 
             ( xfecha_baja is  NULL )
             -- A2006 es el nom de l'empresa en el nou conveni 
             AND (no_tra_laborales.xpersona_id = 'A2006' )
    """
    cursor.execute(query)
    usuaris_db_karat = cursor.fetchall()
    
    
    missatge = []
    
    for usuarikarat in usuaris_db_karat:
        
        usuari_creat = None
        canvi_categoria = None
        canvi_unitat = None
        canvi_dades_personals = None
        creacio_perfil_usuari = None
        canvi_actiu = None
        error = None
        llistatKarat.append(usuarikarat[1][:8])# ens quedem el nif per despres poder donar de baixa al portal  tots aquells que no tinguin data debaixa == null en el karat
        # Actualitzem o creem l'usuari
        usuari_object, created = User.objects.get_or_create(username = usuarikarat[1][:8], #mirem el NIF sense lletra del darrera es mes segur ja que el nif pasa per la ss i es pot rectificar l'alte es el ID en Karat i es clau = no es pot modificar   
                                                      defaults={ 'first_name': usuarikarat[2],
                                                                 'last_name': (unicode(usuarikarat[3]) +' ' + unicode(usuarikarat[4]))[:30],
                                                                 'is_staff': False ,
                                                                 'is_active': True ,
                                                                 'is_superuser': False ,
                                                                 'last_login': datetime.datetime.now(),
                                                                 'date_joined': datetime.datetime.now(),
                                                                 'email': unicode(usuarikarat[5])[:75], # Si volem que sigui obligatori s'ha de treure el  unicode i fer un try-catch aqui
                                                                 'password' :'sha1$5dbfd$c8bca15bf3ec668ef04e6c8c98fbfd3e4add207e'# IDIinicial01, per a que no peti ja que no pot ser null, despres farem l'update
                                                                 } )
        if  created:
            #usuari_object.set_password('IDI'+  usuari_object.username )# Password inicial es "IDIusername"
            usuari_creat = usuari_object.username + ': '+ usuari_object.first_name[:30] + '; ' +  usuari_object.last_name[:30]  
            
        else:
        
            if usuari_object.first_name <> usuarikarat[2]:
                canvi_dades_personals = usuari_object.first_name + ' -> ' + str(usuarikarat[2])
                usuari_object.first_name = usuarikarat[2]  
            if usuari_object.last_name <>  (unicode(usuarikarat[3]) + ', ' + unicode(usuarikarat[4]))[:30]:
                canvi_dades_personals = usuari_object.last_name + ' -> ' + (unicode(usuarikarat[3]) + ', ' + unicode(usuarikarat[4]))[:30]
                usuari_object.last_name =  (unicode(usuarikarat[3]) + ', ' + unicode(usuarikarat[4]))[:30]
            if usuari_object.email == None:
                canvi_dades_personals = ' Sense email  -> ' +  str(usuarikarat[5][:75]) 
                usuari_object.email = usuarikarat[5][:75]
        
        jornada_defecte = 1
        if JornadaAnual.objects.all().filter(es_default= True).count() == 0:
            jornada_defecte =  JornadaAnual.objects.all()[0].id #agafem el primer
            error = "NO HI HA JORNADA ANUAL PER DEFECTE ;"        
        # Actulitzem o creem el profile de l'usuri
        profile, created = AuthUserProfile.objects.get_or_create(user = usuari_object, 
                                                                  defaults={#'user_id':282000029,  
                                                                            'categoria_profesional_id':0,
                                                                            #'nombre_usuario':'lol',
                                                                            'dpo':( datetime.datetime.now().month >= 6 ),# si volem possar-ho automaticament
                                                                            'bloqueado':False,
                                                                            #'password_usuario':'ttt',
                                                                            'unitat_idi_id':MetaUnitatIdiKarat.objects.get(id=usuarikarat[7]).meta_unitat_idi.id,
                                                                            #'unitats_idi_permissos_id':1,
                                                                            'es_idi' : True,
                                                                            'categoria_profesional_karat_id':MetaCategoriaProfesionalKarat.objects.get(id=usuarikarat[6]).id,#no pot ser null i l'hem de possar per defetce algun valor sino peta!!
                                                                            #'data': datetime.datetime.now()
                                                                            'jornada_anual_id': jornada_defecte, #JornadaAnual.objects.all().filter(es_default= True)[0].id,  #agafem el primer
                                                                            'es_actiu': True
                                                                  })

        
        if created:
            creacio_perfil_usuari =  'Categoria profesional Dpos: ' + str( profile.categoria_profesional) + ' ; Categoria Karat: ' + str( profile.categoria_profesional_karat ) + ' ; Unitat ' + str( profile.unitat_idi ) + ' ; Te Dpos: ' + str ( profile.dpo) 
            
        # Comprobem si falta o ha canviat la unitat.
        if profile.unitat_idi <> MetaUnitatIdiKarat.objects.get(id=usuarikarat[7]).meta_unitat_idi:
            canvi_unitat = str(profile.unitat_idi) + ' -> ' + str(MetaUnitatIdiKarat.objects.get(id=usuarikarat[7]).meta_unitat_idi)
            profile.unitat_idi = MetaUnitatIdiKarat.objects.get(id=usuarikarat[7]).meta_unitat_idi
            profile.dpo = ( datetime.datetime.now().month >= 6 )#parlat amb el Cap de RRHH si cambia de unitat pot tenir o no dret a DPOS
            canvi_unitat = canvi_unitat + '  i li pertoquen  DPOS: ' + str(profile.dpo)

        # Comprobem si falta o ha canviat la categoria professional karat 
        categoriakarat = MetaCategoriaProfesionalKarat.objects.get(id=usuarikarat[6])
        # Ho comparo amb l'id perquè al crear al perfil, l'id apuntarà a una categoria inexistent
        if profile.categoria_profesional_karat_id <> categoriakarat.id:
            # Si canvia la categoria en carat no afecta la de DPOS per tant no guardarem un registre
            if profile.categoria_profesional_karat_id  == None:
                canvi_categoria = 'Sense categoria !!! -> ' +str(categoriakarat)  
            else:
                canvi_categoria =  str(profile.categoria_profesional_karat) + ' -> ' +str(categoriakarat)
                
            profile.categoria_profesional_karat = categoriakarat
        
        # Comprovem si estava donat dalta al Portal i despres es va donar de baixa ( excedecia per ex) i ara torna a entrar a treballar 
        # excedencia ==> abaixa ==>  actiu = False i ara tornin a entrar . No s'han de donar d'alta de nou s'ha de possar actiu = True  
        if not profile.es_actiu:
            canvi_actiu = " Ja estava donat d'alta al Portal -> s'ha reactivat el usuari per poder accedir a les 'Eines' i les planilles"
            profile.es_actiu = True 
        
        profile.save()
        usuari_object.save()
        missatge = None
        
        if error <> None:
            missatge = error 
        if  usuari_creat <> None:
            missatge= "Usuari %s (%s ; %s) creat: %s" % (usuari_object.username,usuari_object.last_name,usuari_object.first_name, creacio_perfil_usuari)
        else:
            if creacio_perfil_usuari <> None:
                missatge=  "Usuari %s (%s ; %s) amb nou perfil: %s" % (usuari_object.username,usuari_object.last_name,usuari_object.first_name, creacio_perfil_usuari)

        if canvi_dades_personals <> None:
            missatge =  "Usuari %s (%s ; %s) actualitzat:  %s " % (usuari_object.username,usuari_object.last_name,usuari_object.first_name, canvi_dades_personals)
        if ( canvi_categoria <> None):
            missatge=  "Usuari %s (%s ; %s) amb canvi de categoria Karat: %s" %  (usuari_object.username,usuari_object.last_name,usuari_object.first_name,canvi_categoria)
 
        if ( canvi_unitat <> None ):
            missatge=  "Usuari %s (%s ; %s) amb canvi d'unitat: %s" %  (usuari_object.username,usuari_object.last_name,usuari_object.first_name,canvi_unitat)
        
        if canvi_actiu <> None:
            missatge=  "Usuari %s (%s ; %s) : %s" %  (usuari_object.username,usuari_object.last_name,usuari_object.first_name,canvi_actiu)
       
        #ENVIEM CORREU  al puigari dels canvis realitzats:
        if missatge <> None:
            subject = 'Notificacions en la sincronització KARAT -> Portal'
            sender =  settings.SERVER_EMAIL
            recipients = [settings.MAIL_RRHH]
            if sender:# si te correu li enviem un correu
                recipients.append(sender)
            #enviem si esta en producció sino no que s'ha de tenir configurat el servidor de correu i es un rollo
            if  str(os.environ['DJANGO_SETTINGS_MODULE']) == 'apps.settings':            
                send_mail(subject, missatge, sender, recipients)
            else:
                print 'subject: ' + subject + '; message: ' +  missatge + '; sender: ' +  sender  + ' ;recipients:' + str(recipients)
         
    # Els que estan en el portal i en el karat estan peró tenen datadebaixa <> Null -> els donen de baixa a portal 
    for usuari in  User.objects.all():
        if llistatKarat.count(usuari.username ) == 0:
            authUser , created  = AuthUserProfile.objects.all().get_or_create(user = usuari, 
                                                                  defaults={  
                                                                            'categoria_profesional_id':0,
                                                                            'dpo':False,# si volem possar-ho automaticament
                                                                            'bloqueado':False,
                                                                            'unitat_idi_id':17,
                                                                            'es_idi' : True,
                                                                            'es_actiu': False
                                                                  })

            if authUser.es_actiu:
                
                authUser.es_actiu = False
                authUser.save()
            
                #ENVIEM CORREU  al puigari dels canvis realitzats:
                missatge=  u"Usuari %s (%s ; %s) S'ha donat de baixa, nomes tindrà accés a la intranet del Portal." %  (unicode(usuari.username), unicode(usuari.last_name), unicode(usuari.first_name))
                subject = 'Notificacions en la sincronització KARAT -> Portal'
                sender =  settings.SERVER_EMAIL
                recipients = [settings.MAIL_RRHH]
                if sender:# si te correu li enviem un correu
                    recipients.append(sender)
                #enviem si esta en producció sino no que s'ha de tenir configurat el servidor de correu i es un rollo
                if  str(os.environ['DJANGO_SETTINGS_MODULE']) == 'apps.settings':            
                    send_mail(subject, missatge, sender, recipients)
                else:
                    print 'subject: ' + subject + '; message: ' +  missatge + '; sender: ' +  sender  + ' ;recipients:' + str(recipients)


    return len(usuaris_db_karat)

# Enviar mail asyncron
from django.core.mail import send_mail as core_send_mail
from django.core.mail import EmailMultiAlternatives
import threading

class EmailThread(threading.Thread):
    def __init__(self, subject, body, from_email, recipient_list, fail_silently, html):
        self.subject = subject
        self.body = body
        self.recipient_list = recipient_list
        self.from_email = from_email
        self.fail_silently = fail_silently
        self.html = html
        threading.Thread.__init__(self)

    def run (self):
        msg = EmailMultiAlternatives(self.subject, self.body, self.from_email, self.recipient_list)
        if self.html:
            msg.attach_alternative(self.html, "text/html")
        msg.send(self.fail_silently)

def send_mail(subject, body, from_email, recipient_list, fail_silently=False, html=None, *args, **kwargs):
    EmailThread(subject, body, from_email, recipient_list, fail_silently, html).start()