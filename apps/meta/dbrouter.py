#/###############################################################################3
#
# multiple BD routering
#
#/###############################################################################3

class DBRouter(object):
    """A router to control all database operations on models in
    the contrib.auth application"""

    def db_for_read(self, model, **hints):
        if hasattr(model,'connection_name'):
            return model.connection_name
        return 'default'

    def db_for_write(self, model, **hints):
        if hasattr(model,'connection_name'):
            return model.connection_name
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        "Allow any relation if a model in myapp is involved"
        return True
        
    def allow_syncdb(self, db, model):
        if hasattr(model,'connection_name'):
            return model.connection_name == db
        return db == 'default'
    