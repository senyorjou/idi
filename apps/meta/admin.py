# coding: utf8

'''
En aquest cas el historic Rm VH no volem gestianor-lo ( esta comentat PAcientAdmin )
Pero si hem afegit  la gestio de la auditoria si entrem a 
    http://localhost/admin podrem veure la vista , nomes els usuaris que tenen 
permisos poden veure ( el Sella i el Mateu )

'''

from django.contrib import admin
from apps.meta.auditoria import login_handler,logout_handler

from django.contrib.auth.signals import user_logged_in,user_logged_out
from apps.meta.common import sincronitza_categories_karat_portal, sincronitza_karat_portal
from apps.meta.models import Audit, TipusAudit, MetaOpcions, AuthUserProfile, MetaCategoriaProfesional, MetaCategoriaProfesionalKarat, MetaAuthUserCategoria
from django.contrib.auth.models import User 
from django.contrib.auth.admin import UserAdmin




#from apps.meta.forms import CustomUserCreationForm

class AuditAdmin(admin.ModelAdmin):
    list_display = ('idaudit', 'idauth_user', 'dataevent','idtipusaudit','dades')
    readonly_fields = ('idaudit', 'idauth_user', 'dataevent','idtipusaudit','dades')
    search_fields = ['idauth_user__username', 'idtipusaudit__destipusaudit','dades']
    list_filter = [ 'idtipusaudit']

class TipusAuditAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Tipus de registre', {'fields': ['destipusaudit','actiu']}),
    ]
    list_display = ('idtipusaudit', 'destipusaudit', 'actiu')
    readonly_fields = ['idtipusaudit']
 
class MetaMesAdmin(admin.ModelAdmin):
    list_display = ('id', 'mes')
    readonly_fields = ('id', 'mes')

class MetaOpcionsAdmin(admin.ModelAdmin):
    list_display = ('id', 'key', 'value', 'app_key')
    readonly_fields = ['id']
   
class MetaCategoriaProfesionalAdmin(admin.ModelAdmin):
    list_display = ('id', 'descripcio', 'orden','categoria_profesional_karat')
    readonly_fields = ['id' ]
 
class MetaCategoriaProfesionalKaratAdmin(admin.ModelAdmin):
    list_display = ('id', 'descripcio', 'abreviatura')
    readonly_fields = ['id' ]
    actions = ['sincronitza_karat_portal_admin']
    
    #funcio que cridarem per sincronitzar el portal amb els dades de Karat
    def sincronitza_karat_portal_admin(self, request, queryset):
        total, creats = sincronitza_categories_karat_portal()
        self.message_user(request, "S'han sincronitzat %s categories de les quals %s eren noves."  % (str(total), str(creats)))    

    ##todo:  fer que sigui publica i aixi podrem possar un plana i llençar el procedure on vulquehnmm 
    ##https://docs.djangoproject.com/en/1.2/ref/contrib/admin/actions/
    # aixó el que fa es possar sincronització a atotes les clasees i no es el que volem
    #admin.site.add_action(sincronitza_karat_portal)

    sincronitza_karat_portal_admin.short_description = u"Sincronització"
 
class AuthUserProfilePermissionsInline(admin.StackedInline):
    model = AuthUserProfile.unitats_idi_permissos   


class AuthUserProfileInline(admin.StackedInline):
    model = AuthUserProfile
    max_num = 1
    can_delete = False
    fields = ['es_idi', 'categoria_profesional', 'categoria_profesional_karat',
              'unitat_idi', 'dpo', 'unitats_idi_permissos', 'es_actiu',
              'categories_de_control']
    filter_horizontal  = ('unitats_idi_permissos', 'categories_de_control')
    readonly_fields = ['categoria_profesional_karat']
    #extra = 0

class MetaAuthUserCategoriaInline(admin.StackedInline):
    model = MetaAuthUserCategoria
    fields = ['meta_categoria_profesional', 'categoria_profesional_karat', 'anyo', 'unitat_idi', 'dpo', 'data']
    extra = 0
        
class CustomUserAdmin(UserAdmin):
    list_display = ['username', 'email', 'first_name', 'last_name', 'unitat_idi', 'is_staff']
    inlines = [AuthUserProfileInline]
    #MetaAuthUserCategoriaInline
    def response_add(self, request, obj, post_url_continue=None):
        #send_mail("Benvingut a l'IDI - Usuari creat", settings.TEXT_BENVINGUDA, 
        #          settings.MAIL_INFORMATICA, [obj.email], fail_silently=False)         
        return super(CustomUserAdmin, self).response_add(request, obj)
    
    actions = ['sincronitza_karat_portal']
    filter_horizontal  = ('groups',)
    #funcio que cridarem per sincronitzar el portal amb els dades de Karat
    def sincronitza_karat_portal(self, request, queryset):
        total = sincronitza_karat_portal()   
        #misstge al user en el admin
        self.message_user(request, "S'han processat %s usuaris." % str(total))
            
    ##todo:  fer que sigui publica i aixi podrem possar un plana i llençar el procedure on vulquehnmm 
    ##https://docs.djangoproject.com/en/1.2/ref/contrib/admin/actions/
    # aixó daqui dalt el que fa es possar sincronització a atotes les clasees i no es el que volem
    #admin.site.add_action(sincronitza_karat_portal)
    sincronitza_karat_portal.short_description = u"Sincronització"
    
  
    

"""Pels models que accedeixen a una DB que no és la per defecte"""

class MultiDBModelAdmin(admin.ModelAdmin):
    # A handy constant for the name of the alternate database.
    class Meta:
        using = 'other'
    def save_model(self, request, obj, form, change):
        # Tell Django to save objects to the 'other' database.
        obj.save(using=self.Meta.using)
    def delete_model(self, request, obj):
        # Tell Django to delete objects from the 'other' database
        obj.delete(using=self.Meta.using)
    def queryset(self, request):
        # Tell Django to look for objects on the 'other' database.
        return super(MultiDBModelAdmin, self).queryset(request).using(self.Meta.using)
    
    def list_filter2(self,request):
        # Tell Django to look for objects on the 'other' database.
        return super(MultiDBModelAdmin, self).list_filter(request).using(self.Meta.using)
    
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        # Tell Django to populate ForeignKey widgets using a query
        # on the 'other' database.
        return super(MultiDBModelAdmin, self).formfield_for_foreignkey(db_field, request=request, using=self.Meta.using, **kwargs)
    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        # Tell Django to populate ManyToMany widgets using a query
        # on the 'other' database.
        return super(MultiDBModelAdmin, self).formfield_for_manytomany(db_field, request=request, using=self.Meta.using, **kwargs)

class MultiDBTabularInline(admin.TabularInline):
    class Meta:
        using = 'other'
    def queryset(self, request):
        # Tell Django to look for inline objects on the 'other' database.
        return super(MultiDBTabularInline, self).queryset(request).using(self.Meta.using)
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        # Tell Django to populate ForeignKey widgets using a query
        # on the 'other' database.
        return super(MultiDBTabularInline, self).formfield_for_foreignkey(db_field, request=request, using=self.Meta.using, **kwargs)
    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        # Tell Django to populate ManyToMany widgets using a query
        # on the 'other' database.
        return super(MultiDBTabularInline, self).formfield_for_manytomany(db_field, request=request, using=self.Meta.using, **kwargs)


class AgendaAdmin(MultiDBModelAdmin):
    class Meta:    
        using = "portal_dades"
    list_filter =['hospital_id',]
class AgendaIdiAdmin(MultiDBModelAdmin):
    class Meta:    
        using = "portal_dades"
class AgendaProgramacioAdmin(MultiDBModelAdmin):
    class Meta:    
        using = "portal_dades"
class HospitalAdmin(MultiDBModelAdmin):
    class Meta:
        using = "portal_dades"
class ImportacioFitxerAdmin(MultiDBModelAdmin):
    class Meta:    
        using = "portal_dades"
class ImportacioCampAdmin(MultiDBModelAdmin):
    class Meta:    
        using = "portal_dades"
class MetgeAdmin(MultiDBModelAdmin):
    class Meta:    
        using = "portal_dades"
    list_display = ('cognoms_nom','id')
    search_fields = ('cognoms_nom',)
class MesAdmin(MultiDBModelAdmin):
    class Meta:    
        using = "portal_dades"
class MetgeServeiAdmin(MultiDBModelAdmin):
    class Meta:    
        using = "portal_dades"
class PrestacioAdmin(MultiDBModelAdmin):
    class Meta:    
        using = "portal_dades"   
class PrestacioIdiAdmin(MultiDBModelAdmin):
    class Meta:    
        using = "portal_dades"   
class CodiSeramAdmin(MultiDBModelAdmin):
    class Meta:    
        using = "portal_dades"   
    
#class ProvaOhdimCodificacioGenericaLine(MultiDBTabularInline):
#    using = "portal_dades" 
#    model = ProvaOhdimCodificacioGenerica
#    extra = 1
#    max_num = 1
#    verbose_name = 'Activitat Complementaria'
#    verbose_name_plural = 'Activitat Complementaria'
#    can_delete = False
#    fields = ['id' , 'prova_ohdim', 'codificacio_generica_inicial', 'codificacio_generica_final', 'data','data_modificacio','usuari_modificador_id','validat' ] 
#    readonly_fields = ('id' , 'prova_ohdim', 'codificacio_generica_inicial','data','data_modificacio',)
#

#class ProvaOhdimAdmin(MultiDBModelAdmin):
#    using = "portal_dades"   
#    list_display = ('hospital','id',  'historia_clinica_id','codificacio_generica' ) #',codificacio_generica_modificada_id')
#    search_fields = ('=id',) #t es per a ke no faci el like, triga molt   
#    list_filter2  = ('hospital','codificacio_generica')
#    fields = ('hospital','id' , 'historia_clinica_id','codificacio_generica') #,'codificacio_generica_modificada_id') 
#    readonly_fields = ( 'id', 'hospital', 'historia_clinica_id','codificacio_generica')
#    inlines = [ProvaOhdimCodificacioGenericaLine]
#    
    
#class ProvaOhdimCodificacioGenericaAdmin(MultiDBModelAdmin):
#    using = "portal_dades"   
#    #list_display = ('id' , 'prova_ohdim', 'codificacio_generica_inicial_id', 'codificacio_generica_final_id' )#, 'data','data_modificacio','usuari_modificador_id','validat' )
#    search_fields = ['prova_ohdim__id'] #t es per a ke no faci el like, triga molt   
#    #list_filter  = ('prova_ohdim__hospital',)
#    fields = ['id' , 'prova_ohdim', 'codificacio_generica_inicial', 'codificacio_generica_final' ,'usuari_modificador_id','validat']#    ,'data','data_modificacio','usuari_modificador_id','validat' ] 
#    readonly_fields = ('id' , 'prova_ohdim', 'codificacio_generica_inicial')#,'data','data_modificacio','usuari_modificador_id')


##DEfinixio de les clases de administracio personalitzades , de les taules que estan
# en una altre taula, esclar

class CodiSeramAdminPersonalitzat(CodiSeramAdmin):
    fields = [
     'id', 'descripcio', 'grup_id','temps_sala','temps_metge', 'urv_a' ,'ura_a' ,'urv_p' ,'ura_p' ,
     ]




#admin.site.register(AuthUserProfile, AuthUserProfileAdmin) 
#admin.site.register(Audit, AuditAdmin)
#admin.site.register(TipusAudit, TipusAuditAdmin)
#admin.site.register(MetaMes, MetaMesAdmin)

#admin.site.register(MetaOpcions, MetaOpcionsAdmin)
#
#
#admin.site.register(Agenda, AgendaAdmin)
#admin.site.register(AgendaIdi, AgendaIdiAdmin)
#admin.site.register(AgendaProgramacio, AgendaProgramacioAdmin)
#admin.site.register(Hospital, HospitalAdmin)
#admin.site.register(ImportacioFitxer, ImportacioFitxerAdmin)
#admin.site.register(ImportacioCamp, ImportacioCampAdmin)
#admin.site.register(Metge, MetgeAdmin)
#admin.site.register(Mes, MesAdmin)
#admin.site.register(MetgeServei, MetgeServeiAdmin)
#admin.site.register(Prestacio, PrestacioAdmin)
#admin.site.register(PrestacioIdi, PrestacioIdiAdmin)
#admin.site.register(CodiSeram, CodiSeramAdminPersonalitzat)

#empresa = models.CharField(null=True, blank=True)

#admin.site.register(ProvaOhdim, ProvaOhdimAdmin)
#admin.site.register(ProvaOhdimCodificacioGenerica, ProvaOhdimCodificacioGenericaAdmin)

#

admin.site.register(MetaCategoriaProfesional, MetaCategoriaProfesionalAdmin)
admin.site.register(MetaCategoriaProfesionalKarat, MetaCategoriaProfesionalKaratAdmin)

admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)

#registrem els signals per tal que quan es loguegi es llancin les funcions de auditoria
user_logged_in.connect(login_handler)
user_logged_out.connect(logout_handler)


    