# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TipusAudit'
        db.create_table(u'meta_tipus_audit', (
            ('idtipusaudit', self.gf('django.db.models.fields.AutoField')(primary_key=True, db_column='id_tipus_audit')),
            ('destipusaudit', self.gf('django.db.models.fields.CharField')(max_length=255, db_column='des_tipus_audit', blank=True)),
            ('actiu', self.gf('django.db.models.fields.BooleanField')(default=False, db_column='actiu')),
        ))
        db.send_create_signal('meta', ['TipusAudit'])

        # Adding model 'Audit'
        db.create_table(u'meta_audit', (
            ('idaudit', self.gf('django.db.models.fields.AutoField')(primary_key=True, db_column='id_audit')),
            ('idauth_user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, db_column='id_auth_user', blank=True)),
            ('dataevent', self.gf('django.db.models.fields.DateTimeField')(db_column='data_event', blank=True)),
            ('idtipusaudit', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.TipusAudit'], null=True, db_column='id_tipus_audit', blank=True)),
            ('dades', self.gf('django.db.models.fields.CharField')(max_length=500, db_column='dades', blank=True)),
        ))
        db.send_create_signal('meta', ['Audit'])

        # Adding model 'MetaCentreIdi'
        db.create_table(u'meta_centre_idi', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('descripcio', self.gf('django.db.models.fields.CharField')(max_length=300)),
        ))
        db.send_create_signal('meta', ['MetaCentreIdi'])

        # Adding model 'MetaUnitatIdi'
        db.create_table(u'meta_unitat_idi', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('descripcio', self.gf('django.db.models.fields.CharField')(max_length=600, blank=True)),
            ('centre_idi', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.MetaCentreIdi'])),
            ('unitatat_facturacio_id', self.gf('django.db.models.fields.IntegerField')()),
            ('actiu', self.gf('django.db.models.fields.IntegerField')()),
            ('calendari_laboral', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['planilles.CalendariLaboral'], null=True)),
        ))
        db.send_create_signal('meta', ['MetaUnitatIdi'])

        # Adding model 'MetaGrupCategoriaProfessional'
        db.create_table(u'meta_grup_categoria_professional', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nom', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal('meta', ['MetaGrupCategoriaProfessional'])

        # Adding model 'MetaUnitatIdiKarat'
        db.create_table(u'meta_unitat_idi_karat', (
            ('id', self.gf('django.db.models.fields.IntegerField')(primary_key=True)),
            ('descripcio', self.gf('django.db.models.fields.CharField')(max_length=600)),
            ('meta_unitat_idi', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.MetaUnitatIdi'])),
        ))
        db.send_create_signal('meta', ['MetaUnitatIdiKarat'])

        # Adding model 'MetaCategoriaProfesionalKarat'
        db.create_table(u'meta_categoria_profesional_karat', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('descripcio', self.gf('django.db.models.fields.CharField')(max_length=45)),
            ('abreviatura', self.gf('django.db.models.fields.CharField')(max_length=8)),
        ))
        db.send_create_signal('meta', ['MetaCategoriaProfesionalKarat'])

        # Adding M2M table for field grup on 'MetaCategoriaProfesionalKarat'
        db.create_table(u'meta_categoria_profesional_karat_grup', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('metacategoriaprofesionalkarat', models.ForeignKey(orm['meta.metacategoriaprofesionalkarat'], null=False)),
            ('metagrupcategoriaprofessional', models.ForeignKey(orm['meta.metagrupcategoriaprofessional'], null=False))
        ))
        db.create_unique(u'meta_categoria_profesional_karat_grup', ['metacategoriaprofesionalkarat_id', 'metagrupcategoriaprofessional_id'])

        # Adding model 'MetaCategoriaProfesional'
        db.create_table(u'meta_categoria_profesional', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('descripcio', self.gf('django.db.models.fields.CharField')(max_length=405)),
            ('orden', self.gf('django.db.models.fields.IntegerField')()),
            ('categoria_profesional_karat', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.MetaCategoriaProfesionalKarat'], null=True)),
            ('responsabilitat_karat', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('meta', ['MetaCategoriaProfesional'])

        # Adding model 'AuthUser'
        db.create_table(u'auth_user', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('username', self.gf('django.db.models.fields.CharField')(unique=True, max_length=90)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=90)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=90)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=225)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=384)),
            ('is_staff', self.gf('django.db.models.fields.IntegerField')()),
            ('is_active', self.gf('django.db.models.fields.IntegerField')()),
            ('is_superuser', self.gf('django.db.models.fields.IntegerField')()),
            ('last_login', self.gf('django.db.models.fields.DateTimeField')()),
            ('date_joined', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('meta', ['AuthUser'])

        # Adding model 'AuthUserProfile'
        db.create_table(u'auth_user_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('categoria_profesional', self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['meta.MetaCategoriaProfesional'])),
            ('nombre_usuario', self.gf('django.db.models.fields.CharField')(max_length=405)),
            ('dpo', self.gf('django.db.models.fields.NullBooleanField')(default=False, null=True, blank=True)),
            ('bloqueado', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('password_usuario', self.gf('django.db.models.fields.CharField')(max_length=288, blank=True)),
            ('unitat_idi', self.gf('django.db.models.fields.related.ForeignKey')(default=17, to=orm['meta.MetaUnitatIdi'], blank=True)),
            ('es_idi', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('categoria_profesional_karat', self.gf('django.db.models.fields.related.ForeignKey')(default=29, to=orm['meta.MetaCategoriaProfesionalKarat'], blank=True)),
            ('data', self.gf('django.db.models.fields.DateTimeField')()),
            ('jornada_anual', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['planilles.JornadaAnual'])),
        ))
        db.send_create_signal('meta', ['AuthUserProfile'])

        # Adding M2M table for field unitats_idi_permissos on 'AuthUserProfile'
        db.create_table(u'auth_user_profile_unitats_idi_permissos', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('authuserprofile', models.ForeignKey(orm['meta.authuserprofile'], null=False)),
            ('metaunitatidi', models.ForeignKey(orm['meta.metaunitatidi'], null=False))
        ))
        db.create_unique(u'auth_user_profile_unitats_idi_permissos', ['authuserprofile_id', 'metaunitatidi_id'])

        # Adding model 'MetaAuthUserCategoria'
        db.create_table(u'meta_auth_user_categoria', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('auth_user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.AuthUserProfile'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('meta_categoria_profesional', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.MetaCategoriaProfesional'])),
            ('meta_categoria_profesional_karat', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.MetaCategoriaProfesionalKarat'], null=True)),
            ('anyo', self.gf('django.db.models.fields.IntegerField')()),
            ('unitat_idi', self.gf('django.db.models.fields.related.ForeignKey')(default=17, to=orm['meta.MetaUnitatIdi'], blank=True)),
            ('dpo', self.gf('django.db.models.fields.NullBooleanField')(default=False, null=True, blank=True)),
            ('data', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2013, 1, 15, 0, 0))),
        ))
        db.send_create_signal('meta', ['MetaAuthUserCategoria'])

        # Adding model 'AppPermis'
        db.create_table('meta_apppermis', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('meta', ['AppPermis'])

        # Adding model 'MetaMes'
        db.create_table(u'meta_mes', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True, db_column='IdMes')),
            ('mes', self.gf('django.db.models.fields.CharField')(max_length=135, db_column='Mes', blank=True)),
        ))
        db.send_create_signal('meta', ['MetaMes'])

        # Adding model 'MetaTipoexploracion'
        db.create_table(u'meta_tipoexploracion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True, db_column='IdTipoExploracion')),
            ('tipoexploracion', self.gf('django.db.models.fields.CharField')(max_length=135, db_column='TipoExploracion', blank=True)),
        ))
        db.send_create_signal('meta', ['MetaTipoexploracion'])

        # Adding model 'MetaUnidadfac'
        db.create_table(u'meta_unidadfac', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True, db_column='IdUnidadFac')),
            ('unidadfac', self.gf('django.db.models.fields.CharField')(max_length=135, db_column='UnidadFac', blank=True)),
            ('ufnombrefscal', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='UFNombreFscal', blank=True)),
            ('ns', self.gf('django.db.models.fields.CharField')(max_length=135, db_column='NS', blank=True)),
            ('ufcif', self.gf('django.db.models.fields.CharField')(max_length=36, db_column='UFCIF', blank=True)),
            ('ufdireccionfiscal', self.gf('django.db.models.fields.CharField')(max_length=300, db_column='UFDireccionFiscal', blank=True)),
            ('ufcpfiscal', self.gf('django.db.models.fields.CharField')(max_length=30, db_column='UFCPFiscal', blank=True)),
            ('ufmunicipiofiscal', self.gf('django.db.models.fields.CharField')(max_length=135, db_column='UFMunicipioFiscal', blank=True)),
            ('idcentre', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.MetaCentreIdi'], null=True, db_column='IdCentre', blank=True)),
            ('codup', self.gf('django.db.models.fields.CharField')(max_length=15, db_column='CodUP', blank=True)),
        ))
        db.send_create_signal('meta', ['MetaUnidadfac'])

        # Adding model 'MetaAgenda'
        db.create_table(u'meta_agenda', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True, db_column='IdAgenda')),
            ('codagenda', self.gf('django.db.models.fields.IntegerField')(db_column='CodAgenda')),
            ('agenda', self.gf('django.db.models.fields.CharField')(max_length=135, db_column='Agenda', blank=True)),
            ('idunidadfac', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.MetaUnidadfac'], null=True, db_column='IdUnidadFac', blank=True)),
            ('idtipoexploracion', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.MetaTipoexploracion'], null=True, db_column='IdTipoExploracion', blank=True)),
            ('obs', self.gf('django.db.models.fields.CharField')(max_length=135, db_column='Obs', blank=True)),
            ('unitattractament', self.gf('django.db.models.fields.CharField')(max_length=96, db_column='UnitatTractament', blank=True)),
            ('db', self.gf('django.db.models.fields.CharField')(max_length=135, db_column='DB', blank=True)),
            ('idcentre', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.MetaCentreIdi'], null=True, db_column='IdCentre', blank=True)),
        ))
        db.send_create_signal('meta', ['MetaAgenda'])

        # Adding model 'MetaOpcions'
        db.create_table(u'meta_opcions', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('app_key', self.gf('django.db.models.fields.CharField')(max_length=300)),
        ))
        db.send_create_signal('meta', ['MetaOpcions'])

        # Adding model 'MetaDiaFestiu'
        db.create_table(u'meta_diafestiu', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('centre', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['meta.MetaCentreIdi'], null=True, blank=True)),
            ('desc', self.gf('django.db.models.fields.CharField')(max_length=600, blank=True)),
            ('data', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal('meta', ['MetaDiaFestiu'])


    def backwards(self, orm):
        # Deleting model 'TipusAudit'
        db.delete_table(u'meta_tipus_audit')

        # Deleting model 'Audit'
        db.delete_table(u'meta_audit')

        # Deleting model 'MetaCentreIdi'
        db.delete_table(u'meta_centre_idi')

        # Deleting model 'MetaUnitatIdi'
        db.delete_table(u'meta_unitat_idi')

        # Deleting model 'MetaGrupCategoriaProfessional'
        db.delete_table(u'meta_grup_categoria_professional')

        # Deleting model 'MetaUnitatIdiKarat'
        db.delete_table(u'meta_unitat_idi_karat')

        # Deleting model 'MetaCategoriaProfesionalKarat'
        db.delete_table(u'meta_categoria_profesional_karat')

        # Removing M2M table for field grup on 'MetaCategoriaProfesionalKarat'
        db.delete_table('meta_categoria_profesional_karat_grup')

        # Deleting model 'MetaCategoriaProfesional'
        db.delete_table(u'meta_categoria_profesional')

        # Deleting model 'AuthUser'
        db.delete_table(u'auth_user')

        # Deleting model 'AuthUserProfile'
        db.delete_table(u'auth_user_profile')

        # Removing M2M table for field unitats_idi_permissos on 'AuthUserProfile'
        db.delete_table('auth_user_profile_unitats_idi_permissos')

        # Deleting model 'MetaAuthUserCategoria'
        db.delete_table(u'meta_auth_user_categoria')

        # Deleting model 'AppPermis'
        db.delete_table('meta_apppermis')

        # Deleting model 'MetaMes'
        db.delete_table(u'meta_mes')

        # Deleting model 'MetaTipoexploracion'
        db.delete_table(u'meta_tipoexploracion')

        # Deleting model 'MetaUnidadfac'
        db.delete_table(u'meta_unidadfac')

        # Deleting model 'MetaAgenda'
        db.delete_table(u'meta_agenda')

        # Deleting model 'MetaOpcions'
        db.delete_table(u'meta_opcions')

        # Deleting model 'MetaDiaFestiu'
        db.delete_table(u'meta_diafestiu')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'meta.apppermis': {
            'Meta': {'object_name': 'AppPermis'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'meta.audit': {
            'Meta': {'object_name': 'Audit'},
            'dades': ('django.db.models.fields.CharField', [], {'max_length': '500', 'db_column': "'dades'", 'blank': 'True'}),
            'dataevent': ('django.db.models.fields.DateTimeField', [], {'db_column': "'data_event'", 'blank': 'True'}),
            'idaudit': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'id_audit'"}),
            'idauth_user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'db_column': "'id_auth_user'", 'blank': 'True'}),
            'idtipusaudit': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.TipusAudit']", 'null': 'True', 'db_column': "'id_tipus_audit'", 'blank': 'True'})
        },
        'meta.authuser': {
            'Meta': {'object_name': 'AuthUser', 'db_table': "u'auth_user'"},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '225'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.IntegerField', [], {}),
            'is_staff': ('django.db.models.fields.IntegerField', [], {}),
            'is_superuser': ('django.db.models.fields.IntegerField', [], {}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '384'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '90'})
        },
        'meta.authuserprofile': {
            'Meta': {'object_name': 'AuthUserProfile', 'db_table': "u'auth_user_profile'"},
            'bloqueado': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'categoria_profesional': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['meta.MetaCategoriaProfesional']"}),
            'categoria_profesional_karat': ('django.db.models.fields.related.ForeignKey', [], {'default': '29', 'to': "orm['meta.MetaCategoriaProfesionalKarat']", 'blank': 'True'}),
            'data': ('django.db.models.fields.DateTimeField', [], {}),
            'dpo': ('django.db.models.fields.NullBooleanField', [], {'default': 'False', 'null': 'True', 'blank': 'True'}),
            'es_idi': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jornada_anual': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['planilles.JornadaAnual']"}),
            'nombre_usuario': ('django.db.models.fields.CharField', [], {'max_length': '405'}),
            'password_usuario': ('django.db.models.fields.CharField', [], {'max_length': '288', 'blank': 'True'}),
            'unitat_idi': ('django.db.models.fields.related.ForeignKey', [], {'default': '17', 'to': "orm['meta.MetaUnitatIdi']", 'blank': 'True'}),
            'unitats_idi_permissos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'auth_user_profile_meta_unitats_idi'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['meta.MetaUnitatIdi']"}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'meta.metaagenda': {
            'Meta': {'object_name': 'MetaAgenda', 'db_table': "u'meta_agenda'"},
            'agenda': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'Agenda'", 'blank': 'True'}),
            'codagenda': ('django.db.models.fields.IntegerField', [], {'db_column': "'CodAgenda'"}),
            'db': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'DB'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'IdAgenda'"}),
            'idcentre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCentreIdi']", 'null': 'True', 'db_column': "'IdCentre'", 'blank': 'True'}),
            'idtipoexploracion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaTipoexploracion']", 'null': 'True', 'db_column': "'IdTipoExploracion'", 'blank': 'True'}),
            'idunidadfac': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaUnidadfac']", 'null': 'True', 'db_column': "'IdUnidadFac'", 'blank': 'True'}),
            'obs': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'Obs'", 'blank': 'True'}),
            'unitattractament': ('django.db.models.fields.CharField', [], {'max_length': '96', 'db_column': "'UnitatTractament'", 'blank': 'True'})
        },
        'meta.metaauthusercategoria': {
            'Meta': {'object_name': 'MetaAuthUserCategoria', 'db_table': "u'meta_auth_user_categoria'"},
            'anyo': ('django.db.models.fields.IntegerField', [], {}),
            'auth_user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.AuthUserProfile']"}),
            'data': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 1, 15, 0, 0)'}),
            'dpo': ('django.db.models.fields.NullBooleanField', [], {'default': 'False', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta_categoria_profesional': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCategoriaProfesional']"}),
            'meta_categoria_profesional_karat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCategoriaProfesionalKarat']", 'null': 'True'}),
            'unitat_idi': ('django.db.models.fields.related.ForeignKey', [], {'default': '17', 'to': "orm['meta.MetaUnitatIdi']", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'meta.metacategoriaprofesional': {
            'Meta': {'object_name': 'MetaCategoriaProfesional', 'db_table': "u'meta_categoria_profesional'"},
            'categoria_profesional_karat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCategoriaProfesionalKarat']", 'null': 'True'}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '405'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'orden': ('django.db.models.fields.IntegerField', [], {}),
            'responsabilitat_karat': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'meta.metacategoriaprofesionalkarat': {
            'Meta': {'object_name': 'MetaCategoriaProfesionalKarat', 'db_table': "u'meta_categoria_profesional_karat'"},
            'abreviatura': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'grup': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['meta.MetaGrupCategoriaProfessional']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'meta.metacentreidi': {
            'Meta': {'ordering': "['descripcio']", 'object_name': 'MetaCentreIdi', 'db_table': "u'meta_centre_idi'"},
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'meta.metadiafestiu': {
            'Meta': {'object_name': 'MetaDiaFestiu', 'db_table': "u'meta_diafestiu'"},
            'centre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCentreIdi']", 'null': 'True', 'blank': 'True'}),
            'data': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'desc': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'meta.metagrupcategoriaprofessional': {
            'Meta': {'object_name': 'MetaGrupCategoriaProfessional', 'db_table': "u'meta_grup_categoria_professional'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'meta.metames': {
            'Meta': {'object_name': 'MetaMes', 'db_table': "u'meta_mes'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'IdMes'"}),
            'mes': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'Mes'", 'blank': 'True'})
        },
        'meta.metaopcions': {
            'Meta': {'object_name': 'MetaOpcions', 'db_table': "u'meta_opcions'"},
            'app_key': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        'meta.metatipoexploracion': {
            'Meta': {'object_name': 'MetaTipoexploracion', 'db_table': "u'meta_tipoexploracion'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'IdTipoExploracion'"}),
            'tipoexploracion': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'TipoExploracion'", 'blank': 'True'})
        },
        'meta.metaunidadfac': {
            'Meta': {'object_name': 'MetaUnidadfac', 'db_table': "u'meta_unidadfac'"},
            'codup': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_column': "'CodUP'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'IdUnidadFac'"}),
            'idcentre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCentreIdi']", 'null': 'True', 'db_column': "'IdCentre'", 'blank': 'True'}),
            'ns': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'NS'", 'blank': 'True'}),
            'ufcif': ('django.db.models.fields.CharField', [], {'max_length': '36', 'db_column': "'UFCIF'", 'blank': 'True'}),
            'ufcpfiscal': ('django.db.models.fields.CharField', [], {'max_length': '30', 'db_column': "'UFCPFiscal'", 'blank': 'True'}),
            'ufdireccionfiscal': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'UFDireccionFiscal'", 'blank': 'True'}),
            'ufmunicipiofiscal': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'UFMunicipioFiscal'", 'blank': 'True'}),
            'ufnombrefscal': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'UFNombreFscal'", 'blank': 'True'}),
            'unidadfac': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'UnidadFac'", 'blank': 'True'})
        },
        'meta.metaunitatidi': {
            'Meta': {'ordering': "['descripcio']", 'object_name': 'MetaUnitatIdi', 'db_table': "u'meta_unitat_idi'"},
            'actiu': ('django.db.models.fields.IntegerField', [], {}),
            'calendari_laboral': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['planilles.CalendariLaboral']", 'null': 'True'}),
            'centre_idi': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCentreIdi']"}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unitatat_facturacio_id': ('django.db.models.fields.IntegerField', [], {})
        },
        'meta.metaunitatidikarat': {
            'Meta': {'object_name': 'MetaUnitatIdiKarat', 'db_table': "u'meta_unitat_idi_karat'"},
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '600'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'meta_unitat_idi': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaUnitatIdi']"})
        },
        'meta.tipusaudit': {
            'Meta': {'object_name': 'TipusAudit', 'db_table': "u'meta_tipus_audit'"},
            'actiu': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'actiu'"}),
            'destipusaudit': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_column': "'des_tipus_audit'", 'blank': 'True'}),
            'idtipusaudit': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'id_tipus_audit'"})
        },
        'planilles.calendarilaboral': {
            'Meta': {'ordering': "['nom']", 'object_name': 'CalendariLaboral', 'db_table': "u'plan_calendari_laboral'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'es_mestre': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'planilles.jornadaanual': {
            'Meta': {'object_name': 'JornadaAnual', 'db_table': "'plan_jornada_anual'"},
            'actiu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'hores': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['meta']