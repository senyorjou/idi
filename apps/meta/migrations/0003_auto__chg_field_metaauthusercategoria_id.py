# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'MetaAuthUserCategoria.id'
        db.alter_column(u'meta_auth_user_categoria', 'id', self.gf('django.db.models.fields.AutoField')(primary_key=True))

    def backwards(self, orm):

        # Changing field 'MetaAuthUserCategoria.id'
        db.alter_column(u'meta_auth_user_categoria', 'id', self.gf('django.db.models.fields.IntegerField')(primary_key=True))

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'meta.apppermis': {
            'Meta': {'object_name': 'AppPermis'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'meta.audit': {
            'Meta': {'object_name': 'Audit'},
            'dades': ('django.db.models.fields.CharField', [], {'max_length': '500', 'db_column': "'dades'", 'blank': 'True'}),
            'dataevent': ('django.db.models.fields.DateTimeField', [], {'db_column': "'data_event'", 'blank': 'True'}),
            'idaudit': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'id_audit'"}),
            'idauth_user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'db_column': "'id_auth_user'", 'blank': 'True'}),
            'idtipusaudit': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.TipusAudit']", 'null': 'True', 'db_column': "'id_tipus_audit'", 'blank': 'True'})
        },
        'meta.authuser': {
            'Meta': {'object_name': 'AuthUser', 'db_table': "u'auth_user'"},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '225'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.IntegerField', [], {}),
            'is_staff': ('django.db.models.fields.IntegerField', [], {}),
            'is_superuser': ('django.db.models.fields.IntegerField', [], {}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '90'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '384'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '90'})
        },
        'meta.authuserprofile': {
            'Meta': {'object_name': 'AuthUserProfile', 'db_table': "u'auth_user_profile'"},
            'bloqueado': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'categoria_profesional': ('django.db.models.fields.related.ForeignKey', [], {'default': '0', 'to': "orm['meta.MetaCategoriaProfesional']"}),
            'categoria_profesional_karat': ('django.db.models.fields.related.ForeignKey', [], {'default': '29', 'to': "orm['meta.MetaCategoriaProfesionalKarat']", 'blank': 'True'}),
            'data': ('django.db.models.fields.DateTimeField', [], {}),
            'dpo': ('django.db.models.fields.NullBooleanField', [], {'default': 'False', 'null': 'True', 'blank': 'True'}),
            'es_actiu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'es_idi': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jornada_anual': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': "orm['planilles.JornadaAnual']"}),
            'nombre_usuario': ('django.db.models.fields.CharField', [], {'max_length': '405'}),
            'password_usuario': ('django.db.models.fields.CharField', [], {'max_length': '288', 'blank': 'True'}),
            'unitat_idi': ('django.db.models.fields.related.ForeignKey', [], {'default': '17', 'to': "orm['meta.MetaUnitatIdi']", 'blank': 'True'}),
            'unitats_idi_permissos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'auth_user_profile_meta_unitats_idi'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['meta.MetaUnitatIdi']"}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'meta.metaagenda': {
            'Meta': {'object_name': 'MetaAgenda', 'db_table': "u'meta_agenda'"},
            'agenda': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'Agenda'", 'blank': 'True'}),
            'codagenda': ('django.db.models.fields.IntegerField', [], {'db_column': "'CodAgenda'"}),
            'db': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'DB'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'IdAgenda'"}),
            'idcentre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCentreIdi']", 'null': 'True', 'db_column': "'IdCentre'", 'blank': 'True'}),
            'idtipoexploracion': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaTipoexploracion']", 'null': 'True', 'db_column': "'IdTipoExploracion'", 'blank': 'True'}),
            'idunidadfac': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaUnidadfac']", 'null': 'True', 'db_column': "'IdUnidadFac'", 'blank': 'True'}),
            'obs': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'Obs'", 'blank': 'True'}),
            'unitattractament': ('django.db.models.fields.CharField', [], {'max_length': '96', 'db_column': "'UnitatTractament'", 'blank': 'True'})
        },
        'meta.metaauthusercategoria': {
            'Meta': {'object_name': 'MetaAuthUserCategoria', 'db_table': "u'meta_auth_user_categoria'"},
            'anyo': ('django.db.models.fields.IntegerField', [], {}),
            'auth_user_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.AuthUserProfile']", 'db_column': "'auth_user_id'"}),
            'categoria_profesional_karat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCategoriaProfesionalKarat']", 'null': 'True'}),
            'data': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 1, 22, 0, 0)'}),
            'dpo': ('django.db.models.fields.NullBooleanField', [], {'default': 'False', 'null': 'True', 'blank': 'True'}),
            'es_actiu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jornada_anual': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['planilles.JornadaAnual']", 'null': 'True'}),
            'meta_categoria_profesional': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCategoriaProfesional']"}),
            'unitat_idi': ('django.db.models.fields.related.ForeignKey', [], {'default': '17', 'to': "orm['meta.MetaUnitatIdi']", 'blank': 'True'})
        },
        'meta.metacategoriaprofesional': {
            'Meta': {'object_name': 'MetaCategoriaProfesional', 'db_table': "u'meta_categoria_profesional'"},
            'categoria_profesional_karat': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCategoriaProfesionalKarat']", 'null': 'True'}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '405'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'orden': ('django.db.models.fields.IntegerField', [], {}),
            'responsabilitat_karat': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'meta.metacategoriaprofesionalkarat': {
            'Meta': {'object_name': 'MetaCategoriaProfesionalKarat', 'db_table': "u'meta_categoria_profesional_karat'"},
            'abreviatura': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '45'}),
            'grup': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['meta.MetaGrupCategoriaProfessional']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'meta.metacentreidi': {
            'Meta': {'ordering': "['descripcio']", 'object_name': 'MetaCentreIdi', 'db_table': "u'meta_centre_idi'"},
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'meta.metadiafestiu': {
            'Meta': {'object_name': 'MetaDiaFestiu', 'db_table': "u'meta_diafestiu'"},
            'centre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCentreIdi']", 'null': 'True', 'blank': 'True'}),
            'data': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'desc': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'meta.metagrupcategoriaprofessional': {
            'Meta': {'object_name': 'MetaGrupCategoriaProfessional', 'db_table': "u'meta_grup_categoria_professional'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'meta.metames': {
            'Meta': {'object_name': 'MetaMes', 'db_table': "u'meta_mes'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'IdMes'"}),
            'mes': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'Mes'", 'blank': 'True'})
        },
        'meta.metaopcions': {
            'Meta': {'object_name': 'MetaOpcions', 'db_table': "u'meta_opcions'"},
            'app_key': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        'meta.metatipoexploracion': {
            'Meta': {'object_name': 'MetaTipoexploracion', 'db_table': "u'meta_tipoexploracion'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'IdTipoExploracion'"}),
            'tipoexploracion': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'TipoExploracion'", 'blank': 'True'})
        },
        'meta.metaunidadfac': {
            'Meta': {'object_name': 'MetaUnidadfac', 'db_table': "u'meta_unidadfac'"},
            'codup': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_column': "'CodUP'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'IdUnidadFac'"}),
            'idcentre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCentreIdi']", 'null': 'True', 'db_column': "'IdCentre'", 'blank': 'True'}),
            'ns': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'NS'", 'blank': 'True'}),
            'ufcif': ('django.db.models.fields.CharField', [], {'max_length': '36', 'db_column': "'UFCIF'", 'blank': 'True'}),
            'ufcpfiscal': ('django.db.models.fields.CharField', [], {'max_length': '30', 'db_column': "'UFCPFiscal'", 'blank': 'True'}),
            'ufdireccionfiscal': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'UFDireccionFiscal'", 'blank': 'True'}),
            'ufmunicipiofiscal': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'UFMunicipioFiscal'", 'blank': 'True'}),
            'ufnombrefscal': ('django.db.models.fields.CharField', [], {'max_length': '300', 'db_column': "'UFNombreFscal'", 'blank': 'True'}),
            'unidadfac': ('django.db.models.fields.CharField', [], {'max_length': '135', 'db_column': "'UnidadFac'", 'blank': 'True'})
        },
        'meta.metaunitatidi': {
            'Meta': {'ordering': "['descripcio']", 'object_name': 'MetaUnitatIdi', 'db_table': "u'meta_unitat_idi'"},
            'actiu': ('django.db.models.fields.IntegerField', [], {}),
            'calendari_laboral': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['planilles.CalendariLaboral']", 'null': 'True'}),
            'centre_idi': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaCentreIdi']"}),
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'unitatat_facturacio_id': ('django.db.models.fields.IntegerField', [], {})
        },
        'meta.metaunitatidikarat': {
            'Meta': {'object_name': 'MetaUnitatIdiKarat', 'db_table': "u'meta_unitat_idi_karat'"},
            'descripcio': ('django.db.models.fields.CharField', [], {'max_length': '600'}),
            'id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'meta_unitat_idi': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['meta.MetaUnitatIdi']"})
        },
        'meta.tipusaudit': {
            'Meta': {'object_name': 'TipusAudit', 'db_table': "u'meta_tipus_audit'"},
            'actiu': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_column': "'actiu'"}),
            'destipusaudit': ('django.db.models.fields.CharField', [], {'max_length': '255', 'db_column': "'des_tipus_audit'", 'blank': 'True'}),
            'idtipusaudit': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_column': "'id_tipus_audit'"})
        },
        'planilles.calendarilaboral': {
            'Meta': {'ordering': "['nom']", 'object_name': 'CalendariLaboral', 'db_table': "u'plan_calendari_laboral'"},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'es_mestre': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'planilles.jornadaanual': {
            'Meta': {'object_name': 'JornadaAnual', 'db_table': "'plan_jornada_anual'"},
            'actiu': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'es_default': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hores': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['meta']