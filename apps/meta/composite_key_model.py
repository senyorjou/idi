'''
Created on 12/03/2012

@author: amateu
'''

################################################################################################
# Base Class for weak-emulation of composite primary key. 
# To have composite keys workaround in your model, follow these steps: 
# Step1: Paste this snippet before your models in models.py
# Step2: Extend you class from CompositeKeyModel
# Step3: Define 'unique_together=(("field1","field2","field3"),) in your models Meta class
# Step4: Create db-table by hand.
#
# That's it! The save() on the instances of these models should have the composite key effect.
#
# Send feedback to: pawel.suwala@fsfe.org
##################################################################################################

from django.db import models
from django.db.models.base import ModelBase

class CompositeKeyModel(models.Model): pass

class IntermediateModelBase(ModelBase):
    def __new__(cls, name, bases, attrs):
        if CompositeKeyModel in bases:
            if bases == (CompositeKeyModel,):
                # create the class via the super method
                newclass = ModelBase.__new__(ModelBase, name, (models.Model,), attrs)
                # but then make it inherit from our model class
                newclass.__bases__ = (CompositeKeyModel,)
                return newclass
            else: raise Exception, "IntermediateModelBase does not support more than one base"
        else:
            return type.__new__(cls, name, bases, attrs)


class CompositeKeyModel(models.Model):
    __metaclass__ = IntermediateModelBase
    def save(self, *args, **kwargs):
        filter = {}
        #contruct model filter on the fly for the fields that listed in the
        # 'unique_together' meta attribute

        if not self._meta.unique_together:
            raise ValueError('Specify Meta.unique_together to emulate Composite pk')

        for field_name_list in self._meta.unique_together:
            for field in field_name_list:
                filter[field]='%s' % (self.get_field_value(field))
        #use the generated filter to check if the object already exist
        # if so fetch it
        
        fetched = self.__class__.objects.complex_filter(filter)
        #if fetched, then get its primary key and set it into the object instance
        #that is being saved.
        if(len(fetched) > 0):
            pk = self.get_primary_key()
            self.__setattr__(pk.name,fetched[0].__getattribute__(pk.name))
        #finally call the super class i.e Model save() method
        models.Model.save(self, *args, **kwargs)

    def _is_foreign_key(self, field): # there must be a better way to do this
        meta_class = getattr(field.__class__, '__metaclass__', None)
        return meta_class == ModelBase

    def get_field_value(self, fieldName):
        field_value = getattr(self, fieldName)
        if self._is_foreign_key(field_value):
            return field_value.pk
        return field_value

    def get_primary_key(self):
        for field in self._meta.fields:
            if (field.primary_key):
                return field

        raise Exception('Your model must have a dummy primary key (id)')
