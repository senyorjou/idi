'''
Created on 26/07/2011
@author: amateu
'''
# Create your views here.

#from django.template import Context, loader
#from django.contrib.auth import authenticate, login, logout
#from django.http import HttpResponseNotFound
#import settings
#from django.core.files import File
from django.http import HttpResponse
from apps.historic_rm_vh.models import Pacient,Historias
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.db.models  import Q
import os
import mimetypes
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
import apps.meta.auditoria

historic_de = 'RM_VH'


@login_required
@permission_required('meta.historic_rm_vh', login_url='/accounts/permisdenegat/')
def index(request):
    return render_to_response('historics/index.html' ,
                               {'historic':historic_de,},
                              context_instance=RequestContext(request))


#Formulari de busqueda de pacients per codis ACR
@login_required
@permission_required('meta.historic_rm_vh', login_url='/accounts/permisdenegat/')
def cercaPacientsACR(request):
    return render_to_response('historics/cercapacientsacr.html',
                                {'historic':historic_de,},
                              context_instance=RequestContext(request))


@login_required
@permission_required('meta.historic_rm_vh', login_url='/accounts/permisdenegat/')
def cercaPacientsACRResultat(request):
    
   
    if 'tipusConsulta' in request.GET:
        tipusConsulta =  request.GET['tipusConsulta']
    else:
        tipusConsulta =  "OR"
            
    #if 'codiDI1' in request.GET:   #no cal ja que een el hmtl li hem posat un valor per defecte "" a string buida per tant nomes cal fero en els SELESCTS  
    codiDI1 = request.GET['codiDI1']
    codiDI2 = request.GET['codiDI2']
    codiDI3 = request.GET['codiDI3']
    
    consulta = codiDI1 + ' ' + codiDI2 + ' ' + codiDI3
    consulta = consulta.split()
    numparams = len(consulta)

    historiasList = Historias.objects
    a = Q()
        
    if tipusConsulta == "AND":
        if numparams <> 0:
            for i in range(0,numparams):
                a = a & ( Q(cdi1__icontains=consulta[i]) | Q(cdi2__icontains=consulta[i]) | Q(cdi3__icontains=consulta[i]))    
            historiasList = historiasList.filter(a)
            #print(str(a))
    else:# OR
        if numparams <> 0:
            for i in range(0,numparams):
                a = a | Q(cdi1__icontains=consulta[i])
                a = a | Q(cdi2__icontains=consulta[i])
                a = a | Q(cdi3__icontains=consulta[i])
            historiasList = historiasList.filter(a)
            #print(str(a))
            
    apps.meta.auditoria.request_handler(request,4,'Tipus consulta RM:' + tipusConsulta + '; Consulta: ' + str(consulta ))
              
    return render_to_response('historics/cercapacientsacrresultat.html',
                               {'historic':historic_de, 
                               'historiasambacr': historiasList.order_by('cgn1')[:1000]},
                              context_instance=RequestContext(request))


#Formulari de busqueda de pacients per nom o per id de la blava 
@login_required
@permission_required('meta.historic_rm_vh', login_url='/accounts/permisdenegat/')
def cercaPacients(request):
    return render_to_response('historics/cercapacients.html', 
                              {'historic':historic_de},
                              context_instance=RequestContext(request))


#Resultat de la busqueda ( llistat de  pacients que sasemblesbn a la busqueda)
@login_required
@permission_required('meta.historic_rm_vh', login_url='/accounts/permisdenegat/')
def cercaPacientsResultat(request):
    
    tipusConsulta = "perNom" 
    #, False
    if "pacient" in request.GET:
        if request.GET['pacient'] <> "":
            consulta = request.GET['pacient'].split() 
        else:
            if "pacientNHC" in request.GET:
                if request.GET['pacientNHC'] <> "":
                    consulta =  request.GET['pacientNHC']
                    tipusConsulta = "perIdBlava"
                else:
                    consulta = '#'# 
                    tipusConsulta = "perIdBlava"
            else:
                    consulta = '#'#a 
                    tipusConsulta = "perIdBlava"

    else:
        if "pacientNHC" in request.GET:
            consulta =  request.GET['pacientNHC']
            tipusConsulta = "perIdBlava"
        else:
            consulta = '#'# 
            tipusConsulta = "perIdBlava"
    
     
        
    
    patientsList = Pacient.objects
    
    if tipusConsulta == "perNom":
        for i in range(0, len(consulta)):
            patientsList = patientsList.filter(Q(cgn1__icontains=consulta[i]) 
                                               | Q(cgn2__icontains=consulta[i])
                                               | Q(nomb__icontains=consulta[i]))
    else: #tipusConsulta = "perIDBlava":
            patientsList = patientsList.filter(Q(nex1__icontains=consulta))

                         
    return render_to_response('historics/cercapacientsresultat.html', 
                              {'historic':historic_de,
                               'primers_pacient_list': patientsList.order_by('cgn1')[:1000]},
                              context_instance=RequestContext(request))

#llistat dhistories que te asociat un pacient
@login_required
@permission_required('meta.historic_rm_vh', login_url='/accounts/permisdenegat/')
def pacient(request, pacient_id):
    
    #    return HttpResponse("You're looking pel pacient %s." % pacient_id)
    pacient = get_object_or_404(Pacient, pk=pacient_id)
    historias = pacient.historias_set.all
    apps.meta.auditoria.request_handler(request,3,pacient)    
    return render_to_response('historics/historiespacient.html', 
                              {'historic':historic_de,
                               'historias': historias },
                              context_instance=RequestContext(request))

# envia l'informe de la prova sellecionada
@login_required
@permission_required('meta.historic_rm_vh', login_url='/accounts/permisdenegat/')
def informe(request,historia_id):
    
    mimetypes.init()    
    h = get_object_or_404(Historias, pk=historia_id)
    rutaInf=h.rutaInforme()

    try:
      
        # informe_path = os.path.normpath( settings.STATIC_ROOT + settings.STATIC_URL + '/informes/' + rutaInf)
        informe_path = os.path.normpath( '/var/lib/django/portal/data/informes/' + str(rutaInf))
        informe = open(informe_path,"rb")
        informe_name = os.path.basename(informe_path)
        mime_type_guess = mimetypes.guess_type('/file.doc',strict=False)
       
        if mime_type_guess[0] is not None:
            response = HttpResponse(informe, mimetype=mime_type_guess[0])
        else:
            response = HttpResponse(informe, mimetype='application/vnd.ms-word')
            
        response['Content-Disposition'] = 'attachment; filename=' + informe_name +'.doc'           
    
    except IOError:
          
        response = render_to_response('404.html', {'error_description': "Informe no trobat en la base de dades" },
                              context_instance=RequestContext(request))
    
    apps.meta.auditoria.request_handler(request,5,rutaInf)  
    return response
           




    
    