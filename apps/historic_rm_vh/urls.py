'''
Created on 26/07/2011
@author: amateu
'''
from django.conf.urls.defaults import url,patterns 

urlpatterns = patterns('apps.historic_rm_vh.views',
    url(r'^$', 'index'), # 
    url(r'cercapacients/$', 'cercaPacients'), # inici busqueda per pacient
    url(r'cercapacientsresultat/$', 'cercaPacientsResultat'), # pacients que estan dins de la cerca
    url(r'pacient/$', 'cercaPacientsResultat'),
    url(r'pacient/(?P<pacient_id>\d+)/$', 'pacient'),# mostra totes les hisotires del pacient donat
    url(r'informe/(?P<historia_id>\d+)/$', 'informe'), # mostre el informe de la historia selecionada
    url(r'cercapacientsacr/$', 'cercaPacientsACR'), # inici busqueda per acr
    url(r'cercapacientsacrresultat/$', 'cercaPacientsACRResultat'), # pacients que estan dins de la cerca acr
    )