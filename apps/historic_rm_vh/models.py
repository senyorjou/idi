# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.

from django.db import models

class Pacient(models.Model):
    
    idpacient = models.AutoField(db_column='IdPacient',primary_key=True) # Field name made lowercase.
    nex1 = models.IntegerField(null=True, db_column='NEX1', blank=True) # Field name made lowercase.
    cgn1 = models.CharField(max_length=45, db_column='CGN1', blank=True) # Field name made lowercase.
    cgn2 = models.CharField(max_length=45, db_column='CGN2', blank=True) # Field name made lowercase.
    nomb = models.CharField(max_length=45, db_column='NOMB', blank=True) # Field name made lowercase.
    sexe = models.CharField(max_length=3, db_column='SEXE', blank=True) # Field name made lowercase.
    dnex = models.CharField(max_length=24, db_column='DNEX', blank=True) # Field name made lowercase.
    ptel = models.IntegerField(null=True, db_column='PTEL', blank=True) # Field name made lowercase.
    telf = models.IntegerField(null=True, db_column='TELF', blank=True) # Field name made lowercase.
    telc = models.CharField(max_length=45, db_column='TELC', blank=True) # Field name made lowercase.
    cip = models.CharField(max_length=42, db_column='CIP', blank=True) # Field name made lowercase.
    eliminat = models.IntegerField(db_column='ELIMINAT') # Field name made lowercase.
    class Meta:
        db_table = u'historicrmvh_pacient'

    def __unicode__(self):
        return str(self.idpacient) + ', ' + str(self.nex1) + ', ' + self.cgn1 + ' ' + self.cgn2 + ' ' + self.nomb

    def gethistorias(self):
        h =  Historias.objects.filter(idpacient=self.idpacient)
        return 'YEEEEEEEEHAAAA' + str(h)
    
    def get_dnex(self):
        if self.dnex:
            s = str(self.dnex)
            return str( s[0:4] + '-' + s[4:6] + '-' + s[6:8] )
        else:
            return "-"
         
    
class Historias(models.Model):

    idhistoria = models.AutoField(db_column='IdHistoria',primary_key=True) # Field name made lowercase.
    idpacient = models.ForeignKey(Pacient, null=True, db_column='IdPacient', blank=True) # Field name made lowercase.
    cgn1 = models.CharField(max_length=45, db_column='CGN1', blank=True) # Field name made lowercase.
    cgn2 = models.CharField(max_length=45, db_column='CGN2', blank=True) # Field name made lowercase.
    nomb = models.CharField(max_length=45, db_column='NOMB', blank=True) # Field name made lowercase.
    dexp = models.CharField(max_length=24, db_column='DEXP', blank=True) # Field name made lowercase.
    nex1 = models.FloatField(null=True, db_column='NEX1', blank=True) # Field name made lowercase.
    nex2 = models.FloatField(null=True, db_column='NEX2', blank=True) # Field name made lowercase.
    dvis = models.CharField(max_length=24, db_column='DVIS', blank=True) # Field name made lowercase.
    drec = models.CharField(max_length=24, db_column='DREC', blank=True) # Field name made lowercase.
    cexp = models.CharField(max_length=12, db_column='CEXP', blank=True) # Field name made lowercase.
    peso = models.FloatField(null=True, db_column='PESO', blank=True) # Field name made lowercase.
    pnuh = models.CharField(max_length=3, db_column='PNUH', blank=True) # Field name made lowercase.
    nuhi = models.FloatField(null=True, db_column='NUHI', blank=True) # Field name made lowercase.
    habi = models.CharField(max_length=12, db_column='HABI', blank=True) # Field name made lowercase.
    llit = models.CharField(max_length=6, db_column='LLIT', blank=True) # Field name made lowercase.
    metg = models.CharField(max_length=60, db_column='METG', blank=True) # Field name made lowercase.
    cmet = models.CharField(max_length=9, db_column='CMET', blank=True) # Field name made lowercase.
    cser = models.CharField(max_length=15, db_column='CSER', blank=True) # Field name made lowercase.
    cpro = models.CharField(max_length=6, db_column='CPRO', blank=True) # Field name made lowercase.
    upro = models.FloatField(null=True, db_column='UPRO', blank=True) # Field name made lowercase.
    hexp = models.CharField(max_length=12, db_column='HEXP', blank=True) # Field name made lowercase.
    hfin = models.CharField(max_length=12, db_column='HFIN', blank=True) # Field name made lowercase.
    tpro = models.CharField(max_length=3, db_column='TPRO', blank=True) # Field name made lowercase.
    maqu = models.CharField(max_length=9, db_column='MAQU', blank=True) # Field name made lowercase.
    obse = models.CharField(max_length=90, db_column='OBSE', blank=True) # Field name made lowercase.
    moen = models.CharField(max_length=6, db_column='MOEN', blank=True) # Field name made lowercase.
    entr = models.CharField(max_length=60, db_column='ENTR', blank=True) # Field name made lowercase.
    dent = models.CharField(max_length=24, db_column='DENT', blank=True) # Field name made lowercase.
    clie = models.CharField(max_length=12, db_column='CLIE', blank=True) # Field name made lowercase.
    tari = models.CharField(max_length=12, db_column='TARI', blank=True) # Field name made lowercase.
    situ = models.CharField(max_length=6, db_column='SITU', blank=True) # Field name made lowercase.
    mosi = models.CharField(max_length=6, db_column='MOSI', blank=True) # Field name made lowercase.
    dsit = models.CharField(max_length=24, db_column='DSIT', blank=True) # Field name made lowercase.
    avis = models.CharField(max_length=3, db_column='AVIS', blank=True) # Field name made lowercase.
    pfac = models.CharField(max_length=18, db_column='PFAC', blank=True) # Field name made lowercase.
    clau = models.FloatField(null=True, db_column='CLAU', blank=True) # Field name made lowercase.
    seda = models.FloatField(null=True, db_column='SEDA', blank=True) # Field name made lowercase.
    cco1 = models.CharField(max_length=6, db_column='CCO1', blank=True) # Field name made lowercase.
    dos1 = models.FloatField(null=True, db_column='DOS1', blank=True) # Field name made lowercase.
    cco2 = models.CharField(max_length=6, db_column='CCO2', blank=True) # Field name made lowercase.
    dos2 = models.FloatField(null=True, db_column='DOS2', blank=True) # Field name made lowercase.
    dcl1 = models.CharField(max_length=120, db_column='DCL1', blank=True) # Field name made lowercase.
    dcl2 = models.CharField(max_length=120, db_column='DCL2', blank=True) # Field name made lowercase.
    dex1 = models.CharField(max_length=120, db_column='DEX1', blank=True) # Field name made lowercase.
    cdi1 = models.CharField(max_length=24, db_column='CDI1', blank=True) # Field name made lowercase.
    dex2 = models.CharField(max_length=120, db_column='DEX2', blank=True) # Field name made lowercase.
    cdi3 = models.CharField(max_length=24, db_column='CDI3', blank=True) # Field name made lowercase.
    dex3 = models.CharField(max_length=120, db_column='DEX3', blank=True) # Field name made lowercase.
    cdi2 = models.CharField(max_length=24, db_column='CDI2', blank=True) # Field name made lowercase.
    npla = models.CharField(max_length=9, db_column='NPLA', blank=True) # Field name made lowercase.
    disk = models.FloatField(null=True, db_column='DISK', blank=True) # Field name made lowercase.
    diap = models.FloatField(null=True, db_column='DIAP', blank=True) # Field name made lowercase.
    cexpn = models.CharField(max_length=12, db_column='CEXPN', blank=True) # Field name made lowercase.
    cexp1 = models.CharField(max_length=9, db_column='CEXP1', blank=True) # Field name made lowercase.
    tan1 = models.FloatField(null=True, db_column='TAN1', blank=True) # Field name made lowercase.
    cdap = models.CharField(max_length=18, db_column='CDAP', blank=True) # Field name made lowercase.
    naut = models.CharField(max_length=51, db_column='NAUT', blank=True) # Field name made lowercase.
    nexp = models.FloatField(null=True, db_column='NEXP', blank=True) # Field name made lowercase.
    ordr = models.CharField(max_length=6, db_column='ORDR', blank=True) # Field name made lowercase.
    prio = models.CharField(max_length=3, db_column='PRIO', blank=True) # Field name made lowercase.
    
    class Meta:
        db_table = u'historicrmvh_historias'
    
    def __unicode__(self):
        return str(self.idhistoria) + ', ' + str(self.idpacient) + ', ' + str(self.dexp) 
      
    def printa(self):
        return str(self.dexp + ' , RM ' + self.cexp + '  .'   ) 

    def rutaInforme(self):
        if self.dexp is None:
            return ""        
        else:
            return str( str(self.dexp[0:6]) + '/' + str( self.nex1) + '.' + str( self.nex2))
        
    def get_dexp(self):
        s = str(self.dexp)
        return str( s[0:4] + '-' + s[4:6] + '-' + s[6:8] ) 
    
    def get_hexp(self):
        s = str(self.hexp)
        return str( s[0:2] + ':' + s[2:4] ) 
    
