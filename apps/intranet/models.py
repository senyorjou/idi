from django.db import models
from apps.meta.models import AuthUser
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.conf import settings
    
class Idea(models.Model):
    id = models.IntegerField(primary_key=True)
    data = models.DateTimeField()
    user = models.ForeignKey(AuthUser, null=True, blank=True)
    concepte = models.CharField(max_length=768, blank=True)
    idea = models.TextField(blank=True)
    resposta = models.TextField(blank=True)
    class Meta:
        db_table = u'intranet_idees'    
    def __unicode__(self):
        return unicode(self.concepte)
    def notifica_idea(self):
        send_mail("Nova idea al portal d'idees", self.concepte + "\n" + self.idea, 
                  self.user.email, ['dgomez@idi.catsalut.cat'], fail_silently=False)
    def notifica_resposta(self):
        #email = EmailMessage("Resposta a la teva idea: ", self.concepte + "\n" + self.resposta, 
        #                     settings.DEFAULT_FROM_EMAIL, [self.user.email], 
        #                     headers = {'Content-Type': 'text/plain; charset="utf-8"', 
        #                                'Content-Transfer-Encoding': '8bit'})
        #email.send(fail_silently=False)
        send_mail("Resposta a la teva idea ", self.concepte + "\n" + self.resposta, 
                  'dgomez@idi.catsalut.cat', [self.user.email], fail_silently=False)            