# coding: utf8
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
import os
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.db import connections
import datetime
from apps.intranet.models import Idea
from apps.meta.models import AuthUser,AuthUserProfile
from apps.meta.common import sanitize_path
import time
import apps.meta.auditoria

def permisos_intranet(redirect_to):
    '''
    Decorator propi, si el usuari a clicat sobre "intranet" al logejarse lalvors te permisos per entrar en aquestes vistes
    sino es redirigeix a ·redirect_to· 
    ''' 
    def decorator(view):
        # 'view' es la funcio view per decorar ( es la vista de declarem que al cap ia ala fi es una 
        # funcio també.  Com el primer parametre de la bvista es el request podem treballar amb ell directament
        # '_wrapper' es la fuincio que cridarem en substitucio de la vista , en aquest cas la vista perque es el que pasem a "deco"        
        def _wrapper(request, *args, **kw):
            # Aqui tenim acces al requset i podem fer el que volguem 
            if not( request.session['intranet']  and  AuthUserProfile.objects.get( user = AuthUser.objects.get(username =str(request.user)).id).es_idi):
                    #No ha polsat sobre eines o  no te permisos!!!
                    apps.meta.auditoria.request_handler(request,11,'Ha entrat a apliaciuts o no te permisos de Intranet,  User: ' + str(request.user) ) 
                    return HttpResponseRedirect(redirect_to)
            return view(request, *args, **kw)
        # Aixo ho remana posar-ho el django epr debugging
        #_wrapper.__name__ = view.__name___
        _wrapper.__doc__ = view.__doc__
        # al final retornem el nostre warpper  que substitueix la vista
        return _wrapper
    return decorator 

# TODO: Posar audit 




@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')  
def index(request):
    return render_to_response('intranet/index.html',          
                              context_instance=RequestContext(request))
@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')   
def showDPO(request, any):
    anys = range(2008, int(datetime.datetime.now().year)+1)
    #dpopersona ( userdni , any )
    query = 'call dpopersona_portal(%s, %s)' 
    cursor = connections['dpos'].cursor()
    cursor.execute(query, (request.user.get_profile().id, any))
    resultat = cursor.fetchall()
    dpos = []
    
    valoracio = {0: "Proposat",
                 1: "Validat",
                 2: "Provisional",
                 3: "%"}
    
    estat = u"Valoració DPO %s" % any

    for row in resultat:
        if row[14] != 3:
            estat = u"Evolució DPO %s" % any            
        if row[12] == None:
            desc = ""
        else:
            desc = str(row[12]) + " "
                    
        dpos.append([row[4] + " \n - " + row[5],
                     row[6],#descripcio
                       row[7],
                       row[8],
                        row[9],
                       row[10], desc + valoracio.get(int(row[14]), "Estat desconegut: %s" % row[11])])

    liquidacio = os.path.join("/var/lib/django/portal/data/treballadors", request.user.username, "dpos", "%s.pdf" % any)
    #liquidacio = os.path.join("C:\\treballadors", request.user.username, "dpos", "%s.pdf" % any)
    
    pagada = False
    if os.path.exists(liquidacio):
        pagada = True
    
    apps.meta.auditoria.request_handler(request,11,'showDPO, ' + str(liquidacio) )
    return render_to_response('intranet/dpos.html',                          
                              {'dpos': dpos,
                               'anys': anys,
                               'estat': estat,
                               'curr_any': any,
                               'pagada': pagada},
                              context_instance=RequestContext(request))    

@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')   
def showJustificantDPO(request, any):
    liquidacio = ''
    try:
        liquidacio = os.path.join("/var/lib/django/portal/data/treballadors", request.user.username, "dpos", "%s.pdf" % any)
        #liquidacio = os.path.join("C:\\treballadors", request.user.username, "dpos", "%s.pdf" % any)
  
        nomina = open(liquidacio, "rb")
        response = HttpResponse(nomina, mimetype='application/pdf')
        response['Content-Disposition'] = "attachment; filename=JustificantDPOs_%d.pdf" % (int(any))
    except:
        pass
    apps.meta.auditoria.request_handler(request,11,'showJustificantDPO ; query:' + str(liquidacio) )
    return response 

 
@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')   
def showDPOs(request):
    anys = range(2008, int(datetime.datetime.now().year)+1)
    apps.meta.auditoria.request_handler(request,11,'showDPOS' )
    return render_to_response('intranet/dpos.html',                          
                              {'anys': anys},
                              context_instance=RequestContext(request))    

@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')   
def showNomines(request):
    anys = []
    try:
        nomines_dir = os.path.join("/var/lib/django/portal/data/treballadors", request.user.username, "nomines")
        #nomines_dir = os.path.join("C:\\treballadors", request.user.username, "nomines")
        anys = os.listdir(nomines_dir)
        anys.sort()
    except:
        pass
        
    apps.meta.auditoria.request_handler(request,11,'showNomines' )
    return render_to_response('intranet/nomines.html',    
                              {'anys': anys},                                                    
                              context_instance=RequestContext(request))   
@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')      
def showNominesAny(request, any):
    anys = []
    mesos = []
    extres = []
    try:
        nomines_dir = os.path.join("/var/lib/django/portal/data/treballadors", request.user.username, "nomines")
        #nomines_dir = os.path.join("C:\\treballadors", request.user.username, "nomines")
        mesos = [file.split('.')[0] for file in os.listdir(os.path.join(nomines_dir, any)) if file.lower().endswith('.pdf')]
        mesos.sort()
        try:
            extres = [item.split('.')[0] for item in os.listdir(os.path.join(nomines_dir, any, 'extres'))]
            extres.sort()
        except:
            pass
        anys = os.listdir(nomines_dir)
        anys.sort()
    except:
        pass
    
    apps.meta.auditoria.request_handler(request,11,'showNominesAny' )
    return render_to_response('intranet/nomines.html',    
                              {'curr_any': any,
                               'anys': anys,
                               'extres': extres,
                               'mesos':mesos},                                                    
                              context_instance=RequestContext(request))      

@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')   
def showNomina(request, any, mes):
    nomines_dir = ''
    try:
        nomines_dir = os.path.join("/var/lib/django/portal/data/treballadors", request.user.username, "nomines")
        #nomines_dir = os.path.join("C:\\treballadors", request.user.username, "nomines")
        nomina = open(os.path.join(nomines_dir, any, "%02d.pdf" % int(mes)),"rb")
        response = HttpResponse(nomina, mimetype='application/pdf')
        response['Content-Disposition'] = "attachment; filename=NominaIDI_%d%02d.pdf" % (int(any), int(mes))
    except:
        pass
    apps.meta.auditoria.request_handler(request,11,'showNominesAny,  Dir: '  + str(nomines_dir))
    return response 

@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')
def showNominaExtra(request, any, mes):
    nomines_dir = ''
    try:
        
        nomines_dir = os.path.join("/var/lib/django/portal/data/treballadors", request.user.username, "nomines")
        #nomines_dir = os.path.join("C:\\treballadors", request.user.username, "nomines")
        nomina = open(os.path.join(nomines_dir, any, "extres", "%02d.pdf" % int(mes)),"rb")
        response = HttpResponse(nomina, mimetype='application/pdf')
        response['Content-Disposition'] = "attachment; filename=NominaIDI_%d%02d.pdf" % (int(any), int(mes))
        
        
    except:
        pass
    apps.meta.auditoria.request_handler(request,11,'showNominaExtra,  Dir: ' + str(nomines_dir))
    return response 

@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')
def showIRPFs(request):
    anys = []
    irpf_dir = ''
    try:
        irpf_dir = os.path.join("/var/lib/django/portal/data/treballadors", request.user.username, "irpf")
        #irpf_dir = os.path.join("C:\\treballadors", request.user.username, "irpf")
        anys = [file.split('.')[0] for file in os.listdir(irpf_dir) if file.lower().endswith('.pdf')]
    except:
        pass
    apps.meta.auditoria.request_handler(request,11,'showIRPFs, dir' +  str(irpf_dir))
    return render_to_response('intranet/irpf.html',    
                              {'anys': anys},                                                    
                              context_instance=RequestContext(request))    

@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')
def showIRPFAny(request, any):
    nomines_dir = ''
    try:
        nomines_dir = os.path.join("/var/lib/django/portal/data/treballadors", request.user.username, "irpf")
        #nomines_dir = os.path.join("C:\\treballadors", request.user.username, "irpf")
        nomina = open(os.path.join(nomines_dir, "%04d.pdf" % int(any)),"rb")
        response = HttpResponse(nomina, mimetype='application/pdf')
        response['Content-Disposition'] = "attachment; filename=CertificatIRPFIDI_%04d.pdf" % (int(any))
        
    except:
        pass
    apps.meta.auditoria.request_handler(request,11,'showIRPFAny, dir: ' + str(nomines_dir))
    return response  

@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')
def showPerfil(request):
    apps.meta.auditoria.request_handler(request,11,'showPerfil' )
    return render_to_response('intranet/perfil.html',                                                        
                              context_instance=RequestContext(request))    

@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')
def getIdees(request):
    if 'idea' in request.POST and 'concepte' in request.POST:
        currUser = AuthUser.objects.get(username=request.user.username)
        idea = Idea(data=datetime.datetime.now(), concepte=request.POST['concepte'],
                    idea=request.POST['idea'], user=currUser)
        idea.notifica_idea()
        idea.save()
        
    apps.meta.auditoria.request_handler(request,11,'getIdees'  )
    
    return render_to_response('intranet/portalidees.html',    
                              {'idees':Idea.objects.all().order_by("-data")},                                                    
                              context_instance=RequestContext(request))    

@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')   
def showDocuments(request, document):
    base_dir = "/var/lib/django/portal/data/documents"
    #base_dir = os.path.join("C:\\", "treballadors")
    result = []
    
    document_path = ""
    if document:
        document_path = sanitize_path(document)
        
    full_path = os.path.join(base_dir, document_path)
    
    try:
        if os.path.isdir(full_path):
            files = os.listdir(full_path)
            for file in files:
                rel_file_path = os.path.join(document_path, file)
                full_file_path = os.path.join(base_dir, rel_file_path)
                result.append((rel_file_path, os.path.isdir(full_file_path), 
                               time.strftime("%m/%d/%Y %H:%M:%S",time.localtime(os.path.getmtime(full_file_path)))))
        elif os.path.isfile(full_path):
            document = open(full_path, "rb")
            response = HttpResponse(document, mimetype='application/octet-stream')
            response['Content-Disposition'] = "attachment; filename=%s" % os.path.basename(full_path)
            return response            
    except:
        pass
    apps.meta.auditoria.request_handler(request,11,'showDocuments;  ' + str(document_path))
    return render_to_response('intranet/documents.html',    
                              {'result': result},                                                    
                              context_instance=RequestContext(request))
    
@login_required
@permisos_intranet('/accounts/permisdenegat_intranet/')   
def showDocument(request):
  
    return showDocuments(request, False)


     