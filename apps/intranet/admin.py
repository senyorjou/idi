from apps.intranet.models import Idea
from django.contrib import admin
from apps.meta.models import AuthUser

    
class IdeaAdmin(admin.ModelAdmin):
    list_display = ('id', 'data', 'user', 'concepte', 'idea', 'resposta')
    readonly_fields = ['id']
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "user":
            kwargs["queryset"] = AuthUser.objects.all().order_by('first_name')
            return db_field.formfield(**kwargs)
        return super(IdeaAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)   
    def response_change(self, request, obj, post_url_continue=None):
        obj.notifica_resposta()
        return super(IdeaAdmin, self).response_change(request, obj)
    
admin.site.register(Idea, IdeaAdmin)