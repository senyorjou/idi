from django.conf.urls.defaults import url, patterns
from django.views.generic.simple import redirect_to
from django.shortcuts import render_to_response, redirect

urlpatterns = patterns('apps.intranet.views',
    url(r'^$', redirect_to, {'url' : '/intranet/calendari_personal/'}), #
    url(r'^index$', 'index'), #
    url(r'^dpos/$', 'showDPOs'),
    url(r'^dpos/(\d{4})/$', 'showDPO'),
    url(r'^dpos/(\d{4})/justificant/$', 'showJustificantDPO'),
    url(r'^nomines/$', 'showNomines'), #
    url(r'^nomines/(\d{4})/$', 'showNominesAny'), #
    url(r'^nomines/(\d{4})/(\d{2})/$', 'showNomina'), #
    url(r'^nomines-extra/(\d{4})/(\d{2})/$', 'showNominaExtra'), #    
    url(r'^irpf/$', 'showIRPFs'), #
    url(r'^irpf/(\d{4})/$', 'showIRPFAny'), #
    url(r'^portal-didees/$', 'getIdees'), #
    url(r'^perfil/', 'showPerfil'), #
    url(r'^documents/$', 'showDocument'), #
    url(r'^documents/(.*)$', 'showDocuments'), #
    )


urlpatterns += patterns('apps.idirh.views_calendari',
    url(r'^calendari_personal/$', 'calendari_personal', name='calendari_personal_intranet'),
    url(r'^calendari_personal/(?P<anyo>\d+)/$', 'calendari_personal', name='calendari_personal_intranet'),
)
