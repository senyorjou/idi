import os
import sys


paths = ['/var/lib/django/portal/apps', '/var/lib/django/portal/',
         '/usr/local/src/Django-1.3.1/django', '/usr/local/src/Django-1.3.1/']
for path in paths:
        if path not in sys.path:
                sys.path.append(path)

os.environ['PYTHON_EGG_CACHE'] = '/var/tmp'
os.environ['DJANGO_SETTINGS_MODULE'] = 'apps.settings'

from apps.meta.common import sincronitza_karat_portal

sincronitza_karat_portal()
