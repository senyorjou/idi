// Main scripts for IDI APPS


// Update d'una variable de sessió via AJAX
function update_session(key, value) {
    $.ajax({
        type : 'GET',
        url : '/idirh/tools/update_session/' + key + '/' + value + '/',
        success : function(data) {
            // console.log('Session var updated');
        },
        error: function(data) {
            // console.log('Session var NOT updated');
        }
    });
}

// activa el tooltip de bootstrap on-the-fly
function create_tooltips () {
    $('a[rel="tooltip"]').tooltip({
        'html' : true,
        'placement' : 'top',
        'animation' : false
    });
}


//
// Permet entrar els inputs de TIME posant directament els 4 digits
//
function sanitize_time_input(obj) {
    in_val = obj.val(),
    len = in_val.length,
    out_val = '';

    if (len == 1) {
        out_val = '0' + in_val + ':00';
    } else if (len == 2) {
        out_val = in_val + ':00';
    }
    else if (len == 3) {
        out_val = in_val.substr(0, 2) + ':' + in_val.substr(2, 1) + '0';
    }
    else if (len == 4) {
        out_val = in_val.substr(0, 2) + ':' + in_val.substr(2, 2);
    }

    if (out_val.substr(0, 2) == '24') {
        out_val = '00:' + out_val.substr(3, 2);
    }

    if (out_val.substr(0, 2) == '24') {
        out_val = '00:' + out_val.substr(3, 2);
    }

    obj.val(out_val);
}
